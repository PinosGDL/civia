﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Civica.BLL.Alarm;
using Civica.BLL.Models.Alarm;
using Civica.Domain.CivicaDomain;
using Civica.Infrastructure.BaseClasses;
using Civica.Infrastructure.Providers;
using CivicaServices.Specs.TestUtils;
using NUnit.Framework;

namespace CivicaServices.Specs
{
    [TestFixture]
    public class RunAlarmBlSpec
    {
        private MyTestContext _context;
        private List<MonitoringData> _data;
        private CsvImporter _converter;
        private string dataPath;
        private static readonly string PathPrefix = @"C:\civica\CivicaServicesV2\CivicaServicesV2\CivicaServices.Specs";
        //private readonly string pathPrefix = @"CivicaServices.Specs\";

        private readonly string _monitoringStationAlarmConditionsPath = String.Format(@"{0}\TestData\MonitoringStationAlarmConditions.txt", PathPrefix);
        private readonly string _monitoringSensorAlarmPath = String.Format(@"{0}\TestData\MonitoringSensorAlarm.txt", PathPrefix);
        private readonly string _monitoringSensorPath = String.Format(@"{0}\TestData\MonitoringSensor.txt", PathPrefix);
        private readonly string _monitoringStationPath = String.Format(@"{0}\TestData\MonitoringStation.txt", PathPrefix);
        private readonly string _monitoringStationAlarmPath = String.Format(@"{0}\TestData\MonitoringStationAlarm.txt", PathPrefix);
        private readonly string _timeZonesPath = String.Format(@"{0}\TestData\TimeZone.txt", PathPrefix);
        private readonly string _monitoringStationAlarmSeverity = String.Format(@"{0}\TestData\MonitoringStationAlarmSeverity.txt", PathPrefix);
        private readonly string _monitoringServiceStations = String.Format(@"{0}\TestData\MonitoringServiceStations.txt", PathPrefix);
        private readonly string _monitoringStationAlarmConditionsTypePath = String.Format(@"{0}\TestData\MonitoringStationAlarmConditionsType.txt", PathPrefix);
        private readonly string _monitoringServicePath = String.Format(@"{0}\TestData\MonitoringService.txt", PathPrefix);
        private List<MonitoringStationAlarm> _monitoringStationAlarm;
        private readonly EventLog _monitoringAlarmServiceheventLog = new EventLog();

        [SetUp]
        public void Init()
        {
            _context = new MyTestContext();
            _converter = new CsvImporter();
            _data = new List<MonitoringData>();
            var monitoringSensorAlarm = _converter.GetMonitoringSensorAlarm(_monitoringSensorAlarmPath);
            var monitoringSensor = _converter.GetMonitoringSensor(_monitoringSensorPath);
            var monitoringSensorAlarmCondition = _converter.GetMonitoringStationAlarmConditions(_monitoringStationAlarmConditionsPath);
            var monitoringStation = _converter.GetMonitoringStation(_monitoringStationPath);
            _monitoringStationAlarm = _converter.GetMonitoringStationAlarm(_monitoringStationAlarmPath);
            var timeZones = _converter.GetTimeZone(_timeZonesPath);
            var monitoringStationAlarmSeverity = _converter.GetMonitoringStationAlarmSeverity(_monitoringStationAlarmSeverity);
            var monitoringServiceStations = _converter.GetMonitoringServiceStation(_monitoringServiceStations);
            var monitoringSensorAlarmConditionType = _converter.GetMonitoringStationAlarmConditionsType(_monitoringStationAlarmConditionsTypePath);
            var monitoringService = _converter.GetMonitoringService(_monitoringServicePath);

            // setup foreign entities for navigation properties
            foreach (var monitoringServiceStation in monitoringServiceStations)
            {
                monitoringServiceStation.MonitoringService =
                    monitoringService.FirstOrDefault(x => x.Id == monitoringServiceStation.MonitoringServiceId);
            }

            foreach (var item in monitoringSensorAlarmCondition)
            {
                item.MonitoringStationAlarm =
                    _monitoringStationAlarm.FirstOrDefault(x => x.Id == item.MonitoringStationAlarmId);
                item.MonitoringStationAlarmConditionType =
                   monitoringSensorAlarmConditionType.FirstOrDefault(x => x.Id == item.AlarmConditionTypeId);
            }

            foreach (var station in monitoringStation)
            {
                station.TimeZone = timeZones.FirstOrDefault(x => x.Id == station.TimeZoneId);
                station.MonitoringServiceStations =
                    monitoringServiceStations.Where(x => x.MonitoringStationId == station.Id).ToList();
            }

            foreach (var sensorAlarm in monitoringSensorAlarm)
            {
                sensorAlarm.MonitoringSensor =
                    monitoringSensor.FirstOrDefault(x => x.Id == sensorAlarm.MonitoringSensorId);
            }

            _context.MonitoringStationAlarmConditions.AddRange(monitoringSensorAlarmCondition);
            _context.MonitoringSensorAlarms.AddRange(monitoringSensorAlarm);
            _context.MonitoringStations.AddRange(monitoringStation);
            _context.MonitoringStationAlarmConditionTypes.AddRange(monitoringSensorAlarmConditionType);
            _context.MonitoringSensors.AddRange(monitoringSensor);

            #region Set Up MonitoringStationAlarms

            foreach (var stationAlarm in _monitoringStationAlarm)
            {
                stationAlarm.MonitoringStationAlarmConditions = monitoringSensorAlarmCondition.Where(x => x.MonitoringStationAlarmId == stationAlarm.Id).ToList();
                stationAlarm.MonitoringStationAlarmSeverity = monitoringStationAlarmSeverity.FirstOrDefault(x => x.Id == stationAlarm.SeverityId);
                stationAlarm.MonitoringStation = monitoringStation.FirstOrDefault(x => x.Id == stationAlarm.MonitoringStationId);
            }
            #endregion Set Up MonitoringStationAlarms
        }
       
        #region TestResults

        public static List<AlarmConditionResultModel> Test1 = new List<AlarmConditionResultModel>
        {
            new AlarmConditionResultModel
            {
                CheckFrom = new DateTimeOffset(636069201000000000, new TimeSpan(0, 0, 0)),
                CheckTo = new DateTimeOffset(636070065000000000, new TimeSpan(0, 0, 0)),
                ConditionStatus = BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION
            },

        };

        public static List<AlarmConditionResultModel> Test_789 = new List<AlarmConditionResultModel>
        {
            new AlarmConditionResultModel
            {
                CheckFrom = new DateTimeOffset(2016,8,12,23,55,00,new TimeSpan(-5, 0, 0)),
                CheckTo = new DateTimeOffset(2016,8,12,23,55,00,new TimeSpan(-5, 0, 0)),
                ConditionStatus = BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION
            },
        };

        public static List<AlarmConditionResultModel> Test_790 = new List<AlarmConditionResultModel>
        {
            new AlarmConditionResultModel
            {
                CheckFrom = new DateTimeOffset(2016,8,12,23,55,00,new TimeSpan(-5, 0, 0)),
                CheckTo = new DateTimeOffset(2016,8,13,23,55,00,new TimeSpan(-5, 0, 0)),
                ConditionStatus = BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION
            },
        };

        public static List<AlarmConditionResultModel> Test_791 = new List<AlarmConditionResultModel>
        {
            new AlarmConditionResultModel
            {
                CheckFrom = new DateTimeOffset(2016,8,12,23,55,00,new TimeSpan(-5, 0, 0)),
                CheckTo = new DateTimeOffset(2016,8,13,23,55,00,new TimeSpan(-5, 0, 0)),
                ConditionStatus = BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION
            },
        };

        public static List<AlarmConditionResultModel> Test_792 = new List<AlarmConditionResultModel>
        {
            new AlarmConditionResultModel
            {
                CheckFrom = new DateTimeOffset(2016,8,12,23,55,00,new TimeSpan(-5, 0, 0)),
                CheckTo = new DateTimeOffset(2016,8,13,23,55,00,new TimeSpan(-5, 0, 0)),
                ConditionStatus = BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION
            },
        };

        public static List<AlarmConditionResultModel> Test_793 = new List<AlarmConditionResultModel>
        {
            new AlarmConditionResultModel
            {
                CheckFrom = new DateTimeOffset(2016,8,25,23,55,00,new TimeSpan(-5, 0, 0)),
                CheckTo = new DateTimeOffset(2016,8,26,10,05,00,new TimeSpan(-5, 0, 0)),
                ConditionStatus = BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION
            },
        };

        public static List<AlarmConditionResultModel> Test_794 = new List<AlarmConditionResultModel>
        {
            new AlarmConditionResultModel
            {
                CheckFrom = new DateTimeOffset(2016,2,19,23,55,00,new TimeSpan(-5, 0, 0)),
                CheckTo = new DateTimeOffset(2016,2,20,20,55,00,new TimeSpan(-6, 0, 0)),
                ConditionStatus = BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION
            },
        };

        public static List<AlarmConditionResultModel> Test_795 = new List<AlarmConditionResultModel>
        {
            new AlarmConditionResultModel
            {
                CheckFrom = new DateTimeOffset(2016,2,19,23,55,00,new TimeSpan(-5, 0, 0)),
                CheckTo = new DateTimeOffset(2016,2,20,20,55,00,new TimeSpan(-6, 0, 0)),
                ConditionStatus = BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION
            },
        };

        public static List<AlarmConditionResultModel> Test_796 = new List<AlarmConditionResultModel>
        {
            new AlarmConditionResultModel
            {
                CheckFrom = new DateTimeOffset(2016,2,19,23,55,00,new TimeSpan(-5, 0, 0)),
                CheckTo = new DateTimeOffset(2016,2,20,20,55,00,new TimeSpan(-6, 0, 0)),
                ConditionStatus = BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION
            },
        };

        public static List<AlarmConditionResultModel> Test_797 = new List<AlarmConditionResultModel>
        {
            new AlarmConditionResultModel
            {
                CheckFrom = new DateTimeOffset(2016,2,19,23,55,00,new TimeSpan(-5, 0, 0)),
                CheckTo = new DateTimeOffset(2016,2,20,20,55,00,new TimeSpan(-6, 0, 0)),
                ConditionStatus = BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION
            },
        };
        #endregion

        private static readonly object[] TestData =
        {
            new object[] {788, "2.1.1", new Dictionary<int, int>() { { 4176, 1 }},86400,Test1},
            new object[] {789, "2.1.2", new Dictionary<int, int>() { { 4177, 1 } },86400,Test_789},
            new object[] {790, "2.1.3", new Dictionary<int, int>() { { 4178, 1 }},4178,86400,Test_790},
            new object[] {791, "2.1.3", new Dictionary<int, int>() { { 4178, 1 }},4178,86400,Test_791},
            new object[] {792, "2.1.3", new Dictionary<int, int>() { { 4178, 1 }},4178,86400,Test_792},
            new object[] {793, "2.1.3", new Dictionary<int, int>() { { 4178, 1 }},86400,Test_793},
            new object[] {794, "2.1.7", new Dictionary<int, int>() { { 4179, 1}},86400,Test_794},
            new object[] {795, "2.1.7", new Dictionary<int, int>() { { 4180, 1 }},900,Test_795},
            new object[] {796, "2.1.9", new Dictionary<int, int>() { { 4181, 1 } },900,Test_796},
            new object[] {797, "2.1.10", new Dictionary<int, int>() { { 4182, 1 }, {4183, 3} }, 900, Test_797 }
        };


        /// <summary>
        /// Function to run alarm test
        /// </summary>
        /// <param name="alarmId"></param>
        /// <param name="fileName"></param>
        /// <param name="sensorData"></param>
        /// <param name="timeInterval">
        /// Time interval of Data Transmission, in s. so 15 minutes = 900 seconds, and 24 hours = 86400
        /// </param>
        /// <param name="expectedResult"></param>
        [Test, TestCaseSource(nameof(TestData))]
        public void Run_alarms_test(int alarmId, string fileName, Dictionary<int, int> sensorData, int timeInterval, List<AlarmConditionResultModel> expectedResult)
        {
            TimeProvider.Current = TestTimeProvider.Instance;
            var results = new List<AlarmResultModel>();

            _context.MonitoringStationAlarms.AddRange(_monitoringStationAlarm.Where(x => x.Id == alarmId));
            var rule = new RunAlarmBl(_context, new SenderMock());
            _data.AddRange(GetData(fileName, sensorData));

            DateTimeOffset firstDataPoint = _data.FirstOrDefault().TimeStamp;
            DateTimeOffset lastPoint = _data.LastOrDefault().TimeStamp;

            //start a day before you have data to ensure all initial conditions are setup
            DateTimeOffset alarmRun = new DateTimeOffset(firstDataPoint.Date.AddDays(-1), new TimeSpan(-5, 0, 0));

            while (alarmRun < lastPoint.AddDays(1))
            {
                var currentRunData = _data.Where(x => x.TimeStamp >= alarmRun.AddSeconds(-1 * timeInterval) && x.TimeStamp < alarmRun).ToList();
                if (currentRunData.Any())
                {


                    _context.MonitoringDatas.AddRange(currentRunData);

                    //run the alarm 10 seconds after data comes in, in reality this will be random
                    TestTimeProvider.SetCurrentTime(alarmRun.AddSeconds(10));

                    //do work
                    results.Add(rule.runOneAlarm(alarmId, _monitoringAlarmServiceheventLog));


                }
                alarmRun = alarmRun.AddSeconds(timeInterval);
            }

            Assert.Multiple(() =>
             {
                 foreach (AlarmConditionResultModel t in expectedResult)
                 {
                     Assert.AreEqual(t.ConditionStatus, FindConditionResultFromList(t, results).ConditionStatus);
                 }
             });
        }

        private IEnumerable<MonitoringData> GetData(string fileName, Dictionary<int, int> sensorData)
        {
            var unsortedData = new List<MonitoringData>();
            var dataFilePath = String.Format(@"{0}\TestData\" + fileName + ".csv", PathPrefix);
            foreach (var data in sensorData)
            {
                unsortedData.AddRange(_converter.GetData(dataFilePath, data.Key, data.Value));
            }
            return (unsortedData.OrderBy(x => x.TimeStamp));
        }
        /// <summary>
        /// Returns the found instance of Needle in Haystack
        /// For now just ignore null results
        /// </summary>
        /// <param name="Needle"></param>
        /// <param name="Haystack"></param>
        /// <returns></returns>
        private AlarmConditionResultModel FindConditionResultFromList(AlarmConditionResultModel Needle, List<AlarmResultModel> Haystack)
        {
            foreach (AlarmResultModel t1 in Haystack)
            {
                foreach (AlarmConditionResultModel t in t1.ConditionResults)
                {
                    if (t.CheckFrom == null || t.CheckTo == null)
                        continue;

                    bool result = !(t.CheckFrom.Value != Needle.CheckFrom.Value);

                    if (t.CheckTo.Value != Needle.CheckTo.Value)
                    {
                        result = false;
                    }

                    if (t.ConditionStatus == BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION)
                    {
                        if (t.CheckFrom.Value != Needle.CheckFrom.Value) { }
                    }

                    if (t.ConditionStatus != Needle.ConditionStatus)
                    {
                        result = false;
                    }

                    if (result)
                        return t;
                }
            }

            return null;
        }
    }
}
