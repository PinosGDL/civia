﻿using Civica.BLL;

namespace CivicaServices.Specs
{
    public class SenderMock : ISender
    {
        public void SendPhoneMessage(string[] recipients, string message, string ackMessage, int alarmId, int repeatedIndex)
        {

        }

        public long makePhoneCall(string[] recipients, string message, int alarmId, int repeatedIndex)
        {
            return 0;
        }

        public void SendMail(int alarmId, string recipient, string subject, string message, int repeatedIndex)
        {

        }
    }
}
