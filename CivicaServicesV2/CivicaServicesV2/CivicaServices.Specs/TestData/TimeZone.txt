Id,Name,Code,Offset,SummerOffset,SummerStartDate,SummerEndDate,TZICode
1,Coordinated Universal Time,UTC,.000000,.000000,,,UTC
2,GMT Standard Time,GMTST,.000000,.000000,,,GMT Standard Time
3,Greenwich Standard Time,GWST,.000000,.000000,,,Greenwich Standard Time
4,Baker Island Time,BIT,-12.000000,.000000,,,
5,Niue Time,NUT,-11.000000,.000000,,,Samoa Standard Time
6,Samoa Standard Time,SST,-11.000000,.000000,,,Samoa Standard Time
7,Hawaii-Aleutian Standard Time,HAST,-10.000000,-9.000000,,,Hawaiian Standard Time
8,Cook Island Time,CKT,-10.000000,.000000,,,Hawaiian Standard Time
9,Hawaii Standard Time,HST,-10.000000,.000000,,,Hawaiian Standard Time
10,Tahiti Time,TAHT,-10.000000,.000000,,,Hawaiian Standard Time
11,Alaska Standard Time,AKST,-9.000000,-8.000000,,,Alaskan Standard Time
12,Gambier Island Time,GIT,-9.000000,.000000,,,
13,Pacific Standard Time (North America),PST,-8.000000,-7.000000,,,Pacific Standard Time
14,Clipperton Island Standard Time,CIST,-8.000000,.000000,,,
15,Mountain Standard Time (US),MSTUS,-7.000000,-6.000000,,,US Mountain Standard Time
16,Mountain Standard Time (Mexico),MSTM,-7.000000,-6.000000,,,Mountain Standard Time (Mexico)
17,Mountain Standard Time (North America),MSTNA,-7.000000,-6.000000,,,Mountain Standard Time
18,Easter Island Standard Time,EAST,-6.000000,-5.000000,,,Central Standard Time
19,Central Standart Time (Canada),CSTC,-6.000000,-5.000000,,,Canada Central Standard Time
20,Central Standart Time (Mexico),CSTM,-6.000000,-5.000000,,,Central Standard Time (Mexico)
21,Central Standart Time (North America),CSTNA,-6.000000,-5.000000,,,Central Standard Time
22,SA Pacific Standard Time,SAP,-6.000000,-5.000000,,,SA Pacific Standard Time
23,Galapagos Time,GALT,-6.000000,.000000,,,
24,Colombia Time,COT,-5.000000,-4.000000,,,Eastern Standard Tim
25,Eastern Standard Time (North America),ESTNA,-5.000000,-4.000000,,,Eastern Standard Time
26,Eastern Standard Time (US),ESTUS,-5.000000,-4.000000,,,US Eastern Standard Time
27,Cuba Standard Time,CST,-5.000000,-4.000000,,,Eastern Standard Time
28,Ecuador Time,ECT,-5.000000,.000000,,,
29,Peru Time,PET,-5.000000,.000000,,,
30,Venezuelan Standard Time,VET,-4.500000,.000000,,,Venezuela Standard Time
31,Amazon Time (Brazil),AMT,-4.000000,-3.000000,,,Central Brazilian Standard Time
32,Atlantic Standard Time,AST,-4.000000,-3.000000,,,Atlantic Standard Time
33,Chile Standard Time,CLT,-4.000000,-3.000000,,,Atlantic Standard Time
34,Falkland Islands Time,FKT,-4.000000,-3.000000,,,Atlantic Standard Time
35,Paraguay Time (South America),PYT,-4.000000,-3.000000,,,Paraguay Standard Time
36,Bolivia Time,BOT,-4.000000,.000000,,,
37,Guyana Time,GYT,-4.000000,.000000,,,
38,Newfoundland Standard Time,NST,-3.500000,-2.500000,,,Newfoundland Standard Time
39,Saint Pierre and Miquelon Standard Time,PMST,-3.000000,-2.000000,,,
40,Uruguay Standard Time,UYT,-3.000000,-2.000000,,,
41,SA Eastern Standard Time,SAEST,-3.000000,-2.000000,,,SA Eastern Standard Time
42,Greenland Standard Time,GRDT,-3.000000,-2.000000,,,Montevideo Standard Time
43,Montevideo Standard Time,MONST,-3.000000,-2.000000,,,Greenland Standard Time
44,Argentina Time,ART,-3.000000,.000000,,,Argentina Standard Tim
45,Brasilia Time,BRT,-3.000000,.000000,,,Argentina Standard Tim
46,French Guiana Time,GFT,-3.000000,.000000,,,Argentina Standard Tim
47,Rothera Research Station Time,ROTT,-3.000000,.000000,,,Argentina Standard Tim
48,Suriname Time,SRT,-3.000000,.000000,,,Argentina Standard Tim
49,Fernando de Noronha Time,FNT,-2.000000,.000000,,,UTC-02
50,South Georgia and the South Sandwich Islands,GST,-2.000000,.000000,,,UTC-02
51,Eastern Greenland Time,EGT,-1.000000,.000000,,,Azores Standard Time
52,Azores Standard Time,AZOST,-1.000000,.000000,,,Azores Standard Time
53,Cape Verde Time,CVT,-1.000000,.000000,,,Cape Verde Standard Time
54,Morocco Standard Time,MRCST,-1.000000,.000000,,,Morocco Standard Time
55,Western European Time,WET,.000000,1.000000,,,W. Europe Standard Time
56,Central European Time,CET,1.000000,2.000000,,,Central Europe Standard Time
57,West Africa Time,WAT,1.000000,2.000000,,,Central Europe Standard Time
58,W. Central Africa Standard Time,WCAT,1.000000,2.000000,,,W. Central Africa Standard Time
59,Irish Standard Time[5],IST,1.000000,.000000,,,
60,Eastern European Time,EET,2.000000,3.000000,,,E. Europe Standard Time
61,Central Africa Time,CAT,2.000000,.000000,,,
62,South African Standard Time,SAST,2.000000,.000000,,,South Africa Standard Time
63,Namibia Standard Time,NABST,2.000000,.000000,,,Namibia Standard Time
64,GTB Standard Time,GTBST,3.000000,.000000,,,GTB Standard Time
65,Middle East Standard Time,MEST,3.000000,.000000,,,Middle East Standard Time
66,Syria Standard Time,SRYST,3.000000,.000000,,,Syria Standard Time
67,Egypt Standard Time,EGTST,3.000000,.000000,,,Egypt Standard Time
68,Arabic Standard Time,ARBCST,3.000000,.000000,,,Arabic Standard Time
69,Arab Standard Time,ARBST,3.000000,.000000,,,Arab Standard Time
70,FLE Standard Time,FLEST,3.000000,.000000,,,FLE Standard Time
71,East Africa Time,EAT,3.000000,.000000,,,E. Africa Standard Time
72,Indian Ocean Time,IOT,3.000000,.000000,,,
73,Showa Station Time,SYOT,3.000000,.000000,,,
74,Iran Standard Time,IRST,3.500000,4.500000,,,Iran Standard Time
75,Caucasus Standard Time,CCSST,4.000000,.000000,,,Caucasus Standard Time
76,Azerbaijan Time,AZT,4.000000,.000000,,,Azerbaijan Standard Time
77,Georgia Standard Time,GET,4.000000,.000000,,,Georgian Standard Time
78,Russian Standard Time,RUSST,4.000000,.000000,,,Russian Standard Time
79,Afghanistan Time,AFT,4.500000,.000000,,,Afghanistan Standard Time
80,Heard and McDonald Islands Time,HMT,5.000000,.000000,,,
81,Mawson Station Time,MAWT,5.000000,.000000,,,
82,Maldives Time,MVT,5.000000,.000000,,,
83,Oral Time,ORAT,5.000000,.000000,,,
84,Pakistan Standard Time,PKT,5.000000,.000000,,,Pakistan Standard Time
85,Indian/Kerguelen,TFT,5.000000,.000000,,,
86,Tajikistan Time,TJT,5.000000,.000000,,,
87,Turkmenistan Time,TMT,5.000000,.000000,,,
88,Uzbekistan Time,UZT,5.000000,.000000,,,
89,West Asia Standard Time,WAST,5.000000,.000000,,,West Asia Standard Time
90,Sri Lanka Time,SLST,5.500000,.000000,,,Sri Lanka Standard Time
91,Nepal Time,NPT,5.750000,.000000,,,Nepal Standard Time
92,Central Asia Standard Time,CAST,5.000000,.000000,,,Central Asia Standard Time
93,British Indian Ocean Time,BIOT,6.000000,.000000,,,
94,Bangladesh Standard Time,BST,6.000000,.000000,,,Bangladesh Standard Time
95,Bhutan Time,BTT,6.000000,.000000,,,
96,Kyrgyzstan time,KGT,6.000000,.000000,,,
97,Vostok Station Time,VOST,6.000000,.000000,,,
98,Yekaterinburg Time,YEKT,6.000000,.000000,,,
99,N. Central Asia Standard Time,NCAST,6.000000,.000000,,,N. Central Asia Standard Time
100,Cocos Islands Time,CCT,6.500000,.000000,,,
101,Myanmar Standard Time,MST,6.500000,.000000,,,Myanmar Standard Time
102,SE Asia Standard Time,SEAST,7.000000,.000000,,,SE Asia Standard Time
103,North Asia Standard Time,NAST,7.000000,.000000,,,North Asia Standard Time
104,Christmas Island Time,CXT,7.000000,.000000,,,
105,Davis Time,DAVT,7.000000,.000000,,,
106,Khovd Time,HOVT,7.000000,.000000,,,
107,Indochina Time,ICT,7.000000,.000000,,,
108,Krasnoyarsk Time,KRAT,7.000000,.000000,,,
109,Omsk Time,OMST,7.000000,.000000,,,
110,Thailand Standard Time,THA,7.000000,.000000,,,
111,North Asia East Standard Time,NAEST,8.000000,.000000,,,North Asia East Standard Time
112,Singapore Standard Time,SGPST,8.000000,.000000,,,Singapore Standard Time
113,Australian Western Standard Time,AWST,8.000000,9.000000,,,W. Australia Standard Time
114,ASEAN Common Time,ACT,8.000000,.000000,,,
115,Brunei Time,BDT,8.000000,.000000,,,
116,Choibalsan,CHOT,8.000000,.000000,,,
117,Central Indonesia Time,CIT,8.000000,.000000,,,
118,Hong Kong Time,HKT,8.000000,.000000,,,
119,Malaysia Time,MYT,8.000000,.000000,,,
120,Philippine Time,PHT,8.000000,.000000,,,
121,Ulaanbaatar Time,ULAT,8.000000,.000000,,,Ulaanbaatar Standard Time
122,Eastern Indonesian Time,EIT,9.000000,.000000,,,
123,Yakutsk Time,YKT,9.000000,.000000,,,Yakutsk Standard Time
124,Japan Standard Time,JST,9.000000,.000000,,,Tokyo Standard Time
125,Korea Standard Time,KST,9.000000,.000000,,,Korea Standard Time
126,Timor Leste Time,TLT,9.000000,.000000,,,
127,Australian Central Standard Time,ACST,9.500000,10.500000,,,Cen. Australia Standard Time
128,Marquesas Islands Time,MIT,9.500000,.000000,,,
129,Australian Eastern Standard Time,AEST,10.000000,11.000000,,,E. Australia Standard Time
130,Chamorro Standard Time,CHST,10.000000,.000000,,,
131,Chuuk Time,CHUT,10.000000,.000000,,,
132,Dumont d'Urville Time,DDUT,10.000000,.000000,,,
133,Papua New Guinea Time,PGT,10.000000,.000000,,,
134,Vladivostok Time,VLAT,10.000000,.000000,,,Vladivostok Standard Time
135,Yakutsk Time,YAKT,10.000000,.000000,,,Yakutsk Standard Time
136,Lord Howe Standard Time,LHST,10.500000,11.000000,,,
137,Kosrae Time,KOST,11.000000,.000000,,,Central Pacific Standard Time
138,Macquarie Island Station Time,MIST,11.000000,.000000,,,Central Pacific Standard Time
139,New Caledonia Time,NCT,11.000000,.000000,,,Central Pacific Standard Time
140,Pohnpei Standard Time,PONT,11.000000,.000000,,,Central Pacific Standard Time
141,Sakhalin Island time,SAKT,11.000000,.000000,,,Central Pacific Standard Time
142,Solomon Islands Time,SBT,11.000000,.000000,,,Central Pacific Standard Time
143,Vanuatu Time,VUT,11.000000,.000000,,,Central Pacific Standard Time
144,Norfolk Time,NFT,11.500000,.000000,,,Central Pacific Standard Time
145,New Zealand Standard Time,NZST,12.000000,13.000000,,,New Zealand Standard Time
146,Fiji Time,FJT,12.000000,.000000,,,Fiji Standard Time
147,Gilbert Island Time,GILT,12.000000,.000000,,,UTC+12
148,Marshall Islands,MHT,12.000000,.000000,,,UTC+12
149,Kamchatka Time,PETT,12.000000,.000000,,,Kamchatka Standard Time
150,Tuvalu Time,TVT,12.000000,.000000,,,UTC+12
151,Wake Island Time,WAKT,12.000000,.000000,,,UTC+12
152,Chatham Standard Time,CHAST,12.750000,13.750000,,,
153,Phoenix Island Time,PHOT,13.000000,.000000,,,
154,Tokelau Time,TKT,13.000000,.000000,,,
155,Tonga Time,TOT,13.000000,.000000,,,Tonga Standard Time
156,Line Islands Time,LINT,14.000000,.000000,,,
