﻿using Civica.Domain;
using Civica.Domain.CivicaDomain;
using System.Data.Entity;

namespace CivicaServices.Specs.TestUtils
{
    public class MyTestContext : IDatacurrrentEntitiesContext
    {
        public MyTestContext()
        {
            this.MonitoringStationAlarmConditions = new MonitoringStationAlarmConditionsDbSet();
            this.MonitoringStations = new MonitoringStationDbSet();
            this.MonitoringSensorAlarms = new MonitoringSensorAlarmDbSet();
            this.MonitoringDatas = new MonitoringDataDbSet();
            this.MonitoringStationAlarmConditionLogs = new MonitoringStationAlarmConditionLogDbSet();
            this.MonitoringStationAlarms = new MonitoringStationAlarmDbSet();
            this.MonitoringStationAlarmConditions = new MonitoringStationAlarmConditionsDbSet();
            this.MonitoringStationAlarmConditionTypes= new MonitoringStationAlarmConditionTypesDbSet();
            this.MonitoringStationAlarmLogs = new MonitoringStationAlarmLogsDbSet();
            this.AlarmAckinfoes = new AlarmAckinfoesDbSet();
            this.MonitoringSensors = new MonitoringSensorDbSet();
        }

        public DbSet<MonitoringStationActivity> MonitoringStationActivities { get; }
        public DbSet<MonitoringStationAlarmCondition> MonitoringStationAlarmConditions { get; }
        public DbSet<MonitoringStationAlarmConditionType> MonitoringStationAlarmConditionTypes { get; }
        public DbSet<MonitoringStationAlarmLog> MonitoringStationAlarmLogs { get; }
        public DbSet<MonitoringStationAlarmSeverity> MonitoringStationAlarmSeverities { get; }
        public DbSet<MonitoringStationAlarmType> MonitoringStationAlarmTypes { get; }
        public DbSet<MonitoringStationCatchmentArea> MonitoringStationCatchmentAreas { get; }
        public DbSet<MonitoringStationDocument> MonitoringStationDocuments { get; }
        public DbSet<MonitoringStationManualMeasurment> MonitoringStationManualMeasurments { get; }
        public DbSet<MonitoringStationNote> MonitoringStationNotes { get; }
        public DbSet<MonitoringStationPopulation> MonitoringStationPopulations { get; }
        public DbSet<MonitoringStationsMap> MonitoringStationsMaps { get; }
        public DbSet<MonitoringStationsOffSiteSensor> MonitoringStationsOffSiteSensors { get; }
        public DbSet<MonitoringStationState> MonitoringStationStates { get; }
        public DbSet<MonitoringStationTC> MonitoringStationTCs { get; }
        public DbSet<MonitoringStationTrafficLevel> MonitoringStationTrafficLevels { get; }
        public DbSet<MonitoringStationType> MonitoringStationTypes { get; }
        public DbSet<MonitoringTaskFormat> MonitoringTaskFormats { get; }
        public DbSet<MonitoringTaskPriority> MonitoringTaskPriorities { get; }
        public DbSet<MonitoringTaskStatu> MonitoringTaskStatus { get; }
        public DbSet<MonitoringTaskType> MonitoringTaskTypes { get; }
        public DbSet<MonitoringUnitConversion> MonitoringUnitConversions { get; }
        public DbSet<ObservationType> ObservationTypes { get; }
        public DbSet<Organization> Organizations { get; }
        public DbSet<Organization_History> Organization_History { get; }
        public DbSet<OrganizationDefaultUnitSet> OrganizationDefaultUnitSets { get; }
        public DbSet<OrganizationDefectType> OrganizationDefectTypes { get; }
        public DbSet<OrganizationDocument> OrganizationDocuments { get; }
        public DbSet<OrganizationGeometryAssetLayerData> OrganizationGeometryAssetLayerDatas { get; }
        public DbSet<OrganizationGeometryLayer> OrganizationGeometryLayers { get; }
        public DbSet<OrganizationLayerGroup> OrganizationLayerGroups { get; }
        public DbSet<OrganizationLocation> OrganizationLocations { get; }
        public DbSet<OrganizationPermission> OrganizationPermissions { get; }
        public DbSet<OrganizationPermission_History> OrganizationPermission_History { get; }
        public DbSet<OrganizationPersonelType> OrganizationPersonelTypes { get; }
        public DbSet<OrganizationRemediationType> OrganizationRemediationTypes { get; }
        public DbSet<OrganizationRolePermission> OrganizationRolePermissions { get; }
        public DbSet<OrganizationRole> OrganizationRoles { get; }
        public DbSet<OrganizationService> OrganizationServices { get; }
        public DbSet<OrganizationSiteVisitDefaultTemplate> OrganizationSiteVisitDefaultTemplates { get; }
        public DbSet<OrganizationSiteVisitTemplate> OrganizationSiteVisitTemplates { get; }
        public DbSet<OrganizationSiteVisitTemplateField> OrganizationSiteVisitTemplateFields { get; }
        public DbSet<OrganizationSiteVisitTemplateFieldOption> OrganizationSiteVisitTemplateFieldOptions { get; }
        public DbSet<OrganizationSiteVisitTemplateFieldType> OrganizationSiteVisitTemplateFieldTypes { get; }
        public DbSet<OrganizationTask> OrganizationTasks { get; }
        public DbSet<OrganizationTestType> OrganizationTestTypes { get; }
        public DbSet<OrganizationTool> OrganizationTools { get; }
        public DbSet<OrganizationType> OrganizationTypes { get; }
        public DbSet<OrganizationUserRole> OrganizationUserRoles { get; }
        public DbSet<OrganizationUserRoles_History> OrganizationUserRoles_History { get; }
        public DbSet<PartnerOrganization> PartnerOrganizations { get; }
        public DbSet<Permission> Permissions { get; }
        public DbSet<Project> Projects { get; }
        public DbSet<Project_History> Project_History { get; }
        public DbSet<ProjectAsset> ProjectAssets { get; }
        public DbSet<ProjectBoundary> ProjectBoundaries { get; }
        public DbSet<ProjectDefect> ProjectDefects { get; }
        public DbSet<ProjectDefect_History> ProjectDefect_History { get; }
        public DbSet<ProjectDefectDocument> ProjectDefectDocuments { get; }
        public DbSet<ProjectDefectNote> ProjectDefectNotes { get; }
        public DbSet<ProjectDefectOutlet> ProjectDefectOutlets { get; }
        public DbSet<ProjectDocument> ProjectDocuments { get; }
        public DbSet<ProjectGeometryAssetLayerData> ProjectGeometryAssetLayerDatas { get; }
        public DbSet<ProjectGeometryLayer> ProjectGeometryLayers { get; }
        public DbSet<ProjectLayerGroup> ProjectLayerGroups { get; }
        public DbSet<ProjectLotInspectionResult> ProjectLotInspectionResults { get; }
        public DbSet<ProjectLotInspectionResult_History> ProjectLotInspectionResult_History { get; }
        public DbSet<ProjectLotInspectionResultDocument> ProjectLotInspectionResultDocuments { get; }
        public DbSet<ProjectLotInspectionResultNote> ProjectLotInspectionResultNotes { get; }
        public DbSet<ProjectNote> ProjectNotes { get; }
        public DbSet<ProjectObservation> ProjectObservations { get; }
        public DbSet<ProjectOrganization> ProjectOrganizations { get; }
        public DbSet<ProjectResource> ProjectResources { get; }
        public DbSet<ProjectServiceDocument> ProjectServiceDocuments { get; }
        public DbSet<ProjectServiceNote> ProjectServiceNotes { get; }
        public DbSet<ProjectService> ProjectServices { get; }
        public DbSet<ProjectServices_History> ProjectServices_History { get; }
        public DbSet<ProjectServiceUser> ProjectServiceUsers { get; }
        public DbSet<ProjectType> ProjectTypes { get; }
        public DbSet<ProjectUser> ProjectUsers { get; }
        public DbSet<Province> Provinces { get; }
        public DbSet<RemediationType> RemediationTypes { get; }
        public DbSet<ResourceType> ResourceTypes { get; }
        public DbSet<ResourceTypeUnit> ResourceTypeUnits { get; }
        public DbSet<ServiceLayerType> ServiceLayerTypes { get; }
        public DbSet<ServiceLayerTypeConditionRanx> ServiceLayerTypeConditionRanges { get; }
        public DbSet<ServiceLayerTypeCondition> ServiceLayerTypeConditions { get; }
        public DbSet<ServiceProgress> ServiceProgresses { get; }
        public DbSet<Service> Services { get; }
        public DbSet<ServiceType> ServiceTypes { get; }
        public DbSet<ShapeFileAttributeMapp> ShapeFileAttributeMapps { get; }
        public DbSet<ShapeFileTask> ShapeFileTasks { get; }
        public DbSet<SiteVisit> SiteVisits { get; }
        public DbSet<SiteVisitDocument> SiteVisitDocuments { get; }
        public DbSet<SiteVisitField> SiteVisitFields { get; }
        public DbSet<SiteVisitNote> SiteVisitNotes { get; }
        public DbSet<SiteVisitUser> SiteVisitUsers { get; }
        public DbSet<StreetClass> StreetClasses { get; }
        public DbSet<TaskType> TaskTypes { get; }
        public DbSet<Test> Tests { get; }
        public DbSet<Test_History> Test_History { get; }
        public DbSet<TestAccessibilityType> TestAccessibilityTypes { get; }
        public DbSet<TestAsset> TestAssets { get; }
        public DbSet<TestAsset_History> TestAsset_History { get; }
        public DbSet<TestAssetDefectOutLetPercentage> TestAssetDefectOutLetPercentages { get; }
        public DbSet<TestAssetDefectOutlet> TestAssetDefectOutlets { get; }
        public DbSet<TestAssetDefectOutlets_History> TestAssetDefectOutlets_History { get; }
        public DbSet<TestAssetDocument> TestAssetDocuments { get; }
        public DbSet<TestAssetGroup> TestAssetGroups { get; }
        public DbSet<TestAssetLot> TestAssetLots { get; }
        public DbSet<TestAssetNote> TestAssetNotes { get; }
        public DbSet<TestAssetProgress> TestAssetProgresses { get; }
        public DbSet<TestAssetState> TestAssetStates { get; }
        public DbSet<TestAssetWeather> TestAssetWeathers { get; }
        public DbSet<TestAssetWeather_History> TestAssetWeather_History { get; }
        public DbSet<TestBoundary> TestBoundaries { get; }
        public DbSet<TestContact> TestContacts { get; }
        public DbSet<TestEnityWeather> TestEnityWeathers { get; }
        public DbSet<TestEnityWeather_History> TestEnityWeather_History { get; }
        public DbSet<TestEntity> TestEntities { get; }
        public DbSet<TestEntities_History> TestEntities_History { get; }
        public DbSet<TestEntityDefect> TestEntityDefects { get; }
        public DbSet<TestEntityDefects_History> TestEntityDefects_History { get; }
        public DbSet<TestEntityDocument> TestEntityDocuments { get; }
        public DbSet<TestEntityLotInspectionResult> TestEntityLotInspectionResults { get; }
        public DbSet<TestEntityNote> TestEntityNotes { get; }
        public DbSet<TestGeometryAssetLayerData> TestGeometryAssetLayerDatas { get; }
        public DbSet<TestGeometryBaseLayerData> TestGeometryBaseLayerDatas { get; }
        public DbSet<TestGeometryLayer> TestGeometryLayers { get; }
        public DbSet<TestLayerGroup> TestLayerGroups { get; }
        public DbSet<TestType> TestTypes { get; }
        public DbSet<TimeInterval> TimeIntervals { get; }
        public DbSet<Tool> Tools { get; }
        public DbSet<ToolType> ToolTypes { get; }
        public DbSet<Toronto_Area39_Projected_LatLong> Toronto_Area39_Projected_LatLong { get; }
        public DbSet<TorontoRainfallTask> TorontoRainfallTasks { get; }
        public DbSet<TrafficLevel> TrafficLevels { get; }
        public DbSet<Unit> Units { get; }
        public DbSet<UnitType> UnitTypes { get; }
        public DbSet<WeatherType> WeatherTypes { get; }
        public DbSet<WindDirection> WindDirections { get; }
        public DbSet<WindType> WindTypes { get; }
        public DbSet<WorkOrderPriority> WorkOrderPriorities { get; }
        public DbSet<WorkOrderStatu> WorkOrderStatus { get; }
        public DbSet<WorkOrderType> WorkOrderTypes { get; }
        public DbSet<Toronto_SAN_Sewer> Toronto_SAN_Sewer { get; }
        public DbSet<Toronto_SAN_MH> Toronto_SAN_MH { get; }
        public DbSet<tempView_KawarthaMonitoringStations_Boundary> tempView_KawarthaMonitoringStations_Boundary { get; }
        public DbSet<tempView_KawarthaMonitoringStations_Points> tempView_KawarthaMonitoringStations_Points { get; }
        public DbSet<ViewLotInspectionLotPolygon> ViewLotInspectionLotPolygons { get; }
        public DbSet<ViewLotsInspectionPointsOfInterest> ViewLotsInspectionPointsOfInterests { get; }
        public DbSet<ViewManholeInspectionProgress> ViewManholeInspectionProgresses { get; }
        public DbSet<ViewMenuItemAccess> ViewMenuItemAccesses { get; }
        public DbSet<ViewMonitoringStationBoundary> ViewMonitoringStationBoundaries { get; }
        public DbSet<ViewMonitoringStationPoint> ViewMonitoringStationPoints { get; }
        public DbSet<ViewMonitoringTaskList> ViewMonitoringTaskLists { get; }
        public DbSet<ViewPointsOfInterestCentroid> ViewPointsOfInterestCentroids { get; }
        public DbSet<ViewSmokeTestDefectBoundary> ViewSmokeTestDefectBoundaries { get; }
        public DbSet<ViewSmokeTestDefectCentroid> ViewSmokeTestDefectCentroids { get; }
        public DbSet<ViewSmokeTestGroupBoundary> ViewSmokeTestGroupBoundaries { get; }
        public DbSet<ViewSmokeTestGroupManhole> ViewSmokeTestGroupManholes { get; }
        public DbSet<ViewSmokeTestGroupSewerLine> ViewSmokeTestGroupSewerLines { get; }
        public DbSet<ViewTestDefec> ViewTestDefecs { get; }
        public DbSet<ViewTestDefecsOutlet> ViewTestDefecsOutlets { get; }
        public DbSet<MonitoringTask> MonitoringTasks { get; }
        public DbSet<MonitoringTaskExportDataType> MonitoringTaskExportDataTypes { get; }
        public DbSet<MonitoringTaskSensor> MonitoringTaskSensors { get; }
        public DbSet<AppUser> AppUsers { get; }
        public DbSet<AlarmAckinfo> AlarmAckinfoes { get; }
        public DbSet<MonitoringStationAlarm> MonitoringStationAlarms { get; }
        public DbSet<MonitoringServiceStation> MonitoringServiceStations { get; }
        public DbSet<MonitoringStation> MonitoringStations { get; }
        public DbSet<MonitoringStation_History> MonitoringStation_History { get; }

        public DbSet<MonitoringSensor_History> MonitoringSensor_History { get; }
        public DbSet<MonitoringSensorAlarm> MonitoringSensorAlarms { get; }
        public DbSet<MonitoringSensorAnalysisSetting> MonitoringSensorAnalysisSettings { get; }
        public DbSet<MonitoringSensorAnotation> MonitoringSensorAnotations { get; }
        public DbSet<MonitoringSensorCalculatedDependency> MonitoringSensorCalculatedDependencies { get; }
        public DbSet<MonitoringSensorCleaningSetting> MonitoringSensorCleaningSettings { get; }
        public DbSet<MonitoringSensorDataType> MonitoringSensorDataTypes { get; }
        public DbSet<MonitoringSensorDesignStorm> MonitoringSensorDesignStorms { get; }
        public DbSet<MonitoringSensorEquation> MonitoringSensorEquations { get; }
        public DbSet<MonitoringSensorImportExportMap> MonitoringSensorImportExportMaps { get; }
        public DbSet<MonitoringSensorOffsitePoint> MonitoringSensorOffsitePoints { get; }
        public DbSet<MonitoringSensorStaticData> MonitoringSensorStaticDatas { get; }
        public DbSet<MonitoringSensorStatu> MonitoringSensorStatus { get; }
        public DbSet<MonitoringSensorType> MonitoringSensorTypes { get; }
        public DbSet<MonitoringSensorUser> MonitoringSensorUsers { get; }
        public DbSet<MonitoringSensorUsers_History> MonitoringSensorUsers_History { get; }
        public DbSet<MonitoringService> MonitoringServices { get; }
        public DbSet<MonitoringService_History> MonitoringService_History { get; }
        public DbSet<MonitoringServiceBoundary> MonitoringServiceBoundaries { get; }
        public DbSet<MonitoringServiceGeometryLayerData> MonitoringServiceGeometryLayerDatas { get; }
        public DbSet<MonitoringServiceGeometryLayer> MonitoringServiceGeometryLayers { get; }
        public DbSet<MonitoringServiceLayerGroup> MonitoringServiceLayerGroups { get; }

        public DbSet<MenuItemType> MenuItemTypes { get; }
        public DbSet<MonitoringData> MonitoringDatas { get; }
        public DbSet<MonitoringDataImportLog> MonitoringDataImportLogs { get; }
        public DbSet<MonitoringDataRaw> MonitoringDataRaws { get; }
        public DbSet<MonitoringDataReactorBlueTreeLog> MonitoringDataReactorBlueTreeLogs { get; }
        public DbSet<MonitoringDataReactorBlueTreeMessageType> MonitoringDataReactorBlueTreeMessageTypes { get; }
        public DbSet<MonitoringLogger> MonitoringLoggers { get; }
        public DbSet<MonitoringLoggerChannel> MonitoringLoggerChannels { get; }
        public DbSet<MonitoringLoggerServer> MonitoringLoggerServers { get; }
        public DbSet<MonitoringLoggerServerType> MonitoringLoggerServerTypes { get; }
        public DbSet<MonitoringLoggerServiceData> MonitoringLoggerServiceDatas { get; }
        public DbSet<MonitoringLoggerType> MonitoringLoggerTypes { get; }
        public DbSet<MonitoringSensor> MonitoringSensors { get; }

        public DbSet<MonitoringStationAlarmConditionLog> MonitoringStationAlarmConditionLogs { get; }

        public int SaveChanges()
        {
            return 0;
        }

        public DbSet<AppUserLog> AppUserLogs { get; }
        public DbSet<Address> Addresses { get; }
        public DbSet<AddressType> AddressTypes { get; }
        public DbSet<AppUser_History> AppUser_History { get; }
        public DbSet<AppUserDocument> AppUserDocuments { get; }
        public DbSet<AppUserNote> AppUserNotes { get; }
        public DbSet<AppUserToken> AppUserTokens { get; }
        public DbSet<AreaUnit> AreaUnits { get; }
        public DbSet<Asset> Assets { get; }
        public DbSet<Asset_History> Asset_History { get; }
        public DbSet<AssetAttributeValue> AssetAttributeValues { get; }
        public DbSet<AssetCatchBasin> AssetCatchBasins { get; }
        public DbSet<AssetCatchBasin_History> AssetCatchBasin_History { get; }
        public DbSet<AssetCatchBasinCoverType> AssetCatchBasinCoverTypes { get; }
        public DbSet<AssetCatchBasinStructureType> AssetCatchBasinStructureTypes { get; }
        public DbSet<AssetCondition> AssetConditions { get; }
        public DbSet<AssetCulvert> AssetCulverts { get; }
        public DbSet<AssetCulvert_History> AssetCulvert_History { get; }
        public DbSet<AssetCulvertType> AssetCulvertTypes { get; }
        public DbSet<AssetDepreciationModel> AssetDepreciationModels { get; }
        public DbSet<AssetDocument> AssetDocuments { get; }
        public DbSet<AssetEquipment> AssetEquipments { get; }
        public DbSet<AssetEquipmentLog> AssetEquipmentLogs { get; }
        public DbSet<AssetEquipmentManufacturer> AssetEquipmentManufacturers { get; }
        public DbSet<AssetEquipmentModel> AssetEquipmentModels { get; }
        public DbSet<AssetEquipmentStatu> AssetEquipmentStatus { get; }
        public DbSet<AssetEquipmentType> AssetEquipmentTypes { get; }
        public DbSet<AssetLot> AssetLots { get; }
        public DbSet<AssetLotType> AssetLotTypes { get; }
        public DbSet<AssetManHole> AssetManHoles { get; }
        public DbSet<AssetManHole_History> AssetManHole_History { get; }
        public DbSet<AssetManHolePipeConection> AssetManHolePipeConections { get; }
        public DbSet<AssetManHoleType> AssetManHoleTypes { get; }
        public DbSet<AssetMaterial> AssetMaterials { get; }
        public DbSet<AssetOwnershipStatu> AssetOwnershipStatus { get; }
        public DbSet<AssetPond> AssetPonds { get; }
        public DbSet<AssetPond_History> AssetPond_History { get; }
        public DbSet<AssetPondFunction> AssetPondFunctions { get; }
        public DbSet<AssetPondHydrology> AssetPondHydrologies { get; }
        public DbSet<AssetPondSedimentAccumulation> AssetPondSedimentAccumulations { get; }
        public DbSet<AssetPondStageDischarge> AssetPondStageDischarges { get; }
        public DbSet<AssetPondType> AssetPondTypes { get; }
        public DbSet<AssetSanitaryPumpStation> AssetSanitaryPumpStations { get; }
        public DbSet<AssetSanitaryPumpStation_History> AssetSanitaryPumpStation_History { get; }
        public DbSet<AssetSewerSegment> AssetSewerSegments { get; }
        public DbSet<AssetSewerSegment_History> AssetSewerSegment_History { get; }
        public DbSet<AssetSewerSegmentType> AssetSewerSegmentTypes { get; }
        public DbSet<AssetShape> AssetShapes { get; }
        public DbSet<AssetsNote> AssetsNotes { get; }
        public DbSet<AssetStatu> AssetStatus { get; }
        public DbSet<AssetSurfaceType> AssetSurfaceTypes { get; }
        public DbSet<AssetType> AssetTypes { get; }
        public DbSet<AssetWebCam> AssetWebCams { get; }
        public DbSet<AssetWebCamLease> AssetWebCamLeases { get; }
        public DbSet<CoordinatSystem> CoordinatSystems { get; }
        public DbSet<Country> Countries { get; }
        public DbSet<DataExportHost> DataExportHosts { get; }
        public DbSet<DataExportSensor> DataExportSensors { get; }
        public DbSet<DefaultLayerStyle> DefaultLayerStyles { get; }
        public DbSet<DefaultLayerType> DefaultLayerTypes { get; }
        public DbSet<DefaultMunicipalityBoundaryLayerData> DefaultMunicipalityBoundaryLayerDatas { get; }
        public DbSet<DefaultMunicipalityBoundaryLayer> DefaultMunicipalityBoundaryLayers { get; }
        public DbSet<DefectDischarge> DefectDischarges { get; }
        public DbSet<DefectDrainageDirection> DefectDrainageDirections { get; }
        public DbSet<DefectProgress> DefectProgresses { get; }
        public DbSet<DefectSourceOutlet> DefectSourceOutlets { get; }
        public DbSet<DefectSourceOutletDyeIntensity> DefectSourceOutletDyeIntensities { get; }
        public DbSet<DefectSourceOutletType> DefectSourceOutletTypes { get; }
        public DbSet<DefectState> DefectStates { get; }
        public DbSet<DefectType> DefectTypes { get; }
        public DbSet<DesignStorm> DesignStorms { get; }
        public DbSet<DesignStormDuration> DesignStormDurations { get; }
        public DbSet<DesignStormEquation> DesignStormEquations { get; }
        public DbSet<DesignStormEquationType> DesignStormEquationTypes { get; }
        public DbSet<DesignStormReturnInterval> DesignStormReturnIntervals { get; }
        public DbSet<DocumentStorageType> DocumentStorageTypes { get; }
        public DbSet<DocumentType> DocumentTypes { get; }
        public DbSet<FlowVolumeRelation> FlowVolumeRelations { get; }
        public DbSet<GeometryLayerCategory> GeometryLayerCategories { get; }
        public DbSet<GeometryLayerData> GeometryLayerDatas { get; }
        public DbSet<GeometryLayer> GeometryLayers { get; }
        public DbSet<GeometryLayerType> GeometryLayerTypes { get; }
        public DbSet<HomePageType> HomePageTypes { get; }
        public DbSet<IIAnalysisSummaryEventStat> IIAnalysisSummaryEventStats { get; }
        public DbSet<IIAnalysisSummaryEventStats_Hisotry> IIAnalysisSummaryEventStats_Hisotry { get; }
        public DbSet<IIAnalysisSummaryGWIEOption> IIAnalysisSummaryGWIEOptions { get; }
        public DbSet<IIAnalysisSummaryProjectSetting> IIAnalysisSummaryProjectSettings { get; }
        public DbSet<IIAnalysisSummaryProjectSettings_History> IIAnalysisSummaryProjectSettings_History { get; }
        public DbSet<IIAnalysisSummaryReturnPeriod> IIAnalysisSummaryReturnPeriods { get; }
        public DbSet<IIAnalysisSummaryStationResult> IIAnalysisSummaryStationResults { get; }
        public DbSet<IIAnalysisSummaryStationResults_History> IIAnalysisSummaryStationResults_History { get; }
        public DbSet<IIAnalysisSummaryStationSetting> IIAnalysisSummaryStationSettings { get; }
        public DbSet<IIAnalysisSummaryStationSettings_History> IIAnalysisSummaryStationSettings_History { get; }
        public DbSet<IIAnalysisSummaryStormEvent> IIAnalysisSummaryStormEvents { get; }
        public DbSet<IIAnalysisSummaryStormEvent_History> IIAnalysisSummaryStormEvent_History { get; }
        public DbSet<IMPClientReference> IMPClientReferences { get; }
        public DbSet<IMPProjectReference> IMPProjectReferences { get; }
        public DbSet<IMPSensorReference> IMPSensorReferences { get; }
        public DbSet<IMPSensorStormReference> IMPSensorStormReferences { get; }
        public DbSet<IMPSiteStationReference> IMPSiteStationReferences { get; }
        public DbSet<IMPSiteVisitReference> IMPSiteVisitReferences { get; }
        public DbSet<IMPUserReference> IMPUserReferences { get; }
        public DbSet<LayerSymbologyType> LayerSymbologyTypes { get; }
        public DbSet<MeasuringStationAlarmLog> MeasuringStationAlarmLogs { get; }
        public DbSet<MenuItem> MenuItems { get; }

        public void MarkAsModified(MonitoringStationAlarmCondition item) { }
        public void Dispose() { }
    }
}
