﻿using Civica.Domain.CivicaDomain;
using System.Linq;

namespace CivicaServices.Specs.TestUtils
{

    public class AlarmAckinfoesDbSet : TestDbSet<AlarmAckinfo>
    {
        public override AlarmAckinfo Find(params object[] keyValues)
        {
            return this.SingleOrDefault(product => product.Id == (int)keyValues.Single());
        }
    }

    public class MonitoringStationAlarmLogsDbSet : TestDbSet<MonitoringStationAlarmLog>
    {
        public override MonitoringStationAlarmLog Find(params object[] keyValues)
        {
            return this.SingleOrDefault(product => product.Id == (int)keyValues.Single());
        }
    }

    public class MonitoringStationAlarmConditionTypesDbSet : TestDbSet<MonitoringStationAlarmConditionType>
    {
        public override MonitoringStationAlarmConditionType Find(params object[] keyValues)
        {
            return this.SingleOrDefault(product => product.Id == (int)keyValues.Single());
        }
    }

    public class MonitoringStationAlarmConditionsDbSet : TestDbSet<MonitoringStationAlarmCondition>
    {
        public override MonitoringStationAlarmCondition Find(params object[] keyValues)
        {
            return this.SingleOrDefault(product => product.Id == (int)keyValues.Single());
        }
    }

    public class MonitoringStationDbSet : TestDbSet<MonitoringStation>
    {
        public override MonitoringStation Find(params object[] keyValues)
        {
            return this.SingleOrDefault(product => product.Id == (int)keyValues.Single());
        }
    }

    public class MonitoringSensorAlarmDbSet : TestDbSet<MonitoringSensorAlarm>
    {
        public override MonitoringSensorAlarm Find(params object[] keyValues)
        {
            return this.SingleOrDefault(product => product.Id == (int)keyValues.Single());
        }
    }
    public class MonitoringDataDbSet : TestDbSet<MonitoringData>
    {
        public override MonitoringData Find(params object[] keyValues)
        {
            return this.SingleOrDefault(product => product.Id == (int)keyValues.Single());
        }
    }

    public class MonitoringStationAlarmConditionLogDbSet : TestDbSet<MonitoringStationAlarmConditionLog>
    {
        public override MonitoringStationAlarmConditionLog Find(params object[] keyValues)
        {
            return this.SingleOrDefault(product => product.Id == (int)keyValues.Single());
        }
    }

    public class MonitoringStationAlarmDbSet : TestDbSet<MonitoringStationAlarm>
    {
        public override MonitoringStationAlarm Find(params object[] keyValues)
        {
            return this.SingleOrDefault(product => product.Id == (int)keyValues.Single());
        }
    }

    public class MonitoringStationAlarmConditionDbSet : TestDbSet<MonitoringStationAlarmCondition>
    {
        public override MonitoringStationAlarmCondition Find(params object[] keyValues)
        {
            return this.SingleOrDefault(product => product.Id == (int)keyValues.Single());
        }
    }

    public class MonitoringSensorDbSet : TestDbSet<MonitoringSensor>
    {
        public override MonitoringSensor Find(params object[] keyValues)
        {
            return this.SingleOrDefault(product => product.Id == (int)keyValues.Single());
        }
    }
}
