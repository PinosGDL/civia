﻿using System;
using Civica.Domain.CivicaDomain;
using TimeZone = Civica.Domain.CivicaDomain.TimeZone;

namespace CivicaServices.Specs.TestUtils
{
    public static class TestExtensionMethods
    {
        public static MonitoringStationAlarmCondition FromCsv(this MonitoringStationAlarmCondition me, string csvLine)
        {
            string[] values = csvLine.Split(',');
            MonitoringStationAlarmCondition val = new MonitoringStationAlarmCondition();
            val.Id = Convert.ToInt32(values[0]);
            val.MonitoringStationAlarmId = Convert.ToInt32(values[1]);
            val.AlarmConditionTypeId = Convert.ToInt32(values[2]);
            val.Params = values[3];
            val.MonitoringStationAlarmConditionLogic = Convert.ToBoolean(values[4]);
            val.Active = Convert.ToBoolean(values[5]);
            return val;
        }

        public static MonitoringStationAlarmConditionType FromCsv(this MonitoringStationAlarmConditionType me, string csvLine)
        {
            string[] values = csvLine.Split(',');
            MonitoringStationAlarmConditionType val = new MonitoringStationAlarmConditionType();
            val.Id = Convert.ToInt32(values[0]);
            val.Condition = (values[1]);
            val.Code = (values[2]);
            val.Parameters = values[3];
            val.ParameterTypes = (values[4]);
            val.ShortDescription = (values[5]);
            //val.LongDescription = values[6];
            val.SortOrder = Convert.ToInt32(values[6]);
            val.Active = Convert.ToBoolean(values[7]);
            return val;
        }

        public static MonitoringSensorAlarm FromCsv(this MonitoringSensorAlarm me, string csvLine)
        {
            string[] values = csvLine.Split(',');
            MonitoringSensorAlarm val = new MonitoringSensorAlarm();
            val.Id = Convert.ToInt32(values[0]);
            val.MonitoringSensorId = Convert.ToInt32(values[1]);
            val.MonitoringStationAlarmConditionId = Convert.ToInt32(values[2]);
            val.Active = Convert.ToBoolean(values[3]);
            val.TypeA = string.IsNullOrWhiteSpace(values[4]) ? false : Convert.ToBoolean(values[4]);
            val.TypeB = string.IsNullOrWhiteSpace(values[5]) ? false : Convert.ToBoolean(values[5]);
            return val;
        }

        public static MonitoringSensor FromCsv(this MonitoringSensor me, string csvLine)
        {
            string[] values = csvLine.Split(',');
            MonitoringSensor val = new MonitoringSensor();
            val.Id = Convert.ToInt32(values[0]);
            val.AssetId = string.IsNullOrWhiteSpace(values[1]) ? null : new int?(Convert.ToInt32(values[1]));
            val.MonitorStationId = Convert.ToInt32(values[2]);
            val.ModemId = string.IsNullOrWhiteSpace(values[3]) ? null : new int?(Convert.ToInt32(values[3]));
            val.SensorTypeId = Convert.ToInt32(values[4]);
            val.SensorDataTypeId = Convert.ToInt32(values[5]);
            val.SensorStatusId = Convert.ToInt32(values[6]);
            val.Unit = values[7];
            val.Prominence = string.IsNullOrWhiteSpace(values[8]) ? null : new int?(Convert.ToInt32(values[8]));
            val.Name = values[9];
            return val;
        }

        public static MonitoringStation FromCsv(this MonitoringStation me, string csvLine)
        {
            string[] values = csvLine.Split(',');
            MonitoringStation val = new MonitoringStation();
            val.Id = Convert.ToInt32(values[0]);
            val.TypeId = Convert.ToInt32(values[1]);
            val.TrafficLevelId = string.IsNullOrWhiteSpace(values[2]) ? 0 : Convert.ToInt32(values[2]);
            val.StateId = string.IsNullOrWhiteSpace(values[3]) ? 0 : Convert.ToInt32(values[3]);
            val.Name = values[4];
            val.SensorDisplay = values[5];
            val.HideData = string.IsNullOrWhiteSpace(values[6]) ? false : Convert.ToBoolean(values[6]);
            val.Comments = values[7];
            val.MaintanaceMode = string.IsNullOrWhiteSpace(values[8]) ? false : Convert.ToBoolean(values[8]);
            val.LiveAlarms = string.IsNullOrWhiteSpace(values[9]) ? false : Convert.ToBoolean(values[9]);
            val.StartDate = Convert.ToDateTime(values[14]);
            val.EndDate = Convert.ToDateTime(values[15]);
            val.ExternalRanges = values[16];
            val.TimeZoneId = 25;
            return val;
        }

        public static MonitoringStationAlarm FromCsv(this MonitoringStationAlarm me, string csvLine)
        {
            string[] values = csvLine.Split(',');
            MonitoringStationAlarm val = new MonitoringStationAlarm();
            val.Id = Convert.ToInt32(values[0]);
            val.MonitoringStationId = Convert.ToInt32(values[1]);
            val.TypeId = Convert.ToInt32(values[2]);
            val.SeverityId = string.IsNullOrWhiteSpace(values[3]) ? 0 : Convert.ToInt32(values[3]);
            val.Name = values[4];
            val.Active = string.IsNullOrWhiteSpace(values[5]) ? false : Convert.ToBoolean(values[5]);
            val.AlarmConditionId = string.IsNullOrWhiteSpace(values[6]) ? 0 : Convert.ToInt32(values[6]);
            val.Params = values[6];
            val.IsTriggered = string.IsNullOrWhiteSpace(values[7]) ? false : Convert.ToBoolean(values[7]);
            val.TriggeredTime = string.IsNullOrWhiteSpace(values[8]) ? null : new DateTimeOffset?(Convert.ToDateTime(values[8]));
            val.BackToNormalNotification = string.IsNullOrWhiteSpace(values[9]) ? false : Convert.ToBoolean(values[9]);
            val.Enabled = true;
            return val;
        }

        public static TimeZone FromCsv(this TimeZone me, string csvLine)
        {
            string[] values = csvLine.Split(',');
            TimeZone val = new TimeZone();
            val.Id = Convert.ToInt32(values[0]);
            val.Name = values[1];
            val.Code = values[2];
            val.Offset = string.IsNullOrWhiteSpace(values[3]) ? 0 : Convert.ToDecimal(values[3]);
            val.SummerOffset = string.IsNullOrWhiteSpace(values[4]) ? 0 : Convert.ToDecimal(values[4]);
            val.SummerStartDate = string.IsNullOrWhiteSpace(values[5]) ? null : new DateTime?(Convert.ToDateTime(values[5]));
            val.SummerEndDate = string.IsNullOrWhiteSpace(values[6]) ? null : new DateTime?(Convert.ToDateTime(values[6]));
            val.TZICode = values[7];
            return val;
        }

        public static MonitoringStationAlarmSeverity FromCsv(this MonitoringStationAlarmSeverity me, string csvLine)
        {
            string[] values = csvLine.Split(',');
            MonitoringStationAlarmSeverity val = new MonitoringStationAlarmSeverity();
            val.Id = Convert.ToInt32(values[0]);
            val.Type = values[1];
            val.Code = values[2];
            val.SortOrder = string.IsNullOrWhiteSpace(values[3]) ? 0 : Convert.ToInt32(values[3]);
            val.Active = string.IsNullOrWhiteSpace(values[4]) ? false : Convert.ToBoolean(values[4]);

            return val;
        }

        public static MonitoringServiceStation FromCsv(this MonitoringServiceStation me, string csvLine)
        {
            string[] values = csvLine.Split(',');
            MonitoringServiceStation val = new MonitoringServiceStation();
            val.Id = Convert.ToInt32(values[0]);
            val.MonitoringServiceId = string.IsNullOrWhiteSpace(values[1]) ? 0 : Convert.ToInt32(values[1]);
            val.MonitoringStationId = string.IsNullOrWhiteSpace(values[2]) ? 0 : Convert.ToInt32(values[2]);
            val.IsOffsite = string.IsNullOrWhiteSpace(values[3]) ? false : Convert.ToBoolean(values[3]);
            val.Active = string.IsNullOrWhiteSpace(values[4]) ? false : Convert.ToBoolean(values[4]);
            val.Created = string.IsNullOrWhiteSpace(values[5]) ? new DateTimeOffset() : new DateTimeOffset(Convert.ToDateTime(values[5]));
            val.Modified = string.IsNullOrWhiteSpace(values[6]) ? new DateTimeOffset() : new DateTimeOffset(Convert.ToDateTime(values[6]));
            val.CreatedBy = string.IsNullOrWhiteSpace(values[7]) ? 0 : Convert.ToInt32(values[7]);
            val.ModifiedBy = string.IsNullOrWhiteSpace(values[8]) ? 0 : Convert.ToInt32(values[8]);
            val.Archived = string.IsNullOrWhiteSpace(values[9]) ? false : Convert.ToBoolean(values[9]);
            val.MapSortOrder = string.IsNullOrWhiteSpace(values[10]) ? 0 : Convert.ToInt32(values[10]);

            return val;
        }

        public static MonitoringService FromCsv(this MonitoringService me, string csvLine)
        {
            string[] values = csvLine.Split(',');
            MonitoringService val = new MonitoringService();
            val.Id = Convert.ToInt32(values[0]);
            
            val.BoundaryId = string.IsNullOrWhiteSpace(values[1]) ? 0 : Convert.ToInt32(values[1]);
            val.Name = values[2];
            val.StartDate = string.IsNullOrWhiteSpace(values[3]) ? new DateTimeOffset() : new DateTimeOffset(Convert.ToDateTime(values[3]));
            val.EndDate = string.IsNullOrWhiteSpace(values[4]) ? new DateTimeOffset() : new DateTimeOffset(Convert.ToDateTime(values[4]));
            val.EquipmentCostMultiplier = string.IsNullOrWhiteSpace(values[5]) ? 0 : Convert.ToDouble(values[5]);
            val.DataSource = values[6];
            val.Comment = values[7];
            val.LabelMonitoringStation = string.IsNullOrWhiteSpace(values[8]) ? false : Convert.ToBoolean(values[8]);

            return val;
        }
    }
}
