﻿using Civica.Domain.CivicaDomain;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NUnit.Framework;
using TimeZone = Civica.Domain.CivicaDomain.TimeZone;

namespace CivicaServices.Specs.TestUtils
{
    public class CsvImporter
    {

        public CsvImporter()
        {
        }

        public List<MonitoringData> GetData(string fileName, int sensorId, int dataColumn)
        {
            var result = new List<MonitoringData>();
            using (var reader = new StreamReader(fileName))
            {
                while (!reader.EndOfStream)
                {
                    var line = reader.ReadLine();
                    var values = line.Split(',');
                    var item = new MonitoringData();
                    item.TimeStamp = !string.IsNullOrWhiteSpace(values[0]) ? DateTimeOffset.Parse(values[0], new CultureInfo("en-US")) : DateTimeOffset.Parse(values[2], new CultureInfo("en-US"));
                    item.Value = string.IsNullOrWhiteSpace(values[dataColumn]) ? 0 : decimal.Parse(values[dataColumn]);
                    item.MonitoringSensorId = sensorId;
                    result.Add(item);
                }
            }
            return result;
        }

        public List<MonitoringStationAlarmCondition> GetMonitoringStationAlarmConditions(string fileName)
        {
            return File.ReadAllLines(fileName)
                                            .Skip(1)
                                            .Select(v => new MonitoringStationAlarmCondition().FromCsv(v))
                                            .ToList();
        }
        public List<MonitoringStationAlarmConditionType> GetMonitoringStationAlarmConditionsType(string fileName)
        {
            return File.ReadAllLines(fileName)
                                            .Skip(1)
                                            .Select(v => new MonitoringStationAlarmConditionType().FromCsv(v))
                                            .ToList();
        }
        public List<MonitoringSensorAlarm> GetMonitoringSensorAlarm(string fileName)
        {
            return File.ReadAllLines(fileName)
                                            .Skip(1)
                                            .Select(v => new MonitoringSensorAlarm().FromCsv(v))
                                            .ToList();
        }
        public List<MonitoringSensor> GetMonitoringSensor(string fileName)
        {
            return File.ReadAllLines(fileName)
                                            .Skip(1)
                                            .Select(v => new MonitoringSensor().FromCsv(v))
                                            .ToList();
        }
        public List<MonitoringStation> GetMonitoringStation(string fileName)
        {
            return File.ReadAllLines(fileName)
                                            .Skip(1)
                                            .Select(v => new MonitoringStation().FromCsv(v))
                                            .ToList();
        }

        public List<MonitoringStationAlarm> GetMonitoringStationAlarm(string fileName)
        {
            return File.ReadAllLines(fileName)
                                           .Skip(1)
                                           .Select(v => new MonitoringStationAlarm().FromCsv(v))
                                           .ToList();
        }
        public List<TimeZone> GetTimeZone(string fileName)
        {
            return File.ReadAllLines(fileName)
                                           .Skip(1)
                                           .Select(v => new TimeZone().FromCsv(v))
                                           .ToList();
        }

        public List<MonitoringStationAlarmSeverity> GetMonitoringStationAlarmSeverity(string fileName)
        {
            return File.ReadAllLines(fileName)
                                           .Skip(1)
                                           .Select(v => new MonitoringStationAlarmSeverity().FromCsv(v))
                                           .ToList();
        }

        public List<MonitoringServiceStation> GetMonitoringServiceStation(string fileName)
        {
            return File.ReadAllLines(fileName)
                                           .Skip(1)
                                           .Select(v => new MonitoringServiceStation().FromCsv(v))
                                           .ToList();
        }


        public List<MonitoringService> GetMonitoringService(string fileName)
        {
            return File.ReadAllLines(fileName)
                                           .Skip(1)
                                           .Select(v => new MonitoringService().FromCsv(v))
                                           .ToList();
        }



    }
}
