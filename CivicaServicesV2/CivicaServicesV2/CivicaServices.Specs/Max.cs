﻿using Civica.Domain;
using Civica.Domain.CivicaDomain;
using CivicaServices.Specs.TestUtils;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CivicaServices.Specs
{
    [TestFixture]
    public class Max
    {
        private MyTestContext context;
        private readonly string _monitoringStationAlarmConditionsPath = AppDomain.CurrentDomain.BaseDirectory + @"TestData\MonitoringStationAlarmConditions.txt";
        private readonly string _monitoringStationAlarmConditionsTypePath = AppDomain.CurrentDomain.BaseDirectory + @"TestData\MonitoringStationAlarmConditionsType.txt";
        private readonly string _monitoringSensorAlarmPath = AppDomain.CurrentDomain.BaseDirectory + @"TestData\MonitoringSensorAlarm.txt";
        private readonly string _monitoringSensorPath = AppDomain.CurrentDomain.BaseDirectory + @"TestData\MonitoringSensor.txt";
        private readonly string _monitoringStationPath = AppDomain.CurrentDomain.BaseDirectory + @"TestData\MonitoringStation.txt";
        private readonly string _monitoringStationAlarmPath = AppDomain.CurrentDomain.BaseDirectory + @"TestData\MonitoringStationAlarm.txt";
        private readonly string _timeZonesPath = AppDomain.CurrentDomain.BaseDirectory + @"TestData\TimeZone.txt";
        [SetUp]
        public void Init()
        {
        //    var monitoringStation = new MonitoringStation() { Id = 1, TimeZone = new Civica.Domain.CivicaDomain.TimeZone(), Active = true };
        //    var monitoringStationAlarm = new MonitoringStationAlarm() { Id = 1, MonitoringStation = monitoringStation, MonitoringStationId = 1, Active = true };
        //    var monitoringSensor = new MonitoringSensor() { Id = 1, Active = true };
        //    var monitoringSensorAlarmCondition = new MonitoringStationAlarmCondition() { Id = 1, MonitoringStationAlarmId = 1, Active = true,
        //        MonitoringStationAlarm = monitoringStationAlarm, Params = "x;0.2"
        //    };
        //    var monitoringSensorAlarm = new MonitoringSensorAlarm() { Id = 1, MonitoringStationAlarmCondition = monitoringSensorAlarmCondition,
        //        MonitoringSensor = monitoringSensor, MonitoringSensorId = 1, Active = true, MonitoringStationAlarmConditionId = 1 };
            context = new MyTestContext();
            var converter = new CsvImporter();
            var monitoringSensorAlarm = converter.GetMonitoringSensorAlarm(_monitoringSensorAlarmPath);
            var monitoringSensorAlarmConditionType = converter.GetMonitoringStationAlarmConditionsType(_monitoringStationAlarmConditionsTypePath);
            var monitoringSensor = converter.GetMonitoringSensor(_monitoringSensorPath);
            var monitoringSensorAlarmCondition = converter.GetMonitoringStationAlarmConditions(_monitoringStationAlarmConditionsPath);
            var monitoringStation = converter.GetMonitoringStation(_monitoringStationPath);
            var monitoringStationAlarm = converter.GetMonitoringStationAlarm(_monitoringStationAlarmPath);
            var timeZones = converter.GetTimeZone(_timeZonesPath);

            foreach (var item in monitoringSensorAlarmCondition)
            {
                item.MonitoringStationAlarm =
                    monitoringStationAlarm.FirstOrDefault(x => x.Id == item.MonitoringStationAlarmId);
                item.MonitoringStationAlarmConditionType =
                    monitoringSensorAlarmConditionType.FirstOrDefault(x => x.Id == item.AlarmConditionTypeId);
            }

            foreach (var station in monitoringStation)
            {
                station.TimeZone = timeZones.FirstOrDefault(x => x.Id == station.TimeZoneId);
            }

            foreach (var sensorAlarm in monitoringSensorAlarm)
            {
                sensorAlarm.MonitoringSensor =
                    monitoringSensor.FirstOrDefault(x => x.Id == sensorAlarm.MonitoringSensorId);
            }

            context.MonitoringStationAlarmConditions.AddRange(monitoringSensorAlarmCondition);
            context.MonitoringSensorAlarms.AddRange(monitoringSensorAlarm);
            context.MonitoringStations.AddRange(monitoringStation);

            var dataPath = AppDomain.CurrentDomain.BaseDirectory + @"TestData\2.1.1.csv";
            context.MonitoringDatas.AddRange(converter.GetData(dataPath,4176,1));
        }
        [Test]
        public void CanRun()
        {
            var rule = new Civica.BLL.Alarm.Max(767, context);
            System.Diagnostics.EventLog log = new EventLog();
            
            var res = rule.Check(null,null,log);
        }
    }
}
