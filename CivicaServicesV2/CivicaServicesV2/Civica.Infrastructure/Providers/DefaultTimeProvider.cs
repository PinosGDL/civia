﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civica.Infrastructure.Providers
{
    public class DefaultTimeProvider : TimeProvider
    {
        private readonly static DefaultTimeProvider instance = new DefaultTimeProvider();
        private DefaultTimeProvider() { }

        public override DateTimeOffset Now
        {
            get { return DateTimeOffset.Now; }
        }

        public static DefaultTimeProvider Instance
        {
            get { return DefaultTimeProvider.instance; }
        }
    }
}
