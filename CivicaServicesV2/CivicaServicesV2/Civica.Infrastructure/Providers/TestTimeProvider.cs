﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civica.Infrastructure.Providers
{
    public class TestTimeProvider : TimeProvider
    {
        private readonly static TestTimeProvider instance = new TestTimeProvider();

        private static DateTimeOffset CurrentTime;

        private TestTimeProvider() { }

        public override DateTimeOffset Now
        {
            get { return CurrentTime; }
        }

        public static TestTimeProvider Instance
        {
            get { return TestTimeProvider.instance; }
        }

        public static void SetCurrentTime(DateTimeOffset CurrentTime)
        {
            TestTimeProvider.CurrentTime = CurrentTime;
        }
    }
}
