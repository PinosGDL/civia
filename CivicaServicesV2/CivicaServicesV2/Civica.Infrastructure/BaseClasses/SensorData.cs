﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civica.Infrastructure.BaseClasses
{
    public class SensorData
    {
        public double value;
        public DateTime timestamp;
    }


    public class SensorDataOffsets
    {
        public double value;
        public DateTimeOffset timestamp;
    }

    public class TelogSensorData
    {
        public float? minValue;
        public float? maxValue;
        public float? avgValue;
        public DateTime timestamp;
        public int measurmentId;
        public string measurmentName;
    }


}
