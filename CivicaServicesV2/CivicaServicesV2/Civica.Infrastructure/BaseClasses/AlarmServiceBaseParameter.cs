﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Civica.Infrastructure.BaseClasses
{
    public static class AlarmServiceBaseParameter
    {
        public static readonly int NextVoilateEmailTime = Convert.ToInt32(ConfigurationManager.AppSettings["NextVoilateEmailTime"]);
        public static readonly int moveToNext = Convert.ToInt32(ConfigurationManager.AppSettings["MoveNextRound"]);
        public static readonly string DataCurrentBaseUrl = ConfigurationManager.AppSettings["DataCurrentBaseUrl"];
    }
}
