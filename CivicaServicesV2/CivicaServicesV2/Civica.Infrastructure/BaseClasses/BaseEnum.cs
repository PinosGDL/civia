﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civica.Infrastructure.BaseClasses
{
    public class BaseEnum
    {
        public enum AlarmTriggerType
        {
            ALARMRES_OK = 0,
            ALARMRES_VIOLATION = 1,
            ALARMRES_VIOL_BACK2NORMAL = 2,
            ALARMRES_NODATA = 4,
            ALARMRES_NODATEUPDATE = 5,
            ALARMRES_ERRORS = 6
        }

        public enum ServiceState
        {
            SERVICE_STOPPED = 0x00000001,
            SERVICE_START_PENDING = 0x00000002,
            SERVICE_STOP_PENDING = 0x00000003,
            SERVICE_RUNNING = 0x00000004,
            SERVICE_CONTINUE_PENDING = 0x00000005,
            SERVICE_PAUSE_PENDING = 0x00000006,
            SERVICE_PAUSED = 0x00000007,
        }

        public enum ImportTypes
        {

            TaskTable = 0,
            DetectronicDirectory = 1,
            DetectronicServer = 2,
            IsodaqXMLPath = 3,
            XRSPath = 4,
            ADSFTP = 5,
            ADSProfile = 6,
            TelogDB = 7,
            TelogPath = 8,
            FLWKMap = 9

        }

        public enum LoggerType
        {
            Detectronics = 1,
            Isodaq = 2,
            Telog = 3,
            ADSProfile = 4,
            Solinst = 5,
            RedLion = 6,
            FlowPro = 7,
            Other = 8,
            ADSFTP = 9,
            XRS = 10,
            DigiConnectSensor = 12,
            CampbellScientificCR6 = 13,
            WaterOfficeScraper = 14,
            GOESFieldTest = 15,
            FtsAxiomPhone = 16,
            FtsAxiomTCP = 17,
            BlueSiren = 18,
            WonderwareAPI = 19,
        }


        public enum MonitoringTaskType
        {
            Upload = 1,
            Download = 2,
            Delete = 3,
            Recalculate = 4,
            Export = 5
        }

        public enum MonitoringTaskStatus
        {
            Pending = 1,
            InProgress = 2,
            Success = 3,
            Faild = 4
        }

        public enum MonitoringTaskPriority
        {
            Urgent = 1,
            High = 2,
            Medium = 3,
            Low = 4
        }

        public enum MonitoringTaskFormat
        {
            DataCurrent = 1,
            Detec = 2,
            Delete = 3,
            CustomerFormat1  = 4,
            DefaultExport = 5
        }


        public enum StationState
        {
            Receiving = 1,
            ViewOnly = 2,
            Wearhousing = 3,
            Deleted = 4

        }



        public static class BlueTreeChanels
        {
            public const string AI1 = "AI1";
            public const string AI2 = "AI2";
            public const string AI3 = "AI3";

            public const string DI1 = "DI1";
            public const string DI2 = "DI2";
            public const string DI3 = "DI3";
            public const string DI4 = "DI4";

            public const string PWR = "PWR";

        }

        public enum HOBOChanels
        {
            Scaled_Series_1 = 80,
            Scaled_Series_2 = 81,
            Scaled_Series_3 = 82,
            Scaled_Series_4 = 83
        }
        public enum UnitType
        {
            Area = 1,
            FlowRate = 2,
            Depth = 3,
            Velocity = 4,
            Volume = 5,
            Temperature = 6,
            Voltage = 7,
            NormalizedGroundWaterInfiltration = 8,
            PeakIIRate = 9,
            RainfallIntensity = 10
        }

        public enum DCStandardUnitId
        {
            Area = 1, // Hectare
            FlowRate = 8, //L/S
            Depth = 28, // Meters
            Velocity = 34, // m/s
            Volume = 37, // L
            Temperature = 46, //Celcius
            Voltage = 48, //Volt
            RainfallIntensity = 32, // mm/hr
            PrecipitationDepth = 30, // mm
            PeakIIRate = 53, // L/s/ha
            GrdWaterInfiltration = 12 // l/cap/d

        }

        public enum PipeType
        {
            Rectangular = 2,
            Circular = 1,
            Triangular = 3,
            Parabolic = 4,
            Trapezoidal = 5,
            Irregular = 6
        }


    }

}
