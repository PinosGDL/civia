﻿namespace AllarmService
{
    using System;
    using System.Configuration;
    using System.Linq;
    using System.Net;
    using System.Net.Configuration;
    using System.Net.Mail;
    using System.Threading;
    using Civica.BLL;
    using Civica.BLL.ServiceClasses;
    using Civica.Domain;

    public class SenderProvider : ISender
    {
        public AlarmDataService alarmDataService;
        private IDatacurrrentEntitiesContext _model;

        public SenderProvider() : this(new DatacurrrentEntitiesContext())
        {

        }

        public SenderProvider(IDatacurrrentEntitiesContext model)
        {
            _model = model;
            this.alarmDataService = new AlarmDataService(_model);
        }

        public void SendPhoneMessage(string[] recipients, string message, string ackMessage, int alarmId, int repeatedIndex)
        {
            bool sent = false;
            string license = ConfigurationManager.AppSettings["SMSNotifyAPIKey"];
            message = "This is a datacurrent alarm text message" + "\n" + message;
            TextMessageService.IsmsClient client = new TextMessageService.IsmsClient("sms2wsHttpBinding");
            foreach (var recipient in recipients)
            {
                TextMessageService.SMSResponse resp = client.SimpleSMSsend(recipient, message, new Guid(license));
                DateTime sentTime = DateTime.Now;
                TimeSpan span = DateTime.Now - sentTime;
                while (!sent && span.TotalSeconds < 5)
                {
                    Thread.Sleep(5000);
                    resp = client.GetMessageStatus(resp.MessageID);
                    sent = resp.Sent;
                    span = DateTime.Now - sentTime;
                }
                Console.WriteLine("Message Sent: " + resp.Sent + "\n" + "Error: " + resp.SMSError + "\n" + "Message ID: " + resp.MessageID);
                Console.ReadLine();
                String newackMessage = ackMessage;
                if (ackMessage != null && ackMessage != "")
                {
                    var guid = Guid.NewGuid();
                    newackMessage = newackMessage + guid;
                    TextMessageService.SMSResponse resp1 = client.SimpleSMSsend(recipient, newackMessage, new Guid(license));
                    alarmDataService.insertInfoIntoAlarmAckinfo(guid.ToString(), alarmId, recipient);
                    alarmDataService.insertintoLog(alarmId, "notification-text", "text message send to " + recipient + "; repeated times : " + (repeatedIndex + 1));
                }
            }
            client.Close();
        }
        public long makePhoneCall(string[] recipients, string message, int alarmId, int repeatedIndex)
        {
            try
            {
                WSDL.PhoneNotify PN = new WSDL.PhoneNotify();
                WSDL.AdvancedNotifyRequest[] ANRs = new WSDL.AdvancedNotifyRequest[recipients.Count()];
                for (int i = 0; i < recipients.Length; i++)
                {
                    ANRs[i] = new WSDL.AdvancedNotifyRequest();

                    ANRs[i].CallerIDName = ConfigurationManager.AppSettings["CallerIDName"];
                    ANRs[i].CallerIDNumber = ConfigurationManager.AppSettings["CallerIDNumber"];
                    ANRs[i].PhoneNumberToDial = recipients[i];
                    ANRs[i].TextToSay = message;
                    ANRs[i].VoiceID = Convert.ToInt32(ConfigurationManager.AppSettings["VoiceID"]);
                    ANRs[i].StatusChangePostUrl = "https://www.example.com/callback-URL/"; //Optional
                    ANRs[i].UTCScheduledDateTime = DateTime.UtcNow;
                    ANRs[i].NextTryInSeconds = Convert.ToInt32(ConfigurationManager.AppSettings["NextTryInSeconds"]);
                    ANRs[i].MaxCallLength = 120;
                    ANRs[i].TryCount = Convert.ToInt32(ConfigurationManager.AppSettings["TryCount"]);
                    ANRs[i].TTSvolume = 100;
                    ANRs[i].TTSrate = 25;

                    //ANR.UTCScheduledDateTime = new DateTime(2012, 12, 18, 11, 36, 0).ToUniversalTime(); 
                    //If scheduling calls, the local time will automatically be converted to UTC time when using the line of code above.
                    ANRs[i].LicenseKey = ConfigurationManager.AppSettings["License"];
                }
                WSDL.NotifyReturn[] NR = PN.NotifyMultiplePhoneAdvanced(ANRs);
                Thread.Sleep(60000);
                foreach (var recipient in recipients)
                {
                    var status = PN.GetQueueIDStatusesByPhoneNumber(recipient, ConfigurationManager.AppSettings["License"]);
                    if (status[0].CallAnswered)
                    {
                        var msg = "phone call:" + recipient + " has been answered at " + DateTimeOffset.Now;
                        alarmDataService.insertintoLog(alarmId, "Phone answered", msg);
                    }
                    alarmDataService.insertintoLog(alarmId, "notification-phone", "phone call made to " + recipient + "; repeated times : " + (repeatedIndex + 1));
                }


                return NR[0].QueueID;

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);

            }
        }


        public void SendMail(int alarmId, string recipient, string subject, string message, int repeatedIndex)
        {
            Configuration oConfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var mailSettings = oConfig.GetSectionGroup("system.net/mailSettings") as MailSettingsSectionGroup;
            message = message.Replace("\n", "<br/>");
            if (mailSettings != null)
            {
                int port = mailSettings.Smtp.Network.Port;
                string from = mailSettings.Smtp.From;
                string host = mailSettings.Smtp.Network.Host;
                string pwd = mailSettings.Smtp.Network.Password;
                string uid = mailSettings.Smtp.Network.UserName;

                var mailMessage = new MailMessage
                {
                    From = new MailAddress(@from)
                };
                mailMessage.To.Add(new MailAddress(recipient));
                mailMessage.CC.Add(new MailAddress(from));
                mailMessage.Subject = subject;
                mailMessage.IsBodyHtml = true;
                mailMessage.Body = message;

                var client = new SmtpClient
                {
                    Host = host,
                    Port = port,
                    Credentials = new NetworkCredential(uid, pwd),
                    EnableSsl = true
                };

                try
                {
                    client.Send(mailMessage);
                    alarmDataService.insertintoLog(alarmId, "notification-email", "email send to " + recipient + "; repeated times : " + (repeatedIndex + 1));
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
            }
        }

    }
}
