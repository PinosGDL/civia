﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using Civica.Infrastructure.BaseClasses;
using System.Runtime.InteropServices;
using Civica.BLL.Alarm;

namespace AllarmService
{
    public partial class Service1 : ServiceBase
    {

        [DllImport("advapi32.dll", SetLastError = true)]
        private static extern bool SetServiceStatus(IntPtr handle, ref ServiceStatus serviceStatus);
        private Task AlarmServiceTask;
        private int i = 1;
        public Service1()
        {

            InitializeComponent();
            MonitoringAlarmServiceheventLog = new System.Diagnostics.EventLog();
            if (!System.Diagnostics.EventLog.SourceExists("MonitoringAlarmService"))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    "MonitoringAlarmService", "MonitoringAlarmServiceLog");
            }
            MonitoringAlarmServiceheventLog.Source = "MonitoringServiceAlarm";
            MonitoringAlarmServiceheventLog.Log = "";


        }

        protected override void OnStart(string[] args)
        {
            // initialize the time provider
            Civica.Infrastructure.Providers.TimeProvider.Current = Civica.Infrastructure.Providers.DefaultTimeProvider.Instance;

            MonitoringAlarmServiceheventLog.WriteEntry("MonitoringServiceAlarm Session Start : " + DateTime.Now.ToString());

            // Update the service state to Start Pending.
            ServiceStatus serviceStatus = new ServiceStatus();
            serviceStatus.dwCurrentState = BaseEnum.ServiceState.SERVICE_START_PENDING;
            serviceStatus.dwWaitHint = 100000;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);

            // Set up a timer to trigger every minute.
            System.Timers.Timer timer = new System.Timers.Timer();
            timer.Interval = 300000; // 5 minutes
            timer.Elapsed += new System.Timers.ElapsedEventHandler(this.OnTimer);
            timer.Start();

            // Update the service state to Running.
            serviceStatus.dwCurrentState = BaseEnum.ServiceState.SERVICE_RUNNING;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);

        }
        public void OnTimer(object sender, System.Timers.ElapsedEventArgs args)
        {
            // TODO: Insert monitoring activities here.
            try
            {
                //var service = new RunAlarm();

                //service.ProcessAlarm();
                if ((AlarmServiceTask != null) && (AlarmServiceTask.IsCompleted == false ||
                AlarmServiceTask.Status == TaskStatus.Running ||
                AlarmServiceTask.Status == TaskStatus.WaitingToRun ||
                AlarmServiceTask.Status == TaskStatus.WaitingForActivation))
                {
                    MonitoringAlarmServiceheventLog.WriteEntry("AlarmServiceImportTask Task has attempted to start while already running.  Will try on next cycle");
                }
                else
                {

                    MonitoringAlarmServiceheventLog.WriteEntry("Monitoring the System :" + i, EventLogEntryType.Information, i);  //eventId++
                    i++;
                    MonitoringAlarmServiceheventLog.WriteEntry("AlarmServiceTask Task has began");
                    AlarmServiceTask = Task.Factory.StartNew(() =>
                    {
                        var service = new RunAlarmBl(new SenderProvider());

                        service.ProcessAlarm(MonitoringAlarmServiceheventLog);
                    });
                }
            }
            catch (Exception ex)
            {

                string msg = String.Empty;
                if (ex.Message != null)
                {
                    msg = ex.Message;
                }
                if (ex.InnerException != null)
                {
                    msg = msg + " --- " + ex.InnerException.Message;
                }
                if (ex.StackTrace != null)
                {
                    msg = msg + " --- " + ex.StackTrace;
                }
                MonitoringAlarmServiceheventLog.WriteEntry("Monitoring the System, errors: " + msg, EventLogEntryType.Information, i);  //eventId++
            }
            MonitoringAlarmServiceheventLog.WriteEntry("End of Monitoring the System!", EventLogEntryType.Information, i);  //eventId++
        }

        protected override void OnStop()
        {
            MonitoringAlarmServiceheventLog.WriteEntry("MonitoringServiceAlarm Session Stop : " + DateTime.Now.ToString());
            ServiceStatus serviceStatus = new ServiceStatus();
            serviceStatus.dwCurrentState = BaseEnum.ServiceState.SERVICE_STOPPED;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);
        }

        private void DataSyncheventLog_EntryWritten(object sender, EntryWrittenEventArgs e)
        {
            MonitoringAlarmServiceheventLog.WriteEntry("MonitoringServiceAlarm Session Stop : " + DateTime.Now.ToString());
            ServiceStatus serviceStatus = new ServiceStatus();
            serviceStatus.dwCurrentState = BaseEnum.ServiceState.SERVICE_STOPPED;
            SetServiceStatus(this.ServiceHandle, ref serviceStatus);
        }

    }
}
