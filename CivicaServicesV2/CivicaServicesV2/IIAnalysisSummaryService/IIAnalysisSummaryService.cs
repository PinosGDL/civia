﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Threading;

namespace IIAnalysisSummaryService
{
    public partial class IIAnalysisSummaryService : ServiceBase
    {
   //     [DllImport("advapi32.dll", SetLastError = true)]
   //     private static extern bool SetServiceStatus(IntPtr handle, ref ServiceStatus serviceStatus);
        private Task IIAnalysisSummaryServiceTask;
        private System.Threading.Timer _timer = null;
        private int i = 1;
        public IIAnalysisSummaryService()
        {
            InitializeComponent();
            IIAnalysisSummaryServiceeventLog = new System.Diagnostics.EventLog();
            if (!System.Diagnostics.EventLog.SourceExists("IIAnalysisSummaryService"))
            {
                System.Diagnostics.EventLog.CreateEventSource(
                    "IIAnalysisSummaryService", "IIAnalysisSummaryServiceeventLog");
            }
            IIAnalysisSummaryServiceeventLog.Source = "IIAnalysisSummaryService";
            IIAnalysisSummaryServiceeventLog.Log = "";

        }
        protected override void OnStart(string[] args)
        {
            try
            {

                IIAnalysisSummaryServiceeventLog.WriteEntry("Monitoring Serivce II Analysis Calculation Session Start : " + DateTime.Now.ToString());

                // Update the service state to Start Pending.
           //     ServiceStatus serviceStatus = new ServiceStatus();
             //   serviceStatus.dwCurrentState = BaseEnum.ServiceState.SERVICE_START_PENDING;
                //serviceStatus.dwWaitHint = 100000;
                //SetServiceStatus(this.ServiceHandle, ref serviceStatus);

                // Set up a timer to trigger every minute.
                StartTimer(new TimeSpan(1, 0, 0), new TimeSpan(24, 0, 0));
                // Update the service state to Running.
                //serviceStatus.dwCurrentState = BaseEnum.ServiceState.SERVICE_RUNNING;
                //SetServiceStatus(this.ServiceHandle, ref serviceStatus);
            }
            catch (Exception ex)
            {

                string msg = String.Empty;

                if (ex.Message != null)
                {
                    msg = ex.Message;
                }

                if (ex.InnerException != null)
                {
                    msg = msg + " --- " + ex.InnerException.Message;
                }

                if (ex.StackTrace != null)
                {
                    msg = msg + " --- " + ex.StackTrace;
                }


                IIAnalysisSummaryServiceeventLog.WriteEntry("Monitoring the System, errors: " + msg, EventLogEntryType.Information);
            }
          
        }
        public void StartTimer(TimeSpan scheduledRunTime, TimeSpan timeBetweenEachRun)
        {
            // TODO: Insert monitoring activities here.


            double current = DateTime.Now.TimeOfDay.TotalMilliseconds;
            double scheduledTime = scheduledRunTime.TotalMilliseconds;
            double intervalPeriod = timeBetweenEachRun.TotalMilliseconds;
            // calculates the first execution of the method, either its today at the scheduled time or tomorrow (if scheduled time has already occured today)
            double firstExecution = current > scheduledTime ? intervalPeriod + (intervalPeriod - current) : scheduledTime - current;
            try
            {
                //var service = new RunAlarm();

                //service.ProcessAlarm();
                if ((IIAnalysisSummaryServiceTask != null) && (IIAnalysisSummaryServiceTask.IsCompleted == false ||
                IIAnalysisSummaryServiceTask.Status == TaskStatus.Running ||
                IIAnalysisSummaryServiceTask.Status == TaskStatus.WaitingToRun ||
                IIAnalysisSummaryServiceTask.Status == TaskStatus.WaitingForActivation))
                {
                    IIAnalysisSummaryServiceeventLog.WriteEntry("AlarmServiceImportTask Task has attempted to start while already running.  Will try on next cycle");
                }
                else
                {

                    IIAnalysisSummaryServiceeventLog.WriteEntry("Monitoring the System :" + i, EventLogEntryType.Information, i);  //eventId++
                    i++;
                    IIAnalysisSummaryServiceeventLog.WriteEntry("AlarmServiceTask Task has began");
                    IIAnalysisSummaryServiceTask = Task.Factory.StartNew(() =>
                    {
                        TimerCallback callback = new TimerCallback(RunService);

                        // create timer
                        _timer = new Timer(callback, null, Convert.ToInt32(firstExecution), Convert.ToInt32(intervalPeriod));
                    });
                }



            }
            catch (Exception ex)
            {

                string msg = String.Empty;

                if (ex.Message != null)
                {
                    msg = ex.Message;
                }

                if (ex.InnerException != null)
                {
                    msg = msg + " --- " + ex.InnerException.Message;
                }

                if (ex.StackTrace != null)
                {
                    msg = msg + " --- " + ex.StackTrace;
                }


                IIAnalysisSummaryServiceeventLog.WriteEntry("Monitoring the System, errors: " + msg, EventLogEntryType.Information, i);  //eventId++
            }

            IIAnalysisSummaryServiceeventLog.WriteEntry("End of Monitoring the System!", EventLogEntryType.Information, i);  //eventId++


        }

        private void RunService(object state)
        {
            var service = new IIAnalysisSummaryCalculation();

            service.ProcessIIAnalysisWindowsService(IIAnalysisSummaryServiceeventLog);
        }
        protected override void OnStop()
        {
            IIAnalysisSummaryServiceeventLog.WriteEntry("MonitoringServiceAlarm Session Stop : " + DateTime.Now.ToString());
            //ServiceStatus serviceStatus = new ServiceStatus();
            //serviceStatus.dwCurrentState = BaseEnum.ServiceState.SERVICE_STOPPED;
            //SetServiceStatus(this.ServiceHandle, ref serviceStatus);
        }

        private void DataSyncheventLog_EntryWritten(object sender, EntryWrittenEventArgs e)
        {
            IIAnalysisSummaryServiceeventLog.WriteEntry("MonitoringServiceAlarm Session Stop : " + DateTime.Now.ToString());
            //ServiceStatus serviceStatus = new ServiceStatus();
            //serviceStatus.dwCurrentState = BaseEnum.ServiceState.SERVICE_STOPPED;
            //SetServiceStatus(this.ServiceHandle, ref serviceStatus);
        }
    }
}
