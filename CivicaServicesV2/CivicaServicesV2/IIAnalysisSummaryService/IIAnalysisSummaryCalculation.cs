﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.BLL.ServiceClasses;
using Civica.BLL.Models.AnalysisTools;

namespace IIAnalysisSummaryService
{
    public class IIAnalysisSummaryCalculation
    {
        #region Fields
        private const double stormEndThresh = 3;          //threshold to inidicate the end of storm for statistic calculations
        private const int PRECIP_INTERVAL = 60; // 60min
        private const int _intDefaultPrecip = 5 * 60;       //interval for precip in seconds
        private const int _intDefaultPrecipIIAnalysis = 60 * 60;       //interval for precip in seconds (FOR II ANALYSIS)
        private const double _stormEndThresh = 3;            //threshold to inidicate the end of storm for statistic calculations 
        private const int numAvgPt = 20;                    //number of points used to develop the daily flow pattern
        private const int timeRange = 10;                   //range of time before and after the current time to use for average (min)
        private const double bandtightness = 0.95;           //tightness of the band.  The smaller the value, the tighter
        private const int BANDFAIL = -999;
        private const double weightU = 100;                 //upper bound of weights for flow pattern calculations
        private const double weightL = 50;                  //lower bound of weights for flow pattern calculations
        private int[] _timesteps = new int[] { 5, 10, 15, 20, 30, 60, 120, 180, 240, 360, 720, 1440 };
        private const int dwfLengthMatchMultiple = 1;       //The length in period of Inter Dry period
        private const double dwfTolerance = 0.85;           //The value that calculates DWF outside of a storm that will be allowed to match measured flow.
        private const int LOG_CURVE_POINTS = 10;             //number of points in the logarithmic fitted curve
        private int[] RETURN_PERIODS = new int[] { 2, 5, 10, 25, 50, 100 }; // Standard IDF Return Periods
        private const int DefaultDryPeriod = 12;
        private const int DefaultMinStormSize = 15;
        private const int DefaultGWISelect = 1;
        private const double DefaultGWIE = 85;
        private const int DefaultMinumEventNum = 3;
        private const double DefaultRSquare = 0.03;
        private const bool Defaultislinear = true;
        private const int DefaultReturnInterval = 3;
        #endregion
        private StormEventDataServices eventDataService;
        public IIAnalysisSummaryCalculation()
        {
            this.eventDataService = new StormEventDataServices();

        }

        public void ProcessIIAnalysisWindowsService(System.Diagnostics.EventLog log)
        {

            var projectStations = eventDataService.GetListProjectStations();
            try
            {

                foreach (var projectStation in projectStations)
                {
                    RunOneStationIIAnaysis(projectStation.ProjectId, projectStation.StationId, log);
                }
            }
            catch (Exception ex)
            {
                log.WriteEntry("Exception : " + ex.Message);
            }
        }

        public void RunOneStationIIAnaysis(int projectId, int stationId, System.Diagnostics.EventLog log)
        {
            try
            {
                var stationIIAnalysisSetting = eventDataService.GetIIAnalysisSummaryStationSettingByStationId(stationId);
                var stationTimeZoneoffset = eventDataService.GetMonitoringStationTimeZoneSpanByStationId(stationId);
                var organizationId = eventDataService.GetOrganizationIdByProjectId(projectId);
                Dictionary<int, string> idfSources = new Dictionary<int, string>();
                var defaultDesignStromId = eventDataService.GetDefaultDesignStormId(organizationId);
                Dictionary<int, Dictionary<int, Decimal?>> designStorms = new Dictionary<int, Dictionary<int, Decimal?>>();
                if (defaultDesignStromId != 0)
                {
                    designStorms = eventDataService.LoadDesignStorms(ref designStorms, defaultDesignStromId);
                }
                else
                    return;
                var FlowSensors = eventDataService.GetStationSensors(stationId).Where(x => x.SensorTypeId == 4);
                if (FlowSensors.Count() == 0)
                    return;
                var RainGaugeSensors = eventDataService.GetStationSensors(stationId).Where(x => x.SensorTypeId == 2).ToList();
                if (RainGaugeSensors.Count() == 0)
                    return;
                var stationPopulations = eventDataService.GetMonitoringStationPopulations(stationId).ToList();
                if (stationPopulations.Count == 0)
                    return;
                var defaultFlowSensorId = FlowSensors.FirstOrDefault().Id;
                var defaultRainSensorId = RainGaugeSensors.FirstOrDefault().Id;
                var defaultMonitoringStationPopulation = stationPopulations.FirstOrDefault();
                decimal catchmentarea = 0;
                decimal tc = 0;
                catchmentarea = GetDefaultStationCatchmentArea(stationId);
                if (catchmentarea == 0)
                    return;
                tc = GetDefaultStationTc(stationId);
                if (tc == 0)
                    return;
                var serviceId = eventDataService.GetServiceIdByStationId(stationId);
                if (stationIIAnalysisSetting == null)
                {
                    var flowDataArray = eventDataService.GetEventData(defaultFlowSensorId, null, null).ToArray();
                    var rainDataArray = eventDataService.GetEventData(defaultRainSensorId, null, null).ToArray();
                    var sensorMaxminumTime = eventDataService.GetSensorMaximumTime(defaultFlowSensorId);
                    var stationIIAnalysisSummary = eventDataService.GetIIAnalysisSummaryStationResultByStationId(stationId);
                    var flowInt = eventDataService.GetFlowSensorInt(defaultFlowSensorId);
                    if (stationIIAnalysisSummary == null)
                    {
                        RecalculateIIAnalysisSummarInfo(stationId, projectId, serviceId, defaultFlowSensorId, defaultRainSensorId, flowDataArray, rainDataArray, catchmentarea, tc, DefaultDryPeriod, DefaultMinStormSize, defaultMonitoringStationPopulation, designStorms, Defaultislinear, DefaultReturnInterval, DefaultMinumEventNum, DefaultRSquare, log);
                    }
                    else
                    {
                        DateTime lastRun = DateTime.MinValue;
                        var UpdatedEventStats = new List<EventStatRow>();
                        var eventStatRow = new List<EventStatRow>();
                        var lastRunCurrentTime = stationIIAnalysisSummary.LastRunCurrentTime;
                        var raiGaugesensorMonitoringTasks = eventDataService.GetMonitoringTasksHasNewData(defaultRainSensorId, lastRunCurrentTime.Value).OrderBy(x => x.DataStartTime).ToList();
                        var flowsensorMonitoringTasks = eventDataService.GetMonitoringTasksHasNewData(defaultFlowSensorId, lastRunCurrentTime.Value).OrderBy(x => x.DataStartTime).ToList();
                        if (raiGaugesensorMonitoringTasks.Count > 0 || flowsensorMonitoringTasks.Count > 0)
                        {
                            RecalculateIIAnalysisSummarInfo(stationId, projectId, serviceId, defaultFlowSensorId, defaultRainSensorId, flowDataArray, rainDataArray, catchmentarea, tc, DefaultDryPeriod, DefaultMinStormSize, defaultMonitoringStationPopulation, designStorms, Defaultislinear, DefaultReturnInterval, DefaultMinumEventNum, DefaultRSquare, log);
                        }
                        else
                        {
                            var lastRunOffset = stationIIAnalysisSummary.LastRun;
                            if (lastRunOffset != null)
                            {
                                lastRun = lastRunOffset.Value.ToOffset(stationTimeZoneoffset).DateTime;
                            }
                            if (rainDataArray.LastOrDefault().TimeStamp > lastRun)
                            {
                                var lastStormEventStartTime = stationIIAnalysisSummary.LastRunStormEventStartTime.Value.ToOffset(stationTimeZoneoffset).DateTime;
                                UpateIIAnalysisSummarInfo(stationId, projectId, serviceId, lastStormEventStartTime, defaultFlowSensorId, defaultRainSensorId, flowDataArray, rainDataArray, catchmentarea, tc, DefaultDryPeriod, DefaultMinStormSize, defaultMonitoringStationPopulation, designStorms, Defaultislinear, DefaultReturnInterval, DefaultMinumEventNum, DefaultRSquare, log);
                            }
                        }
                    }

                }
                else
                {
                    var stationSetting = eventDataService.GetCurrentIIAnalysisSummaryStationSetting(stationId);
                    var stationResesult = eventDataService.GetIIAnalysisSummaryStationResultByStationId(stationId);
                    Dictionary<int, Dictionary<int, Decimal?>> newdesignStorms = new Dictionary<int, Dictionary<int, Decimal?>>();
                    if (stationSetting.DesignStromId != 0)
                    {
                        newdesignStorms = eventDataService.LoadDesignStorms(ref newdesignStorms, stationSetting.DesignStromId);
                    }
                    if (stationResesult == null)
                    {
                        var flowsensorId = stationSetting.FlowSensorId;
                        var rainGaugeSensorId = stationSetting.RainGuageSensorId;
                        var newcatchmentarea = stationSetting.DrainageArea;
                        var newtc = stationSetting.tc;
                        var dryPeriod = stationSetting.InterDryPeriod;
                        var minumStormSize = stationSetting.MinumStormSize;
                        var population = stationSetting.Population;
                        var newflowData = eventDataService.GetEventData(flowsensorId, null, null).ToArray();
                        var newraindata = eventDataService.GetEventData(rainGaugeSensorId, null, null).ToArray();
                        RecalculateIIAnalysisSummarInfo(stationId, projectId, serviceId, flowsensorId, rainGaugeSensorId, newflowData, newraindata, newcatchmentarea, newtc, dryPeriod, minumStormSize, Decimal.ToInt32(population), newdesignStorms, stationSetting.isLeaner, stationSetting.returnInterval - 1, stationSetting.MinumNumberOfEvent, stationSetting.MinumRSquare, log);
                    }
                    else
                    {
                        var flowsensorId = stationResesult.FlowSensorId;
                        var rainGaugeSensorId = stationResesult.RainGaugeSensorId;
                        var newtc = stationResesult.TC;
                        var drainageArea = stationResesult.DrainageArea;
                        var dryPeriod = stationResesult.InterDryPeriod;
                        var minumStormSize = stationResesult.MinStormSize;
                        var population = stationResesult.Population;
                        newdesignStorms = new Dictionary<int, Dictionary<int, Decimal?>>();
                        if (stationSetting.DesignStromId != 0)
                        {
                            newdesignStorms = eventDataService.LoadDesignStorms(ref newdesignStorms, stationSetting.DesignStromId);
                        }
                        DateTime lastRun = DateTime.MinValue;
                        var lastRunCurrentTime = stationResesult.LastRunCurrentTime;
                        var flowDataArray = eventDataService.GetEventData(flowsensorId, null, null).ToArray();
                        var rainDataArray = eventDataService.GetEventData(rainGaugeSensorId, null, null).ToArray();
                        var raiGaugesensorMonitoringTasks = eventDataService.GetMonitoringTasksHasNewData(flowsensorId, lastRunCurrentTime.Value).OrderBy(x => x.DataStartTime).ToList();
                        var flowsensorMonitoringTasks = eventDataService.GetMonitoringTasksHasNewData(rainGaugeSensorId, lastRunCurrentTime.Value).OrderBy(x => x.DataStartTime).ToList();
                        if (raiGaugesensorMonitoringTasks.Count > 0 || flowsensorMonitoringTasks.Count > 0)
                        {
                            RecalculateIIAnalysisSummarInfo(stationId, projectId, serviceId, defaultFlowSensorId, defaultRainSensorId, flowDataArray, rainDataArray, catchmentarea, tc, DefaultDryPeriod, DefaultMinStormSize, defaultMonitoringStationPopulation, designStorms, Defaultislinear, DefaultReturnInterval, DefaultMinumEventNum, DefaultRSquare, log);
                        }
                        else
                        {
                            if (stationSetting.FlowSensorId == flowsensorId && stationSetting.RainGuageSensorId == rainGaugeSensorId && stationSetting.InterDryPeriod == dryPeriod && stationSetting.MinumStormSize == minumStormSize)
                            {

                                var lastRunOffset = stationResesult.LastRun;
                                if (lastRunOffset != null)
                                {
                                    lastRun = lastRunOffset.Value.ToOffset(stationTimeZoneoffset).DateTime;
                                }
                                if (stationSetting.DrainageArea == drainageArea && stationSetting.tc == newtc)
                                {
                                    if (rainDataArray.LastOrDefault().TimeStamp > lastRun)
                                    {
                                        var lastStormEventStartTime = stationResesult.LastRunStormEventStartTime.Value.ToOffset(stationTimeZoneoffset).DateTime;
                                        UpateIIAnalysisSummarInfo(stationId, projectId, serviceId, lastStormEventStartTime, flowsensorId, rainGaugeSensorId, flowDataArray, rainDataArray, stationSetting.DrainageArea, stationSetting.tc, stationSetting.InterDryPeriod, stationSetting.MinumStormSize, Convert.ToInt32(stationSetting.Population), newdesignStorms, stationSetting.isLeaner, stationSetting.returnInterval - 1, stationSetting.MinumNumberOfEvent, stationSetting.MinumRSquare, log);
                                    }
                                }
                                else
                                {
                                    RecalculateIIAnalysisSummarInfo(stationId, projectId, serviceId, flowsensorId, rainGaugeSensorId, flowDataArray, rainDataArray, stationSetting.DrainageArea, stationSetting.tc, stationSetting.InterDryPeriod, stationSetting.MinumStormSize, Convert.ToInt32(stationSetting.Population), newdesignStorms, stationSetting.isLeaner, stationSetting.returnInterval - 1, stationSetting.MinumNumberOfEvent, stationSetting.MinumRSquare, log);
                                }

                            }
                            else
                            {
                                var newflowsensorId = stationSetting.FlowSensorId;
                                var newrainGaugeSensorId = stationSetting.RainGuageSensorId;
                                var newcatchmentarea = stationSetting.DrainageArea;
                                var newanothertc = stationSetting.tc;
                                var newdryPeriod = stationSetting.InterDryPeriod;
                                var newminumStormSize = stationSetting.MinumStormSize;
                                var newpopulation = stationSetting.Population;
                                var newflowData = eventDataService.GetEventData(newflowsensorId, null, null).ToArray();
                                var newraindata = eventDataService.GetEventData(newrainGaugeSensorId, null, null).ToArray();
                                RecalculateIIAnalysisSummarInfo(stationId, projectId, serviceId, newflowsensorId, newrainGaugeSensorId, newflowData, newraindata, newcatchmentarea, newanothertc, newdryPeriod, newminumStormSize, Decimal.ToInt32(newpopulation), newdesignStorms, stationSetting.isLeaner, stationSetting.returnInterval - 1, stationSetting.MinumNumberOfEvent, stationSetting.MinumRSquare, log);
                            }
                        }
                      
                    }
                }


            }
            catch (Exception ex)
            {
                log.WriteEntry("Exception : " + ex.Message);
            }


        }

        public void RecalculateIIAnalysisSummarInfo(int stationId, int projectId, int serviceId, int flowSensorId, int rainGuageSensorId, MonitoringDataDTO[] fullflowSensorData, MonitoringDataDTO[] fullRainGuageSensorData, decimal catchmentarea, decimal tc, decimal InterDryPeriod, decimal minumstormsize, int population,
                Dictionary<int, Dictionary<int, Decimal?>> designStorms, bool isLinear, int Interval, int minumNumberEvent, double minRSquare, System.Diagnostics.EventLog log)
        {
            try
            {
                List<DateTime>[] stormList2 = new List<DateTime>[2];
                Periods[] dryFlowArray = new Periods[1];
                Periods[] flowArray = new Periods[1];
                int numDays, numPeriods;
                var startTime = fullflowSensorData[0].TimeStamp;
                var endTime = fullflowSensorData[fullflowSensorData.Count() - 1].TimeStamp;
                var stationoffset = eventDataService.GetMonitoringStationTimeZoneSpanByStationId(stationId);
                eventDataService.InitFlowData(fullflowSensorData, fullRainGuageSensorData, Decimal.ToDouble(InterDryPeriod), Decimal.ToDouble(minumstormsize), ref stormList2, fullflowSensorData[0].TimeStamp, fullflowSensorData[fullflowSensorData.Count() - 1].TimeStamp);
                var stormListArray = new DateTime[stormList2.Length][];
                for (var q = 0; q < stormList2.Length; q++)
                {
                    stormListArray[q] = stormList2[q].ToArray();
                }
                var flowInt = eventDataService.GetFlowSensorInt(flowSensorId);
                var eventStatRow = CalculateEventStats(stationId, fullflowSensorData, fullRainGuageSensorData, flowSensorId, rainGuageSensorId, flowInt, stormList2, DefaultDryPeriod, DefaultMinStormSize, tc, catchmentarea, log);

                eventDataService.InitFlowData(fullflowSensorData, fullRainGuageSensorData, flowInt, DefaultDryPeriod, stormList2, ref dryFlowArray, ref flowArray, startTime, endTime, out numDays, out numPeriods);

                Dictionary<string, FlowStatRow> flowStats = new Dictionary<string, FlowStatRow>();
                InitFlowStats(ref flowStats);
                Nullable<double>[] flowAvgDayArray;
                eventDataService.CalculateFlowStats(ref flowStats, flowArray, dryFlowArray, numDays, numPeriods, population, DefaultGWISelect, DefaultGWIE, catchmentarea, out flowAvgDayArray);
                if (((!flowStats["Peak_II"].Average.HasValue) || ((double)eventStatRow.ElementAt(0).PeakIIFlow > flowStats["Peak_II"].Average.Value)))
                {
                    flowStats["Peak_II"].Average = (double)eventStatRow.ElementAt(0).PeakIIFlow;
                    flowStats["Peak_II"].Normalized = (double)eventStatRow.ElementAt(0).PeakIIRate;
                    for (int i = 0; i < eventStatRow.Count; i++)
                    {
                        if ((double)eventStatRow.ElementAt(i).PeakIIFlow > flowStats["Peak_II"].Average)
                        {
                            flowStats["Peak_II"].Average = (double)eventStatRow.ElementAt(i).PeakIIFlow;
                            flowStats["Peak_II"].Normalized = (double)eventStatRow.ElementAt(i).PeakIIRate;
                        }

                        //II% for FlowStats
                        if (((!flowStats["II%"].Average.HasValue) || ((double)eventStatRow.ElementAt(i).VolumetricRunoffCoef * 100 > flowStats["II%"].Average.Value)))
                        {
                            flowStats["II%"].Average = (double)eventStatRow.ElementAt(i).VolumetricRunoffCoef * 100;
                        }

                    }
                }
                List<SeriesPoints> s_design = new List<SeriesPoints>();
                double? PeakIIFlow = null;
                double? PeakIIRate = null;
                int plotType = 0;
                if (isLinear)
                    plotType = 1;
                else
                    plotType = 2;
                if (eventStatRow.Count > minumNumberEvent)
                {
                    var PeakRaterSquared = eventDataService.CalculateCoeficientOfDetermination(eventStatRow, 5);
                    if (Decimal.ToDouble(PeakRaterSquared) > minRSquare)
                    {
                        PeakIIRate = eventDataService.PlotValue(eventStatRow, designStorms, tc, Interval, 5, plotType);
                    }

                    var PeakFlowrSquared = eventDataService.CalculateCoeficientOfDetermination(eventStatRow, 4);
                    if (Decimal.ToDouble(PeakFlowrSquared) > minRSquare)
                    {
                        PeakIIFlow = eventDataService.PlotValue(eventStatRow, designStorms, tc, Interval, 4, plotType);
                    }

                }
                if (PeakIIRate <= 0)
                {
                    PeakIIRate = null;
                }
                if (PeakIIFlow <= 0)
                {
                    PeakIIFlow = null;
                }
                var newlastRun = new DateTimeOffset(fullRainGuageSensorData.LastOrDefault().TimeStamp, stationoffset);
                var lastRunStormStartTime = new DateTimeOffset(stormListArray[1][stormListArray[1].Length - 2], stationoffset);
                eventDataService.UpdateStormEvents(stormList2, stationId);
                eventDataService.UpdateEventStats(eventStatRow, stationId);
                eventDataService.UpdateIIAnalysisummaryFlowStats(flowStats, stationId, projectId, serviceId, PeakIIRate, PeakIIFlow, newlastRun, lastRunStormStartTime, catchmentarea, tc, (Decimal)population, rainGuageSensorId, flowSensorId, minumstormsize, InterDryPeriod);
            }
            catch (Exception ex)
            {
                log.WriteEntry("Exception : " + ex.Message);
            }

        }


        public void UpateIIAnalysisSummarInfo(int stationId, int projectId, int serviceId, DateTime lastRun, int flowSensorId, int rainGuageSensorId, MonitoringDataDTO[] fullflowSensorData, MonitoringDataDTO[] fullRainGuageSensorData, decimal catchmentarea, decimal tc, decimal InterDryPeriod, decimal minumstormsize, int population,
                Dictionary<int, Dictionary<int, Decimal?>> designStorms, bool isLinear, int Interval, int minumNumberEvent, double minRSquare, System.Diagnostics.EventLog log)
        {
            try
            {


                List<DateTime>[] stormList2 = new List<DateTime>[2];
                Periods[] dryFlowArray = new Periods[1];
                Periods[] flowArray = new Periods[1];
                var startTime = fullflowSensorData[0].TimeStamp;
                var endTime = fullflowSensorData[fullflowSensorData.Count() - 1].TimeStamp;
                var previousEventsatsRow = eventDataService.GetIIAnlysisPreiousEventStatRowByStationId(stationId);
                var eventSatsRow = new List<EventStatRow>();
                var stationoffset = eventDataService.GetMonitoringStationTimeZoneSpanByStationId(stationId);
                var newflowData = fullflowSensorData.Where(x => x.TimeStamp > lastRun).ToArray();
                var newRainData = fullRainGuageSensorData.Where(x => x.TimeStamp > lastRun).ToArray();
                eventDataService.InitFlowData(newflowData, newRainData, Decimal.ToDouble(InterDryPeriod), Decimal.ToDouble(minumstormsize), ref stormList2, newflowData[0].TimeStamp, newflowData[newflowData.Count() - 1].TimeStamp);
                var flowInt = eventDataService.GetFlowSensorInt(flowSensorId);
                int numDays, numPeriods;
                var newStormList = new List<DateTime>[2];
                for (var i = 0; i < newStormList.Length; i++)
                {
                    newStormList[i] = new List<DateTime>();
                }
                var previousStormEvent = eventDataService.GetPreviousStormEvent(stationId);
                for (var i = 0; i < previousStormEvent[0].Count - 1; i++)
                {
                    newStormList[0].Add(previousStormEvent[0][i]);
                    newStormList[1].Add(previousStormEvent[1][i]);
                }
                foreach (var datetime in stormList2[0])
                {
                    newStormList[0].Add(datetime);
                }
                foreach (var datetime in stormList2[1])
                {
                    newStormList[1].Add(datetime);
                }
                var stormListArray = new DateTime[newStormList.Length][];
                for (var q = 0; q < newStormList.Length; q++)
                {
                    stormListArray[q] = newStormList[q].ToArray();
                }
                for (var k = 0; k < previousEventsatsRow.Count; k++)
                {
                    if (previousEventsatsRow[k].StartDate.AddHours(Convert.ToInt32(InterDryPeriod)) == stormList2[0][0])
                    {
                        break;
                    }
                    else
                        eventSatsRow.Add(previousEventsatsRow[k]);
                }
                Dictionary<String, List<decimal>> mValueDynamic = new Dictionary<string, List<decimal>>();

                eventDataService.GetValueDynamic(ref mValueDynamic, newStormList, stationId, catchmentarea, tc);
                var newstormListArray = new DateTime[newStormList.Length][];
                for (var q = 0; q < newStormList.Length; q++)
                {
                    newstormListArray[q] = newStormList[q].ToArray();
                }

                for (var k = previousStormEvent[0].Count - 1; k < newStormList[0].Count; k++)
                {
                    var eventStats = eventDataService.UpdatePreivousEventStats(mValueDynamic, fullflowSensorData, fullRainGuageSensorData, newstormListArray, k, flowInt, flowSensorId, rainGuageSensorId, Decimal.ToDouble(InterDryPeriod), Decimal.ToDouble(minumstormsize), log);
                    if (eventStats != null)
                        eventSatsRow.Add(eventStats);
                }

                eventDataService.InitFlowData(fullflowSensorData, fullRainGuageSensorData, flowInt, Decimal.ToDouble(InterDryPeriod), newStormList, ref dryFlowArray, ref flowArray, startTime, endTime, out numDays, out numPeriods);

                Dictionary<string, FlowStatRow> flowStats = new Dictionary<string, FlowStatRow>();
                InitFlowStats(ref flowStats);
                Nullable<double>[] flowAvgDayArray;
                eventDataService.CalculateFlowStats(ref flowStats, flowArray, dryFlowArray, numDays, numPeriods, population, DefaultGWISelect, DefaultGWIE, catchmentarea, out flowAvgDayArray);
                if (((!flowStats["Peak_II"].Average.HasValue) || ((double)eventSatsRow.ElementAt(0).PeakIIFlow > flowStats["Peak_II"].Average.Value)))
                {
                    flowStats["Peak_II"].Average = (double)eventSatsRow.ElementAt(0).PeakIIFlow;
                    flowStats["Peak_II"].Normalized = (double)eventSatsRow.ElementAt(0).PeakIIRate;
                    for (int i = 0; i < eventSatsRow.Count; i++)
                    {
                        if ((double)eventSatsRow.ElementAt(i).PeakIIFlow > flowStats["Peak_II"].Average)
                        {
                            flowStats["Peak_II"].Average = (double)eventSatsRow.ElementAt(i).PeakIIFlow;
                            flowStats["Peak_II"].Normalized = (double)eventSatsRow.ElementAt(i).PeakIIRate;
                        }

                        //II% for FlowStats
                        if (((!flowStats["II%"].Average.HasValue) || ((double)eventSatsRow.ElementAt(i).VolumetricRunoffCoef * 100 > flowStats["II%"].Average.Value)))
                        {
                            flowStats["II%"].Average = (double)eventSatsRow.ElementAt(i).VolumetricRunoffCoef * 100;
                        }
                    }
                }
                List<SeriesPoints> s_design = new List<SeriesPoints>();
                double? PeakIIFlow = null;
                double? PeakIIRate = null;
                int plotType = 0;
                if (isLinear)
                    plotType = 1;
                else
                    plotType = 2;
                if (eventSatsRow.Count > minumNumberEvent)
                {
                    var PeakRaterSquared = eventDataService.CalculateCoeficientOfDetermination(eventSatsRow, 5);
                    if (Decimal.ToDouble(PeakRaterSquared) > minRSquare)
                    {
                        PeakIIRate = eventDataService.PlotValue(eventSatsRow, designStorms, tc, Interval, 5, plotType);
                    }
                    var PeakFlowrSquared = eventDataService.CalculateCoeficientOfDetermination(eventSatsRow, 4);
                    if (Decimal.ToDouble(PeakFlowrSquared) > minRSquare)
                    {
                        PeakIIFlow = eventDataService.PlotValue(eventSatsRow, designStorms, tc, Interval, 4, plotType);
                    }
                }
                if (PeakIIRate <= 0)
                {
                    PeakIIRate = null;
                }
                if (PeakIIFlow <= 0)
                {
                    PeakIIFlow = null;
                }
                var newlastRun = new DateTimeOffset(fullRainGuageSensorData.LastOrDefault().TimeStamp, stationoffset);
                var lastRunStormStartTime = new DateTimeOffset(stormListArray[1][stormListArray[1].Length - 2], stationoffset);
                eventDataService.UpdateStormEvents(newStormList, stationId);
                eventDataService.UpdateEventStats(eventSatsRow, stationId);
                eventDataService.UpdateIIAnalysisummaryFlowStats(flowStats, stationId, projectId, serviceId, PeakIIRate, PeakIIFlow, newlastRun, lastRunStormStartTime, catchmentarea, tc, (Decimal)population, rainGuageSensorId, flowSensorId, minumstormsize, InterDryPeriod);



            }
            catch (Exception ex)
            {
                log.WriteEntry("Exception : " + ex.Message);
            }
        }

        private void InitFlowStats(ref Dictionary<string, FlowStatRow> FlowStats)
        {
            FlowStats = new Dictionary<string, FlowStatRow>();
            FlowStats.Add("Q", new FlowStatRow("Avg. flow", "Q", "L/s", "L/cap/d", 3));
            FlowStats.Add("DWF", new FlowStatRow("Avg. dry-weather flow", "DWF", "L/s", "L/cap/d", 3));
            FlowStats.Add("Pop_DWF", new FlowStatRow("Avg. population DWF", "Pop_DWF", "L/s", "L/cap/d", 3));
            FlowStats.Add("Min_DWF", new FlowStatRow("Avg. daily minimum DWF", "Min_DWF", "L/s", "L/cap/d", 3));
            FlowStats.Add("Peak_DWF", new FlowStatRow("Avg. daily peak DWF", "Peak_DWF", "L/s", "L/cap/d", 3));
            FlowStats.Add("GWI", new FlowStatRow("Groundwater infiltration", "GWI", "L/s", "L/ha/d", 3));
            FlowStats.Add("Harmon", new FlowStatRow("Measured Harmon peak factor", "Harmon", "", null, 3));
            FlowStats.Add("Theo_Harmon", new FlowStatRow("Theoretical Harmon peak factor", "Theo_Harmon", "", null, 3));
            FlowStats.Add("HCF", new FlowStatRow("Harmon correction factor", "HCF", "", null, 3));
            FlowStats.Add("Peak_WWF", new FlowStatRow("Peak wet-weather flow", "Peak_WWF", "L/s", "L/s/ha"));
            FlowStats.Add("Peak_II", new FlowStatRow("Peak I/I", "Peak_II", "L/s", "L/s/ha"));
            FlowStats.Add("II%", new FlowStatRow("Peak % of precipitation in sanitary", "II%", "%", null, 3));
        }
        public Dictionary<string, FlowStatRow> CalculateFlowStats()
        {

            return null;
        }
        public List<EventStatRow> CalculateEventStats(int siteId, MonitoringDataDTO[] flowDataArray, MonitoringDataDTO[] rainDataArray, int flowSensorId, int rainSensorId, decimal flowInt, List<DateTime>[] stormList, double interDryPeriod, double minStormSize, decimal tc, decimal catchment, System.Diagnostics.EventLog log)
        {
            try
            {
                Dictionary<String, List<decimal>> mValueDynamic = new Dictionary<string, List<decimal>>();

                eventDataService.GetValueDynamic(ref mValueDynamic, stormList, siteId, catchment, tc);
                var stormListArray = new DateTime[stormList.Length][];
                for (var q = 0; q < stormList.Length; q++)
                {
                    stormListArray[q] = stormList[q].ToArray();
                }

                var eventStats = eventDataService.CalculateEventStats(mValueDynamic, flowDataArray, rainDataArray, stormListArray, flowInt, flowSensorId, rainSensorId, interDryPeriod, minStormSize, log);
                return eventStats;
            }
            catch (Exception ex)
            {
                log.WriteEntry("Exception : " + ex.Message);
            }
            return null;
        }



        public decimal GetDefaultStationCatchmentArea(int stationId)
        {
            decimal catchmentarea = 0;
            var catchmentAreas = eventDataService.GetMonitoringStationCatchmentAreaByStationId(stationId).ToList();
            if (catchmentAreas.Count() > 0)
            {
                catchmentarea = catchmentAreas.FirstOrDefault().CatchmentArea;
            }
            //Else get the catchment area from the boundary
            else
            {
                var stationCatchmentArea = eventDataService.GetStationBoundaryArea(stationId);
                if (stationCatchmentArea != null)
                {
                    catchmentarea = (decimal)stationCatchmentArea / 10000;
                }


            }
            return catchmentarea;
        }

        public decimal GetDefaultStationTc(int stationId)
        {
            decimal tc = 0;
            var stationTCs = eventDataService.GetMonitoringStationTcsByStationId(stationId);
            if (stationTCs.Count() > 0)
            {
                tc = stationTCs.FirstOrDefault().TC;
            }
            return tc;
        }




    }
}
