﻿namespace Civica.BLL
{
    public interface ISender
    {
        void SendPhoneMessage(string[] recipients, string message, string ackMessage, int alarmId,
            int repeatedIndex);

        long makePhoneCall(string[] recipients, string message, int alarmId, int repeatedIndex);


        void SendMail(int alarmId, string recipient, string subject, string message, int repeatedIndex);
    }
}
