﻿namespace Civica.BLL.ServiceClasses
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Civica.Domain.CivicaDomain;
    using Civica.Domain;
    using Civica.Infrastructure.BaseClasses;

    public class AlarmDataService
    {
        public datacurrentEntities model;
        private IDatacurrrentEntitiesContext _model;

        public AlarmDataService() : this(new DatacurrrentEntitiesContext())
        {
            // model = new datacurrentEntities();
        }

        public AlarmDataService(IDatacurrrentEntitiesContext models)
        {
            _model = models;
            //model = new datacurrentEntities();
        }

        public List<MonitoringStationAlarmCondition> GetAllAlarmConidtions(int alarmId)
        {
            using (var tmpModel = _model)
            {
                var monitoringstationAlarmConditions = tmpModel.MonitoringStationAlarmConditions.Where(x => x.MonitoringStationAlarmId == alarmId).Where(x => x.Active == true).ToList();

                return monitoringstationAlarmConditions;
            }
        }

        public List<int> GetAllAlarmSensorDataType(int alarmId)
        {
            using (var tmpModel = _model)
            {
                var monitoringstationAlarmConditionIds = tmpModel.MonitoringStationAlarmConditions.Where(x => x.MonitoringStationAlarmId == alarmId).Where(x => x.Active == true).Select(x => x.Id).ToList();
                var sensorDataTypeIds = tmpModel.MonitoringSensorAlarms.Where(x => x.Active == true).Where(x => monitoringstationAlarmConditionIds.Contains(x.MonitoringStationAlarmConditionId)).Select(x => x.MonitoringSensor.SensorDataTypeId).ToList();

                return sensorDataTypeIds;
            }
        }

        public void UpdateAlarmUsePointSensorCalculateLastRun(int alarmId, bool usePointSensor)
        {
            using (var tmpModel = _model)
            {
                var alarm = tmpModel.MonitoringStationAlarms.Where(x => x.Id == alarmId).FirstOrDefault();
                alarm.UserPointSensorCalculateLastRun = usePointSensor;
                tmpModel.SaveChanges();

            }

        }

        public int AlarmConditionCounts(int alarmId)
        {
            using (var tmpModel = _model)
            {
                var monitoringStationAlarms = tmpModel.MonitoringStationAlarms.Where(x => x.Id == alarmId).FirstOrDefault();
                var count = monitoringStationAlarms.MonitoringStationAlarmConditions.Count;

                return count;
            }
        }

        public int GetAlarmFirstConditionTypeId(int alarmId)
        {
            using (var tmpModel = _model)
            {
                var monitoringStationAlarms = tmpModel.MonitoringStationAlarms.Where(x => x.Id == alarmId).FirstOrDefault();
                var typeId = monitoringStationAlarms.MonitoringStationAlarmConditions.FirstOrDefault().AlarmConditionTypeId.Value;

                return typeId;
            }
        }

        public List<MonitoringStationAlarm> GetAllAlarms()
        {
            using (var tmpModel = _model)
            {
                var monitoringStationAlarms = tmpModel.MonitoringStationAlarms.Where(x => x.Active == true).Where(x => x.TypeId == 1).ToList();

                return monitoringStationAlarms;
            }
        }

        public string getStationNameByStationId(int stationId)
        {

            using (var tmpModel = new datacurrentEntities())
            {
                var stationName = tmpModel.MonitoringStations.Where(x => x.Id == stationId).Where(x => x.Active == true).FirstOrDefault().Name;
                return stationName;
            }
        }

        public int getServiceIdByAlarmId(int alarmId)
        {
            using (var tmpModel = _model)
            {
                var monitoringStationAlarm = tmpModel.MonitoringStationAlarms.Where(x => x.Active == true).Where(x => x.Id == alarmId).FirstOrDefault();
                var serviceId = monitoringStationAlarm.MonitoringStation.MonitoringServiceStations.FirstOrDefault().MonitoringServiceId;
                return serviceId;
            }
        }

        public MonitoringStationAlarmCondition GetMonitoringStationAlarmConditionById(int monitoringstationalarmId)
        {
            using (var tmpModel = _model)
            {
                var monitoringStationAlarm = tmpModel.MonitoringStationAlarmConditions.Where(x => x.Id == monitoringstationalarmId).FirstOrDefault();

                return monitoringStationAlarm;
            }
        }

        public MonitoringStationAlarm GetMonitoringStationAlarmById(int alarmId)
        {
            using (var tmpModel = _model)
            {
                var monitoringStationAlarm = tmpModel.MonitoringStationAlarms.Where(x => x.Id == alarmId).FirstOrDefault();

                return monitoringStationAlarm;
            }
        }

        public string GetStationName(int alarmId)
        {
            using (var tmpModel = _model)
            {
                var stationName = tmpModel.MonitoringStationAlarms.Where(x => x.Id == alarmId).FirstOrDefault().MonitoringStation.Name;

                return stationName;
            }
        }

        public string serviceName(int alarmId)
        {
            using (var tmpModel = _model)
            {
                var serviceName = tmpModel.MonitoringStationAlarms.Where(x => x.Id == alarmId).FirstOrDefault().MonitoringStation.MonitoringServiceStations.FirstOrDefault().MonitoringService.Name;

                return serviceName;
            }
        }

        public string GetAlarmConditionTypeParameters(int conditionTypeId)
        {
            using (var tmpModel = _model)
            {
                var parameter = tmpModel.MonitoringStationAlarmConditionTypes.Where(x => x.Id == conditionTypeId).FirstOrDefault().Parameters;

                return parameter;
            }
        }

        public string GetAlarmConditionTypeLongDescription(int conditionTypeId)
        {
            using (var tmpModel = _model)
            {
                var description = tmpModel.MonitoringStationAlarmConditionTypes.Where(x => x.Id == conditionTypeId).FirstOrDefault().LongDescription;

                return description;
            }
        }
        public string stationAlarmSeverity(int alarmId)
        {
            using (var tmpModel = _model)
            {
                var severity = tmpModel.MonitoringStationAlarms.Where(x => x.Id == alarmId).FirstOrDefault().MonitoringStationAlarmSeverity.Type;

                return severity;
            }
        }
        public string getSensorName(int sensorId)
        {
            using (var tmpModel = new datacurrentEntities())
            {
                var sensorName = tmpModel.MonitoringSensors.Where(x => x.Id == sensorId).FirstOrDefault().Name;

                return sensorName;
            }
        }

        public List<MonitoringSensor> GetMonitoringConditionMonitoringSensors(int conditionId)
        {
            using (var tmpModel = _model)
            {
                var MonitoringSensors = tmpModel.MonitoringSensorAlarms.Where(x => x.MonitoringStationAlarmConditionId == conditionId).Where(x => x.Active == true).Select(x => x.MonitoringSensor).ToList();

                return MonitoringSensors;
            }
        }

        public List<int> GetMonitoringSensorIdsByAlarmId(int alarmId)
        {
            using (var tmpModel = _model)
            {
                var monitoringSensorIds = tmpModel.MonitoringSensorAlarms.Where(x => x.MonitoringStationAlarmCondition.MonitoringStationAlarmId == alarmId).Where(x => x.Active == true).Select(x => x.MonitoringSensorId).ToList();

                return monitoringSensorIds;
            }
        }

        public void UpdateLastRun(int alarmId, DateTimeOffset? lastRuntime)
        {
            using (var tmpModel =_model)
            {
                var alarm = tmpModel.MonitoringStationAlarms.Where(x => x.Id == alarmId).FirstOrDefault();
                alarm.LastRun = lastRuntime;
                alarm.LastRunCurrentTime = DateTimeOffset.Now;
                tmpModel.SaveChanges();
            }
        }

        public void UpdateAlarmSth(bool isTriggered, bool ackStatus, DateTimeOffset? triggeredTime, int alarmId)
        {
            using (var tmpModel = _model)
            {
                var alarm = tmpModel.MonitoringStationAlarms.Where(x => x.Id == alarmId).FirstOrDefault();
                alarm.IsTriggered = isTriggered;
                alarm.AckStatus = ackStatus;
                alarm.TriggeredTime = triggeredTime;
                tmpModel.SaveChanges();
            }

        }

        public int? getPhoneGroupIndex(int alarmId)
        {
            using (var tmpModel = _model)
            {
                var phoneGroupIndex = tmpModel.MonitoringStationAlarms.Where(x => x.Id == alarmId).FirstOrDefault().phoneNotificationIndex;

                return phoneGroupIndex;
            }

        }

        public void UpdatePhoneGroupIndex(int alarmId, int index)
        {
            using (var tmpModel = _model)
            {
                var phoneGroup = tmpModel.MonitoringStationAlarms.Where(x => x.Id == alarmId).FirstOrDefault();
                phoneGroup.phoneNotificationIndex = index;
                tmpModel.SaveChanges();
            }

        }

        public int? getPhoneTextGroupIndex(int alarmId)
        {
            using (var tmpModel = _model)
            {
                var phoneGroupTextIndex = tmpModel.MonitoringStationAlarms.Where(x => x.Id == alarmId).FirstOrDefault().phonetextNotificationIndex;

                return phoneGroupTextIndex;
            }
        }

        public void UpdatePhoneTextGroupIndex(int alarmId, int index)
        {
            using (var tmpModel = _model)
            {
                var phoneGroup = tmpModel.MonitoringStationAlarms.Where(x => x.Id == alarmId).FirstOrDefault();
                phoneGroup.phonetextNotificationIndex = index;
                tmpModel.SaveChanges();
            }

        }

        public int? geEmailGroupIndex(int alarmId)
        {
            using (var tmpModel = _model)
            {
                var emailGroupTextIndex = tmpModel.MonitoringStationAlarms.Where(x => x.Id == alarmId).FirstOrDefault().emailNotificationIndex;

                return emailGroupTextIndex; ;
            }
        }

        public void UpdateEmailGroupIndex(int alarmId, int index)
        {
            using (var tmpModel = _model)
            {
                var phoneGroup = tmpModel.MonitoringStationAlarms.Where(x => x.Id == alarmId).FirstOrDefault();
                phoneGroup.emailNotificationIndex = index;
                tmpModel.SaveChanges();
            }

        }

        public int? GetAlarmReaptedTimes(int alarmId)
        {
            using (var tmpModel = _model)
            {
                var alarm = tmpModel.MonitoringStationAlarms.Where(x => x.Id == alarmId).FirstOrDefault();

                return alarm.AlarmReaptedTimes;
            }

        }

        public void UpdateAlarmReaptedTimes(int alarmId, int repeatedTime)
        {
            using (var tmpModel = _model)
            {
                var alarm = tmpModel.MonitoringStationAlarms.Where(x => x.Id == alarmId).FirstOrDefault();
                alarm.AlarmReaptedTimes = repeatedTime;
                tmpModel.SaveChanges();

            }
        }

        public void UpdateSensorStatus(BaseEnum.AlarmTriggerType trigger, List<int> sensorIds)
        {
            using (var tmpModel = new datacurrentEntities())
            {
                if (sensorIds.Count == 0)
                    return;
                int status = 0;
                if (trigger == BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION)
                    status = 4;
                else if (trigger == BaseEnum.AlarmTriggerType.ALARMRES_OK)
                    status = 7;
                var sensors = tmpModel.MonitoringSensors.Where(x => sensorIds.Contains(x.Id));
                foreach (var sensor in sensors)
                {
                    sensor.SensorStatusId = status;
                }
                tmpModel.SaveChanges();

            }
        }

        public void insertintoLog(int alarmId, string description, string notes)
        {
            using (var tmpModel = _model)
            {
                MonitoringStationAlarmLog log = new MonitoringStationAlarmLog();
                log.AlarmId = alarmId;
                log.TimeStamp = DateTimeOffset.Now;
                log.description = description;
                log.note = notes;
                log.Active = true;
                tmpModel.MonitoringStationAlarmLogs.Add(log);
                tmpModel.SaveChanges();
            }
        }

        public void insertInfoIntoAlarmAckinfo(string guid, int alarmId, string emailAddress)
        {
            using (var tmpModel = _model)
            {
                AlarmAckinfo ackInfo = tmpModel.AlarmAckinfoes.Where(x => x.AlarmId == alarmId).Where(x => x.EmailAddress == emailAddress).FirstOrDefault();
                if (ackInfo == null)
                {
                    ackInfo = new AlarmAckinfo();
                    ackInfo.AlarmId = alarmId;
                    ackInfo.EmailAddress = emailAddress;

                }
                ackInfo.GuidId = guid;
                ackInfo.EmailSendTime = DateTimeOffset.Now;
                tmpModel.AlarmAckinfoes.Add(ackInfo);
                tmpModel.SaveChanges();
            }

        }

        public void UpdateLastRunCurrentTime(int alarmId)
        {
            using (var tmpModel = _model)
            {
                var alarm = tmpModel.MonitoringStationAlarms.Where(x => x.Id == alarmId).FirstOrDefault();
                alarm.LastRunCurrentTime = DateTimeOffset.Now;

                tmpModel.SaveChanges();
            }
        }

        public void UpdateNoDataNotificationToTrue(int alarmId)
        {
            using (var tmpModel = _model)
            {
                var alarm = tmpModel.MonitoringStationAlarms.Where(x => x.Id == alarmId).FirstOrDefault();
                alarm.NoDataNotification = true;

                tmpModel.SaveChanges();
            }
        }

        public void UpdateNoDataNotificationToFalse(int alarmId)
        {
            using (var tmpModel = _model)
            {
                var alarm = tmpModel.MonitoringStationAlarms.Where(x => x.Id == alarmId).FirstOrDefault();
                alarm.NoDataNotification = false;

                tmpModel.SaveChanges();

            }
        }
    }
}
