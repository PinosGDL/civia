﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.BLL.ServiceClasses.Interfaces;
using Civica.BLL.Models.AnalysisTools;
using Civica.Domain.CivicaDomain;
using System.Data.Entity;
using Civica.Infrastructure.BaseClasses;
using Civica.BLL.Models;
using System.Diagnostics;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;

namespace Civica.BLL.ServiceClasses
{
    public class StormEventDataServices
    {
        #region Fields
        private const double stormEndThresh = 3;          //threshold to inidicate the end of storm for statistic calculations
        private const int PRECIP_INTERVAL = 60; // 60min
        public datacurrentEntities model;
        private const int _intDefaultPrecip = 5 * 60;       //interval for precip in seconds
        private const int _intDefaultPrecipIIAnalysis = 60 * 60;       //interval for precip in seconds (FOR II ANALYSIS)
        private const double _stormEndThresh = 3;            //threshold to inidicate the end of storm for statistic calculations 
        private const int numAvgPt = 20;                    //number of points used to develop the daily flow pattern
        private const int timeRange = 10;                   //range of time before and after the current time to use for average (min)
        private const double bandtightness = 0.95;           //tightness of the band.  The smaller the value, the tighter
        private const int BANDFAIL = -999;
        private const double weightU = 100;                 //upper bound of weights for flow pattern calculations
        private const double weightL = 50;                  //lower bound of weights for flow pattern calculations
        private int[] _timesteps = new int[] { 5, 10, 15, 20, 30, 60, 120, 180, 240, 360, 720, 1440 };
        private const int dwfLengthMatchMultiple = 1;       //The length in period of Inter Dry period
        private const double dwfTolerance = 0.85;           //The value that calculates DWF outside of a storm that will be allowed to match measured flow.
        private const int LOG_CURVE_POINTS = 10;             //number of points in the logarithmic fitted curve
        private int[] RETURN_PERIODS = new int[] { 2, 5, 10, 25, 50, 100 }; // Standard IDF Return Periods
        private const int DefaultDryPeriod = 12;
        private const int DefaultMinStormSize = 15;
        #endregion

        #region Constructor

        public StormEventDataServices()
        {
            model = new datacurrentEntities();

        }
        #endregion


        public long getseconds(DateTime t1)
        {
            return Convert.ToInt64(Math.Round(t1.ToOADate() * 3600 * 24));
        }

        // <summary>
        // Return newtime which satisfies newtime= start+n*interval and it is as close as possible to the given time
        // </summary>
        // <param name="start">the starting point, in number of seconds</param>
        // <param name="interval">the interval between points, in number of seconds</param>
        // <param name="time">the time to be processed, in number of seconds</param>        
        public long idealizetime(long start, double interval, long time)
        {
            long diff;
            long multiple;
            long reminder;
            long d;

            if (interval == 0)
                return -1;

            diff = time - start;
            multiple = Convert.ToInt64(Math.Truncate((double)(diff / interval)));
            reminder = Convert.ToInt64(diff % interval);
            d = Convert.ToInt64(Math.Ceiling((double)(interval / 2)));

            if (reminder >= d)
                return Convert.ToInt64(start + (multiple + 1) * interval);
            else
                return Convert.ToInt64(start + multiple * interval);

        }

        public DateTime getdates(long s1)
        {
            double ds1 = s1;
            return DateTime.FromOADate(ds1 / (3600 * 24));
        }
        // <summary>
        // align precip points for storm separation and extract the relevent portion for display
        // </summary>
        // <param name="dt">input data to be aligned</param>
        // <param name="at">used to store aligned dates</param>
        // <param name="av">used to store aligned values</param>
        // <param name="interval">interval to group against, in seconds</param>
        // <param name="begint">only look at data after this time</param>
        // <param name="endt">only look at data before this time</param>  
        public void PrecipitationAlign(IEnumerable<MonitoringDataDTO> dt, ref List<DateTime> at, ref List<double> av, double interval, DateTime beginDate, DateTime endDate)
        {
            var beginInterval = idealizetime(getseconds(beginDate.Date), interval, getseconds(beginDate));
            var endInterval = idealizetime(getseconds(beginDate.Date), interval, getseconds(endDate));
            var numbin = (endInterval - beginInterval) / interval + 1;
            var enumerable = dt.GetEnumerator();
            enumerable.MoveNext();
            for (var i = 0; i < numbin; i++)
            {
                var currentIntervalDate = getdates((long)(beginInterval + i * interval));
                var value = 0d;

                while (enumerable.Current != null)
                {
                    var actualDate = enumerable.Current.TimeStamp;
                    var idealizedDate = getdates(idealizetime(getseconds(beginDate.Date), interval, getseconds(actualDate)));

                    if (idealizedDate > currentIntervalDate)
                        break;
                    else if (idealizedDate == currentIntervalDate)
                        value += Convert.ToDouble(enumerable.Current.Value);

                    ///I don't don't why this is done, but it was in the old code
                    if (value == -99)
                        value = 0;

                    enumerable.MoveNext();
                }

                at.Add(currentIntervalDate);
                av.Add(value);
            }

        }

        // <summary>
        // align precip points for storm separation and extract the relevent portion for display
        // </summary>
        // <param name="dt">input data to be aligned</param>
        // <param name="at">used to store aligned dates</param>
        // <param name="av">used to store aligned values</param>
        // <param name="interval">interval to group against, in seconds</param>
        // <param name="begint">only look at data after this time</param>
        // <param name="endt">only look at data before this time</param>  
        public void PrecipitationAlign(MonitoringDataDTO[] dt, ref List<DateTime> at, ref List<double> av, double interval, DateTime beginDate, DateTime endDate)
        {
            var beginInterval = idealizetime(getseconds(beginDate.Date), interval, getseconds(beginDate));
            var endInterval = idealizetime(getseconds(beginDate.Date), interval, getseconds(endDate));
            var numbin = (endInterval - beginInterval) / interval + 1;
            int j = 0;
            for (var i = 0; i < numbin; i++)
            {
                var currentIntervalDate = getdates((long)(beginInterval + i * interval));
                var value = 0d;

                while (j < dt.Length)
                {
                    var actualDate = dt[j].TimeStamp;
                    var idealizedDate = getdates(idealizetime(getseconds(beginDate.Date), interval, getseconds(actualDate)));

                    if (idealizedDate > currentIntervalDate)
                        break;
                    else if (idealizedDate == currentIntervalDate)
                        value += Convert.ToDouble(dt[j].Value);

                    ///I don't don't why this is done, but it was in the old code
                    if (value == -99)
                        value = 0;

                    j++;
                }

                at.Add(currentIntervalDate);
                av.Add(value);
            }

        }

        // <summary>
        // align precip points for storm separation and extract the relevent portion for display
        // </summary>
        // <param name="dt">input data to be aligned</param>
        // <param name="at">used to store aligned dates</param>
        // <param name="av">used to store aligned values</param>
        // <param name="interval">interval to group against, in seconds</param>
        // <param name="begint">only look at data after this time</param>
        // <param name="endt">only look at data before this time</param>  
        public void PrecipitationAlign(IEnumerable<SensorDataDTO> dt, ref List<DateTime> at, ref List<double> av, double interval, DateTime beginDate, DateTime endDate)
        {
            var beginInterval = idealizetime(getseconds(beginDate.Date), (long)interval, getseconds(beginDate));
            var endInterval = idealizetime(getseconds(beginDate.Date), (long)interval, getseconds(endDate));
            var numbin = (endInterval - beginInterval) / interval + 1;
            var enumerable = dt.GetEnumerator();
            enumerable.MoveNext();
            for (var i = 0; i < numbin; i++)
            {
                var currentIntervalDate = getdates((long)(beginInterval + i * interval));
                var value = 0d;

                while (enumerable.Current != null)
                {
                    var actualDate = enumerable.Current.timestamp;
                    var idealizedDate = getdates(idealizetime(getseconds(beginDate.Date), interval, getseconds(actualDate)));

                    if (idealizedDate > currentIntervalDate)
                        break;
                    else if (idealizedDate == currentIntervalDate)
                        value += Convert.ToDouble(enumerable.Current.value);

                    ///I don't don't why this is done, but it was in the old code
                    if (value == -99)
                        value = 0;

                    enumerable.MoveNext();
                }

                at.Add(currentIntervalDate);
                av.Add(value);
            }

        }

        #region Interface Methods
        /// <summary>
        /// Calculate Slope based on Mannings Equation for Scatter Report Tool
        /// </summary>
        /// <param name="flowDepthPoints"></param>
        /// <param name="flowDepthInterval"></param>
        /// <param name="slopeGradient"></param>
        /// <param name="frictionCoef"></param>
        /// <param name="PipeType"></param>
        /// <param name="dimensionOne"></param>
        /// <param name="dimensionTwo"></param>
        /// <param name="dimensionThree"></param>
        /// <param name="dimensionFour"></param>
        /// <returns></returns>
        public List<Point> ManningsCalculation(int flowDepthPoints, double flowDepthMax, int XsensorId, int YsensorId, double slopeGradient, double frictionCoef, int PipeType, double dimensionOne, double dimensionTwo = 0, double dimensionThree = 0, double dimensionFour = 0)
        {
            List<Point> points = new List<Point>();
            var xsensor = model.MonitoringSensors.Where(x => x.Id == XsensorId).FirstOrDefault();
            var sensorTypeId = xsensor.SensorTypeId;
            double flowDepthInterval = flowDepthMax / flowDepthPoints;
            if (sensorTypeId == 1)
            {
                Point currPoint;
                for (var i = 0; i < flowDepthPoints; i++)
                {
                    currPoint = new Point();

                    // CalculatedY = flowDepth[i] * slope + friction + random equation
                    currPoint.X = i * flowDepthInterval;
                    currPoint.Y = Math.Round(GetHydraulicVelocity(currPoint.X, slopeGradient, frictionCoef, PipeType, dimensionOne), 3); // 0.0 is a pipe radius
                    //currPoint.Y =  CalculatedY;
                    points.Add(currPoint);
                }

                return points;
            }
            else
            {
                Point currPoint;
                for (var i = 0; i < flowDepthPoints; i++)
                {
                    currPoint = new Point();

                    // CalculatedY = flowDepth[i] * slope + friction + random equation
                    currPoint.Y = flowDepthInterval * i;
                    currPoint.X = Math.Round(GetHydraulicVelocity(currPoint.Y, slopeGradient, frictionCoef, PipeType, dimensionOne), 5); // 0.0 is a pipe radius

                    //currPoint.Y =  CalculatedY;
                    points.Add(currPoint);
                }

                return points;

            }


        }

        /// <summary>
        /// Calculate Slope based on Mannings Equation for Scatter Report Tool
        /// </summary>
        /// <param name="chartSeries"></param>
        /// <param name="confInterval"></param>
        /// <returns></returns>
        public Dictionary<string, List<Point>> scatterConfBand(Point[] chartSeries, double confInterval)
        {
            Dictionary<string, List<Point>> series = new Dictionary<string, List<Point>>();
            List<Point> Series = new List<Point>();
            List<Point> RangeTop = new List<Point>();
            List<Point> RangeBottom = new List<Point>();

            for (int i = 0; i < chartSeries.Length; i++)
            {
                Series.Add(new Point(chartSeries[i].X, chartSeries[i].Y));
                RangeTop.Add(new Point(chartSeries[i].X, chartSeries[i].Y * (1.0000 + confInterval)));
                RangeBottom.Add(new Point(chartSeries[i].X, chartSeries[i].Y * (1.0000 - confInterval)));
            }
            series["Series"] = Series;
            series.Add("RangeTop", RangeTop);
            series.Add("RangeBottom", RangeBottom);
            return series;

        }

        /// <summary>
        /// Storm Seperate only used for Rain fall tool for City of Toronto
        /// </summary>
        /// <param name="sensorData"></param>
        /// <param name="interDryPeriod"></param>
        /// <param name="stormSize"></param>
        /// <returns></returns>
        public List<StormEvent> SeperateEvents(List<MonitoringDataDTO> sensorData, double interDryPeriod, double stormSize)
        {
            var events = new List<StormEvent>();
            var arrayTimestamps = new List<DateTime>();
            var arrayValues = new List<double>();
            var lstStorm = new List<DateTime>[2];

            var tmplstStorm = new List<DateTime>[2];
            tmplstStorm[0] = new List<DateTime>();
            tmplstStorm[1] = new List<DateTime>();
            try
            {
                if (sensorData.Count > 0)
                {
                    //var timeZoneOffset = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time").BaseUtcOffset;
                    PrecipitationAlign(sensorData.ToArray(), ref arrayTimestamps, ref arrayValues, _intDefaultPrecip, sensorData.First().TimeStamp, sensorData.Last().TimeStamp);

                    lstStorm = StormSeparateToronto(arrayTimestamps, arrayValues, interDryPeriod);
                    var stormListArray = new DateTime[lstStorm.Length][];
                    for (var q = 0; q < lstStorm.Length; q++)
                    {
                        stormListArray[q] = lstStorm[q].ToArray();
                    }
                    var lastCount = 0;
                    var numRow = arrayTimestamps.Count;
                    var eventCount = 0;
                    var i = 0;

                    //if (flowData.Count() == 0)
                    //{
                    //    return new List<StormEvent>();
                    //}
                    var arrayTimestampsArray = arrayTimestamps.ToArray();
                    var arrayValuesArray = arrayValues.ToArray();

                    int k = i;
                    while (k < stormListArray[0].Length)
                    {
                        var total = 0d;
                        var start = stormListArray[0][k];
                        var end = stormListArray[1][k];

                        for (int j = lastCount; j < numRow; j++)
                        {
                            if ((arrayTimestampsArray[j] >= start) && (arrayTimestampsArray[j] <= end))
                            {
                                total += arrayValuesArray[j];
                                lastCount = j;
                            }
                            else if (arrayTimestampsArray[j] > end)
                                break;
                        }

                        if (total >= stormSize)
                        {
                            total = Math.Round(total, 0);

                            /*---------- New storm class ------------*/
                            var storm = new StormEvent();
                            storm.Name = start.ToString("MMM d, yyyy");
                            storm.TotalPrecipitation = Convert.ToDecimal(total);
                            storm.StartDate = start;
                            storm.EndDate = end;
                            /*---------- New storm class END------------*/

                            eventCount++;
                            events.Add(storm);
                            tmplstStorm[0].Add(stormListArray[0][k]);
                            tmplstStorm[1].Add(stormListArray[1][k]);
                            i++;
                            k++;
                        }
                        else
                        {
                            //lstStorm[0].RemoveAt(i);
                            //lstStorm[1].RemoveAt(i);
                            //remove = true;
                            k = k + 1;
                        }
                        //if (!remove)
                        //{
                        //    tmplstStorm[0].Add(stormListArray[0][k]);
                        //    tmplstStorm[1].Add(stormListArray[1][k]);
                        //}

                    }
                }
            }
            catch (Exception ex)
            {
                Debug.Assert(false, ex.ToString());
            }

            return events;
        }

        public List<StormEvent> SeperateEvents(int sensorId, double interDryPeriod, double stormSize, DateTime? fromDate, DateTime? toDate)
        {

            var lstStorm = new List<DateTime>[2];
            return SeperateEvents(sensorId, interDryPeriod, stormSize, fromDate, toDate, out lstStorm);

        }


        /// <summary>
        /// Get the conversion factor for the fromUnit to the toUnit
        /// </summary>
        /// <param name="fromUnitId"></param>
        /// <param name="toUnitId"></param>
        /// <returns></returns>
        private decimal GetConversionFactor(int fromUnitId, int toUnitId)
        {
            var conversionFactor = model.MonitoringUnitConversions.Where(x => x.FromUnit == fromUnitId && x.ToUnit == toUnitId).Select(x => x.ConversionFactor).FirstOrDefault();
            //If the converstion factor isnt found, set it to 1.
            if (conversionFactor == 0)
            {
                var typeId = model.Units.Where(x => x.Id == fromUnitId).FirstOrDefault().TypeId;
                int standardDCUnit = 0;
                //Based on the typeId, get the conversion factor to the datacurrent standard unit
                if (typeId == (int)Civica.Infrastructure.BaseClasses.BaseEnum.UnitType.Area)
                    standardDCUnit = (int)Civica.Infrastructure.BaseClasses.BaseEnum.DCStandardUnitId.Area;
                else if (typeId == (int)Civica.Infrastructure.BaseClasses.BaseEnum.UnitType.Depth)
                    standardDCUnit = (int)Civica.Infrastructure.BaseClasses.BaseEnum.DCStandardUnitId.Depth;
                else if (typeId == (int)Civica.Infrastructure.BaseClasses.BaseEnum.UnitType.FlowRate)
                    standardDCUnit = (int)Civica.Infrastructure.BaseClasses.BaseEnum.DCStandardUnitId.FlowRate;
                else if (typeId == (int)Civica.Infrastructure.BaseClasses.BaseEnum.UnitType.Temperature)
                    standardDCUnit = (int)Civica.Infrastructure.BaseClasses.BaseEnum.DCStandardUnitId.Temperature;
                else if (typeId == (int)Civica.Infrastructure.BaseClasses.BaseEnum.UnitType.Velocity)
                    standardDCUnit = (int)Civica.Infrastructure.BaseClasses.BaseEnum.DCStandardUnitId.Velocity;
                else if (typeId == (int)Civica.Infrastructure.BaseClasses.BaseEnum.UnitType.Voltage)
                    standardDCUnit = (int)Civica.Infrastructure.BaseClasses.BaseEnum.DCStandardUnitId.Voltage;
                else if (typeId == (int)Civica.Infrastructure.BaseClasses.BaseEnum.UnitType.Volume)
                    standardDCUnit = (int)Civica.Infrastructure.BaseClasses.BaseEnum.DCStandardUnitId.Volume;

                conversionFactor = model.MonitoringUnitConversions.Where(x => x.FromUnit == fromUnitId && x.ToUnit == standardDCUnit).Select(x => x.ConversionFactor).FirstOrDefault();
                conversionFactor = conversionFactor * model.MonitoringUnitConversions.Where(x => x.FromUnit == standardDCUnit && x.ToUnit == toUnitId).Select(x => x.ConversionFactor).FirstOrDefault();
                if (conversionFactor == 0)
                    conversionFactor = 1;
            }

            return conversionFactor;
        }
        /// <summary>
        /// Used to seperate events
        /// </summary>
        /// <param name="sensorId"></param>
        /// <param name="interDryPeriod"></param>
        /// <param name="stormSize"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        public List<StormEvent> SeperateEvents(int sensorId, double interDryPeriod, double stormSize, DateTime? fromDate, DateTime? toDate, out List<DateTime>[] outStormList,
            bool forStormSep = false, int flowId = 0)
        {
            var events = new List<StormEvent>();
            var arrayTimestamps = new List<DateTime>();
            var arrayValues = new List<double>();
            var lstStorm = new List<DateTime>[2];

            outStormList = lstStorm;
            var tmplstStorm = new List<DateTime>[2];
            tmplstStorm[0] = new List<DateTime>();
            tmplstStorm[1] = new List<DateTime>();
            try
            {
                var sensor = model.MonitoringSensors.Include("MonitoringStation.TimeZone").Where(item => item.Id == sensorId).FirstOrDefault();
                var timeZoneOffset = TimeZoneInfo.FindSystemTimeZoneById(sensor.MonitoringStation.TimeZone.TZICode).BaseUtcOffset;
                var fromDateOffset = fromDate == null ? DateTimeOffset.MinValue : SetDateTimeToDateTimeOffset(fromDate.Value, timeZoneOffset);
                var toDateOffset = toDate == null ? DateTimeOffset.MaxValue : SetDateTimeToDateTimeOffset(toDate.Value, timeZoneOffset);
                var sensorData = model.MonitoringDatas.AsNoTracking().Where(x => x.MonitoringSensorId == sensorId && x.TimeStamp >= fromDateOffset && x.TimeStamp <= toDateOffset).Where(x => x.Value != 0).OrderBy(item => item.TimeStamp).ToList();

                if (sensor.UnitId != null)
                {
                    decimal factor = GetConversionFactor(sensor.UnitId.Value, (int)Civica.Infrastructure.BaseClasses.BaseEnum.DCStandardUnitId.PrecipitationDepth);
                    sensorData = sensorData.Select(x => { x.Value = (x.Value * factor); return x; }).ToList();
                }
                else
                {
                    int unitId = model.Units.Where(x => x.UnitSymbol.Equals(sensor.Unit)).FirstOrDefault().Id;
                    if (unitId != 0)
                    {
                        decimal factor = GetConversionFactor(unitId, (int)Civica.Infrastructure.BaseClasses.BaseEnum.DCStandardUnitId.PrecipitationDepth);
                        sensorData = sensorData.Select(x => { x.Value = (x.Value * factor); return x; }).ToList();
                    }
                }

                if (sensorData.Count > 0)
                {
                    if (forStormSep)
                        PrecipitationAlign(sensorData, ref arrayTimestamps, ref arrayValues, _intDefaultPrecipIIAnalysis, sensorData.First().TimeStamp.DateTime, sensorData.Last().TimeStamp.DateTime, timeZoneOffset);
                    else
                        PrecipitationAlign(sensorData, ref arrayTimestamps, ref arrayValues, _intDefaultPrecip, sensorData.First().TimeStamp.DateTime, sensorData.Last().TimeStamp.DateTime, timeZoneOffset);

                    lstStorm = StormSeparate(arrayTimestamps, arrayValues, interDryPeriod);
                    var stormListArray = new DateTime[lstStorm.Length][];
                    for (var q = 0; q < lstStorm.Length; q++)
                    {
                        stormListArray[q] = lstStorm[q].ToArray();
                    }
                    var lastCount = 0;
                    var numRow = arrayTimestamps.Count;
                    var eventCount = 0;
                    var i = 0;

                    var flowData = model.MonitoringDatas.Where(x => x.MonitoringSensorId == flowId).OrderBy(x => x.TimeStamp).ToList();
                    //if (flowData.Count() == 0)
                    //{
                    //    return new List<StormEvent>();
                    //}
                    var arrayTimestampsArray = arrayTimestamps.ToArray();
                    var arrayValuesArray = arrayValues.ToArray();

                    int k = i;
                    while (k < stormListArray[0].Length)
                    {
                        //Check if there is flow Data.
                        //bool remove = false;
                        if (forStormSep)
                        {
                            DateTimeOffset stormFromDate = SetDateTimeToDateTimeOffset(stormListArray[0][k], timeZoneOffset);
                            DateTimeOffset stormToDate = SetDateTimeToDateTimeOffset(stormListArray[1][k], timeZoneOffset);
                            if (flowData.Count == 0)
                            {
                                //lstStorm[0].RemoveAt(i);
                                //lstStorm[1].RemoveAt(i);
                                //remove = true;
                                k = k + 1;
                                continue;
                            }
                            else
                            {
                                var flowDataFirst = flowData.First();
                                var flowDataLast = flowData.Last();
                                if (flowDataFirst.TimeStamp > stormToDate || stormFromDate > flowDataLast.TimeStamp)
                                {
                                    //lstStorm[0].RemoveAt(i);
                                    //lstStorm[1].RemoveAt(i);
                                    //remove = true;
                                    k = k + 1;
                                    continue;
                                }
                            }


                        }
                        var total = 0d;
                        var start = stormListArray[0][k];
                        var end = stormListArray[1][k];

                        for (int j = lastCount; j < numRow; j++)
                        {
                            if ((arrayTimestampsArray[j] >= start) && (arrayTimestampsArray[j] <= end))
                            {
                                total += arrayValuesArray[j];
                                lastCount = j;
                            }
                            else if (arrayTimestampsArray[j] > end)
                                break;
                        }

                        if (total >= stormSize)
                        {
                            total = Math.Round(total, 0);

                            /*---------- New storm class ------------*/
                            var storm = new StormEvent();
                            storm.Name = start.ToString("MMM d, yyyy");
                            storm.TotalPrecipitation = Convert.ToDecimal(total);
                            storm.StartDate = start;
                            storm.EndDate = end;
                            /*---------- New storm class END------------*/

                            eventCount++;
                            events.Add(storm);
                            tmplstStorm[0].Add(stormListArray[0][k]);
                            tmplstStorm[1].Add(stormListArray[1][k]);
                            i++;
                            k++;
                        }
                        else
                        {
                            //lstStorm[0].RemoveAt(i);
                            //lstStorm[1].RemoveAt(i);
                            //remove = true;
                            k = k + 1;
                        }
                        //if (!remove)
                        //{
                        //    tmplstStorm[0].Add(stormListArray[0][k]);
                        //    tmplstStorm[1].Add(stormListArray[1][k]);
                        //}

                    }
                }
                outStormList = tmplstStorm;
            }
            catch (Exception ex)
            {
                Debug.Assert(false, ex.ToString());
            }

            return events;
        }

        /// <summary>
        /// Get's the IDF Curve for an idf id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Dictionary<int, Dictionary<int, decimal?>> GetIDFCurves(int id, int intensityUnitId = 0)
        {
            var designStorm = model.DesignStorms.Where(x => x.Id == id).FirstOrDefault();
            var dictionary = new Dictionary<int, Dictionary<int, decimal?>>();
            decimal factor = 1;
            if (designStorm != null)
            {
                //If the design storm has a unit associated with it. Get the factor and multiply it through the values
                if (designStorm.UnitId != null && intensityUnitId != 0)
                {
                    factor = GetConversionFactor(designStorm.Unit.UnitType.Id, intensityUnitId);
                }
                foreach (var idf in designStorm.DesignStormEquations)
                {
                    if (!dictionary.ContainsKey(idf.DesignStormReturnInterval.Value))
                    {
                        dictionary.Add(idf.DesignStormReturnInterval.Value, new Dictionary<int, decimal?>());
                        foreach (var timestep in _timesteps)
                            dictionary[idf.DesignStormReturnInterval.Value].Add(timestep, null);
                    }

                    dictionary[idf.DesignStormReturnInterval.Value][idf.DesignStormDuration.Value] = idf.A * factor;
                }
            }
            return dictionary;
        }

        /// <summary>
        /// Get's the event curve
        /// </summary>
        /// <param name="sensorId"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        public IEnumerable<KeyValuePair<int, decimal?>> GetEventCurve(int sensorId, DateTime fromDate, DateTime toDate, int rainIntensityUnitId)
        {
            var sensor = model.MonitoringSensors.Include("MonitoringStation.TimeZone").Where(item => item.Id == sensorId).FirstOrDefault();
            var offset = TimeZoneInfo.FindSystemTimeZoneById(sensor.MonitoringStation.TimeZone.TZICode).BaseUtcOffset;
            var fromDateOffset = SetDateTimeToDateTimeOffset(fromDate, offset);
            var toDateOffset = SetDateTimeToDateTimeOffset(toDate, offset);
            var sensorData = model.MonitoringDatas.Where(x => x.MonitoringSensorId == sensorId && x.TimeStamp >= fromDateOffset && x.TimeStamp <= toDateOffset).OrderBy(item => item.TimeStamp).ToList();

            if (sensor.UnitId != null)
            {
                sensorData = sensorData.Select(x => { x.Value = (x.Value * GetConversionFactor(sensor.UnitId.Value, (int)Civica.Infrastructure.BaseClasses.BaseEnum.DCStandardUnitId.PrecipitationDepth)); return x; }).ToList();
            }
            else
            {
                int unitId = model.Units.Where(x => x.UnitSymbol.Equals(sensor.Unit)).FirstOrDefault().Id;
                if (unitId != 0)
                {
                    sensorData = sensorData.Select(x => { x.Value = (x.Value * GetConversionFactor(unitId, (int)Civica.Infrastructure.BaseClasses.BaseEnum.DCStandardUnitId.PrecipitationDepth)); return x; }).ToList();
                }
            }

            //Get the factor to multiply by
            var factor = GetConversionFactor((int)Civica.Infrastructure.BaseClasses.BaseEnum.DCStandardUnitId.RainfallIntensity, rainIntensityUnitId);
            var returnValue = new List<KeyValuePair<int, decimal?>>();
            var minTimeStep = CalculateTimestep(sensorData);
            var curve = from timestep in _timesteps
                        select new KeyValuePair<int, decimal?>(timestep, timestep >= minTimeStep / 60 ? new Nullable<decimal>(Convert.ToDecimal(SlidingWindowPeakIntensity(sensorData, timestep)) * factor) : null);

            return curve;
        }

        /// <summary>
        /// Get's the design storms for a sensor
        /// </summary>
        /// <param name="sensorId"></param>
        /// <returns></returns>
        public IEnumerable<DesignStormDTO> GetDesignStorms(int sensorId)
        {
            var query = model.DesignStorms.Include(x => x.MonitoringSensorDesignStorms).Where(item => item.MonitoringSensorDesignStorms.Where(x => x.MonitoringSensorId == sensorId).Count() > 0).ToList().Select(x => DesignStormDTO.Create(x));
            return query;
        }

        /// <summary>
        /// Get's the design storms for a sensor
        /// </summary>
        /// <param name="sensorId"></param>
        /// <returns></returns>
        public IEnumerable<DesignStormDTO> GetDesignStormsByOrganizationId(int id)
        {
            var query = model.DesignStorms.Include(x => x.MonitoringSensorDesignStorms).Where(item => item.OrganizationID == id).ToList().Select(x => DesignStormDTO.Create(x));
            return query;
        }


        #endregion

        #region Date time functions

        /// <summary>
        /// Converts a datetime object to a datetimeoffset object
        /// </summary>
        /// <param name="dateTime"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        private static DateTimeOffset SetDateTimeToDateTimeOffset(DateTime dateTime, TimeSpan offset)
        {
            dateTime = DateTime.SpecifyKind(dateTime, DateTimeKind.Unspecified);
            return new DateTimeOffset(dateTime, offset);
        }

        /// <summary>
        /// Get's the seconds in a date
        /// </summary>
        /// <param name="t1"></param>
        /// <returns></returns>
        private long ConvertDateToSeconds(DateTime t1)
        {
            return Convert.ToInt64(Math.Round(t1.ToOADate() * 3600 * 24));
        }

        /// <summary>
        /// Converts seconds into a date
        /// </summary>
        /// <param name="s1"></param>
        /// <returns></returns>
        private DateTime ConvertSecondsToDate(long s1)
        {
            double ds1 = s1;
            return DateTime.FromOADate(ds1 / (3600 * 24));
        }

        // <summary>
        // Return newtime which satisfies newtime= start+n*interval and it is as close as possible to the given time
        // </summary>
        // <param name="start">the starting point, in number of seconds</param>
        // <param name="interval">the interval between points, in number of seconds</param>
        // <param name="time">the time to be processed, in number of seconds</param>        
        public long IdealizeTime(long start, double interval, long time)
        {
            long diff;
            long multiple;
            long reminder;
            long d;

            if (interval == 0)
                return -1;

            diff = time - start;
            multiple = Convert.ToInt64(Math.Truncate((double)(diff / interval)));
            reminder = Convert.ToInt64(diff % interval);
            d = Convert.ToInt64(Math.Ceiling((double)(interval / 2)));

            if (reminder >= d)
                return Convert.ToInt64(start + (multiple + 1) * interval);
            else
                return Convert.ToInt64(start + multiple * interval);

        }

        #endregion

        #region Calculation Helpers
        /// <summary>
        /// Calculate Hydraulic Velocity.
        /// </summary>
        /// <param name="flowDepth"></param>
        /// <param name="slopeGradient"></param>
        /// <param name="frictionCoef"></param>
        /// <param name="PipeType"></param>
        /// <param name="dimensionOne"></param>
        /// <param name="dimensionTwo"></param>
        /// <param name="dimensionThree"></param>
        /// <param name="dimensionFour"></param>
        /// <param name="systemOfMeasurement"></param>
        /// <returns> Hydrologic velocity</returns>
        private double GetHydraulicVelocity(double flowDepth, double slopeGradient, double frictionCoef, int PipeType, double dimensionOne, double dimensionTwo = 0, double dimensionThree = 0, double dimensionFour = 0, string systemOfMeasurement = "SI")
        {
            // Set variables
            double convFact;
            double hydroRad = 0.0;
            // Check pipetype and call correct hydraulic radius function
            switch (PipeType)
            {
                case (int)Civica.Infrastructure.BaseClasses.BaseEnum.PipeType.Circular:
                    hydroRad = GetCircularHydraulicRadius(flowDepth, dimensionOne / 2000);
                    break;

                case (int)Civica.Infrastructure.BaseClasses.BaseEnum.PipeType.Rectangular:
                    // Do rectangle stuff
                    // Get rectangle Hydraulic velocity
                    break;

                case (int)Civica.Infrastructure.BaseClasses.BaseEnum.PipeType.Triangular:
                    //Dop rectangle stuff
                    // get rectangle hydrolic velocity
                    break;
            }
            // Check imperial or metric and set 'K'
            switch (systemOfMeasurement)
            {
                case "SI":
                    {
                        convFact = 1.0;
                        break;
                    }
                default:
                    {
                        convFact = 1.49;
                        break;
                    }
            }
            //Apply and return hydralic velocity equation
            return convFact * Math.Pow(hydroRad, 2.0 / 3) * Math.Sqrt(slopeGradient) / frictionCoef;
        }


        /// <summary>
        /// Calculate Hydraulic Radius of a circular pipe.
        /// </summary>
        /// <param name="flowDepth"></param>
        /// <param name="pipeRadius"></param>
        /// <returns> Hydrologic radius</returns>
        private double GetCircularHydraulicRadius(double flowDepth, double pipeRadius)
        {
            // Setting up variable conditions
            double theta, circularArea, segmentHeight, flowArea, wettedPerimeter;
            //Setting up segment height and pipe radius
            if (flowDepth < pipeRadius)
            {
                segmentHeight = flowDepth;
                theta = 2.0 * Math.Acos((pipeRadius - segmentHeight) / pipeRadius);
            }
            else
            {
                segmentHeight = 2.0 * pipeRadius - flowDepth;
                theta = 2.0 * Math.Acos((pipeRadius - segmentHeight) / pipeRadius);
            }
            // Computes circularArea based on differences
            circularArea = 0.5 * Math.Pow(pipeRadius, 2.0) * (theta - Math.Sin(theta));

            // Computes and outputs Hydraulic Radius
            if (flowDepth < pipeRadius)
            {
                // Flow area = Circular area in this instance
                wettedPerimeter = pipeRadius * theta;
                if (wettedPerimeter == 0)
                    return 0;
                else
                    return circularArea / wettedPerimeter;

            }
            else
            {
                flowArea = Math.PI * Math.Pow(pipeRadius, 2.0) - circularArea;
                wettedPerimeter = 2.0 * Math.PI * pipeRadius - pipeRadius * theta;
                return flowArea / wettedPerimeter;
            }
        }

        /// <summary>
        /// Add Prediction Pt.
        /// </summary>
        /// <param name="s"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        private void AddPredictionPt(ref List<SeriesPoints> s, decimal x, decimal y)
        {
            //s = new List<SeriesPoints>();
            s.Add(new SeriesPoints(0, y));
            s.Add(new SeriesPoints(x, y));
            s.Add(new SeriesPoints(x, 0));
        }

        /// <summary>
        /// For each provided design storm, find int erpolated/extrapolated value at Tc if it does 
        /// not already exist (in refValues)
        /// </summary>
        /// <param name="DesignStorm">Given design storm: Key - return timestep</param>
        /// <param name="currTc"></param>
        /// <returns>interpolated/extrapolated peak precipitation intensity (mm/Tc)</returns>
        /// <remarks>Interpolation/Extrapolation is linear between the two closest points to Tc</remarks>
        private decimal InterpolateDesignStorm(IDictionary<int, decimal?> DesignStorm, decimal currTc)
        {
            decimal value = 0;
            int i = 0;
            decimal upperValue = 0;
            decimal lowerValue = 0;
            int upperTS = 0;
            int lowerTS = 0;

            if (DesignStorm.Count == 1 || DesignStorm.Count == 1)
            {
                Console.Write("Design storm do not have sufficient data to calculate predicted value");
                return 0;
                //throw new AxisException("Design storm do not have sufficient data to calculate predicted value");
            }

            int[] timesteps = new int[DesignStorm.Count];
            int count = 0;
            foreach (int ts in DesignStorm.Keys)
            {
                timesteps[count] = ts;
                count += 1;
            }
            Array.Sort(timesteps);

            // clear existing value first
            for (i = 0; i <= timesteps.Length - 1; i++)
            {
                if (currTc < timesteps[i] || i == timesteps.Length - 1)
                {
                    if (i == 0)
                    {
                        upperTS = timesteps[i + 1];
                        lowerTS = timesteps[i];
                    }
                    else
                    {
                        upperTS = timesteps[i];
                        lowerTS = timesteps[i - 1];
                    }

                    upperValue = DesignStorm[upperTS].Value;
                    lowerValue = DesignStorm[lowerTS].Value;

                    // interpolate/extrapolate between two values, assuming striaght line
                    value = ((upperValue - lowerValue) / ((decimal)upperTS - (decimal)lowerTS) * ((decimal)currTc - (decimal)lowerTS)) + lowerValue;
                    break; // TODO: might not be correct. Was : Exit For
                }
            }

            // make sure that the value won't be negative
            return Math.Max(value, 0);
        }

        // <summary>
        // align precip points for storm separation and extract the relevent portion for display
        // </summary>
        // <param name="dt">input data to be aligned</param>
        // <param name="at">used to store aligned dates</param>
        // <param name="av">used to store aligned values</param>
        // <param name="interval">interval to group against, in seconds</param>
        // <param name="begint">only look at data after this time</param>
        // <param name="endt">only look at data before this time</param>        
        private void PrecipitationAlign(IEnumerable<MonitoringData> dt, ref List<DateTime> at, ref List<double> av, double interval, DateTime beginDate, DateTime endDate, TimeSpan offset)
        {
            var beginInterval = IdealizeTime(ConvertDateToSeconds(beginDate.Date), interval, ConvertDateToSeconds(beginDate));
            var endInterval = IdealizeTime(ConvertDateToSeconds(beginDate.Date), interval, ConvertDateToSeconds(endDate));
            var numbin = ((endInterval - beginInterval) / interval) + 1;
            var enumerable = dt.GetEnumerator();
            enumerable.MoveNext();
            for (var i = 0; i < numbin; i++)
            {
                var currentIntervalDate = ConvertSecondsToDate((long)(beginInterval + i * interval));
                var value = 0d;

                while (enumerable.Current != null)
                {

                    var actualDate = enumerable.Current.TimeStamp.ToOffset(offset).DateTime;
                    var idealizedDate = ConvertSecondsToDate(IdealizeTime(ConvertDateToSeconds(beginDate.Date), interval, ConvertDateToSeconds(actualDate)));

                    if (idealizedDate > currentIntervalDate)
                        break;
                    else if (idealizedDate == currentIntervalDate)
                        value += Convert.ToDouble(enumerable.Current.Value);

                    ///I don't don't why this is done, but it was in the old code
                    if (value == -99)
                        value = 0;

                    enumerable.MoveNext();
                }

                at.Add(currentIntervalDate);
                av.Add(value);
            }
        }

        /// <summary>
        /// Find the peak intensity over given timesteps of a storm event using a sliding frame
        /// </summary>
        /// <param name="data">precipitation data of a storm event</param>
        /// <param name="timestep">in min</param>
        /// <returns>peak intensity value (mm/h)</returns>
        /// <remarks></remarks>
        private double SlidingWindowPeakIntensity(List<MonitoringData> data, double timestep)
        {
            var queue = new Queue<MonitoringData>();
            var total = 0d;
            var max = 0d;
            foreach (var row in data.OrderBy(item => item.TimeStamp))
            {
                while (queue.Count > 0)
                {
                    var timeDiff = row.TimeStamp - queue.Peek().TimeStamp;
                    if (Convert.ToInt32(Math.Truncate(timeDiff.TotalMinutes)) < timestep)
                        break;

                    total -= Convert.ToDouble(queue.Dequeue().Value);
                }

                queue.Enqueue(row);
                total += Convert.ToDouble(row.Value);
                max = Math.Max(max, total);
            }
            return (max / timestep * 60);
        }

        /// <summary>
        /// Find the peak intensity over given timesteps of a storm event using a sliding frame
        /// </summary>
        /// <param name="data">precipitation data of a storm event</param>
        /// <param name="timestep">in min</param>
        /// <returns>peak intensity value (mm/h)</returns>
        /// <remarks></remarks>
        private double SlidingWindowPeakIntensity(List<MonitoringDataDTO> data, double timestep)
        {
            var queue = new Queue<MonitoringDataDTO>();
            var total = 0d;
            var max = 0d;
            foreach (var row in data.OrderBy(item => item.TimeStamp))
            {
                while (queue.Count > 0)
                {
                    var timeDiff = row.TimeStamp - queue.Peek().TimeStamp;
                    if (Convert.ToInt32(Math.Truncate(timeDiff.TotalMinutes)) < timestep)
                        break;

                    total -= Convert.ToDouble(queue.Dequeue().Value);
                }

                queue.Enqueue(row);
                total += Convert.ToDouble(row.Value);
                max = Math.Max(max, total);
            }
            return (max / timestep * 60);
        }

        /// <summary>
        /// Because of the distructive nature incorrect results gathered from this tool can generate
        /// check the data to make sure that the two closest points are less than 5 minutes apart
        /// if this is not the case, return a double representing the time interval to display 
        /// 
        /// </summary>
        /// <param name="dt"></param>
        /// <returns>
        /// difference value in seconds
        /// </returns>
        /// <remarks></remarks>
        private double CalculateTimestep(List<MonitoringData> data)
        {
            double result = 99999;
            for (var stepCount = 1; stepCount <= data.Count - 1; stepCount++)
            {
                result = Math.Min(result, (data[stepCount].TimeStamp - data[stepCount - 1].TimeStamp).TotalSeconds);
                if (result <= 300)
                    break;

            }

            return result;
        }




        // <summary>
        // separate storm events
        // </summary>
        // <param name="at">precip time</param>
        // <param name="av">precip value (mm/hr)</param>
        // <param name="interDryPeriod">Inter-event dry period to use (hours)</param>
        // <returns>A 2-element array of lists (start and end times)</returns>       
        public List<DateTime>[] StormSeparateToronto(List<DateTime> at, List<double> av, double interDryPeriod)
        {
            List<DateTime>[] stormlist = new List<DateTime>[2];
            stormlist[0] = new List<DateTime>();
            stormlist[1] = new List<DateTime>();
            bool storm = false;
            DateTime stormEnd = at[0];
            DateTime currTime;

            //No data
            int totalRows = at.Count;
            if (totalRows == 0)
            {
                return stormlist;
            }
            var atEnumerator = at.GetEnumerator();
            var avEnumerator = av.GetEnumerator();
            int k = 0;
            //Seperation
            while (atEnumerator.MoveNext() && avEnumerator.MoveNext())
            {
                k++;
                currTime = atEnumerator.Current;
                //separate all the storm events
                if (avEnumerator.Current != 0)
                {
                    if (storm)
                    {
                        if (currTime.Subtract(stormEnd).TotalHours > interDryPeriod)    //if a new storm
                        {
                            /*--------- Jason made this change, since the previous code will cause the inaccurate start and end of the EVENT -------*/
                            stormlist[1].Add(stormEnd.AddHours(1)); // Ahmad Made this change to match dates from V1 datacurrent (Removed .AddHours(1))
                            //   stormCount += 1;
                            stormlist[0].Add(currTime.AddHours(1)); // Ahmad Made this change to match dates from V1 datacurrent
                            /*--------- Jason made this change, since the previous code will cause the inaccurate start and end of the EVENT END-------*/

                            //stormlist[1].Insert(stormCount, _IIFunctions.lowerBound_minute(stormEnd, 60).AddHours(1));
                            //stormCount += 1;
                            //stormlist[0].Insert(stormCount, _IIFunctions.lowerBound_minute(currTime, 60));
                            //else a continuous storm, do nothing
                        }
                    }
                    else //no storm yet, this is the first storm within the period
                    {
                        stormlist[0].Add(currTime);
                        storm = true;
                    }
                    stormEnd = currTime;
                }
                if ((k == totalRows - 1) && storm)  // end of data
                {
                    /*--------- Jason made this change, since the previous code will cause the inaccurate start and end of the EVENT -------*/
                    stormlist[1].Add(stormEnd);

                    //stormlist[1].Insert(stormCount, _IIFunctions.lowerBound_minute(stormEnd, 60).AddHours(1));
                    /*--------- Jason made this change, since the previous code will cause the inaccurate start and end of the EVENT END-------*/
                }
            }

            return stormlist;
        }

        // <summary>
        // separate storm events
        // </summary>
        // <param name="at">precip time</param>
        // <param name="av">precip value (mm/hr)</param>
        // <param name="interDryPeriod">Inter-event dry period to use (hours)</param>
        // <returns>A 2-element array of lists (start and end times)</returns>       
        public List<DateTime>[] StormSeparate(List<DateTime> at, List<double> av, double interDryPeriod)
        {
            List<DateTime>[] stormlist = new List<DateTime>[2];
            stormlist[0] = new List<DateTime>();
            stormlist[1] = new List<DateTime>();
            bool storm = false;
            DateTime stormEnd = at[0];
            DateTime currTime;

            //No data
            int totalRows = at.Count;
            if (totalRows == 0)
            {
                return stormlist;
            }
            var atEnumerator = at.GetEnumerator();
            var avEnumerator = av.GetEnumerator();
            int k = 0;
            //Seperation
            while (atEnumerator.MoveNext() && avEnumerator.MoveNext())
            {
                k++;
                currTime = atEnumerator.Current;
                //separate all the storm events
                if (avEnumerator.Current != 0)
                {
                    if (storm)
                    {
                        if (currTime.Subtract(stormEnd).TotalHours > interDryPeriod)    //if a new storm
                        {
                            /*--------- Jason made this change, since the previous code will cause the inaccurate start and end of the EVENT -------*/
                            stormlist[1].Add(lowerBound_minute(stormEnd, 60).AddHours(1)); // Ahmad Made this change to match dates from V1 datacurrent (Removed .AddHours(1))
                            //   stormCount += 1;
                            stormlist[0].Add(lowerBound_minute(currTime, 60)); // Ahmad Made this change to match dates from V1 datacurrent
                            /*--------- Jason made this change, since the previous code will cause the inaccurate start and end of the EVENT END-------*/

                            //stormlist[1].Insert(stormCount, _IIFunctions.lowerBound_minute(stormEnd, 60).AddHours(1));
                            //stormCount += 1;
                            //stormlist[0].Insert(stormCount, _IIFunctions.lowerBound_minute(currTime, 60));
                            //else a continuous storm, do nothing
                        }
                    }
                    else //no storm yet, this is the first storm within the period
                    {
                        stormlist[0].Add(currTime);
                        storm = true;
                    }
                    stormEnd = currTime;
                }
                if ((k == totalRows - 1) && storm)  // end of data
                {
                    /*--------- Jason made this change, since the previous code will cause the inaccurate start and end of the EVENT -------*/
                    stormlist[1].Add(stormEnd);

                    //stormlist[1].Insert(stormCount, _IIFunctions.lowerBound_minute(stormEnd, 60).AddHours(1));
                    /*--------- Jason made this change, since the previous code will cause the inaccurate start and end of the EVENT END-------*/
                }
            }

            return stormlist;
        }

        /// <summary>
        /// Build the string to display in the return Period Over Tc table
        /// </summary>
        /// <param name="refValues">reference values (design storm): Key - return period</param>
        /// <param name="currValue">Peak intensity over Tc</param>
        /// <returns>string of return period over Tc</returns>
        /// <remarks></remarks>
        private string ComparedReturnPeriod(List<KeyValuePair<int, double>> refValues, double currValue)
        {
            string returnPeriod = "";
            foreach (var element in refValues)
            {
                if (currValue > element.Value)
                {
                    returnPeriod = element.Key.ToString() + " yr";
                }
                else if (currValue < element.Value)
                {
                    // if currValue is smaller than all reference values
                    if (string.IsNullOrEmpty(returnPeriod))
                    {
                        returnPeriod = "< " + element.Key.ToString() + " yr";
                    }
                    else
                    {
                        returnPeriod += " - " + element.Key.ToString() + " yr";
                    }
                    break; // TODO: might not be correct. Was : Exit For
                }
            }

            // if currValue is larger than all reference value
            if (!(returnPeriod.Contains("<") || returnPeriod.Contains("-")))
            {
                returnPeriod = "> " + returnPeriod;
            }

            return returnPeriod;
        }

        /// <summary>
        /// Get the largest lower bound of the input time
        /// </summary>
        /// <param name="t">input time</param>
        /// <param name="m">round to multiples of this parameter in minutes (1-60)</param>
        /// <returns></returns>
        private DateTime lowerBound_minute(DateTime t, int m)
        {
            t = t.AddMinutes(-(t.Minute % m));
            return t.AddSeconds(-t.Second);
        }

        /// <summary>
        /// Calculate the avg daily flow
        /// </summary>
        private void CalculateQ(ref Dictionary<string, FlowStatRow> FlowStats, Periods[] flowArray, int numDays, int numPeriods, int mPopulation)
        {
            // get the average for each time interval, then take the average of all of those

            int currDay = 0;
            int currPeriod = 0;
            int numVals = 0;
            double currTotal = 0;

            // the average flow for every period in the day
            Nullable<double>[] avgFlow = new Nullable<double>[numPeriods];
            for (currPeriod = 0; currPeriod <= numPeriods - 1; currPeriod++)
            {
                currTotal = 0;
                numVals = 0;
                for (currDay = 0; currDay <= numDays - 1; currDay++)
                {
                    if (currDay > flowArray[0].Lines.Count - 1)
                    {
                        continue;
                    }

                    if (currPeriod > flowArray[0].Lines.ElementAt(currDay).Pairs.Count - 1)
                    {
                        continue;
                    }

                    if ((flowArray[0].Lines.ElementAt(currDay).Pairs.ElementAt(currPeriod) != null))
                    {
                        currTotal += (double)flowArray[0].Lines.ElementAt(currDay).Pairs.ElementAt(currPeriod).Value;
                        numVals += 1;
                    }
                }
                avgFlow[currPeriod] = currTotal / numVals;
            }

            currTotal = 0;
            numVals = 0;
            for (currPeriod = 0; currPeriod <= numPeriods - 1; currPeriod++)
            {
                if (double.IsNaN(avgFlow[currPeriod].Value))
                    continue;

                if (avgFlow[currPeriod].HasValue)
                {
                    currTotal += avgFlow[currPeriod].Value;
                    numVals += 1;
                }
            }

            FlowStats["Q"].Average = (currTotal / numVals);
            FlowStats["Q"].Normalized = (currTotal / numVals * 86400 / mPopulation);
        }


        /// <summary>
        /// Calculate Average dry-weather flow
        /// </summary>
        private void CalculateDWF(ref Dictionary<string, FlowStatRow> FlowStats, Periods[] dryFlowArray, int numDays, int numPeriods, int mPopulation, out Nullable<double>[] flowAvgDayArray)
        {
            int currDay = 0;
            int currPeriod = 0;
            double currTotal = 0;
            int numVals = 0;

            // the average DWF for every period in the day
            flowAvgDayArray = new Nullable<double>[numPeriods];
            for (currPeriod = 0; currPeriod <= numPeriods - 1; currPeriod++)
            {
                currTotal = 0;
                numVals = 0;
                for (currDay = 0; currDay <= numDays - 1; currDay++)
                {
                    if (currDay > dryFlowArray[0].Lines.Count - 1)
                    {
                        continue;
                    }

                    if (currPeriod > dryFlowArray[0].Lines.ElementAt(currDay).Pairs.Count - 1)
                    {
                        continue;
                    }

                    if ((dryFlowArray[0].Lines.ElementAt(currDay).Pairs != null))
                    {
                        currTotal += (double)dryFlowArray[0].Lines.ElementAt(currDay).Pairs.ElementAt(currPeriod).Value;
                        numVals += 1;
                    }
                }
                flowAvgDayArray[currPeriod] = currTotal / numVals;
            }

            currTotal = 0;
            numVals = 0;
            // number of periods with data
            for (currPeriod = 0; currPeriod <= numPeriods - 1; currPeriod++)
            {
                if (double.IsNaN(flowAvgDayArray[currPeriod].Value))
                    continue;

                if (flowAvgDayArray[currPeriod].HasValue)
                {
                    currTotal += flowAvgDayArray[currPeriod].Value;
                    numVals += 1;
                }
            }
            FlowStats["DWF"].Average = currTotal / numVals;
            FlowStats["DWF"].Normalized = currTotal / numVals * 86400 / mPopulation;
        }

        /// <summary>
        /// Calculate the GWI value based on the user's selection. If user selected
        /// "percent", then FlowStats("Min_DWF") must be populated
        /// </summary>
        private void CalculateGWI(ref Dictionary<string, FlowStatRow> FlowStats, int GWISelect, double mGwiTypeParam, double mCatchArea)
        {
            double mGwi = -1;
            if (GWISelect == 2)
            {
                // convert from L/ha/d to L/s
                mGwi = mGwiTypeParam * mCatchArea / 86400;
            }
            else
            {
                // a percent of avg min DWF
                mGwi = mGwiTypeParam / 100 * FlowStats["Min_DWF"].Average.Value;
            }

            FlowStats["GWI"].Average = mGwi;
            FlowStats["GWI"].Normalized = mGwi * 86400 / mCatchArea;
        }


        /// <summary>
        /// Calculate the average of each day's minimum DWF
        /// </summary>
        /// <remarks></remarks>
        private void CalculateMin_DWF(ref Dictionary<string, FlowStatRow> FlowStats, Periods[] dryFlowArray, int numDays, int numPeriods)
        {
            int currDay = 0;
            int currPeriod = 0;

            //DateValuePair currData = default(DateValuePair);
            int numVals = 0;
            double currTotal = 0;

            for (currDay = 0; currDay <= numDays - 1; currDay++)
            {

                double currMin = double.MaxValue;
                for (currPeriod = 0; currPeriod <= numPeriods - 1; currPeriod++)
                {
                    if (currDay > dryFlowArray[0].Lines.Count - 1)
                    {
                        continue;
                    }

                    if (currPeriod > dryFlowArray[0].Lines.ElementAt(currDay).Pairs.Count - 1)
                    {
                        continue;
                    }

                    if ((dryFlowArray[0].Lines.ElementAt(currDay).Pairs.ElementAt(currPeriod) != null) && ((double)dryFlowArray[0].Lines.ElementAt(currDay).Pairs.ElementAt(currPeriod).Value < currMin))
                    {
                        currMin = (double)dryFlowArray[0].Lines.ElementAt(currDay).Pairs.ElementAt(currPeriod).Value;
                    }
                }
                if ((currMin < double.MaxValue))
                {
                    currTotal += currMin;
                    numVals += 1;
                }
            }

            FlowStats["Min_DWF"].Average = currTotal / numVals;
            FlowStats["Min_DWF"].Normalized = null;
        }

        /// <summary>
        /// Calculate Average Population-derived dry-weather flow
        /// </summary>
        private void CalculatePop_DWF(ref Dictionary<string, FlowStatRow> FlowStats, int mPopulation)
        {
            // Pop_DWF (avg) is equal to DWF - GWI
            FlowStats["Pop_DWF"].Average = FlowStats["DWF"].Average.Value - FlowStats["GWI"].Average.Value;
            FlowStats["Pop_DWF"].Normalized = FlowStats["Pop_DWF"].Average.Value * 86400 / mPopulation;
        }

        /// <summary>
        /// Calculate the average of each day's peak DWF
        /// </summary>
        /// <remarks></remarks>
        private void CalculatePeak_DWF(ref Dictionary<string, FlowStatRow> FlowStats, Periods[] dryFlowArray, int numDays, int numPeriods)
        {
            int currDay = 0;
            int currPeriod = 0;

            int numVals = 0;
            double currTotal = 0;

            for (currDay = 0; currDay <= numDays - 1; currDay++)
            {
                double currMax = double.MinValue;
                for (currPeriod = 0; currPeriod <= numPeriods - 1; currPeriod++)
                {
                    if (currDay > dryFlowArray[0].Lines.Count - 1)
                    {
                        continue;
                    }

                    if (currPeriod > dryFlowArray[0].Lines.ElementAt(currDay).Pairs.Count - 1)
                    {
                        continue;
                    }

                    if (((dryFlowArray[0].Lines.ElementAt(currDay).Pairs.ElementAt(currPeriod) != null)) && ((double)dryFlowArray[0].Lines.ElementAt(currDay).Pairs.ElementAt(currPeriod).Value > currMax))
                    {
                        currMax = (double)(dryFlowArray[0].Lines.ElementAt(currDay).Pairs.ElementAt(currPeriod).Value);
                    }
                }
                if ((currMax > double.MinValue))
                {
                    currTotal += currMax;
                    numVals += 1;
                }
            }

            FlowStats["Peak_DWF"].Average = currTotal / numVals;
            FlowStats["Peak_DWF"].Normalized = null;
        }

        /// <summary>
        /// Calculate the measured Harmon Peaking Factor, as the ratio of Peak_DWF to DWF
        /// </summary>
        private void CalculateHarmon(ref Dictionary<string, FlowStatRow> FlowStats)
        {
            FlowStats["Harmon"].Average = FlowStats["Peak_DWF"].Average.Value / FlowStats["DWF"].Average.Value;
        }

        /// <summary>
        /// Calculate the theoretical Harmon Peaking Factor, using the equation
        /// </summary>
        private void CalculateTheo_Harmon(ref Dictionary<string, FlowStatRow> FlowStats, int mPopulation)
        {
            FlowStats["Theo_Harmon"].Average = 1 + ((double)14 / (4 + Math.Sqrt((double)mPopulation / 1000)));
        }

        /// <summary>
        /// Calculate the Harmon Correction Factor
        /// </summary>
        /// <remarks>
        /// To do this, solve for K in the equation:
        /// 
        /// Harmon = K * Theo_Harmon
        /// </remarks>
        private void CalculateHCF(ref Dictionary<string, FlowStatRow> FlowStats)
        {
            FlowStats["HCF"].Average = FlowStats["Harmon"].Average.Value / FlowStats["Theo_Harmon"].Average.Value;
        }

        /// <summary>
        /// Calculates the Coeficient of Determination value (R-Squared) of a linear regression
        /// </summary>
        /// <param name="xValues"></param>
        /// <param name="yValues"></param>
        /// <returns></returns>
        public decimal CalculateCoeficientOfDetermination(List<EventStatRow> events, int axis)
        {
            decimal xSum, ySum, sumXSqaured, sumYSquared, sumXY;
            decimal result;
            //List<decimal> returnResults = new List<decimal>();
            SeriesPoints p;
            List<decimal> xValues;
            List<decimal> yValues;


            xValues = new List<decimal>();
            yValues = new List<decimal>();
            xSum = ySum = sumXSqaured = sumXSqaured = sumYSquared = sumXY = result = 0;

            var eventEnumerator = events.GetEnumerator();
            while (eventEnumerator.MoveNext())
            {
                p = new SeriesPoints();
                if (axis < 4)
                {// Total Precip X-Axis
                    if (axis == 0)
                    { //PkIIFlow
                        p.xValue = eventEnumerator.Current.TotalPrecip;
                        p.yValue = eventEnumerator.Current.PeakIIFlow;
                    }
                    else if (axis == 1)
                    { //PKIIRate
                        p.xValue = eventEnumerator.Current.TotalPrecip;
                        p.yValue = eventEnumerator.Current.PeakIIRate;

                    }
                    else if (axis == 2)
                    { // IIVolume
                        p.xValue = eventEnumerator.Current.TotalPrecip;
                        p.yValue = eventEnumerator.Current.VolumeOfII;

                    }
                    else if (axis == 3)
                    { // VolCoef
                        p.xValue = eventEnumerator.Current.TotalPrecip;
                        p.yValue = eventEnumerator.Current.VolumetricRunoffCoef;
                    }
                }
                else
                { // Peak Precip Intensity
                    if (axis == 4)
                    { //PkIIFlow
                        p.xValue = (Decimal)eventEnumerator.Current.PeakIntensityOverTc;
                        p.yValue = eventEnumerator.Current.PeakIIFlow;
                    }
                    else if (axis == 5)
                    { //PKIIRate
                        p.xValue = (Decimal)eventEnumerator.Current.PeakIntensityOverTc;
                        p.yValue = eventEnumerator.Current.PeakIIRate;
                    }
                    else if (axis == 6)
                    { // IIVolume
                        p.xValue = (Decimal)eventEnumerator.Current.PeakIntensityOverTc;
                        p.yValue = eventEnumerator.Current.VolumeOfII;

                    }
                    else if (axis == 7)
                    { // VolCoef
                        p.xValue = (Decimal)eventEnumerator.Current.PeakIntensityOverTc;
                        p.yValue = eventEnumerator.Current.VolumetricRunoffCoef;

                    }
                }
                xValues.Add(p.xValue);
                yValues.Add(p.yValue);

            }
            var xValuesEnumerator = xValues.GetEnumerator();
            var yValuesEnumerator = yValues.GetEnumerator();
            while (xValuesEnumerator.MoveNext() && yValuesEnumerator.MoveNext())
            {
                xSum += xValuesEnumerator.Current;
                ySum += yValuesEnumerator.Current;
                sumXY += (xValuesEnumerator.Current * yValuesEnumerator.Current);
                sumXSqaured += (xValuesEnumerator.Current * xValuesEnumerator.Current);
                sumYSquared += (yValuesEnumerator.Current * yValuesEnumerator.Current);
            }


            var numerator = xValues.Count * (sumXY) - (xSum * ySum);
            var denom = ((xValues.Count * sumXSqaured) - (xSum * xSum)) * ((xValues.Count * sumYSquared) - (ySum * ySum));
            denom = (decimal)Math.Sqrt((double)denom);

            if (denom == 0)
                result = 0;
            else
                result = numerator / denom; // Get r

            result = result * result; //Get r^2
            
            return result;
        }
        #endregion

        #region II Analysis calculations
        private void InterpolateBlanckPoint(double[] v)
        {
            double v1 = 0;
            double v2 = 0;
            int index2 = -1;
            int i = 0;
            int j = 0;
            //Avoid v[0] < 0

            for (i = 1; i <= v.Length - 1; i++)
            {

                if (v[i] <= 0)
                {
                    //Set up V1 and V2
                    v1 = v[i - 1];

                    for (j = i + 1; j <= v.Length - 1; j++)
                    {
                        if ((v[j] >= 0))
                        {
                            v2 = v[j];
                            index2 = j;
                            break; // TODO: might not be correct. Was : Exit For
                        }
                    }
                    //Calculate v[i] which need to be estimated
                    v[i] = (v2 - v1) / (index2 - i + 1) + v1;
                }
            }
        }

        /// <summary>
        /// Get standard Deviation of an array
        /// </summary>
        /// <param name="a">input array whose standard deviation is to be calculated</param>
        /// <returns>standard deviation of the array</returns>
        /// <remarks>-999 is a special number, whose value is not used in the calculation</remarks>
        public static double GetStandardDeviation(double[] a)
        {
            int i = 0;
            int count = 0;
            count = 0;
            double Sum = 0.0;
            double SumOfSqrs = 0.0;
            for (i = 0; i <= a.Length - 1; i++)
            {
                if (a[i] != -999)
                {
                    Sum += a[i];
                    SumOfSqrs += Math.Pow(a[i], 2);
                    count += 1;
                }
            }
            double topSum = (count * SumOfSqrs) - (Math.Pow(Sum, 2));
            double n = Convert.ToDouble(count);
            return Math.Sqrt(topSum / (n * (n - 1)));
        }

        /// <summary>
        /// This function is for getting the data from multiple sensors with different start/end times each
        /// </summary>
        /// <param name="sensorId"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        public Dictionary<int, List<MonitoringDataDTO>> GetEventData(int[] sensorId, DateTime?[] fromDate, DateTime?[] toDate)
        {
            Dictionary<int, List<MonitoringDataDTO>> sensorData = new Dictionary<int, List<MonitoringDataDTO>>();

            for (int i = 0; i < sensorId.Length; i++)
            {
                var currentSensor = sensorId[i];
                var sensor = model.MonitoringSensors.Include("MonitoringStation.TimeZone").Where(item => item.Id == currentSensor).FirstOrDefault();
                var timeZoneOffset = TimeZoneInfo.FindSystemTimeZoneById(sensor.MonitoringStation.TimeZone.TZICode).BaseUtcOffset;
                DateTimeOffset fromDateOffset;
                DateTimeOffset toDateOffset;

                if (fromDate != null)
                    fromDateOffset = fromDate[i] == null ? DateTimeOffset.MinValue : SetDateTimeToDateTimeOffset(fromDate[i].Value, timeZoneOffset);
                else
                    fromDateOffset = DateTimeOffset.MinValue;


                if (toDate != null)
                    toDateOffset = toDate[i] == null ? DateTimeOffset.MaxValue : SetDateTimeToDateTimeOffset(toDate[i].Value, timeZoneOffset);
                else
                    toDateOffset = DateTimeOffset.MaxValue;

                var sensorDataList = model.MonitoringDatas.AsNoTracking().Where(x => x.MonitoringSensorId == currentSensor && x.TimeStamp >= fromDateOffset && x.TimeStamp <= toDateOffset).OrderBy(item => item.TimeStamp).ToList();
                var sensorDataDTOList = new List<MonitoringDataDTO>();
                foreach (var data in sensorDataList)
                {
                    var monitoringDataDTO = new MonitoringDataDTO();
                    monitoringDataDTO.Id = data.Id;
                    monitoringDataDTO.MonitoringSensorId = data.MonitoringSensorId;
                    monitoringDataDTO.Processed = data.Processed;
                    monitoringDataDTO.Value = data.Value;
                    monitoringDataDTO.TimeStamp = data.TimeStamp.ToOffset(timeZoneOffset).DateTime;
                    sensorDataDTOList.Add(monitoringDataDTO);
                }
                sensorData.Add(i, sensorDataDTOList);
            }
            return sensorData;
        }
        //This function is used to return the event data when the user selects an event.
        /// <summary>
        /// This function is for getting sensor data from 1 sensor id.
        /// </summary>
        /// <param name="sensorId"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        public List<MonitoringDataDTO> GetEventData(int sensorId, DateTime? fromDate, DateTime? toDate)
        {
            var sensor = model.MonitoringSensors.Include("MonitoringStation.TimeZone").Where(item => item.Id == sensorId).FirstOrDefault();
            if (sensor == null)
                return null;
            var timeZoneOffset = TimeZoneInfo.FindSystemTimeZoneById(sensor.MonitoringStation.TimeZone.TZICode).BaseUtcOffset;
            var fromDateOffset = fromDate == null ? DateTimeOffset.MinValue : SetDateTimeToDateTimeOffset(fromDate.Value, timeZoneOffset);
            var toDateOffset = toDate == null ? DateTimeOffset.MaxValue : SetDateTimeToDateTimeOffset(toDate.Value, timeZoneOffset);

            var sensorDataList = model.MonitoringDatas.AsNoTracking().Where(x => x.MonitoringSensorId == sensorId && x.TimeStamp >= fromDateOffset && x.TimeStamp <= toDateOffset).OrderBy(item => item.TimeStamp).ToList();
            var sensorDataDTOList = new List<MonitoringDataDTO>();
            foreach (var data in sensorDataList)
            {
                var monitoringDataDTO = new MonitoringDataDTO();
                monitoringDataDTO.Id = data.Id;
                monitoringDataDTO.MonitoringSensorId = data.MonitoringSensorId;
                monitoringDataDTO.Processed = data.Processed;
                monitoringDataDTO.Value = data.Value;
                monitoringDataDTO.TimeStamp = data.TimeStamp.ToOffset(timeZoneOffset).DateTime;
                sensorDataDTOList.Add(monitoringDataDTO);
            }
            return sensorDataDTOList;
        }

        public List<MonitoringDataDTO> GetMoniotirngDate(int sensorId, DateTime? startDate, DateTime? endDate)
        {
            var sensor = model.MonitoringSensors.Include("MonitoringStation.TimeZone").Where(item => item.Id == sensorId).FirstOrDefault();
            var timeZoneOffset = TimeZoneInfo.FindSystemTimeZoneById(sensor.MonitoringStation.TimeZone.TZICode).BaseUtcOffset;
            var fromDateOffset = startDate == null ? DateTimeOffset.MinValue : SetDateTimeToDateTimeOffset(startDate.Value, timeZoneOffset);
            var toDateOffset = endDate == null ? DateTimeOffset.MaxValue : SetDateTimeToDateTimeOffset(endDate.Value, timeZoneOffset);

            var sensorDataList = model.MonitoringDatas.Where(x => x.MonitoringSensorId == sensorId && x.TimeStamp > fromDateOffset && x.TimeStamp < toDateOffset).OrderBy(item => item.TimeStamp).ToList();
            var sensorDataDTOList = new List<MonitoringDataDTO>();
            foreach (var data in sensorDataList)
            {
                var monitoringDataDTO = new MonitoringDataDTO();
                monitoringDataDTO.Id = data.Id;
                monitoringDataDTO.MonitoringSensorId = data.MonitoringSensorId;
                monitoringDataDTO.Processed = data.Processed;
                monitoringDataDTO.Value = data.Value;
                monitoringDataDTO.TimeStamp = data.TimeStamp.ToOffset(timeZoneOffset).DateTime;
                sensorDataDTOList.Add(monitoringDataDTO);
            }
            return sensorDataDTOList;
        }
        /// <summary>
        /// This function is for getting event data from multiple sensor id's 
        /// </summary>
        /// <param name="sensorId"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        public Dictionary<int, List<MonitoringDataDTO>> GetEventData(int[] sensorId, DateTime? fromDate, DateTime? toDate)
        {
            Dictionary<int, List<MonitoringDataDTO>> sensorData = new Dictionary<int, List<MonitoringDataDTO>>();
            for (int i = 0; i < sensorId.Length; i++)
            {
                var currentSensor = sensorId[i];
                var sensor = model.MonitoringSensors.Include("MonitoringStation.TimeZone").Where(item => item.Id == currentSensor).FirstOrDefault();
                var timeZoneOffset = TimeZoneInfo.FindSystemTimeZoneById(sensor.MonitoringStation.TimeZone.TZICode).BaseUtcOffset;
                var fromDateOffset = fromDate == null ? DateTimeOffset.MinValue : SetDateTimeToDateTimeOffset(fromDate.Value, timeZoneOffset);
                var toDateOffset = toDate == null ? DateTimeOffset.MaxValue : SetDateTimeToDateTimeOffset(toDate.Value, timeZoneOffset);
                //Save key by i = 0, 1  ... ; Might have to make key Sensor Id. Not sure.
                var sensorDataList = model.MonitoringDatas.Where(x => x.MonitoringSensorId == currentSensor && x.TimeStamp >= fromDateOffset && x.TimeStamp <= toDateOffset).OrderBy(item => item.TimeStamp).ToList();
                var sensorDataDTOList = new List<MonitoringDataDTO>();
                foreach (var data in sensorDataList)
                {
                    var monitoringDataDTO = new MonitoringDataDTO();
                    monitoringDataDTO.Id = data.Id;
                    monitoringDataDTO.MonitoringSensorId = data.MonitoringSensorId;
                    monitoringDataDTO.Processed = data.Processed;
                    monitoringDataDTO.Value = data.Value;
                    monitoringDataDTO.TimeStamp = data.TimeStamp.ToOffset(timeZoneOffset).DateTime;
                    sensorDataDTOList.Add(monitoringDataDTO);
                }
                sensorData.Add(i, sensorDataDTOList);
            }
            return sensorData;
        }

        //find max precip intensity over tc
        public double GetTcPrecipIntensity(List<MonitoringDataDTO> data, double tc)
        {
            double tcPrecipIntensity = 0.0;
            tcPrecipIntensity = SlidingWindowPeakIntensity(data, tc);
            return tcPrecipIntensity;
        }

        /// <summary>
        /// Return whether the input time t is influence by a storm event
        /// </summary>
        /// <param name="t">Timestamp to check</param>
        /// <param name="stormlist">See remarks</param>
        /// <param name="stormSeparationPeriod">Inter-event dry period</param>
        /// <returns></returns>
        /// <remarks>
        ///   The "stormlist" paramater is a 2-element array.  The first element contains
        ///   a list of storm start times, the second element a list of corresponding storm
        ///   end times.
        /// </remarks>
        private static bool stormy(DateTime t, DateTime[][] stormlist, double stormSeparationPeriod)
        {
            // no storm event
            if (stormlist == null || stormlist[0].Length == 0)
                return false;

            int sCount = 0;
            Boolean inStorm = false;
            DateTime stormTime;
            stormTime = stormlist[1][sCount];
            DateTime tmpTime;
            //try
            //{
            tmpTime = stormTime.AddHours(stormSeparationPeriod);
            //}
            //catch (Exception ex)
            //{
            //    throw Exception
            //    //throw new AxisException(ex.StackTrace + " stormTime=" + stormTime);
            //}
            while (t > tmpTime)
            {
                sCount++;
                if (sCount >= stormlist[1].Length) // last storm event
                    return false;
                else
                    stormTime = stormlist[1][sCount];
            }   // end of while loop
            stormTime = stormlist[0][sCount];
            if (t >= stormTime)
                inStorm = true;
            return inStorm;
        }


        private Boolean Stormy(DateTime neighbour, DateTime[][] stormList, double interDryPeriod)
        {
            bool isStormy = true;

            int stormNum = stormList[0].Length;
            for (int index = 0; index <= stormNum - 2; index++)
            {
                if (neighbour > Convert.ToDateTime(stormList[1][index].AddHours(interDryPeriod)) &&
                    neighbour < Convert.ToDateTime(stormList[0][index + 1]))
                {
                    isStormy = false;
                    break;
                }


            }

            return isStormy;
        }

        private Boolean StormyNew(DateTime neighbour, DateTime[][] stormList, double interDryPeriod, int stormindex, Boolean pre)
        {
            bool isStormy = true;

            int stormNum = stormList[0].Length;

            if (pre)
            {
                for (int index = stormindex; index >= 1; index--)
                {
                    if (neighbour > Convert.ToDateTime(stormList[1][index - 1].AddHours(interDryPeriod)) &&
                      neighbour < Convert.ToDateTime(stormList[0][index]))
                    {
                        isStormy = false;
                        break;
                    }
                }

                if (neighbour < stormList[0][0])
                {
                    isStormy = false;
                }
            }
            else
            {
                for (int index = stormindex; index <= stormNum - 2; index++)
                {
                    if (neighbour > Convert.ToDateTime(stormList[1][index].AddHours(interDryPeriod)) &&
                        neighbour < Convert.ToDateTime(stormList[0][index + 1]))
                    {
                        isStormy = false;
                        break;
                    }
                }
                if (neighbour > stormList[1][stormNum - 1])
                {
                    isStormy = false;
                }
            }

            return isStormy;

        }
        /// <summary>
        /// align flow data, and place it into two collections
        /// </summary>
        /// <param name="data">input data to be aligned</param>
        /// </param name="mIntFlow">Used as interval</param>
        /// <param name="at">aligned datetime</param>
        /// <param name="av">aligned value</param>
        /// <remarks></remarks>
        public void FlowAlign(MonitoringDataDTO[] data, int sensorId, decimal mIntFlow, ref List<DateTime> at, ref List<Double> av, DateTime? fromDate, DateTime? toDate)
        {
            int i = 0;
            int j = 0;
            //Interval is flow sensor interval multiplied by 60 in old code
            decimal interval = mIntFlow * 60;
            //var sensorData = _uow.Repository<MonitoringData>().Query().Where(x => x.MonitoringSensorId == sensorId).OrderBy(item => item.TimeStamp).ToList();
            int count = data.Length;
            if (count <= 0)
                return;

            //Convert from DateTimeOffset to DateTime to use for comparisons.
            var sensor = model.MonitoringSensors.Include("MonitoringStation.TimeZone").Where(item => item.Id == sensorId).FirstOrDefault();
            var timeZoneOffset = TimeZoneInfo.FindSystemTimeZoneById(sensor.MonitoringStation.TimeZone.TZICode).BaseUtcOffset;

            DateTime beginT = data.First().TimeStamp;
            DateTime endT = data.Last().TimeStamp;

            long begintI = 0;
            long endtI = 0;

            //Idealize time.
            beginT = lowerBound_minute(beginT, 60);

            begintI = IdealizeTime(ConvertDateToSeconds(beginT), Convert.ToInt64(interval), ConvertDateToSeconds(beginT));
            endtI = IdealizeTime(ConvertDateToSeconds(beginT), Convert.ToInt64(interval), ConvertDateToSeconds(endT));

            int numbin = (Convert.ToInt32(endtI - begintI) / (int)interval) + 1;

            for (i = 0; i < numbin; i++)
            {
                at.Add(ConvertSecondsToDate(begintI + i * (long)interval));
                av.Add(0);
            }
            var avArray = av.ToArray();
            var atArray = at.ToArray();
            DateTime currTime = default(DateTime);
            long currTimeI = 0;
            int lastcount = 0;
            for (j = 0; j < data.Count(); j++)
            {
                currTime = data[j].TimeStamp;
                currTimeI = IdealizeTime(ConvertDateToSeconds(beginT), Convert.ToInt64(interval), ConvertDateToSeconds(currTime));
                for (i = lastcount; i <= numbin - 1; i++)
                {
                    var x = ConvertSecondsToDate(currTimeI);
                    if (ConvertSecondsToDate(currTimeI) == atArray[i])
                    {
                        avArray[i] = Convert.ToDouble(data[j].Value);
                        lastcount = i;
                        break;
                    }
                }
            }

            InterpolateBlanckPoint(avArray);
            at = atArray.ToList();
            av = avArray.ToList();
        }

        //****************************************************************************************************
        // Inverse cdf for normal distribution
        // input parameter: probabiliy (between 0 and 1)
        // output: value of x (normalized)
        //****************************************************************************************************
        public static double SNormInv(Double p)
        {
            Double q, r;

            //Coefficients in rational approximations.
            const double A1 = -39.696830286653757;
            const double A2 = 220.9460984245205;
            const double A3 = -275.92851044696869;
            const double A4 = 138.357751867269;
            const double A5 = -30.66479806614716;
            const double A6 = 2.5066282774592392;

            const double B1 = -54.476098798224058;
            const double B2 = 161.58583685804089;
            const double B3 = -155.69897985988661;
            const double B4 = 66.80131188771972;
            const double B5 = -13.280681552885721;

            const double C1 = -0.0077848940024302926;
            const double C2 = -0.32239645804113648;
            const double C3 = -2.4007582771618381;
            const double C4 = -2.5497325393437338;
            const double C5 = 4.3746641414649678;
            const double C6 = 2.9381639826987831;

            const double D1 = 0.0077846957090414622;
            const double D2 = 0.32246712907003983;
            const double D3 = 2.445134137142996;
            const double D4 = 3.7544086619074162;

            // Define break-points.
            const double P_LOW = 0.02425;
            const double P_HIGH = 1 - P_LOW;

            if (p > 0 && p < P_LOW)
            {
                // Rational approximation for lower region.
                q = Math.Sqrt(-2 * Math.Log(p));
                return (((((C1 * q + C2) * q + C3) * q + C4) * q + C5) * q + C6) /
                   ((((D1 * q + D2) * q + D3) * q + D4) * q + 1);
            }
            else if (p >= P_LOW && p <= P_HIGH)
            {
                // Rational approximation for central region.
                q = p - 0.5;
                r = q * q;
                return (((((A1 * r + A2) * r + A3) * r + A4) * r + A5) * r + A6) * q /
                  (((((B1 * r + B2) * r + B3) * r + B4) * r + B5) * r + 1);
            }
            else if (p > P_HIGH && p < 1)
            {
                // Rational approximation for upper region.
                q = Math.Sqrt(-2 * Math.Log(1 - p));
                return -(((((C1 * q + C2) * q + C3) * q + C4) * q + C5) * q + C6) /
                       ((((D1 * q + D2) * q + D3) * q + D4) * q + 1);
            }
            else
            {
                return BANDFAIL;
            }
        }

        /// <summary>
        /// Calculate the DWF Patterns for a day of the week.
        /// </summary>
        /// <param name="DOW">The Day of the week to get the data for</param>
        /// <param name="flowId"></param>
        /// <param name="rainId"></param>
        /// <param name="stormList"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <returns></returns>
        public Dictionary<string, List<MonitoringDataDTO>[]> CalculateDryWeatherFlowPatterns(string DOW, int flowId, int rainId, DateTime[][] stormList, DateTime startDate, DateTime endDate)
        {

            decimal flowInterval = model.MonitoringSensors.Where(x => x.Id == flowId).FirstOrDefault().TimeInterval.Value;
            //Rough estimate of how many points should be present
            //If Dry data is interupted by storm, guage points by this value to determin if it is mapped or not
            var minNumOfPoints = ((60 / flowInterval) * 24); // Had -1 here for some reason...

            var PointsInWeek = minNumOfPoints * 7; // Get the number of points in the week for the given interval

            //Calculate the number of weeks the dates span
            double numOfDays = (endDate.Date - startDate.Date).TotalDays;
            double numOfWeeks = Math.Ceiling(numOfDays / 7);

            //Hold the dry weather flow patters for the days of the week.
            //Can have multiple patterns for each day depending on time range
            Dictionary<string, List<MonitoringDataDTO>[]> DryWeatherFlow = new Dictionary<string, List<MonitoringDataDTO>[]>()
            {
                { DayOfWeek.Sunday.ToString(), new List<MonitoringDataDTO>[(int)numOfWeeks]},
                { DayOfWeek.Monday.ToString(), new List<MonitoringDataDTO>[(int)numOfWeeks]},
                { DayOfWeek.Tuesday.ToString(), new List<MonitoringDataDTO>[(int)numOfWeeks]},
                { DayOfWeek.Wednesday.ToString(), new List<MonitoringDataDTO>[(int)numOfWeeks]},
                { DayOfWeek.Thursday.ToString(), new List<MonitoringDataDTO>[(int)numOfWeeks]},
                { DayOfWeek.Friday.ToString(), new List<MonitoringDataDTO>[(int)numOfWeeks]},
                { DayOfWeek.Saturday.ToString(), new List<MonitoringDataDTO>[(int)numOfWeeks]},
            };

            //Initilize date Lists to be used to get data for the various occurences of the DOW selected.
            List<DateTime?> DOWStartTime = new List<DateTime?>();
            List<DateTime?> DOWEndTime = new List<DateTime?>();
            DateTime tempStartDate;
            DateTime tempeEndDate;
            bool nullDate = false;
            if (startDate != null)
                tempStartDate = startDate;
            else
            {
                nullDate = true;
                tempStartDate = DateTime.MinValue;
            }

            if (endDate != null)
                tempeEndDate = endDate;
            else
            {
                nullDate = true;
                tempeEndDate = DateTime.MaxValue;
            }

            if (!nullDate)
            {
                //For the number of days the dates span
                var addedDates = false;
                for (var i = 0; i < PointsInWeek; i++)
                {
                    //If all the dates have been added, end the loop.
                    if (addedDates)
                        break;
                    //If the date is the same as the DOW that is being generated, save that start date
                    if (tempStartDate.DayOfWeek.ToString() == DOW)
                    {
                        if (tempStartDate < endDate)
                        {
                            DateTime tempDate = tempStartDate;
                            tempeEndDate = tempDate.AddHours(23).AddMinutes(59); //Get the end date, the end of that day  11:59pm
                            if (tempeEndDate < endDate) // If both the start and end date are within the range, add them to the date lists.
                            {
                                DOWStartTime.Add(tempStartDate);
                                DOWEndTime.Add(tempeEndDate);
                                addedDates = true;
                            }
                            else
                                break;
                        }
                        else
                        {
                            break;
                        }
                        for (var j = 0; j < numOfWeeks + 1; j++)
                        {

                            DateTime tempDate = tempStartDate.AddDays(7); // Add 7 to get the next occurence of the day of the week
                            if (tempDate < endDate)
                            {
                                tempeEndDate = tempDate.AddHours(23).AddMinutes(59); //Get the end date, the end of that day  11:59pm
                                //If both the start and end date are within the given range, add them to the Date Lists.
                                if (tempeEndDate < endDate)
                                {
                                    tempStartDate = tempDate;
                                    DOWStartTime.Add(tempStartDate);
                                    DOWEndTime.Add(tempeEndDate);
                                }
                                else
                                    break;
                            }
                            else
                                break;

                        }
                    }
                    else
                    { // Else, increment date by flow interval (5min, 10 min,... etc)
                        tempStartDate = tempStartDate.AddMinutes((double)flowInterval);
                    }
                }
            }


            DateTime currentTimeStamp = default(DateTime);
            DayOfWeek dow = default(DayOfWeek);
            DayOfWeek startDay = default(DayOfWeek);
            DayOfWeek prevDay = default(DayOfWeek);

            bool firstDay = true;

            List<MonitoringDataDTO> dt_dry = new List<MonitoringDataDTO>();
            //Initilze a counter for the current week
            int weekCount = 0;

            for (var j = 0; j < DOWStartTime.Count; j++)
            {
                var dt_wet = GetEventData(flowId, DOWStartTime[j], DOWEndTime[j]);
                var rainData = GetEventData(rainId, DOWStartTime[j], DOWEndTime[j]);
                int count_wet = dt_wet.Count;

                if (dt_wet.Count < 0 || rainData.Count < 0)
                    return null;

                for (var i = 0; i < dt_wet.Count; i++)
                {
                    currentTimeStamp = dt_wet.ElementAt(i).TimeStamp;
                    if (currentTimeStamp.DayOfWeek.ToString() != DOW)
                        continue;

                    bool storm = false;

                    //Check the storms list to make sure the flow data is dry and not from a storm  
                    for (var k = 0; k < stormList[0].Length; k++)
                    {
                        //If the current flow data is within a storm event, not dry weather flow
                        if (currentTimeStamp > stormList[0].ElementAt(k) && currentTimeStamp < stormList[1].ElementAt(k))
                        {
                            storm = true;
                            dt_dry = new List<MonitoringDataDTO>(); // Reset the Dry Data.
                            break;
                        }
                    }
                    //If the point is from a storm, continue to next point
                    if (storm)
                        continue;

                    //Get the current Day of the week.
                    dow = currentTimeStamp.DayOfWeek;

                    //Get the first day of the data, this will serve as the start of the week.
                    if (firstDay)
                    {
                        startDay = currentTimeStamp.DayOfWeek;
                        //Initilize the previous day Variable.
                        prevDay = currentTimeStamp.DayOfWeek;
                        //Add to the dry flow list
                        dt_dry.Add(dt_wet.ElementAt(i));
                        //reset first day variable
                        firstDay = false;
                    }
                    else
                    {
                        //Its a new Day or end of data/
                        if (dow != prevDay || i == (dt_wet.Count - 1))
                        {
                            //Add the last data point for the day
                            if (dow == prevDay)
                            {
                                if (dt_wet.ElementAt(i) != null)
                                    dt_dry.Add(dt_wet.ElementAt(i));
                            }


                            //If there arent enough points, or too many, disregard this Dry data.
                            if (dt_dry.Count < (minNumOfPoints) || dt_dry.Count > (minNumOfPoints))
                                dt_dry = new List<MonitoringDataDTO>();

                            //Add the dry flow list to the corresponding day of the week, as well as the current week;
                            DryWeatherFlow[prevDay.ToString()][weekCount] = dt_dry;
                            //reset the dry list;
                            dt_dry = new List<MonitoringDataDTO>();
                            //Set the new prev Day;
                            prevDay = dow;

                            //Check if its a new week
                            if (dow == startDay)
                            {
                                weekCount++;
                            }
                        }
                        else // Same day
                        {
                            //Add to the dry flow list
                            dt_dry.Add(dt_wet.ElementAt(i));
                            //i * flowTimeInt / 60
                        }
                    }
                }// End For Loop
            }
            return DryWeatherFlow;
        }


        /// <summary>
        /// Dry weather and wet weather separation
        /// </summary>
        /// <param name="dt_wet">original flow data</param>
        /// <param name="sensorId">Used to get TimeZoneOffset</param>
        /// <param name="interDryPeriod">Inter-event dry period</param>
        /// <param name="mIntFlow">Interval value of the flow sensor</param>
        /// <param name="fromDate">start date</param>
        /// <param name="toDate">end Date</param>
        /// <returns>A DataTable named "flow_est_dry" containing estimated dry-weather flow</returns>
        public MonitoringDataDTO[] Flow_Seperate(MonitoringDataDTO[] dt_wet, int sensorId, decimal mIntFlow, int interDryPeriod, DateTime? fromDate, DateTime? toDate, DateTime[][] stormList, int stormindex, ref bool posssibleUpdate,
           int StartCount = 0, int EndCount = 0, int selection = 0, bool isPassed = false)
        {
            //int selection = 0;
            int count_wet = dt_wet.Length;
            int startCount, endCount;

            if (StartCount == 0 || EndCount == 0)
            {
                //Find the subset
                startCount = 0;
                endCount = count_wet - 1;
                mIntFlow *= 60;
                DateTime? t1 = fromDate;
                DateTime? t2 = toDate;
                if (!isPassed)
                {
                    t1 = fromDate.Value.AddMinutes(-(double)mIntFlow / 2);
                    t2 = toDate.Value.AddMinutes((double)mIntFlow / 2);
                }
                for (int l = 0; l < count_wet - 1; l++)
                {
                    if (dt_wet[l].TimeStamp >= t1)
                    {
                        startCount = l;
                        break;
                    }
                }

                for (int p = startCount; p < count_wet - 1; p++)
                {
                    if (dt_wet[p].TimeStamp > t2)
                    {
                        endCount = p - 1;
                        break;
                    }
                    else if (p == (count_wet - 1))
                    {
                        endCount = p;
                    }
                }
            }
            else
            {
                startCount = StartCount;
                endCount = EndCount;
            }
            var arrayTimestamps = new List<DateTime>();
            var arrayValues = new List<double>();

            //var numRow = at.Count;
            var i = 0;
            var j = 0;

            //reset i,j;
            i = 0;
            j = 0;

            var dt_dryLength = endCount - startCount + 1;
            MonitoringDataDTO[] dt_dry = new MonitoringDataDTO[dt_dryLength];
            int index = 0;
            for (var q = startCount; q <= endCount; q++)
            {
                var t = new MonitoringDataDTO();
                t.Id = dt_wet[q].Id;
                //t.IsTransient = item.IsTransient;
                t.MonitoringSensorId = dt_wet[q].MonitoringSensorId;
                t.TimeStamp = dt_wet[q].TimeStamp;
                t.Value = dt_wet[q].Value;
                t.Processed = dt_wet[q].Processed;

                dt_dry[index] = t;
                index++;

            }

            int count_dry = count_wet;
            int k = 0;

            DateTime currentTimeStamp = default(DateTime);
            DateTime neighbourTimestamp = default(DateTime);
            DayOfWeek dow = default(DayOfWeek);
            DayOfWeek neighbourweek = default(DayOfWeek);
            double[] extract = new double[numAvgPt];
            // store the data for calculating pattern
            DateTime[] extractT = new DateTime[numAvgPt];
            bool weekend = false;
            TimeSpan currTime = default(TimeSpan);
            TimeSpan neighbourTime = default(TimeSpan);
            index = 0;
            int rightnumber = 0;
            for (i = startCount; i <= endCount; i++)
            {

                for (j = 0; j <= numAvgPt - 1; j++)
                {
                    extract[j] = -999;
                    extractT[j] = DateTime.MinValue;
                }

                currentTimeStamp = dt_dry[index].TimeStamp;
                currTime = currentTimeStamp.TimeOfDay;
                dow = currentTimeStamp.DayOfWeek;

                if (selection > 0)
                {
                    DateTime start_of_storm = stormList[0][selection - 1];
                    //only in the case of i/i comparison. Otherwise,stormList(0)(selection - 1) 
                    DateTime end_of_storm = stormList[1][selection - 1];

                    if (currentTimeStamp < start_of_storm || currentTimeStamp > end_of_storm.AddHours(interDryPeriod))
                    {
                        dt_dry[index].Value = dt_wet[i].Value;
                        continue;
                    }
                }
                //Check if its the weekend
                if (dow > DayOfWeek.Sunday && dow < DayOfWeek.Saturday)
                {
                    weekend = false;
                }
                else
                {
                    weekend = true;
                }

                j = 1;
                k = 0;

                while (k < numAvgPt)
                {
                    // the neighbour before the current time0
                    if (i - j >= 0)
                    {
                        neighbourTimestamp = dt_wet[i - j].TimeStamp;

                        if (!(neighbourTimestamp >= stormList[0][stormindex] && neighbourTimestamp <= stormList[1][stormindex].AddHours(interDryPeriod)))
                        {
                            if (neighbourTimestamp < stormList[0][stormindex])
                            {
                                if (!StormyNew(neighbourTimestamp, stormList, interDryPeriod, stormindex, true))
                                {

                                    neighbourTime = neighbourTimestamp.TimeOfDay;
                                    // check if the neighbouring point within the time range
                                    if (Math.Abs(neighbourTime.Subtract(currTime).TotalMinutes) <= timeRange)
                                    {
                                        neighbourweek = neighbourTimestamp.DayOfWeek;
                                        // check if the neighbouring point weekday or weekend
                                        if ((weekend == false && (neighbourweek > DayOfWeek.Sunday && neighbourweek < DayOfWeek.Saturday))
                                   || (weekend == true && (neighbourweek == DayOfWeek.Sunday || neighbourweek == DayOfWeek.Saturday)))
                                        {
                                            extract[k] = Convert.ToDouble(dt_wet.ElementAt(i - j).Value);
                                            extractT[k] = dt_wet.ElementAt(i - j).TimeStamp;
                                            k += 1;
                                            if (k == numAvgPt)
                                            {
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            if (neighbourTimestamp > stormList[1][stormindex].AddHours(interDryPeriod))
                            {
                                if (!StormyNew(neighbourTimestamp, stormList, interDryPeriod, stormindex, false))
                                {
                                    neighbourTime = neighbourTimestamp.TimeOfDay;
                                    if (Math.Abs(neighbourTime.Subtract(currTime).TotalMinutes) <= timeRange)
                                    {
                                        neighbourweek = neighbourTimestamp.DayOfWeek;
                                        // check if the neighbouring point weekday or weekend
                                        if ((weekend == false && (neighbourweek > DayOfWeek.Sunday && neighbourweek < DayOfWeek.Saturday))
                                   || (weekend == true && (neighbourweek == DayOfWeek.Sunday || neighbourweek == DayOfWeek.Saturday)))
                                        {
                                            extract[k] = Convert.ToDouble(dt_wet.ElementAt(i - j).Value);
                                            extractT[k] = dt_wet.ElementAt(i - j).TimeStamp;
                                            k += 1;
                                            if (k == numAvgPt)
                                            {
                                                break;
                                            }
                                        }
                                    }

                                }

                            }


                        }

                    }


                    // the neighbour after the current time
                    if (i + j < count_wet)
                    {

                        neighbourTimestamp = dt_wet[i + j].TimeStamp;

                        if (!(neighbourTimestamp >= stormList[0][stormindex] && neighbourTimestamp <= stormList[1][stormindex].AddHours(interDryPeriod)))
                        {
                            if (neighbourTimestamp < stormList[0][stormindex])
                            {
                                if (!StormyNew(neighbourTimestamp, stormList, interDryPeriod, stormindex, true))
                                {


                                    neighbourTime = neighbourTimestamp.TimeOfDay;
                                    // check if the neighbouring point within the time range
                                    if (Math.Abs(neighbourTime.Subtract(currTime).TotalMinutes) <= timeRange)
                                    {
                                        neighbourweek = neighbourTimestamp.DayOfWeek;
                                        // check if the neighbouring point weekday or weekend
                                        if ((weekend == false && (neighbourweek > DayOfWeek.Sunday && neighbourweek < DayOfWeek.Saturday))
                                            || (weekend == true && (neighbourweek == DayOfWeek.Sunday || neighbourweek == DayOfWeek.Saturday)))
                                        {
                                            extract[k] = Convert.ToDouble(dt_wet.ElementAt(i + j).Value);
                                            extractT[k] = neighbourTimestamp;
                                            rightnumber = rightnumber + 1;
                                            k += 1;
                                            //if (k == numAvgPt)
                                            //{
                                            //    break;
                                            //}
                                        }
                                    }
                                }
                            }
                            if (neighbourTimestamp > stormList[1][stormindex].AddHours(interDryPeriod))
                            {
                                if (!StormyNew(neighbourTimestamp, stormList, interDryPeriod, stormindex, false))
                                {

                                    neighbourTime = neighbourTimestamp.TimeOfDay;
                                    // check if the neighbouring point within the time range
                                    if (Math.Abs(neighbourTime.Subtract(currTime).TotalMinutes) <= timeRange)
                                    {
                                        neighbourweek = neighbourTimestamp.DayOfWeek;
                                        // check if the neighbouring point weekday or weekend
                                        if ((weekend == false && (neighbourweek > DayOfWeek.Sunday && neighbourweek < DayOfWeek.Saturday))
                                            || (weekend == true && (neighbourweek == DayOfWeek.Sunday || neighbourweek == DayOfWeek.Saturday)))
                                        {
                                            extract[k] = Convert.ToDouble(dt_wet.ElementAt(i + j).Value);
                                            extractT[k] = neighbourTimestamp;
                                            rightnumber = rightnumber + 1;
                                            k += 1;
                                            //if (k == numAvgPt)
                                            //{
                                            //    break;
                                            //}
                                        }
                                    }
                                }

                            }

                        }

                    }
                    j += 1;
                    if (i - j < 0 && i + j >= count_wet)
                    {
                        break;
                    }
                }

                //Find the average
                if (k != 0)
                {
                    double norm = SNormInv(bandtightness);

                    //get the Preliminary Avg
                    double avg = GetAvg(extract);

                    //Get standard Deviation
                    double sigma = GetStandardDeviation(extract);

                    // check if a particular point is out of range (spike or any other abnormaly)
                    // take out that point if out of range
                    for (j = 0; j <= extract.Length - 1; j++)
                    {
                        if (extract[j] != -999 && Math.Abs(extract[j] - avg) > norm * sigma)
                        {
                            extract[j] = -999;
                        }
                    }

                    // re-calculate average, with weights this time
                    DateTime eT = default(DateTime);
                    double diff = 0;
                    double maxDiff = -999;
                    for (j = 0; j <= extractT.Length - 1; j++)
                    {
                        eT = extractT[j];
                        diff = eT.Subtract(currentTimeStamp).Duration().TotalMinutes;
                        if (maxDiff < diff)
                        {
                            maxDiff = diff;
                        }
                    }

                    double perMinWeight = (weightU - weightL) / maxDiff;
                    double sum = 0;
                    double totalWeight = 0;
                    double currWeight = 0;

                    for (j = 0; j <= extract.Length - 1; j++)
                    {
                        if (extract[j] != -999)
                        {
                            eT = extractT[j];
                            diff = eT.Subtract(currentTimeStamp).Duration().TotalMinutes;
                            currWeight = weightU - diff * perMinWeight;
                            sum += extract[j] * currWeight;
                            totalWeight += currWeight;
                        }
                    }
                    //SAME UP TO THIS POINT!!!!
                    if (selection > 0)
                    {
                        if (totalWeight == 0)
                            totalWeight = 1;

                        DateTime start_of_storm = stormList[0][selection - 1];
                        //only in the case of i/i comparison. Otherwise,stormList(0)(selection - 1) 
                        DateTime end_of_storm = stormList[1][selection - 1];
                        Double value = (double)dt_wet[i].Value;//Convert.ToDouble(dt_wet.Rows(i)("value"));

                        if (currentTimeStamp < start_of_storm | currentTimeStamp > (end_of_storm.AddHours(interDryPeriod * dwfLengthMatchMultiple)))
                        {
                            if (value / (sum / totalWeight) < 1)
                            {
                                if (value / (sum / totalWeight) < dwfTolerance)
                                {
                                    dt_dry[index].Value = dt_wet[i].Value;

                                }
                                else
                                {
                                    dt_dry[index].Value = (Decimal)(sum / totalWeight);
                                }
                            }
                            else
                            {
                                if ((sum / totalWeight) / value > dwfTolerance)
                                {
                                    dt_dry[index].Value = dt_wet[i].Value;

                                }
                                else
                                {
                                    dt_dry[index].Value = (Decimal)(sum / totalWeight);
                                }
                            }
                        }
                        else
                        {
                            dt_dry[index].Value = (Decimal)(sum / totalWeight);
                        }


                    }
                    else
                    {
                        if (totalWeight == 0)
                            totalWeight = 1;
                        dt_dry[index].Value = (Decimal)(sum / totalWeight);
                    }

                }
                //End of (if k != 0)
                index++;

            }

            if (rightnumber != 0 && rightnumber < numAvgPt / 2)
            {
                posssibleUpdate = true;
            }
            //Next flow Data

            return dt_dry;
        }


        /// <summary>
        /// align precip points for storm separation and extract the relevent portion for display
        /// USED FOR II ANALYSIS ONLY
        /// </summary>
        /// <param name="dt">input data to be aligned</param>
        /// <param name="at">used to store aligned dates</param>
        /// <param name="av">used to store aligned values</param>
        /// <param name="forStormSep">whether the alignment is for storm separation or not</param>
        /// <param name="forAnalysis">whether the alignment is for analysis or not</param>
        /// <remarks>it's just a variation of the sub "flowAlign"</remarks>
        public void PrecipAlign(List<MonitoringDataDTO> dt, int sensorId, double tc, double timestep, ref List<DateTime> at, ref List<double> av, bool forStormSep, bool forAnalysis, DateTime? fromDate, DateTime? toDate)
        {
            //Used for Timezone stuff
            var sensor = model.MonitoringSensors.Include("MonitoringStation.TimeZone").Where(item => item.Id == sensorId).FirstOrDefault();
            var timeZoneOffset = TimeZoneInfo.FindSystemTimeZoneById(sensor.MonitoringStation.TimeZone.TZICode).BaseUtcOffset;

            if (fromDate == null)
                fromDate = dt.First().TimeStamp;
            if (toDate == null)
                toDate = dt.Last().TimeStamp;


            double interval = 0;
            // in seconds

            if (forStormSep)
            {
                interval = _intDefaultPrecip;
            }
            else
            {
                if (forAnalysis)
                {
                    interval = Convert.ToInt32(tc * 60);
                }
                else
                {
                    interval = Convert.ToInt32(timestep * 60);
                }


            }

            DateTime begint = dt.First().TimeStamp;
            DateTime endt = dt.Last().TimeStamp;
            PrecipitationAlign(dt, ref at, ref av, interval, begint, endt);

        }

        /// <summary>
        /// Calculate stats for the data series.
        /// </summary>
        /// <param name="step"></param>
        /// <param name="tc"></param>
        /// <param name="catcharea"></param>
        /// <param name="tcPrecipInten"></param>
        /// <param name="mIntFlow"></param>
        /// <param name="WWFSeries"></param>
        /// <param name="DWFSeries"></param>
        /// <param name="PrecipSeries"></param>
        /// <param name="IISeries"></param>
        /// <param name="fromDate"></param>
        /// <param name="toDate"></param>
        /// <returns></returns>
        public Dictionary<String, Double> Analysis(Boolean step, double tc, double catcharea, double tcPrecipInten, decimal mIntFlow, double[] WWFSeries, double[] DWFSeries, Dictionary<DateTime, Double> PrecipSeries, Dictionary<DateTime, Double> IISeries, DateTime? fromDate, DateTime? toDate)
        {
            double pTotal = 0;
            double pTotalcubicM = 0;
            double WWFVolTotal = 0;
            double DWFVolTotal = 0;
            double inflowVolTotal = 0;
            double infilVolTotal = 0;
            double IIVolume = 0;
            DateTime stormEndTimePlus2Tc = default(DateTime);
            mIntFlow *= 60;

            //Dictionary used to hold all the calculated values.
            var myAnalysis = new Dictionary<String, Double>();

            try
            {
                // calculate total volume (For the time period displayed) in m3)
                for (int i = 0; i < WWFSeries.Length; i++)
                {
                    WWFVolTotal += WWFSeries[i];
                }
                WWFVolTotal *= (double)mIntFlow;

                myAnalysis.Add("WWFVolTotal", WWFVolTotal);
                // integrate

                // calcualte total DWF volume (For the time period displayed) in m3)
                for (int i = 0; i < DWFSeries.Length; i++)
                {
                    DWFVolTotal += DWFSeries[i];
                }
                DWFVolTotal *= (double)mIntFlow;
                // integrate

                //FIRST way of getting DWF
                //myAnalysis.Add("DWFVolTotal", DWFVolTotal);

                //Calculate total Runoff Volume
                foreach (KeyValuePair<DateTime, Double> entry in IISeries)
                {
                    if (entry.Key >= fromDate)
                        IIVolume += entry.Value;
                }
                IIVolume *= (double)mIntFlow;
                //Integrate
                myAnalysis.Add("IIVolume", IIVolume);

                //Another way of getting DWF
                DWFVolTotal = Math.Round(WWFVolTotal) - Math.Round(IIVolume);
                myAnalysis.Add("DWFVolTotal", DWFVolTotal);

                // calculate total inflow volume (Wet weather volume during the storm and two Tc after last 3mm in one hour)
                // find the last stormEndThresh in one hour for the event               
                foreach (KeyValuePair<DateTime, Double> entry in PrecipSeries)
                {
                    if (entry.Value >= stormEndThresh || entry.Key <= fromDate)
                    {
                        stormEndTimePlus2Tc = entry.Key.AddMinutes(tc * 2);
                    }
                }

                //Calculate inflowVolTotal
                foreach (KeyValuePair<DateTime, Double> entry in IISeries)
                {
                    if (entry.Key >= fromDate && entry.Key <= stormEndTimePlus2Tc)
                    {
                        inflowVolTotal += entry.Value;
                    }
                }

                inflowVolTotal *= (double)mIntFlow;
                // integrate
                myAnalysis.Add("inflowVolTotal", inflowVolTotal);

                // calculate total infiltration volume (From two Tc to end of display)
                foreach (KeyValuePair<DateTime, Double> entry in IISeries)
                {
                    if (entry.Key > stormEndTimePlus2Tc)
                    {
                        infilVolTotal += entry.Value;
                    }
                }

                infilVolTotal *= (double)mIntFlow;
                // integrate
                myAnalysis.Add("infilVolTotal", infilVolTotal);

                // set all the display elements

                // time of concentration
                //Tc.InnerHtml = Convert.ToInt32(Math.Round(tcon)).ToString + unitTime;

                // find total precip
                foreach (KeyValuePair<DateTime, Double> entry in PrecipSeries)
                {
                    if (entry.Key >= fromDate && entry.Key <= toDate)
                        pTotal += entry.Value;
                }

                //pTotal = getpTotal(step);
                pTotalcubicM = Math.Round(pTotal, 1) * catcharea * 10000 / 1000; // Correct Value.
                myAnalysis.Add("pTotalcubicM", pTotalcubicM);

            }
            catch (Exception)
            {

            }
            return myAnalysis;
        }
        #endregion

        #region II Normalization

        public void InitFlowData(MonitoringDataDTO[] dt, MonitoringDataDTO[] rg, decimal flowInt, double interDryPeriod, List<DateTime>[] stormList, ref Periods[] dryFlowArray, ref Periods[] flowArray, DateTime startDate, DateTime endDate, out int numDays, out int numPeriods)
        {
            int currDay = 0;
            int currPeriod = 0;
            Boolean isNewDay = false;
            int tempDay;
            DateTime currDate = default(DateTime);
            Decimal currVal = 0;
            DateValueLine line = new DateValueLine();
            DateValueLine tempLine = new DateValueLine();
            //DataRow row = default(DataRow);

            flowArray = new Periods[1];
            dryFlowArray = new Periods[1];
            //initilize Array;
            for (int k = 0; k < flowArray.Length; k++)
            {
                flowArray[k] = new Periods();
                dryFlowArray[k] = new Periods();
            }
            numDays = 0;
            numPeriods = 0;
            List<Periods> tempList = new List<Periods>();

            //Loop through all the flow sensors selected.

            numDays = Convert.ToInt32(Math.Ceiling((endDate - startDate).TotalDays));

            if ((endDate - startDate).Minutes == 0 & (endDate - startDate).Hours == 0)
            {
                //if the start and end times of the site are the same we need to add ana extra day because there
                //will be one entry in the array
                numDays = numDays + 1;
            }

            numPeriods = Convert.ToInt32(Math.Floor(1440 / (double)flowInt));
            //Matrix to store values.

            for (int i = 0; i < dt.Length; i++)
            {
                currDate = dt[i].TimeStamp;
                currVal = dt[i].Value;

                currDay = Convert.ToInt32(Math.Floor((currDate - startDate).TotalDays));

                if (currDay == 310)
                {
                    //...I dont know
                }
                currPeriod = Convert.ToInt32(currDate.TimeOfDay.TotalMinutes / (double)flowInt);
                var pair = new TimeValue();
                var tempPair = new TimeValue();

                pair.Value = currVal;
                pair.Timestamp = currDate;
                line.Pairs.Add(pair);

                tempLine.Pairs.Add(tempPair); // Add empty temp pair to the list.

                //To Avoid index out of bounds.
                if (i + 1 >= dt.Length)
                {
                    tempDay = currDate.Day;
                }
                else
                {
                    var tempDate = dt[i + 1].TimeStamp;
                    tempDay = tempDate.Day;
                }

                //Checkk if there is a new Day.
                if (tempDay != currDate.Day)
                    isNewDay = true;

                if (isNewDay || (i == dt.Length - 1))
                {
                    dryFlowArray[0].Lines.Add(tempLine); // Add an empty line.

                    flowArray[0].Lines.Add(line);
                    line = new DateValueLine(); // Initilize the new line    
                    tempLine = new DateValueLine();

                    isNewDay = false;
                }
                else
                {
                    continue;
                }

            }


            List<DateTime> arrayLTimeP = new List<DateTime>();
            List<double> arrayLValueP = new List<double>();
            List<DateTime> at_copy = new List<DateTime>();
            List<double> av_copy = new List<double>();

            PrecipitationAlign(rg, ref arrayLTimeP, ref arrayLValueP, PRECIP_INTERVAL * 60, startDate, endDate);

            for (int i = 0; i < arrayLTimeP.Count; i++)
                at_copy.Add(arrayLTimeP.ElementAt(i));

            for (int i = 0; i < arrayLValueP.Count; i++)
                av_copy.Add(arrayLValueP.ElementAt(i));
            var stormListArray = new DateTime[stormList.Length][];
            for (var q = 0; q < stormList.Length; q++)
            {
                stormListArray[q] = stormList[q].ToArray();
            }

            //Populate dry weather flow
            for (currDay = 0; currDay <= numDays - 1; currDay++)
            {
                for (currPeriod = 0; currPeriod <= numPeriods - 1; currPeriod++)
                {
                    if (currDay > flowArray[0].Lines.Count - 1)
                        break;
                    if (currPeriod > flowArray[0].Lines.ElementAt(currDay).Pairs.Count - 1)
                        break;
                    if (flowArray[0].Lines.ElementAt(currDay).Pairs.ElementAt(currPeriod).Timestamp == null || flowArray[0].Lines.ElementAt(currDay).Pairs.ElementAt(currPeriod).Value == 0)
                        continue;

                    if (!stormy(flowArray[0].Lines.ElementAt(currDay).Pairs.ElementAt(currPeriod).Timestamp, stormListArray, interDryPeriod))
                    {
                        dryFlowArray[0].Lines.ElementAt(currDay).Pairs.ElementAt(currPeriod).Value = flowArray[0].Lines.ElementAt(currDay).Pairs.ElementAt(currPeriod).Value;
                        dryFlowArray[0].Lines.ElementAt(currDay).Pairs.ElementAt(currPeriod).Timestamp = flowArray[0].Lines.ElementAt(currDay).Pairs.ElementAt(currPeriod).Timestamp;
                    }
                }
            }
        }



        public void InitFlowData(MonitoringDataDTO[] dt, MonitoringDataDTO[] rg, double interDryPeriod, double minStormSize, ref List<DateTime>[] stormList, DateTime startDate, DateTime endDate)
        {

            // Align precipitation and populate "stormList"
            List<DateTime> arrayLTimeP = new List<DateTime>();
            List<double> arrayLValueP = new List<double>();
            List<DateTime> at_copy = new List<DateTime>();
            List<double> av_copy = new List<double>();
            var sensorId = dt[0].MonitoringSensorId;
            var sensor = model.MonitoringSensors.Include("MonitoringStation.TimeZone").Where(item => item.Id == sensorId).FirstOrDefault();
            var timeZoneOffset = TimeZoneInfo.FindSystemTimeZoneById(sensor.MonitoringStation.TimeZone.TZICode).BaseUtcOffset;
            PrecipitationAlign(rg, ref arrayLTimeP, ref arrayLValueP, PRECIP_INTERVAL * 60, startDate, endDate);

            for (int i = 0; i < arrayLTimeP.Count; i++)
                at_copy.Add(arrayLTimeP.ElementAt(i));

            for (int i = 0; i < arrayLValueP.Count; i++)
                av_copy.Add(arrayLValueP.ElementAt(i));

            stormList = StormSeparate(at_copy, av_copy, interDryPeriod);
           
         



        }


        public void GetValueDynamic(ref Dictionary<String, List<Decimal>> mValueDynamic, List<DateTime>[] stormList, int stationId, decimal catchment, decimal Tconc)
        {

            List<Decimal> stationTc = new List<Decimal>();
            List<Decimal> stationDa = new List<Decimal>();

            if (stationId == 0)
                return;
            int n_Station = stationId;
            var tcNum = model.MonitoringStationTCs.Where(x => x.MonitoringStationId == n_Station);
            var daNum = model.MonitoringStationCatchmentAreas.Where(x => x.MonitoringStationId == n_Station);
            var stationtc = model.MonitoringStationTCs.Where(x => x.MonitoringStationId == n_Station).OrderByDescending(x => x.TimeStamp);
            var stationCatchmentArea = model.MonitoringStationCatchmentAreas.Where(x => x.MonitoringStationId == n_Station).OrderByDescending(x => x.TimeStamp);
            foreach (DateTime storm in stormList.First())
            {
                //If there is only 1 recoreded TC or Catchment Area, simply use that area for all timestamps.
                //If the Tc is inputed by the user, use that value over all others.
                if (tcNum.Count() == 1)
                {
                    if (tcNum.First().TC != Tconc)
                        stationTc.Add(Tconc);
                    else
                        stationTc.Add(tcNum.First().TC);
                }
                else
                {
                    var tc = stationtc.Where(x => x.TimeStamp <= storm).FirstOrDefault();
                    if (tc != null)
                    {
                        if (tc.TC != Tconc)
                            stationTc.Add(Tconc);
                        else
                            stationTc.Add(tc.TC);
                    }
                    else
                    {
                        stationTc.Add(Tconc);
                    }
                }
                //Same applies for Da
                if (daNum.Count() == 1)
                {
                    if (daNum.First().CatchmentArea != catchment)
                        stationDa.Add(catchment);
                    else
                    {
                        if (daNum.First().UnitId != null)
                        {
                            var tempCatch = daNum.First().CatchmentArea * GetConversionFactor(daNum.First().UnitId.Value, (int)Civica.Infrastructure.BaseClasses.BaseEnum.DCStandardUnitId.Area);
                            stationDa.Add(tempCatch);
                        }
                        else
                        {
                            stationDa.Add(daNum.First().CatchmentArea);
                        }
                    }
                }
                else
                {
                    var da = stationCatchmentArea.Where(x => x.TimeStamp < storm).FirstOrDefault();
                    if (da != null)
                    {
                        if (da.CatchmentArea != catchment)
                            stationDa.Add(catchment);
                        else
                        {
                            if (da.UnitId != null)
                            {
                                var tempCatch = da.CatchmentArea * GetConversionFactor(da.UnitId.Value, (int)Civica.Infrastructure.BaseClasses.BaseEnum.DCStandardUnitId.Area);
                                stationDa.Add(tempCatch);
                            }
                            else
                            {
                                stationDa.Add(da.CatchmentArea);
                            }
                        }
                    }
                    else
                    {
                        stationDa.Add(catchment);
                    }
                }

            }
            mValueDynamic.Add("tc", stationTc);
            mValueDynamic.Add("da", stationDa);
        }
        /// <summary>
        /// Get dynamic constants value for each storm event
        /// </summary>
        /// <remarks></remarks>
        public void GetValueDynamic(ref List<Dictionary<String, List<Decimal>>> mValueDynamic, Dictionary<int, List<DateTime>[]> stormList, int[] stationId, decimal[] catchment, decimal[] Tconc)
        {

            List<Decimal>[] stationTc = new List<Decimal>[stormList.Count];
            List<Decimal>[] stationDa = new List<Decimal>[stormList.Count];

            if (stationId == null || stationId.Length <= 0)
                return;

            for (int j = 0; j < stationId.Length; j++) //Station Id has the same number of elemnts as Tc and Catchments
            {
                stationTc[j] = new List<Decimal>();
                stationDa[j] = new List<Decimal>();
                mValueDynamic.Insert(j, new Dictionary<string, List<decimal>>());
                int n_Station = stationId[j];
                var tcNum = model.MonitoringStationTCs.Where(x => x.MonitoringStationId == n_Station).OrderByDescending(x => x.TimeStamp);
                var daNum = model.MonitoringStationCatchmentAreas.Where(x => x.MonitoringStationId == n_Station).OrderByDescending(x => x.TimeStamp);
                foreach (DateTime storm in stormList[j].First())
                {
                    //If there is only 1 recoreded TC or Catchment Area, simply use that area for all timestamps.
                    //If the Tc is inputed by the user, use that value over all others.
                    if (tcNum.Count() == 1)
                    {
                        if (tcNum.First().TC != Tconc[j])
                            stationTc[j].Add(Tconc[j]);
                        else
                            stationTc[j].Add(tcNum.First().TC);
                    }
                    else
                    {
                        var tc = tcNum.Where(x => x.TimeStamp <= storm).FirstOrDefault();
                        if (tc != null)
                        {
                            if (tc.TC != Tconc[j])
                                stationTc[j].Add(Tconc[j]);
                            else
                                stationTc[j].Add(tc.TC);
                        }
                        else
                        {
                            stationTc[j].Add(Tconc[j]);
                        }
                    }
                    //Same applies for Da
                    if (daNum.Count() == 1)
                    {
                        if (daNum.First().CatchmentArea != catchment[j])
                            stationDa[j].Add(catchment[j]);
                        else
                            stationDa[j].Add(daNum.First().CatchmentArea);
                    }
                    else
                    {
                        var da = daNum.Where(x => x.TimeStamp < storm).FirstOrDefault();
                        if (da != null)
                        {
                            if (da.CatchmentArea != catchment[j])
                                stationDa[j].Add(catchment[j]);
                            else
                                stationDa[j].Add(da.CatchmentArea);
                        }
                        else
                        {
                            stationDa[j].Add(catchment[j]);
                        }
                    }

                }
                mValueDynamic.ElementAt(j).Add("tc", stationTc[j]);
                mValueDynamic.ElementAt(j).Add("da", stationDa[j]);
            }
        }


        public EventStatRow UpdatePreivousEventStats(Dictionary<String, List<Decimal>> mValueDynamic, MonitoringDataDTO[] flowData, MonitoringDataDTO[] rainData, DateTime[][] stormList, int stormEventIndex, decimal flowInt, int flowId, int rainId, double interDryPeriod, double minStormSize, System.Diagnostics.EventLog log)
        {
            try
            {
                int stormIndex = stormEventIndex;
                List<EventStatRow> EventStats = new List<EventStatRow>();
                EventStatRow currStats = default(EventStatRow);

                int currFlowStartIdx = 0;

                int currFlowEndIdx = 0;

                DateTime currDate = default(DateTime);
                Decimal currVal = 0;
                Decimal currDryVal = 0;
                int i = 0;
                bool HaveFlowData = false;

                currFlowStartIdx = 0; //reset index
                DateTime[][] stormListArray = new DateTime[2][];
                stormListArray[0] = stormList[0].ToArray();
                stormListArray[1] = stormList[1].ToArray();


                currStats = new EventStatRow();
                currStats.StartDate = stormList[0][stormIndex].AddHours(-interDryPeriod).AddMinutes(-stormList[0][stormIndex].Minute).AddSeconds(-stormList[0][stormIndex].Second);
                currStats.EndDate = stormList[1][stormIndex].AddHours(interDryPeriod);
                currStats.DrainageArea = mValueDynamic["da"].ElementAt(stormIndex);//mValueDynamic("da")(stormIdx);
                                                                                   // Get the start and end row indices for the precip data tabl
                currStats.TotalPrecip = 0;
                currStats.TotalPrecip = rainData.Where(x => x.TimeStamp >= currStats.StartDate).Where(x => x.TimeStamp <= currStats.EndDate).Sum(x => x.Value);
                if (((double)currStats.TotalPrecip < minStormSize))
                {
                    return null;
                }
                bool possibleUpdate = false;

                // Get the start and end row indices for the flow data table
                HaveFlowData = false;
                i = currFlowStartIdx;


                while (i < flowData.Length)
                {
                    if (flowData[i].TimeStamp >= currStats.StartDate)
                    {
                        currFlowStartIdx = i;
                        i += 1;
                        HaveFlowData = true;
                        break; // TODO: might not be correct. Was : Exit While
                    }
                    i += 1;
                }
                if (HaveFlowData)
                {
                    i = currFlowStartIdx;
                    while ((i < flowData.Length))
                    {
                        currDate = flowData[i].TimeStamp;
                        if (currDate <= currStats.EndDate)
                        {
                            currFlowEndIdx = i;
                        }
                        else if (currDate > currStats.EndDate)
                        {
                            break; // TODO: might not be correct. Was : Exit While
                        }

                        i += 1;
                    }
                }
                if (currFlowStartIdx >= currFlowEndIdx)
                    return null;

                // find the max intensity over the display interval
                currStats.PeakIntensity = 0;
                List<DateTime> arrayLTime = new List<DateTime>();
                List<double> arrayLValue = new List<double>();

                PrecipitationAlign(rainData, ref arrayLTime, ref arrayLValue, PRECIP_INTERVAL * 60, currStats.StartDate, currStats.EndDate);

                //Calculate Peak intensity for this storm.
                var arrayValueEnumerator = arrayLValue.GetEnumerator();

                while (arrayValueEnumerator.MoveNext())
                {
                    if (arrayValueEnumerator.Current > currStats.PeakIntensity)
                    {
                        currStats.PeakIntensity = arrayValueEnumerator.Current;
                    }
                }


                // change back to mm/h
                currStats.PeakIntensity = currStats.PeakIntensity / (PRECIP_INTERVAL / 60);

                // find the max intensity over Tc
                currStats.PeakIntensityOverTc = 0;
                if (mValueDynamic["tc"].Count > 0)
                {
                    currStats.Tc = mValueDynamic["tc"].ElementAt(stormIndex);

                    if ((mValueDynamic["tc"].ElementAt(stormIndex) == PRECIP_INTERVAL))
                    {
                        currStats.PeakIntensityOverTc = currStats.PeakIntensity;
                    }
                    else
                    {
                        // get the data lumped into buckets of Tc first
                        arrayLTime = new List<DateTime>();
                        arrayLValue = new List<double>();

                        //New Way to do it, using sliding window peak, same as II Analysis TOol.
                        currStats.PeakIntensityOverTc = GetTcPrecipIntensity(rainData.ToList().Where(x => x.TimeStamp >= currStats.StartDate && x.TimeStamp <= currStats.EndDate).ToList(), Convert.ToDouble(mValueDynamic["tc"].ElementAt(stormIndex)));//GetTcPrecipIntensity(rainData[j], (int)mValueDynamic.ElementAt(j)["tc"].ElementAt(stormIndex));

                    }
                }

                // Calculate flow statistics
                if (HaveFlowData)
                {
                    List<DateTime> arrayLDryTime = new List<DateTime>();
                    List<double> arrayLDryValue = new List<double>();

                    MonitoringDataDTO[] dt_Dry;

                    if (currFlowEndIdx > currFlowStartIdx)
                    {
                        dt_Dry = Flow_Seperate(flowData, flowId, flowInt, (int)interDryPeriod, currStats.StartDate, currStats.EndDate, stormListArray, stormIndex, ref possibleUpdate, currFlowStartIdx, currFlowEndIdx);
                    }
                    else
                    {
                        dt_Dry = new MonitoringDataDTO[0];
                    }

                    //Reset values.
                    currStats.PeakIIFlow = 0;
                    currStats.VolumeOfII = 0;
                    currStats.MeasuredPeakFlow = 0;
                    currStats.EstDWFAtTd = 0;
                    decimal maxDWF = 0;

                    //var dt_DryEnumerator = dt_Dry.GetEnumerator();
                    //dt_DryEnumerator.MoveNext();
                    //int index = 0;
                    //while (index <= currFlowStartIdx)
                    //{
                    //    dt_DryEnumerator.MoveNext();
                    //    index++;
                    //}
                    int index = 0;
                    for (i = currFlowStartIdx; i <= currFlowEndIdx; i++)
                    {
                        //currRow = dtF.Rows(i);

                        currVal = flowData[i].Value;//Convert.ToDouble(currRow("value"));
                        currDate = flowData[i].TimeStamp;//Convert.ToDateTime(currRow("timestamp"));
                        currDryVal = dt_Dry[index].Value;//Convert.ToDouble(dtFdry.Rows(i)("value"));

                        if (maxDWF < currDryVal)
                            maxDWF = currDryVal;

                        //Get Measured Peak FLow
                        if (currVal > currStats.MeasuredPeakFlow)
                        {
                            currStats.MeasuredPeakFlow = currVal;
                            currStats.TimeOfPeakWWF = currDate;
                        }

                        // Get time of peak I/I flow
                        // This is the max difference between measured flow and estimated DWF
                        if ((currVal - currDryVal) > currStats.PeakIIFlow)
                        {
                            currStats.PeakIIFlow = currVal - currDryVal;
                            currStats.TimeOfPeakII = currDate;
                            currStats.EstDWFAtTd = currDryVal;
                        }

                        // Calculate total I/I Volume (m3)
                        currStats.VolumeOfII += Math.Max(0, currVal - currDryVal) * ((decimal)flowInt * 60) / 1000;

                        index++;
                        //dt_DryEnumerator.MoveNext();
                    }
                    //Peak II / DWF
                    if (currStats.VolumeOfII != 0)
                        currStats.PeakIIOverPeakDWF = currStats.PeakIIFlow / maxDWF;
                    else
                        currStats.PeakIIOverPeakDWF = 0;

                    // Peak I/I Rate
                    currStats.PeakIIRate = currStats.PeakIIFlow / mValueDynamic["da"].ElementAt(stormIndex);//mValueDynamic("da")(stormIdx);

                    // Volume of Precip (in m3)
                    Decimal VolumeOfPrecip = currStats.TotalPrecip * mValueDynamic["da"].ElementAt(stormIndex) * 10000 / 1000;

                    // Volumetric Runoff I/I Coefficient
                    currStats.VolumetricRunoffCoef = currStats.VolumeOfII / VolumeOfPrecip;

                    if (currStats.PeakIntensityOverTc != 0)
                        currStats.PeakIiCoef = (currStats.PeakIIFlow * 3600 / 1000) / (Decimal)(currStats.PeakIntensityOverTc * (double)(mValueDynamic["da"].ElementAt(stormIndex)) * 10000 / 1000);
                    else
                        currStats.PeakIiCoef = 0;


                }
                else
                {
                    // no flow data - set a flag
                    currStats.PeakIIFlow = -1;
                }

                currStats.TotalPrecip = Math.Round(currStats.TotalPrecip, 2);
                currStats.PeakIntensity = Math.Round(currStats.PeakIntensity, 2);
                currStats.PeakIntensityOverTc = Math.Round(currStats.PeakIntensityOverTc, 2);
                currStats.PeakIIFlow = Math.Round(currStats.PeakIIFlow, 2);
                currStats.PeakIIRate = Math.Round(currStats.PeakIIRate, 3);
                currStats.VolumeOfII = Math.Round(currStats.VolumeOfII, 2);
                currStats.PeakIiCoef = Math.Round(currStats.PeakIiCoef, 5);
                currStats.VolumetricRunoffCoef = Math.Round(currStats.VolumetricRunoffCoef, 5);
                currStats.PossibleUpdate = possibleUpdate;
                //MISSING ONE EVENT FOR SOME REASON....
                return currStats;

            }
            catch (Exception ex)
            {
                log.WriteEntry("Exception : " + ex.Message);
            }
            return null;


        }

        public List<EventStatRow> CalculateEventStats(Dictionary<String, List<Decimal>> mValueDynamic, MonitoringDataDTO[] flowData, MonitoringDataDTO[] rainData, DateTime[][] stormList, decimal flowInt, int flowId, int rainId, double interDryPeriod, double minStormSize, System.Diagnostics.EventLog log)
        {
            List<EventStatRow> EventStats = new List<EventStatRow>();
            try
            {
                int stormIndex = 0;

                EventStatRow currStats = default(EventStatRow);

                int currFlowStartIdx = 0;

                int currFlowEndIdx = 0;

                DateTime currDate = default(DateTime);
                Decimal currVal = 0;
                Decimal currDryVal = 0;
                int i = 0;
                bool HaveFlowData = false;

                currFlowStartIdx = 0; //reset index
                DateTime[][] stormListArray = new DateTime[2][];
                stormListArray[0] = stormList[0].ToArray();
                stormListArray[1] = stormList[1].ToArray();

                for (stormIndex = 0; stormIndex <= stormList.First().Length - 1; stormIndex++)
                {
                    currStats = new EventStatRow();
                    currStats.StartDate = stormList[0][stormIndex].AddHours(-interDryPeriod).AddMinutes(-stormList[0][stormIndex].Minute).AddSeconds(-stormList[0][stormIndex].Second);
                    currStats.EndDate = stormList[1][stormIndex].AddHours(interDryPeriod);
                    currStats.DrainageArea = mValueDynamic["da"].ElementAt(stormIndex);//mValueDynamic("da")(stormIdx);
                                                                                       // Get the start and end row indices for the precip data tabl
                    currStats.TotalPrecip = 0;
                    currStats.TotalPrecip = rainData.Where(x => x.TimeStamp >= currStats.StartDate).Where(x => x.TimeStamp <= currStats.EndDate).Sum(x => x.Value);
                    bool possibleUpdate = false;
                    if (((double)currStats.TotalPrecip < minStormSize))
                    {
                        continue;
                    }

                    // Get the start and end row indices for the flow data table
                    HaveFlowData = false;
                    i = currFlowStartIdx;


                    while (i < flowData.Length)
                    {
                        if (flowData[i].TimeStamp >= currStats.StartDate)
                        {
                            currFlowStartIdx = i;
                            i += 1;
                            HaveFlowData = true;
                            break; // TODO: might not be correct. Was : Exit While
                        }
                        i += 1;
                    }
                    if (HaveFlowData)
                    {
                        i = currFlowStartIdx;
                        while ((i < flowData.Length))
                        {
                            currDate = flowData[i].TimeStamp;
                            if (currDate <= currStats.EndDate)
                            {
                                currFlowEndIdx = i;
                            }
                            else if (currDate > currStats.EndDate)
                            {
                                break; // TODO: might not be correct. Was : Exit While
                            }

                            i += 1;
                        }
                    }
                    else
                    {
                        // we have no flow data for this storm
                        continue;
                    }

                    //There is no flow data for this storm event.
                    if (currFlowStartIdx == currFlowEndIdx)
                        continue;

                    // find the max intensity over the display interval
                    currStats.PeakIntensity = 0;
                    List<DateTime> arrayLTime = new List<DateTime>();
                    List<double> arrayLValue = new List<double>();

                    PrecipitationAlign(rainData, ref arrayLTime, ref arrayLValue, PRECIP_INTERVAL * 60, currStats.StartDate, currStats.EndDate);

                    //Calculate Peak intensity for this storm.
                    var arrayValueEnumerator = arrayLValue.GetEnumerator();

                    while (arrayValueEnumerator.MoveNext())
                    {
                        if (arrayValueEnumerator.Current > currStats.PeakIntensity)
                        {
                            currStats.PeakIntensity = arrayValueEnumerator.Current;
                        }
                    }


                    // change back to mm/h
                    currStats.PeakIntensity = currStats.PeakIntensity / (PRECIP_INTERVAL / 60);

                    // find the max intensity over Tc
                    currStats.PeakIntensityOverTc = 0;
                    if (mValueDynamic["tc"].Count > 0)
                    {
                        currStats.Tc = mValueDynamic["tc"].ElementAt(stormIndex);

                        if ((mValueDynamic["tc"].ElementAt(stormIndex) == PRECIP_INTERVAL))
                        {
                            currStats.PeakIntensityOverTc = currStats.PeakIntensity;
                        }
                        else
                        {
                            // get the data lumped into buckets of Tc first
                            arrayLTime = new List<DateTime>();
                            arrayLValue = new List<double>();

                            //New Way to do it, using sliding window peak, same as II Analysis TOol.
                            currStats.PeakIntensityOverTc = GetTcPrecipIntensity(rainData.ToList().Where(x => x.TimeStamp >= currStats.StartDate && x.TimeStamp <= currStats.EndDate).ToList(), Convert.ToDouble(mValueDynamic["tc"].ElementAt(stormIndex)));//GetTcPrecipIntensity(rainData[j], (int)mValueDynamic.ElementAt(j)["tc"].ElementAt(stormIndex));

                        }
                    }

                    // Calculate flow statistics
                    if (HaveFlowData)
                    {
                        List<DateTime> arrayLDryTime = new List<DateTime>();
                        List<double> arrayLDryValue = new List<double>();

                        MonitoringDataDTO[] dt_Dry;

                        if (currFlowEndIdx > currFlowStartIdx)
                        {
                            dt_Dry = Flow_Seperate(flowData, flowId, flowInt, (int)interDryPeriod, currStats.StartDate, currStats.EndDate, stormListArray, stormIndex, ref possibleUpdate, currFlowStartIdx, currFlowEndIdx);
                        }
                        else
                        {
                            dt_Dry = new MonitoringDataDTO[0];
                        }

                        //Reset values.
                        currStats.PeakIIFlow = 0;
                        currStats.VolumeOfII = 0;
                        currStats.MeasuredPeakFlow = 0;
                        currStats.EstDWFAtTd = 0;
                        decimal maxDWF = 0;

                        //var dt_DryEnumerator = dt_Dry.GetEnumerator();
                        //dt_DryEnumerator.MoveNext();
                        //int index = 0;
                        //while (index <= currFlowStartIdx)
                        //{
                        //    dt_DryEnumerator.MoveNext();
                        //    index++;
                        //}
                        int index = 0;
                        for (i = currFlowStartIdx; i <= currFlowEndIdx; i++)
                        {
                            //currRow = dtF.Rows(i);

                            currVal = flowData[i].Value;//Convert.ToDouble(currRow("value"));
                            currDate = flowData[i].TimeStamp;//Convert.ToDateTime(currRow("timestamp"));
                            currDryVal = dt_Dry[index].Value;//Convert.ToDouble(dtFdry.Rows(i)("value"));

                            if (maxDWF < currDryVal)
                                maxDWF = currDryVal;

                            //Get Measured Peak FLow
                            if (currVal > currStats.MeasuredPeakFlow)
                            {
                                currStats.MeasuredPeakFlow = currVal;
                                currStats.TimeOfPeakWWF = currDate;
                            }

                            // Get time of peak I/I flow
                            // This is the max difference between measured flow and estimated DWF
                            if ((currVal - currDryVal) > currStats.PeakIIFlow)
                            {
                                currStats.PeakIIFlow = currVal - currDryVal;
                                currStats.TimeOfPeakII = currDate;
                                currStats.EstDWFAtTd = currDryVal;
                            }

                            // Calculate total I/I Volume (m3)
                            currStats.VolumeOfII += Math.Max(0, currVal - currDryVal) * ((decimal)flowInt * 60) / 1000;

                            index++;
                            //dt_DryEnumerator.MoveNext();
                        }
                        //Peak II / DWF
                        if (currStats.VolumeOfII != 0)
                            currStats.PeakIIOverPeakDWF = currStats.PeakIIFlow / maxDWF;
                        else
                            currStats.PeakIIOverPeakDWF = 0;

                        // Peak I/I Rate
                        currStats.PeakIIRate = currStats.PeakIIFlow / mValueDynamic["da"].ElementAt(stormIndex);//mValueDynamic("da")(stormIdx);

                        // Volume of Precip (in m3)
                        Decimal VolumeOfPrecip = currStats.TotalPrecip * mValueDynamic["da"].ElementAt(stormIndex) * 10000 / 1000;

                        // Volumetric Runoff I/I Coefficient
                        currStats.VolumetricRunoffCoef = currStats.VolumeOfII / VolumeOfPrecip;

                        if (currStats.PeakIntensityOverTc != 0)
                            currStats.PeakIiCoef = (currStats.PeakIIFlow * 3600 / 1000) / (Decimal)(currStats.PeakIntensityOverTc * (double)(mValueDynamic["da"].ElementAt(stormIndex)) * 10000 / 1000);
                        else
                            currStats.PeakIiCoef = 0;


                    }
                    else
                    {
                        // no flow data - set a flag
                        currStats.PeakIIFlow = -1;
                    }

                    currStats.TotalPrecip = Math.Round(currStats.TotalPrecip, 2);
                    currStats.PeakIntensity = Math.Round(currStats.PeakIntensity, 2);
                    currStats.PeakIntensityOverTc = Math.Round(currStats.PeakIntensityOverTc, 2);
                    currStats.PeakIIFlow = Math.Round(currStats.PeakIIFlow, 2);
                    currStats.PeakIIRate = Math.Round(currStats.PeakIIRate, 3);
                    currStats.VolumeOfII = Math.Round(currStats.VolumeOfII, 2);
                    currStats.PeakIiCoef = Math.Round(currStats.PeakIiCoef, 5);
                    currStats.VolumetricRunoffCoef = Math.Round(currStats.VolumetricRunoffCoef, 5);
                    currStats.PossibleUpdate = possibleUpdate;
                    //MISSING ONE EVENT FOR SOME REASON....
                    EventStats.Add(currStats);
                }

            }
            catch (Exception ex)
            {
                log.WriteEntry("Exception : " + ex.Message);
            }
            return EventStats;
        }




        /// <summary>
        /// Calculate the best fitted linear curve and plot on the chart
        /// Also called RenderEqtTbl to list the equations in a table
        /// </summary>
        /// <remarks></remarks>
        public double PlotValue(List<EventStatRow> events, Dictionary<int, Dictionary<int, Decimal?>> designStorms, decimal tc, int returnIntervalIndex,int axis ,int plotType)
        {

            bool Lin, Log;
            List<SeriesPoints> currentSeries = new List<SeriesPoints>(); // Reset the current Series
            Dictionary<String, List<String>> FittedEqts = new Dictionary<String, List<string>>();

            SeriesPoints p;

            //Based on the axis selected, calculate teh series points to be used.
            //Also calculate the max_Series value.
            var eventsEnumerator = events.GetEnumerator();
            if (axis == 5)
            {
                while (eventsEnumerator.MoveNext())
                {
                    
                        p = new SeriesPoints();
                        p.xValue = (Decimal)eventsEnumerator.Current.PeakIntensityOverTc;
                        p.yValue = eventsEnumerator.Current.PeakIIRate;
                        //Check the max x value            
                        currentSeries.Add(p);
                }
            }
            else
            {
                if (axis == 4)
                {
                    while (eventsEnumerator.MoveNext())
                    {
                        p = new SeriesPoints();
                        p.xValue = (Decimal)eventsEnumerator.Current.PeakIntensityOverTc;
                        p.yValue = eventsEnumerator.Current.PeakIIFlow;
                        currentSeries.Add(p);
                    }
                }

            }
            //for (int i = 0; i < events.Count; i++)
            //{
            //    p = new SeriesPoints();
            //    p.xValue = (Decimal)events.ElementAt(i).PeakIntensityOverTc;
            //    p.yValue = events.ElementAt(i).PeakIIRate;
            //    //Check the max x value            
            //    currentSeries.Add(p);
            //}
            if (plotType == 1)
            {
                Lin = true;
                Log = false;
            }
            else
            {
                Lin = false;
                Log = true;
            }

            double[] parameters = GetCurveFitParameters(currentSeries, Lin);

            double x = 0;
            double y = 0;
            // populate the fitted line and the predicted values from design storms

            x = (double)InterpolateDesignStorm(designStorms[RETURN_PERIODS[returnIntervalIndex]], tc);
            if (plotType == 1)
            {
                y = EvaluateLinearEqt(x, Math.Round(parameters[0], 3), Math.Round(parameters[1], 3), "y");
            }
            else
            {
                y = EvaluateLinearEqt(Math.Log(x), Math.Round(parameters[0], 3), Math.Round(parameters[1], 3), "y");
            }

            return y;
        }



        /// <summary>
        /// Calculate the best fitted linear curve and plot on the chart
        /// Also called RenderEqtTbl to list the equations in a table
        /// </summary>
        /// <remarks></remarks>
        public Dictionary<String, List<String>> PlotFittedCurve(ref List<SeriesPoints> s_design, List<EventStatRow> events, Dictionary<string, Dictionary<int, Dictionary<int, Decimal?>>> designStorms, string designStormNames, decimal tc, int axis, bool linear)
        {
            bool addedDesign = false;
            double max_design = 0;
            double max_x = 0;
            Decimal max_series = 0;
            int seriesCount = 0;
            int j = 0;
            bool Lin;

            List<SeriesPoints> s_fit = new List<SeriesPoints>();
            s_design = new List<SeriesPoints>();
            List<SeriesPoints> currentSeries;
            Dictionary<String, List<String>> FittedEqts = new Dictionary<String, List<string>>();
            SeriesPoints p;

            Lin = linear;

            seriesCount = 0; // Reset count.
            currentSeries = new List<SeriesPoints>(); // Reset the current Series.
            addedDesign = false;
            //Based on the axis selected, calculate teh series points to be used.
            //Also calculate the max_Series value.
            var eventsEnumerator = events.GetEnumerator();
            while (eventsEnumerator.MoveNext())
            {
                p = new SeriesPoints();
                if (axis < 4)
                {// Total Precip X-Axis
                    if (axis == 0)
                    { //PkIIFlow

                        p.xValue = eventsEnumerator.Current.TotalPrecip;
                        p.yValue = eventsEnumerator.Current.PeakIIFlow;


                        //Check the max x value
                        if (max_series < eventsEnumerator.Current.TotalPrecip)
                            max_series = eventsEnumerator.Current.TotalPrecip;

                    }
                    else if (axis == 1)
                    { //PKIIRate

                        p.xValue = eventsEnumerator.Current.TotalPrecip;
                        p.yValue = eventsEnumerator.Current.PeakIIRate;
                        //Check the max x value
                        if (max_series < eventsEnumerator.Current.TotalPrecip)
                            max_series = eventsEnumerator.Current.TotalPrecip;

                    }
                    else if (axis == 2)
                    { // IIVolume
                        p.xValue = eventsEnumerator.Current.TotalPrecip;
                        p.yValue = eventsEnumerator.Current.VolumeOfII;
                        //Check the max x value
                        if (max_series < eventsEnumerator.Current.TotalPrecip)
                            max_series = eventsEnumerator.Current.TotalPrecip;

                    }
                    else if (axis == 3)
                    { // VolCoef
                        p.xValue = eventsEnumerator.Current.TotalPrecip;
                        p.yValue = eventsEnumerator.Current.VolumetricRunoffCoef;
                        //Check the max x value
                        if (max_series < eventsEnumerator.Current.TotalPrecip)
                            max_series = eventsEnumerator.Current.TotalPrecip;

                    }
                }
                else
                { // Peak Precip Intensity
                    if (axis == 4)
                    { //PkIIFlow
                        p.xValue = (Decimal)eventsEnumerator.Current.PeakIntensityOverTc;
                        p.yValue = eventsEnumerator.Current.PeakIIFlow;
                        //Check the max x value
                        if (max_series < (Decimal)eventsEnumerator.Current.PeakIntensityOverTc)
                            max_series = (Decimal)eventsEnumerator.Current.PeakIntensityOverTc;

                    }
                    else if (axis == 5)
                    { //PKIIRate
                        p.xValue = (Decimal)eventsEnumerator.Current.PeakIntensityOverTc;
                        p.yValue = eventsEnumerator.Current.PeakIIRate;
                        //Check the max x value
                        if (max_series < (Decimal)eventsEnumerator.Current.PeakIntensityOverTc)
                            max_series = (Decimal)eventsEnumerator.Current.PeakIntensityOverTc;

                    }
                    else if (axis == 6)
                    { // IIVolume
                        p.xValue = (Decimal)eventsEnumerator.Current.PeakIntensityOverTc;
                        p.yValue = eventsEnumerator.Current.VolumeOfII;

                        //Check the max x value
                        if (max_series < (Decimal)eventsEnumerator.Current.PeakIntensityOverTc)
                            max_series = (Decimal)eventsEnumerator.Current.PeakIntensityOverTc;

                    }
                    else if (axis == 7)
                    { // VolCoef
                        p.xValue = (Decimal)eventsEnumerator.Current.PeakIntensityOverTc;
                        p.yValue = eventsEnumerator.Current.VolumetricRunoffCoef;

                        //Check the max x value
                        if (max_series < (Decimal)eventsEnumerator.Current.PeakIntensityOverTc)
                            max_series = (Decimal)eventsEnumerator.Current.PeakIntensityOverTc;
                    }
                }
                currentSeries.Add(p);

            }


            if (designStorms != null && designStorms.Count > 0)
            {
                //Go through the design storm names, dont need second if statement bloc like in old code.
                for (int n = 0; n < designStormNames.Length; n++)
                {
                    for (j = RETURN_PERIODS.Length - 1; j >= 0; j += -1)
                    {
                        try
                        {
                            if (designStorms[designStormNames].ContainsKey(RETURN_PERIODS[j]))
                            {
                                decimal currMaxD = InterpolateDesignStorm(designStorms[designStormNames][RETURN_PERIODS[j]], tc);
                                if (max_design < (double)currMaxD)
                                {
                                    max_design = (double)currMaxD;
                                }
                                break; // TODO: might not be correct. Was : Exit For
                            }
                        }
                        catch (KeyNotFoundException e)
                        {
                            Console.Write("Exception: " + e);
                            return null;
                        }
                    }
                }
            }


            if ((double)max_series > max_design)
            {
                if ((double)max_series * 0.1 > 10)
                {
                    max_x = (double)max_series + 10;
                }
                else
                {
                    max_x = (double)max_series * 1.1;
                }
            }
            else
            {
                max_x = max_design;
            }
            for (int cs = 0; cs < currentSeries.Count; cs++)
            {
                //Loop through the plot options (Currently only 2 (Log & Linear), need to make dynamic based on how many there are. Leaving as 2 for testing purposes)
                for (int pOp = 0; pOp < 2; pOp++)
                {
                    double[] parameters = GetCurveFitParameters(currentSeries, Lin);
                    //If neither of the options are selected, continue loop.

                    if (!FittedEqts.ContainsKey(seriesCount.ToString()))
                    {
                        FittedEqts.Add(seriesCount.ToString(), new List<string>());
                    }

                    double x = 0;
                    double y = 0;
                    // populate the fitted line and the predicted values from design storms
                    double interval = 0;
                    interval = max_x / LOG_CURVE_POINTS;

                    for (j = 1; j <= LOG_CURVE_POINTS; j++)
                    {
                        SeriesPoints tempP = new SeriesPoints();
                        x = j * interval;
                        if (Lin)
                        {
                            y = EvaluateLinearEqt(x, parameters[0], parameters[1], "y");
                        }
                        else
                        {
                            y = EvaluateLinearEqt(Math.Log(x), parameters[0], parameters[1], "y");
                        }
                        tempP.xValue = (decimal)x;
                        tempP.yValue = (decimal)y;
                        s_fit.Add(tempP);
                    }
                    // add the design storm points
                    if (designStorms != null && designStorms.Count > 0)
                    {
                        if (!addedDesign)
                        {
                            if (designStorms != null || designStorms.Count > 0)
                            {
                                //Go through the design storm names, dont need second if statement bloc like in old code.
                                for (int n = 0; n < designStormNames.Length; n++)
                                {
                                    for (int rp = 0; rp < RETURN_PERIODS.Length; rp++)
                                    {
                                        //Leave 5 for testing...
                                        //Rounding Values to 3 deicmal places
                                        x = (double)InterpolateDesignStorm(designStorms[designStormNames][RETURN_PERIODS[rp]], tc);
                                        if (Lin)
                                        {
                                            y = EvaluateLinearEqt(x, Math.Round(parameters[0], 3), Math.Round(parameters[1], 3), "y");
                                        }
                                        else
                                        {
                                            y = EvaluateLinearEqt(Math.Log(x), Math.Round(parameters[0], 3), Math.Round(parameters[1], 3), "y");
                                        }
                                        //Add the prediction points.
                                        AddPredictionPt(ref s_design, (decimal)x, (decimal)y);
                                    }
                                }

                            }
                        }
                    }
                    if (Lin)
                    {
                        // append to the end of the fitted equation list
                        if (parameters[1] >= 0)
                        {
                            if (FittedEqts[seriesCount.ToString()].Count <= 0)
                            {
                                FittedEqts[seriesCount.ToString()].Add("y = " + parameters[0].ToString("n3") + "x + " + parameters[1].ToString("n3"));
                                FittedEqts[seriesCount.ToString()].Add(parameters[0].ToString("n3") + "," + parameters[1].ToString("n3"));
                            }
                        }
                        else
                        {
                            FittedEqts[seriesCount.ToString()].Add("y = " + parameters[0].ToString("n3") + "x - " + Math.Abs(parameters[1]).ToString("n3"));
                            FittedEqts[seriesCount.ToString()].Add(parameters[0].ToString("n3") + "," + parameters[1].ToString("n3"));
                        }
                    }
                    else
                    {
                        if (parameters[1] >= 0)
                        {
                            FittedEqts[seriesCount.ToString()].Add("y = " + parameters[0].ToString("n3") + "ln(x) + " + parameters[1].ToString("n3"));
                            FittedEqts[seriesCount.ToString()].Add(parameters[0].ToString("n3") + "," + parameters[1].ToString("n3"));
                        }
                        else
                        {
                            FittedEqts[seriesCount.ToString()].Add("y = " + parameters[0].ToString("n3") + "ln(x) - " + Math.Abs(parameters[1]).ToString("n3"));
                            FittedEqts[seriesCount.ToString()].Add(parameters[0].ToString("n3") + "," + parameters[1].ToString("n3"));
                        }
                    }
                    if (Lin == true)
                    {
                        Lin = false; // Set linear to false so we dont loop through it again.
                    }

                }
                seriesCount++;
            }

            return FittedEqts;
        }

        /// <summary>
        /// load design storms from database
        /// </summary>
        /// <remarks></remarks>
        public Dictionary<int, Dictionary<int, Decimal?>> LoadDesignStorms(ref Dictionary<int, Dictionary<int, Decimal?>> designStorms, int idfSources)
        {

            designStorms = new Dictionary<int, Dictionary<int, Decimal?>>();

            if (idfSources == 0)
                return null;

            var idfDic = GetIDFCurves(idfSources);
            designStorms = idfDic;

            return designStorms;
        }

        /// <summary>
        /// load design storms from database
        /// </summary>
        /// <remarks></remarks>
        public Dictionary<string,Dictionary<int, Dictionary<int, Decimal?>>> LoadDesignStorms(ref Dictionary<string, Dictionary<int, Dictionary<int, Decimal?>>> designStorms, Dictionary<int, string> idfSources)
        {

            designStorms = new Dictionary<string, Dictionary<int, Dictionary<int, Decimal?>>>();

            if (idfSources == null || idfSources.Count <= 0)
                return null;
            for (int i = 0; i < idfSources.Count; i++)
            {
                if (!designStorms.ContainsKey(idfSources.ElementAt(i).Value))
                {
                    designStorms.Add(idfSources.ElementAt(i).Value, new Dictionary<int, Dictionary<int, Decimal?>>());
                }
            }

            for (int i = 0; i < designStorms.Count; i++)
            {
                var idfDic = GetIDFCurves(idfSources.ElementAt(i).Key);
                designStorms[idfSources.ElementAt(i).Value] = idfDic;
            }

            return designStorms;
        }
        #endregion

        #region Sanitary Report
        public void CalculateFlowStats(ref Dictionary<string, FlowStatRow> FlowStats, Periods[] flowArray, Periods[] dryFlowArray, int numDays, int numPeriods, int population, int GWISelect, double GWIEst, decimal catchArea, out Nullable<double>[] flowAvgDayArray)
        {

            CalculateQ(ref FlowStats, flowArray, numDays, numPeriods, population);
            CalculateDWF(ref FlowStats, dryFlowArray, numDays, numPeriods, population, out flowAvgDayArray);
            CalculateMin_DWF(ref FlowStats, dryFlowArray, numDays, numPeriods);
            CalculateGWI(ref FlowStats, GWISelect, GWIEst, (double)catchArea);
            CalculatePop_DWF(ref FlowStats, population);
            CalculatePeak_DWF(ref FlowStats, dryFlowArray, numDays, numPeriods);
            CalculateHarmon(ref FlowStats);
            CalculateTheo_Harmon(ref FlowStats, population);
            CalculateHCF(ref FlowStats);

            double peakWWf = 0;
            //Peak WWF is the max value of the flow series in the main chart
            for (int i = 0; i < flowArray[0].Lines.Count; i++)
            {
                for (int j = 0; j < flowArray[0].Lines.ElementAt(i).Pairs.Count; j++)
                    if (peakWWf <= (double)flowArray[0].Lines.ElementAt(i).Pairs.ElementAt(j).Value)
                        peakWWf = (double)flowArray[0].Lines.ElementAt(i).Pairs.ElementAt(j).Value;
            }

            FlowStats["Peak_WWF"].Average = peakWWf;
            FlowStats["Peak_WWF"].Normalized = FlowStats["Peak_WWF"].Average.Value / (double)catchArea;

        }

        public Dictionary<string, List<Point>> PopulateAvgDwfChart(Nullable<double>[] flowAvgDayArray, Periods[] flowDwfArray, double mConfBand, double flowTimeInt, int numDays, int numPeriods)
        {
            Dictionary<string, List<Point>> series = new Dictionary<string, List<Point>>();
            List<Point> avgFlowSeries = new List<Point>();
            List<Point> avgFlowRangeTop = new List<Point>();
            List<Point> avgFlowRangeBottom = new List<Point>();

            for (int i = 0; i < numPeriods; i++)
            {
                avgFlowSeries.Insert(i, new Point(i * flowTimeInt / 60, flowAvgDayArray[i].Value));
            }
            series["avgFlowSeries"] = avgFlowSeries;

            var InverseCDF = SNormInv(mConfBand / 100);

            for (int currPeriod = 0; currPeriod < numPeriods; currPeriod++)
            {
                if (!flowAvgDayArray[currPeriod].HasValue)
                    continue;

                // get the standard deviation of the points for this period
                List<double> pointList = new List<double>();
                double stdDev = 0;
                double currHour = currPeriod * flowTimeInt / 60;
                for (int currDay = 0; currDay < numDays - 1; currDay++)
                {
                    if (currDay > flowDwfArray[0].Lines.Count - 1)
                    {
                        continue;
                    }

                    if (currPeriod > flowDwfArray[0].Lines.ElementAt(currDay).Pairs.Count - 1)
                    {
                        continue;
                    }

                    if (((flowDwfArray[0].Lines.ElementAt(currDay).Pairs.ElementAt(currPeriod) != null)))
                    {
                        pointList.Add((double)flowDwfArray[0].Lines.ElementAt(currDay).Pairs.ElementAt(currPeriod).Value);
                    }
                }
                stdDev = GetStandardDeviation(pointList.ToArray());

                avgFlowRangeTop.Insert(currPeriod, new Point(currHour, flowAvgDayArray[currPeriod].Value + (InverseCDF * stdDev)));
                avgFlowRangeBottom.Insert(currPeriod, new Point(currHour, flowAvgDayArray[currPeriod].Value - (InverseCDF * stdDev)));

                //avgFlowRangeTop.ElementAt(currPeriod).X = flowAvgDayArray[currPeriod].Value + (InverseCDF * stdDev);
                //avgFlowRangeBottom.ElementAt(currPeriod).Y = flowAvgDayArray[currPeriod].Value - (InverseCDF * stdDev);

            }

            series.Add("avgFlowRangeTop", avgFlowRangeTop);
            series.Add("avgFlowRangeBottom", avgFlowRangeBottom);
            return series;
        }
        #endregion


        #region mathamatical Functions
        /// <summary>
        /// Calculate average of a list, ignoring -999 values.
        /// </summary>
        /// <param name="a"></param>
        /// <returns></returns>
        private double GetAvg(double[] a)
        {
            double sum = 0;
            int i, count;
            count = 0;
            for (i = 0; i < a.Length; i++)
            {
                if (a[i] != -999)
                {
                    sum += a[i];
                    count += 1;
                }
            }
            return sum / count;
        }

        private double[] GetCurveFitParameters(List<SeriesPoints> s, bool linear)
        {
            double avg_x = 0;
            double avg_y = 0;
            double sum_x = 0;
            double sum_y = 0;
            double sum_x2 = 0;
            double sum_xy = 0;
            double sum_lnx = 0;
            double sum_lnx2 = 0;
            double sum_ylnx = 0;
            int count = 0;
            //Dundas.Charting.WebControl.DataPoint p = default(Dundas.Charting.WebControl.DataPoint);

            // initialize variables
            sum_x = 0;
            sum_y = 0;
            sum_x2 = 0;
            sum_xy = 0;
            sum_lnx = 0;
            sum_lnx2 = 0;
            sum_ylnx = 0;
            // get the fitted line equation parameters
            count = s.Count;
            double[] parameters = new double[2];
            // y = mx + b
            if (linear)
            {
                for (int i = 0; i < s.Count; i++)
                {
                    sum_x += (double)s[i].xValue;
                    sum_y += (double)s[i].yValue;
                    sum_x2 += (double)(s[i].xValue * s[i].xValue);
                    sum_xy += (double)(s[i].xValue * s[i].yValue);
                }
                avg_x = sum_x / count;
                avg_y = sum_y / count;

                if (count == 1)
                {
                    parameters[0] = sum_y / sum_x; // m
                    var m = sum_y / sum_x;
                    parameters[1] = sum_y - (m * sum_x); // y = mx + b => b = y - mx
                }
                else
                {
                    parameters[0] = (sum_xy - count * avg_x * avg_y) / (sum_x2 - count * avg_x * avg_x);// m

                    parameters[1] = (avg_y * sum_x2 - avg_x * sum_xy) / (sum_x2 - count * avg_x * avg_x);// b
                }
            }
            else
            {
                for (int i = 0; i < s.Count; i++)
                {
                    sum_y += (double)s[i].yValue;
                    sum_lnx += Math.Log((double)s[i].xValue);
                    sum_lnx2 += Math.Log((double)s[i].xValue) * Math.Log((double)s[i].xValue);
                    sum_ylnx += (double)s[i].yValue * Math.Log((double)s[i].xValue);
                }

                parameters[0] = (count * sum_ylnx - sum_y * sum_lnx) / (count * sum_lnx2 - sum_lnx * sum_lnx);
                // m
                parameters[1] = (sum_y - parameters[0] * sum_lnx) / count;
                // b

            }
            return parameters;
        }

        /// <summary>
        /// Evaluate linear function in the form of y = mx + b
        /// </summary>
        /// <param name="val">x/y value</param>
        /// <param name="m"></param>
        /// <param name="b"></param>
        /// <param name="solveFor">"x" - solve for x, val = y; "y" - solve for y, val = x</param>
        /// <returns>solved value</returns>
        /// <remarks></remarks>
        private double EvaluateLinearEqt(double val, double m, double b, string solveFor)
        {
            double res = 0;
            if (solveFor == "x")
            {
                res = (val - b) / m;
            }
            else
            {
                res = m * val + b;
            }
            return res;
        }

        /// <summary>
        /// Noise filtering/clearning for Data
        /// </summary>
        /// <param name="noisy"></param>
        /// <param name="range"></param>
        /// <param name="decay"></param>
        /// <returns></returns>
        public double[] CleanData(double[] noisy, int range, double decay)
        {
            double[] clean = new double[noisy.Length];
            double[] coefficients = Coefficients(range, decay);

            // Calculate divisor value.
            double divisor = 0;
            for (int i = -range; i <= range; i++)
                divisor += coefficients[Math.Abs(i)];

            // Clean main data.
            for (int i = range; i < clean.Length - range; i++)
            {
                double temp = 0;
                for (int j = -range; j <= range; j++)
                    temp += noisy[i + j] * coefficients[Math.Abs(j)];
                clean[i] = temp / divisor;
            }

            // Calculate leading and trailing slopes.
            double leadSum = 0;
            double trailSum = 0;
            int leadRef = range;
            int trailRef = clean.Length - range - 1;
            for (int i = 1; i <= range; i++)
            {
                leadSum += (clean[leadRef] - clean[leadRef + i]) / i;
                trailSum += (clean[trailRef] - clean[trailRef - i]) / i;
            }
            double leadSlope = leadSum / range;
            double trailSlope = trailSum / range;

            // Clean edges.
            for (int i = 1; i <= range; i++)
            {
                clean[leadRef - i] = clean[leadRef] + leadSlope * i;
                clean[trailRef + i] = clean[trailRef] + trailSlope * i;
            }
            return clean;
        }

        static private double[] Coefficients(int range, double decay)
        {
            // Precalculate coefficients.
            double[] coefficients = new double[range + 1];
            for (int i = 0; i <= range; i++)
                coefficients[i] = Math.Pow(decay, i);
            return coefficients;
        }
        #endregion


        /// <summary>
        /// 
        /// </summary>
        /// <param name="stationId"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public IEnumerable<SensorDTO> GetMonitoringStationSensorsByStationAdmin(int stationId)
        {
            var offsidesensors = model.MonitoringStationsOffSiteSensors.Where(x => x.Active == true && x.MonitoringStationId == stationId).Select(x => x.MonitoringSensorId);

            var sensors = model.MonitoringSensors.Where(x => x.Active == true).Where(x => offsidesensors.Contains(x.Id)).ToList().Select(x => SensorDTO.Create(x));

            var onSiteSensors = model.MonitoringSensors.Where(x => x.Active == true).Where(x => x.MonitorStationId == stationId).Select(x => SensorDTO.Create(x));

            var allSensors = sensors.Union(onSiteSensors);
            allSensors = allSensors.Distinct();

            return allSensors;
        }

        public IIAnalysisSummaryStationSetting GetIIAnalysisSummaryStationSettingByStationId(int stationId)
        {
            var IIAnalysisSummaryStationSettings = model.IIAnalysisSummaryStationSettings.Where(x => x.Active == true).Where(x => x.StationId == stationId);
            if (IIAnalysisSummaryStationSettings.Count() > 0)
                return IIAnalysisSummaryStationSettings.FirstOrDefault();
            else
                return null;
        }

        public List<MonitoringSensor> GetStationSensors(int stationId)
        {
            var offsidesensors = model.MonitoringStationsOffSiteSensors.Where(x => x.Active == true && x.MonitoringStationId == stationId).Select(x => x.MonitoringSensorId);

            var sensors = model.MonitoringSensors.Where(x => x.Active == true).Where(x => offsidesensors.Contains(x.Id)).ToList();

            var onSiteSensors = model.MonitoringSensors.Where(x => x.Active == true).Where(x => x.MonitorStationId == stationId).ToList();
            var allSensors = onSiteSensors.Union(sensors);
            return allSensors.ToList();
        }

        public DateTime GetSensorMaximumTime(int sensorId)
        {
            var maximumTime = model.MonitoringDatas.Where(x => x.MonitoringSensorId == sensorId).OrderByDescending(x => x.TimeStamp).Select(x => x.TimeStamp).FirstOrDefault();
            if (maximumTime != null)
            {
                return maximumTime.UtcDateTime;
            }

            else
                return DateTime.MinValue;
        }

        public IIAnalysisSummaryStationResult GetIIAnalysisSummaryStationResultByStationId(int stationId)
        {
            var analysisSummaryStationResults = model.IIAnalysisSummaryStationResults.Where(x => x.Active == true).Where(x => x.StationId == stationId);
            if (analysisSummaryStationResults.Count() > 0)
                return analysisSummaryStationResults.FirstOrDefault();
            else
                return null;
        }

        public decimal GetFlowSensorInt(int flowSensorId)
        {
            var flowSensor = model.MonitoringSensors.Where(x => x.Id == flowSensorId).FirstOrDefault();
            if (flowSensor != null)
            {
                var flowSensorInterval = flowSensor.TimeInterval;
                if (flowSensorInterval != null)
                    return flowSensorInterval.Value;
                else
                    return 0;
            }
            else
                return 0;
        }

        public IEnumerable<MonitoringStationTCDTO> GetMonitoringStationTcsByStationId(int stationId)
        {

            var result = new List<MonitoringStationTCDTO>();
            var tc = model.MonitoringStationTCs.Where(x => x.MonitoringStationId == stationId && x.Active == true);

            if (tc != null)
            {

                foreach (var t in tc)
                {
                    var r = new MonitoringStationTCDTO();
                    r.Active = t.Active;
                    r.Id = t.Id;
                    r.TC = t.TC;
                    r.TimeStamp = t.TimeStamp;
                    r.MonitoringStationId = t.MonitoringStationId;
                    result.Add(r);
                }

            }

            return result;
        }


        public IEnumerable<MonitoringStationCatchmentAreaDTO> GetMonitoringStationCatchmentAreaByStationId(int stationId)
        {

            var result = new List<MonitoringStationCatchmentAreaDTO>();
            var ca = model.MonitoringStationCatchmentAreas.Where(x => x.MonitoringStationId == stationId && x.Active == true);

            if (ca != null)
            {

                foreach (var t in ca)
                {
                    var r = new MonitoringStationCatchmentAreaDTO();
                    r.Active = t.Active;
                    r.Id = t.Id;
                    r.CatchmentArea = Math.Round(t.CatchmentArea, 2);
                    r.TimeStamp = t.TimeStamp;
                    r.MonitoringStationId = t.MonitoringStationId;
                    r.UnitId = t.UnitId;
                    if (t.UnitId != 0 && t.UnitId != null)
                    {
                        r.UnitSymbol = t.Unit.UnitSymbol;
                    }
                    else
                    {
                        r.UnitSymbol = "ha";
                    }
                    result.Add(r);
                }

            }

            return result;
        }

        public double? GetStationBoundaryArea(int stationId)
        {
            var stationBoundary = model.MonitoringStations.Where(x => x.Id == stationId).FirstOrDefault().Boundary;
            if (stationBoundary != null)
                return stationBoundary.Area;
            return null;
        }

        public List<int> GetMonitoringStationPopulations(int stationId)
        {
            var monitoringStationPopulations = model.MonitoringStationPopulations.Where(x => x.MonitoringStationId == stationId).Where(x => x.Active == true).OrderByDescending(x => x.TimeStamp).Select(x => x.Population).ToList();
            return monitoringStationPopulations;
        }

        public bool CheckMonitoringStationHasNewManulData(int stationId)
        {
            var monitoringtasks = model.MonitoringTasks.Where(x => x.MonitoringStationId == stationId).Where(x => x.Active == true).Where(x => x.DataStartDate != null).Where(x => x.DataSEndDate != null);
            if (monitoringtasks.Count() > 0)
                return true;
            else
                return false;
        }

        public List<MonitoringAnalysisTaskDTO> GetMonitoringTasksHasNewData(int sensorId, DateTimeOffset lastRunCurrentTime)
        {
            var stationId = model.MonitoringSensors.Where(x => x.Id == sensorId).FirstOrDefault().MonitorStationId;
            List<MonitoringAnalysisTaskDTO> taskList = new List<MonitoringAnalysisTaskDTO>();
            var monitoringtasks = model.MonitoringTasks.Where(x => x.MonitoringStationId == stationId).Where(x => x.Active == true).Where(x => x.DataStartDate != null).Where(x => x.DataSEndDate != null).Where(x => x.Modified >= lastRunCurrentTime);
            var stationTimeZoneoffset = GetMonitoringStationTimeZoneSpanByStationId(stationId);
            if (monitoringtasks.Count() > 0)
            {
                foreach (var monitoringTask in monitoringtasks)
                {
                    MonitoringAnalysisTaskDTO task = new MonitoringAnalysisTaskDTO();
                    task.StationId = monitoringTask.MonitoringStationId;
                    task.DataStartTime = monitoringTask.DataStartDate.Value.ToOffset(stationTimeZoneoffset).DateTime;
                    task.DataEndTime = monitoringTask.DataSEndDate.Value.ToOffset(stationTimeZoneoffset).DateTime;
                    taskList.Add(task);
                }
            }
            return taskList;
        }

        public List<EventStatRow> GetIIAnlysisPreiousEventStatRowByStationId(int stationId)
        {
            var eventStatsList = model.IIAnalysisSummaryEventStats.Where(x => x.StationId == stationId).Where(x => x.Active == true).ToList();
            var eventStatRowList = new List<EventStatRow>();
            foreach (var eventStats in eventStatsList)
            {
                eventStatRowList.Add(EventStatRow.Create(eventStats));
            }
            return eventStatRowList;
        }



        public TimeSpan GetMonitoringStationTimeZoneSpanByStationId(int stationId)
        {
            var station = model.MonitoringStations.Where(x => x.Id == stationId).FirstOrDefault();
            var timeZoneOffset = TimeZoneInfo.FindSystemTimeZoneById(station.TimeZone.TZICode).BaseUtcOffset;
            return timeZoneOffset;
        }
        // public DateTime GetLast

        public IIAnalysisStationSettingDTO GetCurrentIIAnalysisSummaryStationSetting(int stationId)
        {
            var stationSetting = new IIAnalysisStationSettingDTO();
            var IIAnalysisSummaryStationSetting = model.IIAnalysisSummaryStationSettings.Where(x => x.StationId == stationId).Where(x => x.Active == true).FirstOrDefault();
            if (IIAnalysisSummaryStationSetting != null)
            {
                stationSetting.Id = IIAnalysisSummaryStationSetting.Id;
                stationSetting.StationId = IIAnalysisSummaryStationSetting.StationId;
                stationSetting.FlowSensorId = IIAnalysisSummaryStationSetting.FlowSensorId;
                stationSetting.RainGuageSensorId = IIAnalysisSummaryStationSetting.RainGaugeSensorId;
                stationSetting.DrainageArea = IIAnalysisSummaryStationSetting.DraianageArea;
                stationSetting.tc = IIAnalysisSummaryStationSetting.TC;
                stationSetting.Population = IIAnalysisSummaryStationSetting.Population;
                stationSetting.MinumStormSize = IIAnalysisSummaryStationSetting.IIAnalysisSummaryProjectSetting.MinStromSize;
                stationSetting.InterDryPeriod = IIAnalysisSummaryStationSetting.IIAnalysisSummaryProjectSetting.InterDryPeriod;
                stationSetting.DesignStromId = IIAnalysisSummaryStationSetting.IIAnalysisSummaryProjectSetting.IDFSourceID;
                stationSetting.isLeaner = IIAnalysisSummaryStationSetting.IIAnalysisSummaryProjectSetting.IsLinear;
                stationSetting.MinumNumberOfEvent = IIAnalysisSummaryStationSetting.IIAnalysisSummaryProjectSetting.MinNoEvents;
                stationSetting.MinumRSquare = Decimal.ToDouble(IIAnalysisSummaryStationSetting.IIAnalysisSummaryProjectSetting.MinRSquare);
                stationSetting.returnInterval = Convert.ToInt32(IIAnalysisSummaryStationSetting.IIAnalysisSummaryProjectSetting.ReturnPeriodId);
                return stationSetting;
            }
            return null;


        }

        public IIAnalysisStationSettingDTO GetMostRecentHistoryIIAnalysisSummaryStationSetting(int stationId)
        {
            var stationSetting = new IIAnalysisStationSettingDTO();
            var IIAnalysisSummaryStationSettings = model.IIAnalysisSummaryStationSettings_History.Where(x => x.StationId == stationId).Where(x => x.Active == true);
            
            if (IIAnalysisSummaryStationSettings.Count() > 0)
            {
                var iiAnalysisSummaryStationSettingsettingList = IIAnalysisSummaryStationSettings.OrderBy(x => x.Date).ToList();
                 var iiAnalysisSummaryStationSetting = iiAnalysisSummaryStationSettingsettingList.LastOrDefault();

                stationSetting.Id = iiAnalysisSummaryStationSetting.Id;
                stationSetting.StationId = iiAnalysisSummaryStationSetting.StationId;
                stationSetting.FlowSensorId = iiAnalysisSummaryStationSetting.FlowSensorId;
                stationSetting.RainGuageSensorId = iiAnalysisSummaryStationSetting.RainGaugeSensorId;
                stationSetting.DrainageArea = iiAnalysisSummaryStationSetting.DraianageArea;
                stationSetting.tc = iiAnalysisSummaryStationSetting.TC;
                stationSetting.Population = iiAnalysisSummaryStationSetting.Population;
                var projectSettingHistory = model.IIAnalysisSummaryProjectSettings_History.Where(x => x.Id == iiAnalysisSummaryStationSetting.ProjectSettingId).Where(x => x.Active == true);
                if (projectSettingHistory.Count() > 0)
                {
                    var projectSetting = projectSettingHistory.LastOrDefault();
                    stationSetting.MinumStormSize = projectSetting.MinStromSize;
                    stationSetting.InterDryPeriod = projectSetting.InterDryPeriod;
                }
                else
                {
                    var projectSetting = model.IIAnalysisSummaryProjectSettings.Where(x => x.Id == iiAnalysisSummaryStationSetting.ProjectSettingId).Where(x => x.Active == true).FirstOrDefault();
                    stationSetting.MinumStormSize = projectSetting.MinStromSize;
                    stationSetting.InterDryPeriod = projectSetting.InterDryPeriod;
                }
               
                return stationSetting;
            }
            return null;

        }

        public void UpdateIIAnalysisummaryFlowStats(Dictionary<string, FlowStatRow> FlowStats, int stationId,int projectId ,int serviceId,double? IIPeakRate, double? IIPeakFlow, DateTimeOffset lastRun, DateTimeOffset lastRunStormStartTime ,decimal DrainAgeArea, decimal TC, decimal Population, int rainGaugeSensorId, int flowSensorId, decimal minStormSize,decimal interDryPeriod)
        {
            try
            {
                var stationResult = model.IIAnalysisSummaryStationResults.Where(x => x.StationId == stationId).Where(x => x.Active == true).FirstOrDefault();
                if (stationResult != null)
                {
                    var newStationResult = new IIAnalysisSummaryStationResults_History();
                    newStationResult.Id = stationResult.Id;
                    newStationResult.ProjectId = stationResult.ProjectId;
                    newStationResult.ServiceId = stationResult.ServiceId;
                    newStationResult.StationId = stationResult.StationId;
                    newStationResult.ProjectedPeakIIFlow = stationResult.ProjectedPeakIIFlow;
                    newStationResult.ProjectedPeakIIRate = stationResult.ProjectedPeakIIRate;
                    newStationResult.AvgFlow = stationResult.AvgFlow;
                    newStationResult.AvgFlowRate = stationResult.AvgFlowRate;
                    newStationResult.AvgDryWeatherFlow = stationResult.AvgDryWeatherFlow;
                    newStationResult.AvgDryWeatherFlowRate = stationResult.AvgDryWeatherFlowRate;
                    newStationResult.AvgpopulationDWF = stationResult.AvgpopulationDWF;
                    newStationResult.AvgpopulationDWFRate = stationResult.AvgpopulationDWFRate;
                    newStationResult.AvgdailypeakDWF = stationResult.AvgdailypeakDWF;
                    newStationResult.AvgdailypeakDWFRate = stationResult.AvgdailypeakDWFRate;
                    newStationResult.GroundwaterInfiltration = stationResult.GroundwaterInfiltration;
                    newStationResult.GroundwaterInfiltrationRate = stationResult.GroundwaterInfiltrationRate;
                    newStationResult.MeasuredHarmonpeakfactor = stationResult.MeasuredHarmonpeakfactor;
                    newStationResult.MeasuredHarmonpeakfactorRate = stationResult.MeasuredHarmonpeakfactorRate;
                    newStationResult.TheoreticalHarmonpeakfactor = stationResult.TheoreticalHarmonpeakfactor;
                    newStationResult.TheoreticalHarmonpeakfactorRate = stationResult.TheoreticalHarmonpeakfactorRate;
                    newStationResult.Harmoncorrectionfactor = stationResult.Harmoncorrectionfactor;
                    newStationResult.HarmoncorrectionfactorRate = stationResult.HarmoncorrectionfactorRate;
                    newStationResult.Peakwetweatherflow = stationResult.Peakwetweatherflow;
                    newStationResult.PeakwetweatherflowRate = stationResult.PeakwetweatherflowRate;
                    newStationResult.ObservedPeakII = stationResult.ObservedPeakII;
                    newStationResult.ObservedPeakRateIIRate = stationResult.ObservedPeakIIRate;
                    newStationResult.Peakpercentageofprecipitationinsanitary = stationResult.Peakpercentageofprecipitationinsanitary;
                    newStationResult.LastRun = stationResult.LastRun;
                    newStationResult.Active = stationResult.Active;
                    newStationResult.AvgDailyMinDWF = stationResult.AvgDailyMinDWF;
                    newStationResult.AvgDailyMinDWFRate = stationResult.AvgDailyMinDWFRate;
                    newStationResult.DrainageArea = stationResult.DrainageArea;
                    newStationResult.DrainageArea = stationResult.DrainageArea;
                    newStationResult.Population = stationResult.Population;
                    newStationResult.TC = stationResult.TC;
                    newStationResult.LastRunCurrentTime = stationResult.LastRunCurrentTime;
                    newStationResult.LastRunStormEventStartTime = stationResult.LastRunStormEventStartTime;
                    newStationResult.Active = stationResult.Active;
                    newStationResult.FlowSensorId = stationResult.FlowSensorId;
                    newStationResult.RainGaugeSensorId = stationResult.RainGaugeSensorId;
                    newStationResult.InterDryPeriod = stationResult.InterDryPeriod;
                    newStationResult.MinStormSize = stationResult.MinStormSize;
                    model.IIAnalysisSummaryStationResults_History.Add(newStationResult);
                    model.IIAnalysisSummaryStationResults.Remove(stationResult);
                }
                var IIAnalysisstationResult = new IIAnalysisSummaryStationResult();
                IIAnalysisstationResult.ProjectId = projectId;
                IIAnalysisstationResult.ServiceId = serviceId;
                IIAnalysisstationResult.StationId = stationId;
                if (FlowStats["Q"].Average != null)
                    IIAnalysisstationResult.AvgFlow = Convert.ToDecimal(FlowStats["Q"].Average.Value);
                else
                    IIAnalysisstationResult.AvgFlow = null;
                if (FlowStats["Q"].Normalized != null)
                    IIAnalysisstationResult.AvgFlowRate = Convert.ToDecimal(FlowStats["Q"].Normalized.Value);
                else
                    IIAnalysisstationResult.AvgFlowRate = null;
                if (FlowStats["DWF"].Average != null)
                    IIAnalysisstationResult.AvgDryWeatherFlow = Convert.ToDecimal(FlowStats["DWF"].Average.Value);
                else
                    IIAnalysisstationResult.AvgDryWeatherFlow = null;
                if (FlowStats["DWF"].Normalized != null)
                    IIAnalysisstationResult.AvgDryWeatherFlowRate = Convert.ToDecimal(FlowStats["DWF"].Normalized.Value);
                else
                    IIAnalysisstationResult.AvgDryWeatherFlowRate = null;
                if (FlowStats["Pop_DWF"].Average != null)
                    IIAnalysisstationResult.AvgpopulationDWF = Convert.ToDecimal(FlowStats["Pop_DWF"].Average.Value);
                else
                    IIAnalysisstationResult.AvgpopulationDWF = null;
                if (FlowStats["Pop_DWF"].Normalized != null)
                    IIAnalysisstationResult.AvgpopulationDWFRate = Convert.ToDecimal(FlowStats["Pop_DWF"].Normalized.Value);
                else
                    IIAnalysisstationResult.AvgpopulationDWFRate = null;
                if (FlowStats["Min_DWF"].Average != null)
                    IIAnalysisstationResult.AvgDailyMinDWF = Convert.ToDecimal(FlowStats["Min_DWF"].Average.Value);
                else
                    IIAnalysisstationResult.AvgDailyMinDWF = null;
                if (FlowStats["Min_DWF"].Normalized != null)
                    IIAnalysisstationResult.AvgDailyMinDWFRate = Convert.ToDecimal(FlowStats["Min_DWF"].Normalized.Value);
                else
                    IIAnalysisstationResult.AvgDailyMinDWFRate = null;
                if (FlowStats["Peak_DWF"].Average != null)
                    IIAnalysisstationResult.AvgdailypeakDWF = Convert.ToDecimal(FlowStats["Peak_DWF"].Average.Value);
                else
                    IIAnalysisstationResult.AvgdailypeakDWF = null;
                if (FlowStats["Peak_DWF"].Normalized != null)
                    IIAnalysisstationResult.AvgDailyMinDWFRate = Convert.ToDecimal(FlowStats["Peak_DWF"].Normalized.Value);
                else
                    IIAnalysisstationResult.AvgDailyMinDWFRate = null;
                if (FlowStats["GWI"].Average != null)
                    IIAnalysisstationResult.GroundwaterInfiltration = Convert.ToDecimal(FlowStats["GWI"].Average.Value);
                else
                    IIAnalysisstationResult.GroundwaterInfiltration = null;
                if (FlowStats["GWI"].Normalized != null)
                    IIAnalysisstationResult.GroundwaterInfiltrationRate = Convert.ToDecimal(FlowStats["GWI"].Normalized.Value);
                else
                    IIAnalysisstationResult.GroundwaterInfiltrationRate = null;
                if (FlowStats["Harmon"].Average != null)
                    IIAnalysisstationResult.MeasuredHarmonpeakfactor = Convert.ToDecimal(FlowStats["Harmon"].Average.Value);
                else
                    IIAnalysisstationResult.MeasuredHarmonpeakfactor = null;
                if (FlowStats["Harmon"].Normalized != null)
                    IIAnalysisstationResult.MeasuredHarmonpeakfactorRate = Convert.ToDecimal(FlowStats["Harmon"].Normalized.Value);
                else
                    IIAnalysisstationResult.MeasuredHarmonpeakfactorRate = null;
                if (FlowStats["Theo_Harmon"].Average != null)
                    IIAnalysisstationResult.TheoreticalHarmonpeakfactor = Convert.ToDecimal(FlowStats["Theo_Harmon"].Average.Value);
                else
                    IIAnalysisstationResult.TheoreticalHarmonpeakfactor = null;
                if (FlowStats["Theo_Harmon"].Normalized != null)
                    IIAnalysisstationResult.TheoreticalHarmonpeakfactorRate = Convert.ToDecimal(FlowStats["Theo_Harmon"].Normalized.Value);
                else
                    IIAnalysisstationResult.TheoreticalHarmonpeakfactorRate = null;
                if (FlowStats["HCF"].Average != null)
                    IIAnalysisstationResult.Harmoncorrectionfactor = Convert.ToDecimal(FlowStats["HCF"].Average.Value);
                else
                    IIAnalysisstationResult.Harmoncorrectionfactor = null;
                if (FlowStats["HCF"].Normalized != null)
                    IIAnalysisstationResult.HarmoncorrectionfactorRate = Convert.ToDecimal(FlowStats["HCF"].Normalized.Value);
                else
                    IIAnalysisstationResult.HarmoncorrectionfactorRate = null;
                if (FlowStats["Peak_WWF"].Average != null)
                    IIAnalysisstationResult.Peakwetweatherflow = Convert.ToDecimal(FlowStats["Peak_WWF"].Average.Value);
                else
                    IIAnalysisstationResult.Peakwetweatherflow = null;
                if (FlowStats["Peak_WWF"].Normalized != null)
                    IIAnalysisstationResult.PeakwetweatherflowRate = Convert.ToDecimal(FlowStats["Peak_WWF"].Normalized.Value);
                else
                    IIAnalysisstationResult.PeakwetweatherflowRate = null;
                if (FlowStats["Peak_II"].Average != null)
                    IIAnalysisstationResult.ObservedPeakII = Convert.ToDecimal(FlowStats["Peak_II"].Average.Value);
                else
                    IIAnalysisstationResult.ObservedPeakII = null;
                if (FlowStats["Peak_II"].Normalized != null)
                    IIAnalysisstationResult.ObservedPeakIIRate = Convert.ToDecimal(FlowStats["Peak_II"].Normalized.Value);
                else
                    IIAnalysisstationResult.ObservedPeakIIRate = null;
                if (FlowStats["II%"].Average != null)
                    IIAnalysisstationResult.Peakpercentageofprecipitationinsanitary = Convert.ToDecimal(FlowStats["II%"].Average.Value);
                else
                    IIAnalysisstationResult.Peakpercentageofprecipitationinsanitary = null;
                if (IIPeakFlow != null)
                    IIAnalysisstationResult.ProjectedPeakIIFlow = Convert.ToDecimal(IIPeakFlow.Value);
                else
                    IIAnalysisstationResult.ProjectedPeakIIFlow = null;
                if (IIPeakRate != null)
                    IIAnalysisstationResult.ProjectedPeakIIRate = Convert.ToDecimal(IIPeakRate.Value);
                else
                    IIAnalysisstationResult.ProjectedPeakIIRate = null;
                IIAnalysisstationResult.Active = true;
                IIAnalysisstationResult.DrainageArea = DrainAgeArea;
                IIAnalysisstationResult.TC = TC;
                IIAnalysisstationResult.Population = Population;
                IIAnalysisstationResult.LastRun = lastRun;
                IIAnalysisstationResult.LastRunCurrentTime = DateTimeOffset.Now;
                IIAnalysisstationResult.LastRunStormEventStartTime = lastRunStormStartTime;
                IIAnalysisstationResult.FlowSensorId = flowSensorId;
                IIAnalysisstationResult.RainGaugeSensorId = rainGaugeSensorId;
                IIAnalysisstationResult.InterDryPeriod = interDryPeriod;
                IIAnalysisstationResult.MinStormSize = minStormSize;
                model.IIAnalysisSummaryStationResults.Add(IIAnalysisstationResult);
                model.SaveChanges();

            }
            catch (Exception ex)
            {
               
            }
      

        }

        public List<DateTime>[] GetPreviousStormEvent(int stationId)
        {
            var stormList = new List<DateTime>[2];
            for (var i = 0; i < stormList.Length; i++)
            {
                stormList[i] = new List<DateTime>();
            }
            try
            {
                var storms = model.IIAnalysisSummaryStormEvents.Where(x => x.StationId == stationId).Where(x => x.Active == true).ToList();
                foreach (var storm in storms)
                {
                    stormList[0].Add(storm.StartDate);
                    stormList[1].Add(storm.EndDate);
                }
                return stormList;
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        public void UpdateStormEvents(List<DateTime>[] stormList, int stationId)
        {

            string BIConString = ConfigurationManager.ConnectionStrings["LocalSQL"].ConnectionString;
            try
            {
                using (SqlConnection dbConnection = new SqlConnection(BIConString))
                {
                    dbConnection.Open();
                    using (var command = new SqlCommand("SP_UpateIIAnalysisSummaryStorm", dbConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        var eventTable = new DataTable();
                        eventTable.Columns.Add("StationId", typeof(int));
                        eventTable.Columns.Add("StartDate", typeof(DateTime));
                        eventTable.Columns.Add("EndDate", typeof(DateTime));
                        for (var i = 0; i <　stormList[0].Count; i++)
                        {
                            var datarow = eventTable.NewRow();
                            datarow["StationId"] = stationId;
                            datarow["StartDate"] = stormList[0][i];
                            datarow["EndDate"] = stormList[1][i];
                            eventTable.Rows.Add(datarow);
                        }
                    
                        var eventStatsPararms = new SqlParameter("@StormEvent", SqlDbType.Structured);
                        eventStatsPararms.TypeName = "IIAnalysisSummaryStormEventTable";
                        eventStatsPararms.Value = eventTable;
                        command.Parameters.Add(eventStatsPararms);
                        command.Parameters.Add(new SqlParameter("@StationId", stationId));
                        command.ExecuteNonQuery();


                    }
                }
            }
            catch (Exception ex)
            {

            }
        }

        public void UpdateEventStats(List<EventStatRow> eventStats, int stationId)
        {

            string BIConString = ConfigurationManager.ConnectionStrings["LocalSQL"].ConnectionString;
            try
            {
                using (SqlConnection dbConnection = new SqlConnection(BIConString))
                {
                    dbConnection.Open();
                    using (var command = new SqlCommand("SP_UpateIIAnalysisSummaryEventStats", dbConnection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        var eventStatsTable = new DataTable();
                        eventStatsTable.Columns.Add("StationId", typeof(int));
                        eventStatsTable.Columns.Add("StartDate", typeof(DateTime));
                        eventStatsTable.Columns.Add("EndDate", typeof(DateTime));
                        eventStatsTable.Columns.Add("TotalPercip", typeof(Decimal));
                        eventStatsTable.Columns.Add("PeakIntensity", typeof(Decimal));
                        eventStatsTable.Columns.Add("PeakIntensityOverTc", typeof(Decimal));
                        eventStatsTable.Columns.Add("PeakIIFlow", typeof(Decimal));
                        eventStatsTable.Columns.Add("PeakIIRate", typeof(Decimal));
                        eventStatsTable.Columns.Add("VolumeOfII", typeof(Decimal));
                        eventStatsTable.Columns.Add("VolumetricRunoffCoef", typeof(Decimal));
                        eventStatsTable.Columns.Add("PeakIICoef", typeof(Decimal));
                        eventStatsTable.Columns.Add("TimeofPeakII", typeof(DateTime));
                        eventStatsTable.Columns.Add("TimeofPeakWWF", typeof(DateTime));
                        eventStatsTable.Columns.Add("MeasuredPeakFlow", typeof(Decimal));
                        eventStatsTable.Columns.Add("EstDWFAtId", typeof(Decimal));
                        eventStatsTable.Columns.Add("PeakIIOverPeakDWF", typeof(Decimal));
                        eventStatsTable.Columns.Add("DryWeatherFlowDataPossibleUpdate", typeof(bool));
                        foreach (var eventStatRow in eventStats)
                        {
                            var datarow = eventStatsTable.NewRow();
                            datarow["StationId"] = stationId;
                            datarow["StartDate"] = eventStatRow.StartDate;
                            datarow["EndDate"] = eventStatRow.EndDate;
                            datarow["TotalPercip"] = eventStatRow.TotalPrecip;
                            datarow["PeakIntensity"] = eventStatRow.PeakIntensity;
                            datarow["PeakIntensityOverTc"] = eventStatRow.PeakIntensityOverTc;
                            datarow["PeakIIFlow"] = eventStatRow.PeakIIFlow;
                            datarow["PeakIIRate"] = eventStatRow.PeakIIRate;
                            datarow["VolumeOfII"] = eventStatRow.VolumeOfII;
                            datarow["VolumetricRunoffCoef"] = eventStatRow.VolumetricRunoffCoef;
                            datarow["PeakIICoef"] = eventStatRow.PeakIiCoef;
                            datarow["TimeofPeakII"] = eventStatRow.TimeOfPeakII;
                            datarow["TimeofPeakWWF"] = eventStatRow.TimeOfPeakWWF;
                            datarow["MeasuredPeakFlow"] = eventStatRow.MeasuredPeakFlow;
                            datarow["EstDWFAtId"] = eventStatRow.EstDWFAtTd;
                            datarow["PeakIIOverPeakDWF"] = eventStatRow.PeakIIOverPeakDWF;
                            datarow["DryWeatherFlowDataPossibleUpdate"] = eventStatRow.PossibleUpdate;
                         
                            eventStatsTable.Rows.Add(datarow);
                        }
                        var eventStatsPararms = new SqlParameter("@EventStats", SqlDbType.Structured);
                        eventStatsPararms.TypeName = "IIAnalysisSummaryEventStatsTable";
                        eventStatsPararms.Value = eventStatsTable;
                        command.Parameters.Add(eventStatsPararms);
                        command.Parameters.Add(new SqlParameter("@StationId", stationId));
                        command.ExecuteNonQuery();


                    }
                }
            }
            catch (Exception ex)
            {

            }


        }

        public int GetOrganizationIdByProjectId(int projectId)
        {
            var organizationId = model.Projects.Where(x => x.Id == projectId).FirstOrDefault().OrganizationId;
            return organizationId;
        }

        public int GetDefaultDesignStormId(int organizationId)
        {
            var designStorms = model.DesignStorms.Where(x => x.OrganizationID == organizationId);
            if (designStorms.Count() > 0)
                return designStorms.FirstOrDefault().Id;
            else
                return 0;
        }

        public string GetDesignStormNameById(int designStormId)
        {
            var designStorm = model.DesignStorms.Where(x => x.Id == designStormId).FirstOrDefault();
            if (designStorm != null)
                return designStorm.FullName;
            else
                return null;

        }

        public List<ProjectStationDTO> GetTestProjectStations()
        {
            var stations = model.MonitoringServiceStations.Where(x => x.MonitoringServiceId == 255).Where(x => x.Active == true).Select(x => x.MonitoringStation);
            var projectstationList = new List<ProjectStationDTO>();
            foreach (var station in stations)
            {
                if (station != null)
                {
                    var projectId = station.MonitoringServiceStations.FirstOrDefault().MonitoringService.ProjectService.ProjectId;
                    var projectName = station.MonitoringServiceStations.FirstOrDefault().MonitoringService.ProjectService.Project.Name;
                    var projectstationdto = new ProjectStationDTO();
                    projectstationdto.ProjectId = projectId;
                    projectstationdto.ProjectName = projectName;
                    projectstationdto.StationId = station.Id;
                    projectstationdto.StationName = station.Name;
                    projectstationList.Add(projectstationdto);

                }
            }
            return projectstationList;
        }

        public int GetServiceIdByStationId(int stationId)
        {
            var serviceId = model.MonitoringServiceStations.Where(x => x.MonitoringStationId == stationId).FirstOrDefault().MonitoringServiceId;
            return serviceId;
        }
        public List<ProjectStationDTO> GetListProjectStations()
        {
            var stations = model.MonitoringStations.Where(x => x.Active == true).ToList();
            var projectstationList = new List<ProjectStationDTO>();
            foreach (var station in stations)
            {
                if (station != null)
                {
                    var projectId = station.MonitoringServiceStations.FirstOrDefault().MonitoringService.ProjectService.ProjectId;
                    var projectName = station.MonitoringServiceStations.FirstOrDefault().MonitoringService.ProjectService.Project.Name;
                    var projectstationdto = new ProjectStationDTO();
                    projectstationdto.ProjectId = projectId;
                    projectstationdto.ProjectName = projectName;
                    projectstationdto.StationId = station.Id;
                    projectstationdto.StationName = station.Name;
                    projectstationList.Add(projectstationdto);

                }
            }
            return projectstationList;
        }

    }



}
