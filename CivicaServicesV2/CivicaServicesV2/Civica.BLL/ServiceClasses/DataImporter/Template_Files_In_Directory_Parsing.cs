﻿using System;
using System.Collections.Generic;
using System.Linq;
using Civica.BLL.BaseClasses;
using System.IO;
using NLog;
using Civica.BLL.Models;
using Civica.DBL.DBClasses;
using System.Collections;
using System.Globalization;
using Civica.Infrastructure.BaseClasses;
using Civica.BLL.CommonClasses;
using Civica.BLL.ServiceClasses.DataImporter.Interfaces;

namespace Civica.BLL.ServiceClasses.DataImporter
{

    public class Template_Files_In_Directory_Parsing : LoggerBase, ITemplate_Files_In_Directory_Parsing
    {

        private static string[] formats = new string[]
        {
            "dd/MM/yyyy h:mm:ss tt",
            "d/MM/yyyy h:mm:ss tt",
            "d/MM/yyyy HH:mm:ss"
        };

        public bool StartTemplate(string path, ref DCLogger logger)
        {
            var t = true;
            var ttime = string.Empty;
            string _monitorid = String.Empty;
            var bc = new LoggerBase();
            var processData = new DataProcessing();

            try
            {
                var res = bc.GetFilesInDirectory(path);

                if (res != null)
                {
                    if (res.Files != null)
                    {
                        if (res.Files.Count > 0)
                        {
                            foreach (var f in res.Files)
                            {
                                bool r = true;
                                var response = new ServiceResult<bool>(r);
                                string filename = String.Empty;
                                string extension = String.Empty;
                                int stationId = 0;
                                string uniqueId = String.Empty;

                                var updateTimestamps = new List<DateTimeOffset>();
                                var p = f.FilePath.LastIndexOf("\\");
                                filename = f.FilePath.Substring(p + 1);

                                var c = filename.LastIndexOf(".");
                                extension = filename.Substring(c + 1);

                                try
                                {
                                    using (var file = System.IO.File.OpenText(f.FilePath))
                                    {
                                        var name = ((FileStream)file.BaseStream).Name;

                                        // Parse file here
                                        var smapList = new List<SensorMap>();

                                        String line1 = file.ReadLine();
                                        if (line1 == null)
                                        {
                                            continue;
                                        }

                                        // Retriving the station information based on unique ID
                                        var stres = StationDBMethods.GetStationByUniqueId(_monitorid);
                                        if (stres == null)
                                        {
                                            continue;
                                        }

                                        stationId = stres.Id;

                                        var station = StationDTO.Create(stres);

                                        //Common data upload for physical and calculated sensors
                                        // need to be enclosed in as many if{} statments, as need, for checking the correctness of 
                                        // the data in the file while reading.  Here the reading of the file has tobe done and sensor maps  
                                        // and timestamp intervals collected

                                        if (response.DTO)
                                        {
                                            updateTimestamps.Sort();
                                            var ts = updateTimestamps.Distinct().ToList();
                                            var msensorupdate = processData.BulckUploadPhysicalSensors(smapList);

                                            if (!msensorupdate.DTO)
                                            {
                                                var m = String.Empty;
                                                foreach (var s in msensorupdate.ErrorResults)
                                                {
                                                    m = m + "Key : " + s.Key + " , Value : " + s.Value + ", ";
                                                }
                                                response.ErrorResults.Add("Could Not Import Data", "Problem with Builk Import : " + m);
                                                response.DTO = false;
                                            }
                                            else
                                            {
                                                var calculatedSensorsMap = new CalculatedSensorMap();

                                                calculatedSensorsMap.Sensors = station.Sensors.Where(x => x.Equations.Count() > 0).ToList();

                                                calculatedSensorsMap.TimeSeries = ts;

                                                var calsResult = processData.BulckUploadCalculatedsensors(calculatedSensorsMap);
                                                var m = String.Empty;
                                                if (!calsResult.DTO)
                                                {
                                                    m = String.Empty;
                                                    foreach (var s in calsResult.ErrorResults)
                                                    {
                                                        m = m + "Key : " + s.Key + " , Value : " + s.Value + ", ";
                                                    }
                                                    response.ErrorResults.Add("Could Not Import Data", "Problem with Builk Import Calculated Sensors : " + m);
                                                    response.DTO = false;
                                                }

                                                m = String.Empty;
                                                if (!calsResult.DTO)
                                                {

                                                    foreach (var s in calsResult.ErrorResults)
                                                    {
                                                        m = m + "Key : " + s.Key + " , Value : " + s.Value + ", ";
                                                    }
                                                    response.ErrorResults.Add("Could Not Import Data", "Problem with Builk Import Calculated Sensors : " + m);

                                                    response.DTO = false;
                                                }

                                                m = String.Empty;
                                                if (calsResult.ServiceLogs.Count > 0)
                                                {
                                                    foreach (var l in calsResult.ServiceLogs)
                                                    {
                                                        m = m + "Key : " + l.Key + " , Value : " + l.Value + ", ";
                                                    }
                                                    response.ServiceLogs.Add("Warnings while Importing", "There were problems while importing data : " + m);
                                                }

                                                if (updateTimestamps.Count() > 0)
                                                {
                                                    var updatedStation = SensorDBMethods.RecalculateStationLastTimeStamp(station.Id);
                                                }
                                            }

                                        }
                                            file.Close();
                                        } // End of   Using a specific file

                                    }
                                catch (IOException ioe)
                                {
                                    var m = ioe.Message;
                                    continue;
                                }
                                catch (Exception e)
                                {
                                    var archivepath = f.RootDirectory + "\\ErrorArchive\\" + filename;

                                    if (File.Exists(archivepath))
                                    {
                                        File.Delete(archivepath);
                                    }
                                    File.Move(f.FilePath, archivepath);
                                    ImportLog.AddLogEntry("HOBOware", "Error", stationId, filename, uniqueId, e.Message, e.StackTrace, DateTimeOffset.Now.ToUniversalTime());
                                    continue;
                                }


                                if (response.DTO)
                                {
                                    var archivepath = f.RootDirectory + "\\Archive\\" + filename;

                                    if (File.Exists(archivepath))
                                    {
                                        File.Delete(archivepath);
                                    }
                                    File.Move(f.FilePath, archivepath);
                                    if (response.ServiceLogs != null)
                                    {

                                        if (response.ServiceLogs.Count > 0)
                                        {
                                            var message = String.Empty;
                                            foreach (var ms in response.ServiceLogs)
                                            {
                                                message = message + " / " + ms.Key + " : " + ms.Value;

                                            }
                                            ImportLog.AddLogEntry("HOBOware", "Warning", stationId, filename, uniqueId, message, "", DateTimeOffset.Now.ToUniversalTime());
                                        }
                                    }
                                }
                                else
                                {
                                    var archivepath = f.RootDirectory + "\\ErrorArchive\\" + filename;

                                    if (File.Exists(archivepath))
                                    {
                                        File.Delete(archivepath);
                                    }
                                    File.Move(f.FilePath, archivepath);
                                    var message = String.Empty;
                                    if (response.ErrorResults != null)
                                    {
                                        foreach (var ms in response.ErrorResults)
                                        {
                                            message = message + " / " + ms.Key + " : " + ms.Value;

                                        }
                                    }
                                    ImportLog.AddLogEntry("HOBOware", "Error", stationId, filename, uniqueId, message, "", DateTimeOffset.Now.ToUniversalTime());
                                }

                            } // End of   foreach (var f in res.Files)
                        } // End of   if (res.Files.Count > 0)
                    } // End of   if (res.Files != null)
                } // End of   if (res != null)
            }
            catch (IOException e)
            {
                var m = e.Message;
            }
            catch (Exception e)
            {
                var m = e.Message;
            }

            return t;
        }
    }
}
