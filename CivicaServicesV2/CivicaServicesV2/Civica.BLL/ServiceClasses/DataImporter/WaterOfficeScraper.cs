﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Threading;
using NLog;
using Civica.BLL.BaseClasses;
using Civica.BLL.Models;
using Civica.DBL.DBClasses;
using Civica.Infrastructure.BaseClasses;
using Civica.BLL.CommonClasses;
using Civica.BLL.ServiceClasses.DataImporter.Interfaces;
using Civica.BLL.ServiceClasses.DataImporter;


using ScrapySharp.Core;
using ScrapySharp.Html.Parsing;
using ScrapySharp.Network;
using HtmlAgilityPack;
using ScrapySharp.Extensions;
using ScrapySharp.Html.Forms;

namespace Civica.BLL.ServiceClasses.DataImporter
{
    public class WaterOfficeScraper : LoggerBase, IWaterOfficeScraper
    {
        private static string[] formats = new string[]
       {
            "dd/MM/yyyy h:mm:ss tt",
            "d/MM/yyyy h:mm:ss tt"
       };


        public bool Transfer(ref DCLogger logger)
        {
            var ttime = string.Empty;
            var bc = new LoggerBase();

            var response = new ServiceResult<bool>(true);
            var sectionsize = 10;
            var sectioncount = 0;

            List<List<StationDTO>> stationsections = new List<List<StationDTO>>();
            List<Civica.Domain.CivicaDomain.MonitoringLoggerChannel> channels = SensorEquationDBMethods.GetMonitoringLoggerChannels((int)BaseEnum.LoggerType.WaterOfficeScraper);

            try
            {
                var stations = StationDBMethods.GetListOfStationsByLoggerType((int)BaseEnum.LoggerType.WaterOfficeScraper);

                if (stations != null)
                {
                    if (stations.Count() > 0)
                    {
                        var section = new List<StationDTO>();

                        //choping the list of stations in chuncks, so we can parallelize the process
                        foreach (var station in stations)
                        {
                            section.Add(StationDTO.Create(station));

                            if (sectioncount < sectionsize)
                            {
                                sectioncount++;
                            }
                            else
                            {
                                stationsections.Add(section);
                                section = new List<StationDTO>();
                                sectioncount = 0;
                            }
                        }

                        stationsections.Add(section);

                        if (stationsections != null)
                        {
                            if (stationsections.Count() > 0)
                            {
                                foreach (var s in stationsections)
                                {
                                    var re = ProcessSection(s, channels);
                                }
                            }
                        }
                    }
                    else
                    {
                        response.AddError("Process Error", "No Stations Found");
                    }
                }
                else
                {
                    response.AddError("Process Error", "No Stations Found");
                }

            }
            catch (Exception e)
            {
                response.AddError("System Error", e.Message);
            }
            return response.DTO;
        }
        private ServiceResult<bool> ProcessSection(List<StationDTO> section, List<Civica.Domain.CivicaDomain.MonitoringLoggerChannel> channels)
        {
            var t = true;

            double v = 0.0;
            var response = new ServiceResult<bool>(t);
            var processData = new DataProcessing();
            var updateTimestamps = new List<DateTimeOffset>();
            var aggregate = string.Empty;
            var lt = DateTime.Now;

            foreach (var station in section)
            {
                try
                {
                    var loggerId = station.Logger.UniqueId;

                    if (station != null && station.Sensors.Count > 0)
                    {
                        var smapList = new List<SensorMap>();

                        List<List<SensorData>> res = new List<List<SensorData>>();

                        foreach (var sensor in station.Sensors)
                        {
                            DateTimeOffset startDateTimeOffset = DateTime.Parse("2016-09-01 00:00:00");

                            //if there are no equations, the automated importer of data won't do anything
                            if (sensor.Equations.Count() == 0)
                            {
                                continue;
                            }

                            bool waterOfficeChannel = false;

                            //just verify that the current channel is actually a water office logger. 
                            for (int i = 0; i < channels.Count; i++)
                            {
                                if (channels[i].Channel.ToLower() == sensor.CurrentEquation.LoggerChannel.Channel.ToLower())
                                {
                                    waterOfficeChannel = true;
                                    break;
                                }
                            }

                            if (waterOfficeChannel == false)
                            {
                                //next sensor
                                continue;
                            }

                            var Lasttimestamp = SensorDBMethods.GetLastSensorTime(sensor.Id);

                            if (Lasttimestamp.DTO.HasValue && Lasttimestamp.DTO.Value > startDateTimeOffset)
                            {
                                startDateTimeOffset = Lasttimestamp.DTO.Value;
                            }

                            try
                            {
                                res = GetWebData(station.Logger.UniqueId, startDateTimeOffset);
                            }
                            catch (Exception ex)
                            {

                            }

                            var newsmap = new SensorMap();

                            double value;
                            //Create a new chanel map record
                            newsmap.Sensor = sensor;
                            newsmap.Chanel = sensor.CurrentEquation.LoggerChannel.Channel;

                            int listIndex = -1;

                            if (sensor.CurrentEquation.LoggerChannel.Channel.ToLower() == "col_1")
                            {
                                listIndex = 0;
                            }
                            else if (sensor.CurrentEquation.LoggerChannel.Channel.ToLower() == "col_2")
                            {
                                listIndex = 1;
                            }

                            foreach (var sv in res[listIndex])
                            {
                                v = sv.value;
                                var calculated = processData.EvaluateMSensor(sensor.CurrentEquation.Equation, sensor.CurrentEquation.LoggerChannel.Channel, v);
                                if (calculated != null)
                                {
                                    value = (double)calculated;
                                }
                                else
                                {
                                    value = v;
                                }

                                var dateOffset = new DateTimeOffset(sv.timestamp, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time").BaseUtcOffset);

                                newsmap.table.Rows.Add(sensor.Id, value, dateOffset, 0);
                                newsmap.tableraw.Rows.Add(sensor.Id, value, dateOffset);
                                updateTimestamps.Add(dateOffset);
                            }
                            smapList.Add(newsmap);
                        }

                        updateTimestamps.Sort();
                        var ts = updateTimestamps.Distinct().ToList();

                        var msensorupdate = processData.BulckUploadPhysicalSensors(smapList);

                        if (!msensorupdate.DTO)
                        {
                            var m = String.Empty;
                            foreach (var s in msensorupdate.ErrorResults)
                            {
                                m = m + "Key : " + s.Key + " , Value : " + s.Value + ", ";
                            }
                            response.ErrorResults.Add("Could Not Import Data for Station" + station.Id, "Problem with Builk Import : " + m);
                            response.DTO = false;

                        }
                        else
                        {
                            var calculatedSensorsMap = new CalculatedSensorMap();

                            calculatedSensorsMap.Sensors = station.Sensors.Where(x => x.Equations.Count() > 0).ToList();
                            calculatedSensorsMap.TimeSeries = ts;

                            var calsResult = processData.BulckUploadCalculatedsensors(calculatedSensorsMap);

                            var m = String.Empty;
                            if (!calsResult.DTO)
                            {

                                foreach (var s in calsResult.ErrorResults)
                                {
                                    m = m + "Key : " + s.Key + " , Value : " + s.Value + ", ";
                                }
                                response.ErrorResults.Add("Could Not Import Data for Station" + station.Id, "Problem with Builk Import Calculated Sensors : " + m);
                                response.DTO = false;
                            }

                            m = String.Empty;
                            if (calsResult.ServiceLogs.Count > 0)
                            {
                                foreach (var l in calsResult.ServiceLogs)
                                {
                                    m = m + "Key : " + l.Key + " , Value : " + l.Value + ", ";
                                }
                                response.ServiceLogs.Add("Warnings while Importing Station" + station.Id, "There were problems while importing data : " + m);
                            }
                            var updatedStation = SensorDBMethods.RecalculateStationLastTimeStamp(station.Id);
                        }
                    }
                }
        
            catch(Exception ex)
            {

            }
            }
            return response;
        }

        public static List<List<SensorData>> GetWebData(String StationId,DateTimeOffset Start)
        {
            List<List<SensorData>> resultData = new List<List<SensorData>>();

            ScrapingBrowser Browser = new ScrapingBrowser();
            Browser.AllowAutoRedirect = true; // Browser has many settings you can access in setup
            Browser.AllowMetaRedirect = true;

            //go to the home page
            WebPage PageResult = Browser.NavigateToPage(new Uri("http://wateroffice.ec.gc.ca/report/real_time_e.html?mode=Table&type=&stn=" + StationId + "&startDate=" + Start.DateTime.ToString("yyyy-MM-dd") + "& endDate=" + DateTime.Now.ToString("yyyy-MM-dd") + "&prm1=46&y1Max=&y1Min=&prm2=47&y2Max=&y2Min="));
            //WebPage PageResult = Browser.NavigateToPage(new Uri("http://wateroffice.ec.gc.ca/report/report_e.html?mode=Table&type=realTime&stn=02HB004&dataType=&startDate=2016-11-24&endDate=2016-12-01&prm1=46&prm2=47"));

            HtmlNode x = PageResult.Html.CssSelect(".btn-primary").First().ParentNode;
            PageWebForm form = new PageWebForm(PageResult.Html.CssSelect(".btn-primary").ToList()[1].ParentNode, Browser);
            form.Action = @"/disclaimer_e.html";
            form["disclaimer_action"] = "I Agree";
            form.Method = HttpVerb.Post;
            WebPage resultsPage = form.Submit();
            HtmlNode resultHTML = resultsPage.Html;

            List<SensorData> one = new List<SensorData>();
            List<SensorData> two = new List<SensorData>();

            foreach (HtmlNode table in resultHTML.SelectNodes("//tbody"))
            {
                foreach (HtmlNode row in table.SelectNodes("tr"))
                {
                    SensorData itemOne = new SensorData();
                    SensorData itemTwo = new SensorData();

                    try
                    {
                        itemOne.timestamp = DateTime.Parse(row.SelectNodes("th|td")[0].InnerText);
                        itemOne.value = Convert.ToDouble(row.SelectNodes("th|td")[1].InnerText);
                        if(itemOne.timestamp > Start.DateTime) { 
                            one.Add(itemOne);
                        }
                    }
                    catch(Exception ex)
                    {

                    }

                    try
                    {
                        itemTwo.timestamp = DateTime.Parse(row.SelectNodes("th|td")[0].InnerText);
                        itemTwo.value = Convert.ToDouble(row.SelectNodes("th|td")[2].InnerText);

                        if (itemTwo.timestamp > Start.DateTime) { 
                            two.Add(itemTwo);
                        }
                    }
                    catch(Exception ex)
                    {

                    }
                }
                resultData.Add(one);
                resultData.Add(two);
                //just do the first table, all other tables are manual measurements
                break;
            }
            return resultData;
        }
    }
}
