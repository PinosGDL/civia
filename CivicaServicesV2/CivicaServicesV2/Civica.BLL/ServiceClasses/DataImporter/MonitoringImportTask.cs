﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.BLL.BaseClasses;
using Civica.BLL.ServiceClasses.DataImporter.Interfaces;
using Civica.DBL.DBClasses;
using Civica.BLL.Models;
using NLog;
using Civica.Infrastructure.BaseClasses;
using Civica.BLL.CommonClasses;
using Civica.Domain.CivicaDomain;


namespace Civica.BLL.ServiceClasses.DataImporter
{
    public class MonitoringImportTask : LoggerBase, IMonitoringImportTask
    {

        public bool StartMonitoringImportTask(string connection, string undopath, ref DCLogger logger)
        {
            var t = true;
            var processData = new DataProcessing();
            var pendingTasks = MonitoringTaskDBMethods.GetPendingTasks();

            DateTimeOffset endTimeOffset;
            DateTimeOffset startTimeOffset;
            DateTime endTime = DateTime.Now;
            var mesg = String.Empty;

            if (pendingTasks != null)
            {
                foreach (var mtask in pendingTasks)
                {
                    var result = new FileResult();
                    mesg = String.Empty;
                    try
                    {
                        startTimeOffset = DateTimeOffset.Now.ToUniversalTime();
                        var dto = MonitoringTaskDTO.Create(mtask);
                        MonitoringStation st = null;
                        if (mtask.MonitoringStationId != null)
                            st = StationDBMethods.GetStationById(mtask.MonitoringStationId.Value);

                        if (mtask.TypeId == (int)BaseEnum.MonitoringTaskType.Delete)
                        {
                            result = this.GetTextFile(dto);
                            if ((result.Exists) && (result.Messages.Count == 0))
                            {
                                ServiceResult<MonitoringTaskStartTimeAndEndTimeDTO> res = new ServiceResult<MonitoringTaskStartTimeAndEndTimeDTO>(new MonitoringTaskStartTimeAndEndTimeDTO());
                                var delTask = new DeleteTask();
                                res = delTask.ProcessDeleteFile(result, dto);


                                if ((res.DTO.Passed) && (res.ErrorResults.Count() == 0))
                                {
                                    foreach (var ms in res.ServiceLogs)
                                    {
                                        mesg = mesg + "; " + ms;
                                    }
                                    endTimeOffset = DateTimeOffset.Now.ToUniversalTime();
                                    MonitoringTaskDBMethods.UpdateTask(mtask.Id, 3, mesg, startTimeOffset, endTimeOffset, res.DTO.StartTime, res.DTO.EndTime);
                                }
                                else if ((res.DTO.Passed) && (res.ErrorResults.Count() != 0))
                                {
                                    foreach (var ms in res.ErrorResults)
                                    {
                                        mesg = mesg + "; " + ms;
                                    }

                                    foreach (var ms in res.ServiceLogs)
                                    {
                                        mesg = mesg + "; " + ms;
                                    }
                                    endTimeOffset = DateTimeOffset.Now.ToUniversalTime();
                                    MonitoringTaskDBMethods.UpdateTask(mtask.Id, 3, mesg, startTimeOffset, endTimeOffset, res.DTO.StartTime, res.DTO.EndTime);
                                }
                                else
                                {
                                    foreach (var ms in res.ErrorResults)
                                    {
                                        mesg = mesg + "; " + ms;
                                    }

                                    foreach (var ms in res.ServiceLogs)
                                    {
                                        mesg = mesg + "; " + ms;
                                    }
                                    endTimeOffset = DateTimeOffset.Now.ToUniversalTime();
                                    MonitoringTaskDBMethods.UpdateTask(mtask.Id, 4, mesg, startTimeOffset, endTimeOffset);
                                }

                            }
                            else
                            {
                                foreach (var ms in result.Messages)
                                {
                                    mesg = mesg + "; " + ms;
                                }

                                endTimeOffset = DateTimeOffset.Now.ToUniversalTime();
                                MonitoringTaskDBMethods.UpdateTask(mtask.Id, 4, mesg, startTimeOffset, endTimeOffset);
                                logger.CreateTaskLog(LogLevel.Error, mesg, mtask.Id, mtask.CreatedBy, mtask.FilePath, mtask.MonitoringStationId, "");
                            }
                        }
                        else
                        {
                            if (mtask.TypeId != (int)BaseEnum.MonitoringTaskType.Export)
                            {
                                if (st == null)
                                {
                                    MonitoringStation inactivestation = null;
                                    if (mtask.MonitoringStationId != null)
                                        inactivestation = StationDBMethods.GetInactiveStationByStationById(mtask.MonitoringStationId.Value);

                                    if (inactivestation != null)
                                    {
                                        if (inactivestation.MaintanaceMode)
                                        {
                                            mesg = "Station ID : " + mtask.MonitoringStationId + " is in MaintanaceMode. It is not ready to receive data yet.";
                                            endTimeOffset = DateTimeOffset.Now.ToUniversalTime();
                                            MonitoringTaskDBMethods.UpdateTask(mtask.Id, 3, mesg, startTimeOffset, endTimeOffset);
                                            continue;
                                        }
                                        else
                                        {
                                            if (inactivestation.StateId != (int)BaseEnum.StationState.Receiving)
                                            {
                                                mesg = "Station ID : " + mtask.MonitoringStationId + " is not in Recieving State. It can not recieve data.";
                                                endTimeOffset = DateTimeOffset.Now.ToUniversalTime();
                                                MonitoringTaskDBMethods.UpdateTask(mtask.Id, 3, mesg, startTimeOffset, endTimeOffset);
                                                continue;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        mesg = "No station ready to recieve has been found for ID : " + mtask.MonitoringStationId + ". Check to see if the station exists or if it is in maintanace mode.";
                                        endTimeOffset = DateTimeOffset.Now.ToUniversalTime();
                                        MonitoringTaskDBMethods.UpdateTask(mtask.Id, 3, mesg, startTimeOffset, endTimeOffset);
                                        continue;
                                    }
                                }

                                var station = StationDTO.Create(st);

                                if ((mtask.FormatId == (int)BaseEnum.MonitoringTaskFormat.DataCurrent) || (mtask.FormatId == (int)BaseEnum.MonitoringTaskFormat.Delete))
                                {
                                    result = this.GetTextFile(dto);
                                }
                                else if (mtask.FormatId == (int)BaseEnum.MonitoringTaskFormat.Detec)
                                {
                                    result = this.GetFile(dto);
                                }
                                else
                                {
                                    continue;
                                }

                                if ((result.Exists) && (result.Messages.Count == 0))
                                {
                                    ServiceResult<MonitoringTaskStartTimeAndEndTimeDTO> res = new ServiceResult<MonitoringTaskStartTimeAndEndTimeDTO>(new MonitoringTaskStartTimeAndEndTimeDTO());

                                    if (mtask.FormatId == (int)BaseEnum.MonitoringTaskFormat.DataCurrent)
                                    {
                                        var civicaCsv = new CivicaCSV();
                                        res = civicaCsv.ProcessFileData(result, dto, station, undopath);
                                    }
                                    else if (mtask.FormatId == (int)BaseEnum.MonitoringTaskFormat.Detec)
                                    {
                                        var detecDat = new DetecDAT();
                                        res = detecDat.ProcessFileData(dto, station, undopath);
                                    }

                                    if ((res.DTO.Passed) && (res.ErrorResults.Count() == 0))
                                    {
                                        foreach (var ms in res.ServiceLogs)
                                        {
                                            mesg = mesg + "; " + ms;
                                        }
                                        endTimeOffset = DateTimeOffset.Now.ToUniversalTime();
                                        MonitoringTaskDBMethods.UpdateTask(mtask.Id, 3, mesg, startTimeOffset, endTimeOffset, res.DTO.StartTime, res.DTO.EndTime);
                                    }
                                    else if ((res.DTO.Passed) && (res.ErrorResults.Count() != 0))
                                    {
                                        foreach (var ms in res.ErrorResults)
                                        {
                                            mesg = mesg + "; " + ms;
                                        }

                                        foreach (var ms in res.ServiceLogs)
                                        {
                                            mesg = mesg + "; " + ms;
                                        }
                                        endTimeOffset = DateTimeOffset.Now.ToUniversalTime();
                                        MonitoringTaskDBMethods.UpdateTask(mtask.Id, 3, mesg, startTimeOffset, endTimeOffset);
                                    }
                                    else
                                    {
                                        foreach (var ms in res.ErrorResults)
                                        {
                                            mesg = mesg + "; " + ms;
                                        }

                                        foreach (var ms in res.ServiceLogs)
                                        {
                                            mesg = mesg + "; " + ms;
                                        }
                                        endTimeOffset = DateTimeOffset.Now.ToUniversalTime();
                                        MonitoringTaskDBMethods.UpdateTask(mtask.Id, 4, mesg, startTimeOffset, endTimeOffset);
                                    }
                                }
                                else
                                {
                                    foreach (var ms in result.Messages)
                                    {
                                        mesg = mesg + "; " + ms;
                                    }

                                    endTimeOffset = DateTimeOffset.Now.ToUniversalTime();
                                    MonitoringTaskDBMethods.UpdateTask(mtask.Id, 4, mesg, startTimeOffset, endTimeOffset);
                                    //  logger.CreateTaskLog(LogLevel.Error, mesg, mtask.Id, mtask.CreatedBy, mtask.FilePath, mtask.MonitoringStationId, "");
                                }
                            }
                            else
                            {

                                var serviceId = mtask.MonitoringServiceId;
                                var res = ExportTask.ExportMonitoringSerivceData(mtask.Id,mtask.FormatId,mtask.MonitoringTaskExportDataTypeId,serviceId.Value, mtask.DataStartDate, mtask.DataSEndDate);
                                if ((res.DTO) && (res.ErrorResults.Count() == 0))
                                {
                                    foreach (var ms in res.ServiceLogs)
                                    {
                                        mesg = mesg + "; " + ms;
                                    }
                                    endTimeOffset = DateTimeOffset.Now.ToUniversalTime();
                                    MonitoringTaskDBMethods.UpdateTask(mtask.Id, 3, mesg, startTimeOffset, endTimeOffset);
                                }
                                else if ((res.DTO) && (res.ErrorResults.Count() != 0))
                                {
                                    foreach (var ms in res.ErrorResults)
                                    {
                                        mesg = mesg + "; " + ms;
                                    }

                                    foreach (var ms in res.ServiceLogs)
                                    {
                                        mesg = mesg + "; " + ms;
                                    }
                                    endTimeOffset = DateTimeOffset.Now.ToUniversalTime();
                                    MonitoringTaskDBMethods.UpdateTask(mtask.Id, 3, mesg, startTimeOffset, endTimeOffset);
                                }
                                else
                                {
                                    foreach (var ms in res.ErrorResults)
                                    {
                                        mesg = mesg + "; " + ms;
                                    }

                                    foreach (var ms in res.ServiceLogs)
                                    {
                                        mesg = mesg + "; " + ms;
                                    }
                                    endTimeOffset = DateTimeOffset.Now.ToUniversalTime();
                                    MonitoringTaskDBMethods.UpdateTask(mtask.Id, 4, mesg, startTimeOffset, endTimeOffset);
                                }


                            }
                        }  // endTime of If statment for task type
                    }
                    catch (Exception e)
                    {
                        startTimeOffset = DateTimeOffset.Now.ToUniversalTime();
                        endTimeOffset = DateTimeOffset.Now.ToUniversalTime();
                        MonitoringTaskDBMethods.UpdateTask(mtask.Id, 4, e.Message, startTimeOffset, endTimeOffset);
                    }
                } //foreach (var mtask in pendingTasks)
            }
            return t;
        }

    }
}
