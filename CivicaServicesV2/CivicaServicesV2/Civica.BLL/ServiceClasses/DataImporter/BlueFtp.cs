﻿using System;
using System.Collections.Generic;
using System.Linq;
using Civica.BLL.BaseClasses;
using System.IO;
using NLog;
using Civica.BLL.Models;
using Civica.DBL.DBClasses;
using System.Collections;
using System.Globalization;
using Civica.Infrastructure.BaseClasses;
using Civica.BLL.CommonClasses;
using Civica.BLL.ServiceClasses.DataImporter.Interfaces;

namespace Civica.BLL.ServiceClasses.DataImporter
{
    public class BlueFTP : LoggerBase, IBlueFTP
    {

        private static string[] formats = new string[]
        {
            "M/d/yyyy HH:mm:ss",
            "d/MM/yyyy h:mm:ss tt",
            "d/MM/yyyy HH:mm:ss"
        };

        public bool StartBlue(string path, ref DCLogger logger)
        {
            var t = true;

            var ttime = string.Empty;
            string _monitorid = String.Empty;
            int lineCount = 0;
            var bc = new LoggerBase();
            var processData = new DataProcessing();
            var recievingpath = path;

            try
            {
                var res = bc.GetFilesInDirectory(recievingpath);

                if (res != null)
                {
                    if (res.Files != null)
                    {
                        if (res.Files.Count > 0)
                        {
                            foreach (var f in res.Files)
                            {
                                bool r = true;
                                var response = new ServiceResult<bool>(r);
                                string filename = String.Empty;
                                string extension = String.Empty;
                                int stationId = 0;
                                string uniqueId = String.Empty;

                                var filePath = f.FilePath;

                                var updateTimestamps = new List<DateTimeOffset>();
                                var p = f.FilePath.LastIndexOf("\\");

                                filename = f.FilePath.Substring(p + 1);
                                var c = filename.LastIndexOf(".");
                                extension = filename.Substring(c + 1);

                                if (extension.ToUpper() != "CSV")
                                {
                                    var archivepath = f.RootDirectory + "\\Other\\" + filename;

                                    if (File.Exists(archivepath))
                                    {
                                        File.Delete(archivepath);
                                    }
                                    File.Move(f.FilePath, archivepath);
                                    continue;
                                }

                                try
                                {
                                    var creationDate = File.GetCreationTime(f.FilePath);

                                    using (var file = System.IO.File.OpenText(f.FilePath))
                                    {
                                        var name = ((FileStream)file.BaseStream).Name;

                                        if (name.Length >= 15)
                                        {
                                            var fileNameSections = filename.Split('_');
                                            _monitorid = fileNameSections[0];

                                            uniqueId = _monitorid;

                                            String line1 = file.ReadLine();
                                            if (line1 == null)
                                            {
                                                continue;
                                            }

                                            var sensorsInFile = line1.Split(',');

                                            lineCount = 1;

                                            if (sensorsInFile.Length > 1)
                                            {
                                                var stres = StationDBMethods.GetStationByUniqueId(_monitorid);
                                                if (stres == null)
                                                {  
                                                    if (creationDate.AddDays(30) < DateTime.Now)
                                                    {
                                                        response.ToBeMoved = true;
                                                    }
                                                    else
                                                    {
                                                        continue;
                                                    }
                                                }
                                                else 
                                                {
                                                    stationId = stres.Id;
                                                    var station = StationDTO.Create(stres);

                                                    if (station != null)
                                                    {
                                                        if (station.Sensors.Count > 0)
                                                        {
                                                            var smapList = new List<SensorMap>();

                                                            foreach (var sensor in station.Sensors)
                                                            {
                                                                if (sensor.Equations.Count() == 0)
                                                                {
                                                                    continue;
                                                                }

                                                                if (sensor.CurrentEquation.LoggerChannel == null)
                                                                {
                                                                    continue;
                                                                }

                                                                bool blueSirenSensor = false;

                                                                //just verify that the current channel is actually a Blue Siren logger. 
                                                                List<Civica.Domain.CivicaDomain.MonitoringLoggerChannel> channels = SensorEquationDBMethods.GetMonitoringLoggerChannels((int)BaseEnum.LoggerType.BlueSiren);

                                                                for (int i = 0; i < channels.Count; i++)
                                                                {
                                                                    if (channels[i].Channel.ToLower() == sensor.CurrentEquation.LoggerChannel.Channel.ToLower())
                                                                    {
                                                                        blueSirenSensor = true;
                                                                        break;
                                                                    }
                                                                }

                                                                if (blueSirenSensor == false)
                                                                {
                                                                    //next sensor
                                                                    continue;
                                                                }

                                                                var newsmap = new SensorMap();

                                                                //Create a new chanel map record
                                                                newsmap.Sensor = sensor;
                                                                newsmap.Chanel = sensor.CurrentEquation.LoggerChannel.Channel;

                                                                string blueChannel = sensor.CurrentEquation.LoggerChannel.Channel;
                                                                int lastIndexOfUnderscore = 0;

                                                                for (int i = 0; i < blueChannel.Count(); i++)
                                                                {
                                                                    if (blueChannel[i] == '_')
                                                                        lastIndexOfUnderscore = i;
                                                                }

                                                               newsmap.Position = Convert.ToInt32(sensor.CurrentEquation.LoggerChannel.Channel.Substring(lastIndexOfUnderscore + 1));
                                                               smapList.Add(newsmap);
                                                            }

                                                            //Read the datalines
                                                            if (smapList.Count() > 0)
                                                            {
                                                                while (file.Peek() >= 0)
                                                                {
                                                                    var dataline = file.ReadLine();
                                                                    if (dataline == null) { break; }
                                                                    lineCount++;

                                                                    var dataValues = dataline.Split(',');
                                                                    if ((dataValues.Length > 1) && (sensorsInFile.Length == dataValues.Length))
                                                                    {
                                                                        var datestamp = dataValues[0];
                                                                        foreach (var sen in smapList)
                                                                        {
                                                                            double value;
                                                                            if (double.TryParse(dataValues[sen.Position], out value))
                                                                            {
                                                                                var calculated = processData.EvaluateMSensor(sen.Sensor.CurrentEquation.Equation, sen.Sensor.CurrentEquation.LoggerChannel.Channel, value);
                                                                                if (calculated != null)
                                                                                {
                                                                                    value = (double)calculated;
                                                                                }

                                                                                SensorData d = new SensorData();
                                                                                ttime = datestamp;

                                                                                if (!DateTime.TryParseExact(datestamp,formats[0], CultureInfo.InvariantCulture, DateTimeStyles.None, out d.timestamp))
                                                                                {
                                                                                    if (!DateTime.TryParseExact(datestamp, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out d.timestamp))
                                                                                    {
                                                                                        response.ErrorResults.Add("Unrecognized Date Format", "Unrecognized datetime format was encountered : " + datestamp);
                                                                                        response.DTO = false;
                                                                                        break;
                                                                                    }
                                                                                }

                                                                                var dateOffset = new DateTimeOffset(d.timestamp, station.TimeZoneOffset);
                                                                                sen.table.Rows.Add(sen.Sensor.Id, value, dateOffset, 0);
                                                                                sen.tableraw.Rows.Add(sen.Sensor.Id, value, dateOffset);
                                                                                updateTimestamps.Add(dateOffset);
                                                                            }                                                                           
                                                                        }
                                                                    }
                                                                    else
                                                                    {
                                                                        if (file.ReadLine() != null)
                                                                        {
                                                                            throw new System.NotSupportedException("The the file : " + name + " has problematic line at line position :" + lineCount);
                                                                        }
                                                                        else
                                                                        {
                                                                            response.AddServiceLog("Warning: ", "The the file : " + name + " has problematic last line at line position :" + lineCount);
                                                                        }
                                                                    }
                                                                }  // End of for loop for
                                                            } // End of for loop for  

                                                            if (response.DTO)
                                                            {
                                                                updateTimestamps.Sort();
                                                                var ts = updateTimestamps.Distinct().ToList();

                                                                var msensorupdate = processData.BulckUploadPhysicalSensors(smapList);

                                                                if (!msensorupdate.DTO)
                                                                {
                                                                    var m = String.Empty;
                                                                    foreach (var s in msensorupdate.ErrorResults)
                                                                    {
                                                                        m = m + "Key : " + s.Key + " , Value : " + s.Value + ", ";
                                                                    }
                                                                    response.ErrorResults.Add("Could Not Import Data", "Problem with Builk Import : " + m);
                                                                    response.DTO = false;
                                                                }
                                                                else
                                                                {
                                                                    var calculatedSensorsMap = new CalculatedSensorMap();

                                                                    calculatedSensorsMap.Sensors = station.Sensors.Where(x => x.Equations.Count() > 0).ToList();

                                                                    calculatedSensorsMap.TimeSeries = ts;

                                                                    var calsResult = processData.BulckUploadCalculatedsensors(calculatedSensorsMap);
                                                                    var m = String.Empty;
                                                                    if (!calsResult.DTO)
                                                                    {
                                                                        m = String.Empty;
                                                                        foreach (var s in calsResult.ErrorResults)
                                                                        {
                                                                            m = m + "Key : " + s.Key + " , Value : " + s.Value + ", ";
                                                                        }
                                                                        response.ErrorResults.Add("Could Not Import Data", "Problem with Builk Import Calculated Sensors : " + m);
                                                                        response.DTO = false;

                                                                    }

                                                                    m = String.Empty;
                                                                    if (!calsResult.DTO)
                                                                    {

                                                                        foreach (var s in calsResult.ErrorResults)
                                                                        {
                                                                            m = m + "Key : " + s.Key + " , Value : " + s.Value + ", ";
                                                                        }
                                                                        response.ErrorResults.Add("Could Not Import Data", "Problem with Builk Import Calculated Sensors : " + m);

                                                                        response.DTO = false;
                                                                    }

                                                                    m = String.Empty;
                                                                    if (calsResult.ServiceLogs.Count > 0)
                                                                    {
                                                                        foreach (var l in calsResult.ServiceLogs)
                                                                        {
                                                                            m = m + "Key : " + l.Key + " , Value : " + l.Value + ", ";
                                                                        }
                                                                        response.ServiceLogs.Add("Warnings while Importing", "There were problems while importing data : " + m);
                                                                    }
                                                                }

                                                                var updatedStation = SensorDBMethods.RecalculateStationLastTimeStamp(station.Id);
                                                           
                                                            } // if the parsing of the files has errors, do not do the upload.
                                                        } // End of  if (station.Sensors.Count > 0) 
                                                    } // End of   if (station != null) 
                                                }
                                            } //if (sensorsInFile.Length > 1)  
                                        } // if (name.Length >= 20)

                                        file.Close();
                                    } // end of using (var file = System.IO.File.OpenText(_filePath))

                                    //Move the file if it has been in the root directory for more than 30 days
                                    if(response.ToBeMoved)
                                    {
                                        var archivepath = f.RootDirectory + "\\ErrorArchive\\" + filename;

                                        if (File.Exists(archivepath))
                                        {
                                            File.Delete(archivepath);
                                        }
                                        File.Move(f.FilePath, archivepath);
                                        ImportLog.AddLogEntry("ADSPath", "Warning", stationId, filename, uniqueId, "File : " + filename + " was moved to ErrorArchive because it is oldern than30 days", "", DateTimeOffset.Now.ToUniversalTime());
                                        continue;
                                    }
                                }
                                catch (IOException ioe)
                                {
                                    var m = ioe.Message;
                                    continue;
                                }
                                catch (Exception e)
                                {
                                    var archivepath = f.RootDirectory + "\\ErrorArchive\\" + filename;

                                    if (File.Exists(archivepath))
                                    {
                                        File.Delete(archivepath);
                                    }
                                    File.Move(f.FilePath, archivepath);
                                    ImportLog.AddLogEntry("ADSPath","Error",stationId, filename, uniqueId, e.Message, e.StackTrace, DateTimeOffset.Now.ToUniversalTime());
                                    continue;
                                }

                                if (response.DTO)
                                {
                                    ImportLog.AddLogEntry("ADSPath", "Information", stationId, filename, uniqueId, "File : " + filename + " was successfully uploaded to DataCurrent", "", DateTimeOffset.Now.ToUniversalTime());

                                    var archivepath = f.RootDirectory + "\\Archive\\" + filename;

                                    if (File.Exists(archivepath))
                                    {
                                        File.Delete(archivepath);
                                    }
                                    File.Move(f.FilePath, archivepath);
                                    if (response.ServiceLogs != null)
                                    {
                                        if (response.ServiceLogs.Count > 0)
                                        {
                                            var message = String.Empty;
                                            foreach (var ms in response.ServiceLogs)
                                            {
                                                message = message + " / " + ms.Key + " : " + ms.Value;

                                            }
                                            ImportLog.AddLogEntry("ADSPath", "Warning", stationId, filename, uniqueId, message, "", DateTimeOffset.Now.ToUniversalTime());
                                        }       
                                    }
                                }
                                else
                                {
                                    var archivepath = f.RootDirectory + "\\ErrorArchive\\" + filename;

                                    if (File.Exists(archivepath))
                                    {
                                        File.Delete(archivepath);
                                    }
                                    File.Move(f.FilePath, archivepath);
                                    var message = String.Empty;
                                    if (response.ErrorResults != null)
                                    {
                                        foreach (var ms in response.ErrorResults)
                                        {
                                            message = message + " / " + ms.Key + " : " + ms.Value;

                                        }
                                    }
                                    ImportLog.AddLogEntry("ADSPath", "Error", stationId, filename, uniqueId, message, "", DateTimeOffset.Now.ToUniversalTime());
                                }

                            } //foreach (var f in fileList)
                        }
                    }
                }
                PreDeleteEmptySubDirectories(recievingpath);
            }
            catch (IOException e)
            {
                var m = e.Message;
            }
            catch (Exception e)
            {
                var m = e.Message;
            }

            return t;
        }

        private static void PreDeleteEmptySubDirectories(string startLocation)
        {   // Peterborough
            foreach (var directory in Directory.GetDirectories(startLocation))
            {   //Archive
                //Blue Siren
                //etc

                foreach (var directory2 in Directory.GetDirectories(directory))
                {
                    if (directory2.ToLower() != "archive" && directory2.ToLower() != "errorarchive" && directory2.ToLower() != "other")
                    {
                        DeleteEmptySubDirectories(directory2);
                    }
                }
            }
        }

        private static void DeleteEmptySubDirectories(string startLocation)
        {
            foreach (var directory in Directory.GetDirectories(startLocation))
            {
                DeleteEmptySubDirectories(directory);
                if (Directory.GetFiles(directory).Length == 0 &&
                    Directory.GetDirectories(directory).Length == 0)
                {
                    Directory.Delete(directory, false);
                }
            }
        }
    }
}
