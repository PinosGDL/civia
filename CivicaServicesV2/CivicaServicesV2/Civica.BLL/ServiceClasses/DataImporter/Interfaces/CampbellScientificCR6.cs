﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.Infrastructure.BaseClasses;

namespace Civica.BLL.ServiceClasses.DataImporter.Interfaces
{
   public interface ICampbellScientificCR6
    {
        bool Transfer(ref DCLogger logger);
    }
}
