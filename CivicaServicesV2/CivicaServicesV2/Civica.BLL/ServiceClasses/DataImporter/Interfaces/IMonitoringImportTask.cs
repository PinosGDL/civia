﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using Civica.Infrastructure.BaseClasses;


namespace Civica.BLL.ServiceClasses.DataImporter.Interfaces
{
    public interface IMonitoringImportTask
    {
        bool StartMonitoringImportTask(string connection, string undopath, ref DCLogger logger);
    }
}
