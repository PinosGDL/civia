﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;
using Civica.Infrastructure.BaseClasses;

namespace Civica.BLL.ServiceClasses.DataImporter.Interfaces
{
    public interface IFTSPHONE
    {
         bool Start(string path, ref DCLogger logger);
    }
}
