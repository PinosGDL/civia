﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using Civica.BLL.BaseClasses;
using NLog;
using Civica.BLL.Models;
using Civica.DBL.DBClasses;
using System.Collections;
using System.Globalization;
using Civica.Infrastructure.BaseClasses;
using Civica.BLL.CommonClasses;
using Civica.BLL.ServiceClasses.DataImporter.Interfaces;

namespace Civica.BLL.ServiceClasses.DataImporter
{
    public class FTSTCP : LoggerBase, IFTSTCP
    {
        public bool Start(ref DCLogger logger)
        {
            var ttime = string.Empty;
            var bc = new LoggerBase();

            var response = new ServiceResult<bool>(true);
            var sectionsize = 10;
            var sectioncount = 0;

            List<List<StationDTO>> stationsections = new List<List<StationDTO>>();
            

            try
            {
                var stations = StationDBMethods.GetListOfStationsByLoggerType((int)BaseEnum.LoggerType.FtsAxiomTCP);

                if (stations != null)
                {
                    if (stations.Count() > 0)
                    {
                        var section = new List<StationDTO>();

                        //choping the list of stations in chuncks, so we can parallelize the process
                        foreach (var station in stations)
                        {
                            section.Add(StationDTO.Create(station));

                            if (sectioncount < sectionsize)
                            {
                                sectioncount++;
                            }
                            else
                            {
                                stationsections.Add(section);
                                section = new List<StationDTO>();
                                sectioncount = 0;
                            }
                        }

                        if (sectioncount > 0)
                            stationsections.Add(section);

                        if (stationsections != null && stationsections.Count() > 0)
                        {
                            foreach (var s in stationsections)
                            {
                                var re = ProcessSection(s);
                            }
                        }
                    }
                    else
                    {
                        response.AddError("Process Error", "No Stations Found");
                    }
                }
                else
                {
                    response.AddError("Process Error", "No Stations Found");
                }
            }
            catch (Exception e)
            {
                response.AddError("System Error", e.Message);
            }
            return true;
        }

        private ServiceResult<bool> ProcessSection(List<StationDTO> section)
        {
            var t = true;
            double v = 0.0;
            var response = new ServiceResult<bool>(t);
            var processData = new DataProcessing();
            var updateTimestamps = new List<DateTimeOffset>();
            var aggregate = string.Empty;
            var lt = DateTime.Now;
            List<string> rawData = new List<string>();

            foreach (var station in section)
            {
                String server = station.Logger.UniqueId.Substring(0, station.Logger.UniqueId.IndexOf(":"));
                int port = Convert.ToInt32(station.Logger.UniqueId.Substring(station.Logger.UniqueId.IndexOf(":") + 1));

                if (station != null && station.Sensors.Count > 0)
                {
                    //just get the data, there is the case that a monitor can be configured with no sensors, but this is just for glen,
                    //so I won't cover that case here
                    rawData = GetFTSResponse(server, port, GetCollectStartDate(station));
                }

                var smapList = new List<SensorMap>();

                foreach (var sensor in station.Sensors)
                {
                    if (!IsFTSSensor(sensor))
                        continue;

                    var newsmap = new SensorMap();

                    double value;
                    //Create a new chanel map record
                    newsmap.Sensor = sensor;
                    newsmap.Chanel = sensor.CurrentEquation.LoggerChannel.Channel;

                    List<SensorData> res = new List<SensorData>();

                    // get list of sensor data from string
                    res = GetFTSData(rawData, newsmap.Chanel);

                    ImportLog.SaveMonitoringDataToCSV("Data From FTS Axiom via ftp", res, sensor.MonitorStationId, sensor.Id);

                    foreach (var sv in res)
                    {
                        v = sv.value;
                        var calculated = processData.EvaluateMSensor(sensor.CurrentEquation.Equation, sensor.CurrentEquation.LoggerChannel.Channel, v);

                        if (calculated != null)
                        {
                            value = (double)calculated;
                        }
                        else
                        {
                            value = v;
                        }

                        var dateOffset = new DateTimeOffset(sv.timestamp, station.TimeZoneOffset);
                        newsmap.table.Rows.Add(sensor.Id, value, dateOffset, 0);
                        newsmap.tableraw.Rows.Add(sensor.Id, value, dateOffset);
                        updateTimestamps.Add(dateOffset);
                    }
                    smapList.Add(newsmap);
                }

                updateTimestamps.Sort();
                var ts = updateTimestamps.Distinct().ToList();
                ImportLog.SaveTimestampListToCSV("Data From FTS Axiom via tcp", updateTimestamps);
                var msensorupdate = processData.BulckUploadPhysicalSensors(smapList);

                if (!msensorupdate.DTO)
                {
                    var m = String.Empty;
                    foreach (var s in msensorupdate.ErrorResults)
                    {
                        m = m + "Key : " + s.Key + " , Value : " + s.Value + ", ";
                        ImportLog.AddLogEntryDebugging("FTSTCP", "Error", station.Id, "", station.Logger.UniqueId, s.Key + " " + s.Value, "There has been an error with the FTSTCP Import", DateTimeOffset.UtcNow);
                    }

                    response.ErrorResults.Add("Could Not Import Data for Station" + station.Id, "Problem with Builk Import : " + m);
                    response.DTO = false;
                }
                else
                {
                    var calculatedSensorsMap = new CalculatedSensorMap();
                    calculatedSensorsMap.Sensors = station.Sensors.Where(x => x.Equations.Count() > 0).ToList();
                    calculatedSensorsMap.TimeSeries = ts;

                    var calsResult = processData.BulckUploadCalculatedsensors(calculatedSensorsMap);

                    var m = String.Empty;
                    if (!calsResult.DTO)
                    {
                        foreach (var s in calsResult.ErrorResults)
                        {
                            m = m + "Key : " + s.Key + " , Value : " + s.Value + ", ";
                        }
                        response.ErrorResults.Add("Could Not Import Data for Station" + station.Id, "Problem with Builk Import Calculated Sensors : " + m);
                        response.DTO = false;
                    }

                    m = String.Empty;
                    if (calsResult.ServiceLogs.Count > 0)
                    {
                        foreach (var l in calsResult.ServiceLogs)
                        {
                            m = m + "Key : " + l.Key + " , Value : " + l.Value + ", ";
                        }
                        response.ServiceLogs.Add("Warnings while Importing Station" + station.Id, "There were problems while importing data : " + m);
                    }
                    var updatedStation = SensorDBMethods.RecalculateStationLastTimeStamp(station.Id);
                }
            }
            return response;
        }

        /// <summary>
        /// </summary>
        /// <param name="Server"></param>
        /// <param name="Port"></param>
        /// <param name="ChannelId"></param>
        /// <param name="DataStart"></param>
        /// <param name="SensorTimestamp"></param>
        /// <returns></returns>
        public static List<String> GetFTSResponse(String Server, int Port, DateTimeOffset DataStart)
        {
            string COMMANDTEXT = "getdatarange";
            Byte[] data = System.Text.Encoding.ASCII.GetBytes("\r\n");
            int readingSize = 25;
            List<String> result = new List<String>();

            TcpClient client = new TcpClient();
            client.Connect(Server, Port);

            NetworkStream stream = client.GetStream();
            stream.Write(data, 0, data.Length);
            stream.Write(data, 0, data.Length);
            stream.Write(data, 0, data.Length);

            String responseData = String.Empty;
            data = new byte[readingSize];

            int bytes = stream.Read(data, 0, data.Length);
            bool handshake = false;

            while (bytes > 0)
            {
                responseData = System.Text.Encoding.ASCII.GetString(data, 0, data.Length);

                if (responseData.Contains(">"))
                {
                    handshake = true;
                    break;
                }

                bytes = 0;
                bytes = stream.Read(data, 0, data.Length);
            }

            if (handshake)
            {
                data = System.Text.Encoding.ASCII.GetBytes(COMMANDTEXT + " " + DataStart.ToString("yyyy'/'MM'/'dd") + "\r\n ");
                stream.Write(data, 0, data.Length);

                using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
                {
                    string line;
                    try
                    {
                        while ((line = reader.ReadLine()) != null)
                        {

                            //skip all of the header lines
                            if (line == "")
                                continue;

                            if (line.Contains('>'))
                                continue;

                            if (line.Contains("Data From"))
                                continue;

                            if (line.Contains("Date"))
                                continue;

                            if (line.Contains("YYYY"))
                                continue;

                            if (line[0] == ' ')
                                continue;

                            if (line.Contains("complete"))
                                break;

                            if (line.ToLower().Contains("timed out"))
                                break;

                            //done skipping all of the header lines
                            result.Add(line);
                        }
                    }
                    catch (Exception Ex)
                    {

                    }
                }
            }
            else
            {
                //logging here
            }
            client.Close();
            return result;
        }

        /// <summary>
        /// </summary>
        /// <param name="Server"></param>
        /// <param name="Port"></param>
        /// <param name="ChannelId"></param>
        /// <param name="DataStart"></param>
        /// <param name="SensorTimestamp"></param>
        /// <returns></returns>
        public static List<SensorData> GetFTSData(List<string> Data, string ChannelId)
        {
            int readingIndex = Convert.ToInt32(ChannelId.Substring(ChannelId.LastIndexOf('_') + 1));

            List<SensorData> result = new List<SensorData>();

            try { 
                foreach(string line in Data)
                {
                    string dateString = line.Substring(0, 19);

                    int last = 0;
                    int commaCount = 0;
                    int numberOfCommas = line.Count(x => x == ',');
                    for (int i = 0; i < line.Count(); i++)
                    {
                        if (line[i] == ',')
                            commaCount++;

                        if (commaCount == readingIndex + 1)
                        {
                            last = i + 1;
                            break;
                        }
                    }

                    if (last == 0)
                        continue;

                    SensorData temp = new SensorData();

                    temp.timestamp = DateTime.Parse(dateString);

                    if(commaCount < numberOfCommas) { 
                        temp.value = Convert.ToDouble(line.Substring(last, line.IndexOf(',', last + 1) - last));
                    }
                    else
                    {
                        temp.value = Convert.ToDouble(line.Substring(last));
                    }

                    if (temp.value != -99999)
                    result.Add(temp);
                }
            }
            catch(Exception ex)
            {

            }
            return result;
        }

        private DateTimeOffset GetCollectStartDate(StationDTO station)
        {
            DateTimeOffset start = DateTimeOffset.Now;

            foreach(SensorDTO x in station.Sensors)
            {
                if(IsFTSSensor(x))
                {
                    var Lasttimestamp = SensorDBMethods.GetLastSensorTime(x.Id);

                    if (Lasttimestamp.DTO != null)
                    {
                        if (Lasttimestamp.DTO.Value < start)
                            start = Lasttimestamp.DTO.Value;
                    }
                }
            }
            DateTimeOffset projectStart = new DateTimeOffset(new DateTime(2017, 04, 10));
            if (start < projectStart)
            {
                start = projectStart;
            }
            return start;
        }

        private bool IsFTSSensor(SensorDTO sensor)
        {
            List<Civica.Domain.CivicaDomain.MonitoringLoggerChannel> channels = SensorEquationDBMethods.GetMonitoringLoggerChannels((int)BaseEnum.LoggerType.FtsAxiomTCP);

            if (sensor.Equations.Count() == 0)
            {
                return false;
            }

            bool ftsChannel = false;

            //just verify that the current channel is actually a digi logger. 
            for (int i = 0; i < channels.Count; i++)
            {
                if (channels[i].Channel.ToLower() == sensor.CurrentEquation.LoggerChannel.Channel.ToLower())
                {
                    ftsChannel = true;
                    break;
                }
            }
            return ftsChannel;
        }
    }
}
