﻿using System;
using System.Collections.Generic;
using System.Linq;
using Civica.BLL.BaseClasses;
using System.IO;
using NLog;
using Civica.BLL.Models;
using Civica.DBL.DBClasses;
using System.Collections;
using System.Globalization;
using Civica.Infrastructure.BaseClasses;
using Civica.BLL.CommonClasses;
using Civica.BLL.ServiceClasses.DataImporter.Interfaces;


namespace Civica.BLL.ServiceClasses.DataImporter
{
    public class DetectronicDirectory : LoggerBase, IDetectronicDirectory
    {

        public bool StartDetectronicDirectory(ref DCLogger logger)
        {
            var t = true;
            var ttime = string.Empty;
            var bc = new LoggerBase();
            var response = new ServiceResult<bool>(t);
            var sectionsize = 5;
            var sectioncount = 0;
            List<List<StationDTO>> stationsections = new List<List<StationDTO>>();

            try
            {
                var stations = StationDBMethods.GetListOfStationsByLoggerType((int)BaseEnum.LoggerType.Detectronics);

                if (stations != null)
                {
                    if (stations.Count() > 0)
                    {
                        var section = new List<StationDTO>();

                        //choping the list of stations in chuncks, sowe can parallelize the process
                        foreach (var station in stations)
                        {
                            if (sectioncount < sectionsize)
                            {
                                section.Add(StationDTO.Create(station));
                                sectioncount++;
                            }
                            else
                            {
                                var temp = new List<StationDTO>();
                                foreach (var r in section)
                                {
                                    temp.Add(r);
                                }
                                stationsections.Add(temp);
                                section.Clear();
                                section.Add(StationDTO.Create(station));
                                sectioncount = 1;
                            }
                        }

                        stationsections.Add(section);
                        if (stationsections != null)
                        {
                            if (stationsections.Count() > 0)
                            {
                                foreach (var s in stationsections)
                                {
                                    var re = ProcessSection(s);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                var m = e.Message;
            }
            return t;
        }


        private ServiceResult<bool> ProcessSection(List<StationDTO> section)
        {
            var t = true;

            var response = new ServiceResult<bool>(t);
            var processData = new DataProcessing();
            var updateTimestamps = new List<DateTimeOffset>();
            var bc = new LoggerBase();
            var lt = DateTime.Now;
            
            var list = new List<SensorData>();
            var res = new ServiceResult<List<SensorData>>(list);
            DateTime minTime = DateTime.MaxValue;
            DateTime maxTime = DateTime.MinValue;

            foreach (var station in section)
            {
                try
                {
                    var loggerId = station.Logger.UniqueId;
                    if (station != null)
                    {
                        if ((station.Sensors.Count > 0) && (station.Logger != null))
                        {
                            if (station.Logger.Server != null)
                            {
                                if (station.Logger.Server.Path != null)
                                {
                                    var path = station.Logger.Server.Path;
                                    var dir = bc.GetDetecFilesInDirectory(station.Logger.Server.Path, loggerId);

                                    if (dir.Files != null)
                                    {
                                        if (dir.Files.Count() > 0)
                                        {
                                            var smapList = new List<SensorMap>();

                                            foreach (var f in dir.Files)
                                            {
                                                var sensors = new List<SensorDTO>();
                                                string filename = String.Empty;
                                                string filechanel = String.Empty;
                                                var p = f.FilePath.LastIndexOf("\\");

                                                filename = f.FilePath.Substring(p + 1);

                                                if (filename.Count(fo => fo == '_') != 1)
                                                {
                                                    continue;
                                                }

                                                var c = f.FilePath.LastIndexOf(".");
                                                filechanel = f.FilePath.Substring(c - 2, 2);

                                                //remove sensors with null channel info, cant be processed.
                                                for(int i = 0; i < station.Sensors.Count;i++)
                                                {
                                                    if(station.Sensors[i].CurrentEquation.LoggerChannel == null)
                                                    {
                                                        station.Sensors.Remove(station.Sensors[i]);
                                                        i--;
                                                    }
                                                }
                                                switch (filechanel)
                                                {
                                                    case "01":
                                                        sensors = station.Sensors.Where(x => x.CurrentEquation.LoggerChannel.Channel == "DTC1").ToList();
                                                        break;
                                                    case "02":
                                                        sensors = station.Sensors.Where(x => x.CurrentEquation.LoggerChannel.Channel == "DTC2" || x.CurrentEquation.LoggerChannel.Channel == "DTCU" || x.CurrentEquation.LoggerChannel.Channel == "DTCR").ToList();
                                                        break;
                                                    case "03":
                                                        sensors = station.Sensors.Where(x => x.CurrentEquation.LoggerChannel.Channel == "DTC3").ToList();
                                                        break;
                                                    case "04":
                                                        sensors = station.Sensors.Where(x => x.CurrentEquation.LoggerChannel.Channel == "DTC4").ToList();
                                                        break;
                                                    case "05":
                                                        sensors = station.Sensors.Where(x => x.CurrentEquation.LoggerChannel.Channel == "DTC5").ToList();
                                                        break;
                                                    case "06":
                                                        sensors = station.Sensors.Where(x => x.CurrentEquation.LoggerChannel.Channel == "DTC6" || x.CurrentEquation.LoggerChannel.Channel == "DTCD").ToList();
                                                       // sensors = station.Sensors.Where(x => x.CurrentEquation.LoggerChannel.Channel == "DTC6").ToList();
                                                        break;
                                                    case "07":
                                                        sensors = station.Sensors.Where(x => x.CurrentEquation.LoggerChannel.Channel == "DTC7" || x.CurrentEquation.LoggerChannel.Channel == "DTCV").ToList();
                                                        break;
                                                    case "08":
                                                        sensors = station.Sensors.Where(x => x.CurrentEquation.LoggerChannel.Channel == "DTC8" || x.CurrentEquation.LoggerChannel.Channel == "DTCB").ToList();
                                                        break;
                                                    default:
                                                        continue;
                                                }

                                                if (sensors != null)
                                                {
                                                    if (sensors.Count() > 0)
                                                    {
                                                        var dat = new DetecDAT();
                                                        var smaps = dat.ParseFile(sensors, f.FilePath, station.TimeZoneOffset, ref updateTimestamps,ref minTime, ref maxTime);
                                                        if (smaps != null)
                                                        {
                                                            foreach (var newsmap in smaps)
                                                            {
                                                                smapList.Add(newsmap);
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            updateTimestamps.Sort();
                                            var ts = updateTimestamps.Distinct().ToList();

                                            if (smapList.Count() > 0)
                                            {
                                                var msensorupdate = processData.BulckUploadPhysicalSensors(smapList);

                                                if (!msensorupdate.DTO)
                                                {
                                                    var m = String.Empty;
                                                    foreach (var s in msensorupdate.ErrorResults)
                                                    {
                                                        m = m + "Key : " + s.Key + " , Value : " + s.Value + ", ";
                                                    }
                                                    response.ErrorResults.Add("Could Not Import Data", "Problem with Builk Import : " + m);
                                                    response.DTO = false;

                                                }
                                                else
                                                {
                                                    var calculatedSensorsMap = new CalculatedSensorMap();

                                                    calculatedSensorsMap.Sensors = station.Sensors.Where(x => x.Equations.Count() > 0).ToList();
                                                    calculatedSensorsMap.TimeSeries = ts;

                                                    var calsResult = processData.BulckUploadCalculatedsensors(calculatedSensorsMap);

                                                    var m = String.Empty;
                                                    if (!calsResult.DTO)
                                                    {

                                                        foreach (var s in calsResult.ErrorResults)
                                                        {
                                                            m = m + "Key : " + s.Key + " , Value : " + s.Value + ", ";
                                                        }
                                                        response.ErrorResults.Add("Could Not Import Data", "Problem with Builk Import Calculated Sensors : " + m);

                                                        response.DTO = false;
                                                    }

                                                    m = String.Empty;
                                                    if (calsResult.ServiceLogs.Count > 0)
                                                    {
                                                        foreach (var l in calsResult.ServiceLogs)
                                                        {
                                                            m = m + "Key : " + l.Key + " , Value : " + l.Value + ", ";
                                                        }
                                                        response.ServiceLogs.Add("Warnings while Importing", "There were problems while importing data : " + m);
                                                    }

                                                    var updatedStation = SensorDBMethods.RecalculateStationLastTimeStamp(station.Id);

                                                    ImportLog.AddLogEntry("Detectronic", "Information", station.Id, "", station.Logger.UniqueId, "Data successfully uploaded to DataCurrent", "", DateTimeOffset.Now.ToUniversalTime());
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    ImportLog.AddLogEntry("DetecDirectory", "Error", station.Id, "", station.Logger.UniqueId, ex.Message, "", DateTimeOffset.Now.ToUniversalTime());
                }
              


            }
            return response;
        }



    }
}

