﻿using System;
using System.Collections.Generic;
using System.Linq;
using Civica.BLL.BaseClasses;
using System.IO;
using NLog;
using Civica.BLL.Models;
using Civica.DBL.DBClasses;
using System.Collections;
using System.Globalization;
using Civica.Infrastructure.BaseClasses;
using Civica.BLL.CommonClasses;
using Civica.BLL.ServiceClasses.DataImporter.Interfaces;
using Civica.BLL.ServiceClasses.DataImporter;

namespace Civica.BLL.ServiceClasses.DataImporter
{
    public class TelogDB : LoggerBase, ITelogDB
    {

        private static string[] formats = new string[]
       {
            "dd/MM/yyyy h:mm:ss tt",
            "d/MM/yyyy h:mm:ss tt"
       };



        public bool StartTelogDB( ref DCLogger logger)
        {
            var t = true; 

            var ttime = string.Empty;
            var bc = new LoggerBase();
  
            var response = new ServiceResult<bool>(t);
            var sectionsize = 5;
            var sectioncount = 0;

            List<List<StationDTO>> stationsections = new List<List<StationDTO>>();

            try
            {
                var stations = StationDBMethods.GetListOfStationsByLoggerType((int)BaseEnum.LoggerType.Telog);

                if (stations != null)
                {
                    if (stations.Count() > 0)
                    {
                        var section = new List<StationDTO>();

                        //choping the list of stations in chuncks, sowe can parallelize the process
                        foreach (var station in stations)
                        {
                            if (sectioncount < sectionsize)
                            {
                                section.Add(StationDTO.Create(station));
                                sectioncount++;
                            }
                            else
                            {
                                stationsections.Add(section);
                                section.Clear();
                                sectioncount = 0;
                            }
                        }


                        stationsections.Add(section);

                        if (stationsections != null)
                        {
                            if (stationsections.Count() > 0)
                            {
                                foreach (var s in stationsections)
                                {
                                    var re = ProcessSection(s);
                                }
                            }
                        }
                    }
                    else
                    {
                        response.AddError("Process Error", "No Stations Found");
                    }
                }
                else
                {
                    response.AddError("Process Error", "No Stations Found");
                }

            }
            catch (Exception e)
            {
                response.AddError("System Error", e.Message);
            }
            return response.DTO;
        }


        private ServiceResult<bool>  ProcessSection(List<StationDTO> section)
        {
            var t = true;

            double v = 0.0;
            var response = new ServiceResult<bool>(t);
            var processData = new DataProcessing();
            var updateTimestamps = new List<DateTimeOffset>();
            var aggregate = string.Empty;
            var  lt = DateTime.Now;

            var list = new List<TelogSensorData>();

            var res = new ServiceResult<List<TelogSensorData>>(list);

            foreach (var station in section)
            {

                var loggerId = station.Logger.UniqueId;


                if (station != null)
                {
                    if (station.Sensors.Count > 0)
                    {
                        var smapList = new List<SensorMap>();

                        foreach (var sensor in station.Sensors)
                        {
                            var tlgabreviation = String.Empty;
                            if (sensor.Equations.Count() == 0)
                            {
                                continue;
                            }

                            if (sensor.CurrentEquation.Equation.ToUpper().Contains("(MAX)"))
                            {
                                aggregate = "MAX";
                                sensor.CurrentEquation.Equation.Replace("(MAX)", "");
                                sensor.CurrentEquation.Equation.Replace("(max)", "");
                            }
                            else if (sensor.CurrentEquation.Equation.ToUpper().Contains("(MIN)"))
                            {
                                aggregate = "MIN";
                                sensor.CurrentEquation.Equation.Replace("(MIN)", "");
                                sensor.CurrentEquation.Equation.Replace("(min)", "");
                            }
                            else
                            {
                                aggregate = "";
                            }

                            var newsmap = new SensorMap();
                            double value;
                            //Create a new chanel map record
                            newsmap.Sensor = sensor;


                            if (sensor.CurrentEquation.LoggerChannel.Channel.Contains("TLG"))
                            {
                                tlgabreviation = sensor.CurrentEquation.LoggerChannel.Channel.Substring(3);
                            }

                            newsmap.Chanel = sensor.CurrentEquation.LoggerChannel.Channel;

                            var Lasttimestamp = SensorDBMethods.GetLastSensorTime(sensor.Id);
                            if (Lasttimestamp.DTO != null)
                            {
                                var dt = (DateTimeOffset)Lasttimestamp.DTO;
                                lt = dt.ToOffset(station.TimeZoneOffset).DateTime;
                                res = SensorDBMethods.GetTelogSensorData(station.Logger.UniqueId, lt, tlgabreviation, false);
                            }
                            else
                            {
                                res = SensorDBMethods.GetTelogSensorData(station.Logger.UniqueId, DateTime.Now, tlgabreviation, true);
                            }
                           

                            if (res.DTO != null)
                            {
                                if (res.DTO.Count() > 0)
                                {
                                    foreach(var sv in res.DTO)
                                    {


                                        if (aggregate == "MIN")
                                            v = (double)sv.minValue;
                                        else if (aggregate == "MAX")
                                            v = (double)sv.maxValue;
                                        else
                                            v = (double)sv.avgValue;


                                        var calculated = processData.EvaluateMSensor(sensor.CurrentEquation.Equation, sensor.CurrentEquation.LoggerChannel.Channel, v);
                                        if (calculated != null)
                                        {
                                            value = (double)calculated;
                                        }
                                        else
                                        {
                                            value = v;
                                        }

                                        var dateOffset = new DateTimeOffset(sv.timestamp, station.TimeZoneOffset);
                                        newsmap.table.Rows.Add(sensor.Id, value, dateOffset, 0);
                                        newsmap.tableraw.Rows.Add(sensor.Id, value, dateOffset);
                                        updateTimestamps.Add(dateOffset);
                                    }
                                }
                            }
                            smapList.Add(newsmap);
                        }

                        updateTimestamps.Sort();
                        var ts = updateTimestamps.Distinct().ToList();

                        var msensorupdate = processData.BulckUploadPhysicalSensors(smapList);

                        if (!msensorupdate.DTO)
                        {
                            var m = String.Empty;
                            foreach (var s in msensorupdate.ErrorResults)
                            {
                                m = m + "Key : " + s.Key + " , Value : " + s.Value + ", ";
                            }
                            response.ErrorResults.Add("Could Not Import Data", "Problem with Builk Import : " + m);
                            response.DTO = false;

                        }
                        else
                        {
                            var calculatedSensorsMap = new CalculatedSensorMap();

                            calculatedSensorsMap.Sensors = station.Sensors.Where(x => x.Equations.Count() > 0).ToList();

                            calculatedSensorsMap.TimeSeries = ts;

                            var calsResult = processData.BulckUploadCalculatedsensors(calculatedSensorsMap);

                            var m = String.Empty;
                            if (!calsResult.DTO)
                            {

                                foreach (var s in calsResult.ErrorResults)
                                {
                                    m = m + "Key : " + s.Key + " , Value : " + s.Value + ", ";
                                }
                                response.ErrorResults.Add("Could Not Import Data", "Problem with Builk Import Calculated Sensors : " + m);

                                response.DTO = false;
                            }

                            m = String.Empty;
                            if (calsResult.ServiceLogs.Count > 0)
                            {
                                foreach (var l in calsResult.ServiceLogs)
                                {
                                    m = m + "Key : " + l.Key + " , Value : " + l.Value + ", ";
                                }
                                response.ServiceLogs.Add("Warnings while Importing", "There were problems while importing data : " + m);
                            }

                            var updatedStation = SensorDBMethods.RecalculateStationLastTimeStamp(station.Id);

                        }
                    }
                }
            }

            return response;
        }

    }
}
