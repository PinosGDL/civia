﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Threading;
using NLog;
using Civica.BLL.BaseClasses;
using Civica.BLL.Models;
using Civica.DBL.DBClasses;
using Civica.Infrastructure.BaseClasses;
using Civica.BLL.CommonClasses;
using Civica.BLL.ServiceClasses.DataImporter.Interfaces;
using Civica.BLL.ServiceClasses.DataImporter;


using ScrapySharp.Core;
using ScrapySharp.Html.Parsing;
using ScrapySharp.Network;
using HtmlAgilityPack;
using ScrapySharp.Extensions;
using ScrapySharp.Html.Forms;

namespace Civica.BLL.ServiceClasses.DataImporter
{
    public class GOESFieldTest : LoggerBase, IGOESFieldTest
    {
        private static string[] formats = new string[]
       {
            "dd/MM/yyyy h:mm:ss tt",
            "d/MM/yyyy h:mm:ss tt"
       };

        public bool Transfer(ref DCLogger logger)
        {
            var ttime = string.Empty;
            var bc = new LoggerBase();

            var response = new ServiceResult<bool>(true);
            var sectionsize = 10;
            var sectioncount = 0;

            List<List<StationDTO>> stationsections = new List<List<StationDTO>>();
            List<Civica.Domain.CivicaDomain.MonitoringLoggerChannel> channels = SensorEquationDBMethods.GetMonitoringLoggerChannels((int)BaseEnum.LoggerType.GOESFieldTest);

            try
            {
                var stations = StationDBMethods.GetListOfStationsByLoggerType((int)BaseEnum.LoggerType.GOESFieldTest);

                if (stations != null)
                {
                    if (stations.Count() > 0)
                    {
                        var section = new List<StationDTO>();

                        //choping the list of stations in chuncks, so we can parallelize the process
                        foreach (var station in stations)
                        {
                            if (sectioncount < sectionsize)
                            {
                                section.Add(StationDTO.Create(station));
                                sectioncount++;
                            }
                            else
                            {
                                stationsections.Add(section);
                                section.Clear();
                                sectioncount = 0;
                            }
                        }

                        stationsections.Add(section);

                        if (stationsections != null)
                        {
                            if (stationsections.Count() > 0)
                            {
                                foreach (var s in stationsections)
                                {
                                    var re = ProcessSection(s, channels);
                                }
                            }
                        }
                    }
                    else
                    {
                        response.AddError("Process Error", "No Stations Found");
                    }
                }
                else
                {
                    response.AddError("Process Error", "No Stations Found");
                }

            }
            catch (Exception e)
            {
                response.AddError("System Error", e.Message);
            }
            return response.DTO;
        }
        private ServiceResult<bool> ProcessSection(List<StationDTO> section, List<Civica.Domain.CivicaDomain.MonitoringLoggerChannel> channels)
        {
            var t = true;

            double v = 0.0;
            var response = new ServiceResult<bool>(t);
            var processData = new DataProcessing();
            var updateTimestamps = new List<DateTimeOffset>();
            var aggregate = string.Empty;
            var lt = DateTime.Now;

            foreach (var station in section)
            {
                try
                {
                    var loggerId = station.Logger.UniqueId;

                    if (station != null && station.Sensors.Count > 0)
                    {
                        var smapList = new List<SensorMap>();

                        List<SensorData> res = new List<SensorData>();
                        DateTimeOffset startDateTimeOffset = DateTime.Parse("2016-09-01 00:00:00");

                        foreach (var sensor in station.Sensors)
                        {
                            //if there are no equations, the automated importer of data won't do anything
                            if (sensor.Equations.Count() == 0)
                            {
                                continue;
                            }

                            bool GOESChannel = false;

                            //just verify that the current channel is actually a water office logger. 
                            for (int i = 0; i < channels.Count; i++)
                            {
                                if (channels[i].Channel.ToLower() == sensor.CurrentEquation.LoggerChannel.Channel.ToLower())
                                {
                                    GOESChannel = true;
                                    break;
                                }
                            }

                            if (GOESChannel == false)
                            {
                                continue;
                            }

                            var Lasttimestamp = SensorDBMethods.GetLastSensorTime(sensor.Id);

                            if (Lasttimestamp.DTO.HasValue && Lasttimestamp.DTO.Value > startDateTimeOffset)
                            {
                                startDateTimeOffset = Lasttimestamp.DTO.Value;
                            }

                            try
                            {
                                string channel = sensor.CurrentEquation.LoggerChannel.Channel.ToLower();
                                int columnNumber = Convert.ToInt32(channel.Substring(channel.LastIndexOf('_') + 1));
                                if(Lasttimestamp.DTO.HasValue)
                                { 
                                    res = GetWebData(station.Logger.UniqueId, columnNumber,Lasttimestamp.DTO.Value.UtcDateTime);
                                }
                                else
                                {
                                    //can only go back 72 hours, should be pleanty
                                    res = GetWebData(station.Logger.UniqueId, columnNumber, new DateTime(2017,1,1));
                                }
                            }
                            catch (Exception ex)
                            {

                            }
                      
                            var newsmap = new SensorMap();

                            double value;
                            //Create a new chanel map record
                            newsmap.Sensor = sensor;
                            newsmap.Chanel = sensor.CurrentEquation.LoggerChannel.Channel;

                            Lasttimestamp = SensorDBMethods.GetLastSensorTime(sensor.Id);

                            foreach (var sv in res)
                            {
                                v = sv.value;
                                var calculated = processData.EvaluateMSensor(sensor.CurrentEquation.Equation, sensor.CurrentEquation.LoggerChannel.Channel, v);
                                if (calculated != null)
                                {
                                    value = (double)calculated;
                                }
                                else
                                {
                                    value = v;
                                }

                                var dateOffset = new DateTimeOffset(sv.timestamp, TimeZoneInfo.Utc.BaseUtcOffset);

                                newsmap.table.Rows.Add(sensor.Id, value, dateOffset, 0);
                                newsmap.tableraw.Rows.Add(sensor.Id, value, dateOffset);
                                updateTimestamps.Add(dateOffset);
                            }
                            smapList.Add(newsmap);
                        }

                        updateTimestamps.Sort();
                        var ts = updateTimestamps.Distinct().ToList();

                        var msensorupdate = processData.BulckUploadPhysicalSensors(smapList);

                        if (!msensorupdate.DTO)
                        {
                            var m = String.Empty;
                            foreach (var s in msensorupdate.ErrorResults)
                            {
                                m = m + "Key : " + s.Key + " , Value : " + s.Value + ", ";
                            }
                            response.ErrorResults.Add("Could Not Import Data for Station" + station.Id, "Problem with Builk Import : " + m);
                            response.DTO = false;

                        }
                        else
                        {
                            var calculatedSensorsMap = new CalculatedSensorMap();

                            calculatedSensorsMap.Sensors = station.Sensors.Where(x => x.Equations.Count() > 0).ToList();
                            calculatedSensorsMap.TimeSeries = ts;

                            var calsResult = processData.BulckUploadCalculatedsensors(calculatedSensorsMap);

                            var m = String.Empty;
                            if (!calsResult.DTO)
                            {

                                foreach (var s in calsResult.ErrorResults)
                                {
                                    m = m + "Key : " + s.Key + " , Value : " + s.Value + ", ";
                                }
                                response.ErrorResults.Add("Could Not Import Data for Station" + station.Id, "Problem with Builk Import Calculated Sensors : " + m);
                                response.DTO = false;
                            }

                            m = String.Empty;
                            if (calsResult.ServiceLogs.Count > 0)
                            {
                                foreach (var l in calsResult.ServiceLogs)
                                {
                                    m = m + "Key : " + l.Key + " , Value : " + l.Value + ", ";
                                }
                                response.ServiceLogs.Add("Warnings while Importing Station" + station.Id, "There were problems while importing data : " + m);
                            }
                            var updatedStation = SensorDBMethods.RecalculateStationLastTimeStamp(station.Id);
                        }
                    }
                }
        
            catch(Exception ex)
            {

            }
            }
            return response;
        }

        public static List<SensorData> GetWebData(string DCPID, int ReadingIndex,DateTime LastReading)
        {
            List<SensorData> result = new List<SensorData>();

            ScrapingBrowser Browser = new ScrapingBrowser();
            Browser.AllowAutoRedirect = true; // Browser has many settings you can access in setup
            Browser.AllowMetaRedirect = true;

            WebPage PageResult = Browser.NavigateToPage(new Uri("http://eddn.usgs.gov/fieldtest.html"));

            PageWebForm form = new PageWebForm(PageResult.Html.ChildNodes[0].ChildNodes[2].ChildNodes[1], Browser);
            form.Action = @"https://eddn.usgs.gov/cgi-bin/fieldtest.pl";
            form["DCPID"] = "45B0A5AE";
            form["SINCE"] = "72";
            form.Method = HttpVerb.Post;
            WebPage resultsPage = form.Submit();

            HtmlNode[] p = resultsPage.Html.CssSelect("p").ToArray();
            DateTime currentDate = DateTime.UtcNow;
            foreach (HtmlNode line in p)
            {
                int commaCount = 0;

                foreach (char x in line.ChildNodes[0].InnerText)
                {
                    if (x == ',')
                        commaCount++;
                }

                if (line.ChildNodes[0].InnerText == "EDDN Field Retrieval")
                {
                    // do nothing, header line, potentially add a flag to throw an exception if the format changes
                    //format of the header is as follows:
                    //45B0A5AE17020164641G40+1NN019EXE00329 16:45:40,z,+12.88
                    //45B0A5AE                                                DCPID
                    //        17                                              Two Digit Year
                    //          020                                           Day of the year. i.e day 32 = feb 1st 
                    //             164641                                     HHMMSS – Time the message arrived at the Wallops receive station 
                    //                   G                                    Failure code G = good ? = parity error
                    //                    40                                  Signal strength, 32-57
                    //                      +1N                               Modulation Index
                    //                         N                              Data Quality, N = normal
                    //                          019EXE00329 16:45:40,z,+12.88 No idea, couldn't read documentation. https://eddn.usgs.gov/dcpformat.html
                }
                else if (line.InnerText.ToLower().Contains("from"))
                {

                }
                else if (line.InnerText.Contains(DCPID))
                {
                    string firstDigit = DateTime.Now.ToString("yyyy").Substring(0, 1);
                    int century = Convert.ToInt32(firstDigit) * 1000;

                    //message line use to figure our the date
                    string dateString = line.InnerText.Substring(10, 3);
                    currentDate = new DateTime(Convert.ToInt32(line.InnerText.Substring(8, 2)) + century, 1, 1).AddDays(Convert.ToInt32(dateString) - 1);
                }
                else if (line.InnerText.Trim() == "")
                {
                    // do nothing, empty paragraph tag
                }
                else
                {
                    //valid data line, read it.
                    string[] readings = line.ChildNodes[0].InnerText.Split(',');

                    if (readings.Count() < ReadingIndex - 1)
                    {
                        throw new Exception("Data stream not present in field test webpage");
                    }
                    try
                    {
                        SensorData temp = new SensorData();
                        temp.timestamp = DateTime.Parse(currentDate.ToString("yyyy-MM-dd") + " " + readings[0]);

                        if (temp.timestamp.Hour == 23 && result[result.Count - 1].timestamp.Hour == 0)
                        { //the date has rolled over to the previous day, ensure that the current date is updated
                            if (temp.timestamp.Day == result[result.Count - 1].timestamp.Day)
                            {
                                //this should never be hit, but just in case
                                currentDate = currentDate.Subtract(new TimeSpan(1, 0, 0, 0));
                                temp.timestamp = DateTime.Parse(currentDate.ToString("yyyy-MM-dd") + " " + readings[0]);
                            }
                        }
                        temp.value = Convert.ToDouble(readings[ReadingIndex]);

                        if(temp.timestamp > LastReading)
                            result.Add(temp);
                    }
                    catch (Exception ex)
                    {
                        //logging here
                    }
                }
            }
            return result;
        }
    }
}
