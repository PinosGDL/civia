﻿using System;
using System.Collections.Generic;
using System.Linq;
using Civica.BLL.BaseClasses;
using System.IO;
using NLog;
using Civica.BLL.Models;
using Civica.DBL.DBClasses;
using System.Collections;
using System.Globalization;
using Civica.Infrastructure.BaseClasses;
using Civica.BLL.CommonClasses;
using Civica.BLL.ServiceClasses.DataImporter.Interfaces;

namespace Civica.BLL.ServiceClasses.DataImporter
{

    public class RemoteSense : LoggerBase, IRemoteSense
    {
        private static string[] formats = new string[]
        {
            "dd/MM/yyyy h:mm:ss tt",
            "d/MM/yyyy h:mm:ss tt",
            "d/MM/yyyy HH:mm:ss"
        };

        public bool StartRemoteSense(string path, ref DCLogger logger)
        {
            var t = true;
            var ttime = string.Empty;
            string _monitorid = String.Empty;
            int lineCount = 0;
            var bc = new LoggerBase();
            var processData = new DataProcessing();

            try
            {
                var res = bc.GetFilesInDirectory(path);

                if (res != null)
                {
                    if (res.Files != null)
                    {
                        if (res.Files.Count > 0)
                        {
                            foreach (var f in res.Files)
                            {
                                bool r = true;
                                var response = new ServiceResult<bool>(r);
                                string filename = String.Empty;
                                string extension = String.Empty;
                                int stationId = 0;
                                string uniqueId = String.Empty;

                                var updateTimestamps = new List<DateTimeOffset>();
                                var p = f.FilePath.LastIndexOf("\\");
                                filename = f.FilePath.Substring(p + 1);

                                var c = filename.LastIndexOf(".");
                                extension = filename.Substring(c + 1);

                                if (extension.ToUpper() != "CSV")
                                {
                                    var archivepath = f.RootDirectory + "\\Other\\" + filename;

                                    if (File.Exists(archivepath))
                                    {
                                        File.Delete(archivepath);
                                    }
                                    File.Move(f.FilePath, archivepath);

                                    continue;
                                }


                                try
                                {
                                    var creationDate = File.GetCreationTime(f.FilePath);

                                    using (var file = System.IO.File.OpenText(f.FilePath))
                                    {
                                        var name = ((FileStream)file.BaseStream).Name;

                                        // Parse file here
   
                                        var station = new StationDTO();

                                        String line1 = file.ReadLine();
                                        if ((line1 == null) || (line1 == "") || !line1.Any(char.IsDigit))
                                        {
                                            String line2 = file.ReadLine();
                                            if ((line2 == null) || (line2 == "") )
                                            {
                                                response.ErrorResults.Add("Parse Error", "There are two empty lines in the begining of the file");
                                                response.DTO = false;
                                            }
                                            else
                                            {
                                                _monitorid = line2.Replace(",", "").Replace("\t","");
                                            }      
                                        }
                                        else
                                        {
                                            _monitorid = line1.Replace(",", "");
                                        }

                                        if(_monitorid != String.Empty)
                                        {
                                            // Retriving the station information based on unique ID
                                            var stres = StationDBMethods.GetStationByUniqueId(_monitorid);
                                            if (stres == null)
                                            {
                                                if (creationDate.AddDays(30) < DateTime.Now)
                                                {
                                                    response.ToBeMoved = true;
                                                    response.DTO = false;
                                                }
                                                else
                                                {
                                                    continue;
                                                }

                                            }
                                            else
                                            {
                                                stationId = stres.Id;
                                                station = StationDTO.Create(stres);


                                                String chanelLine = file.ReadLine();

                                                var sensorsInFile = chanelLine.Split(',');

                                                if (station != null)
                                                {
                                                    if (station.Sensors.Count > 0)
                                                    {

                                                        var smapList = new List<SensorMap>();

                                                        foreach (var sensor in station.Sensors)
                                                        {

                                                            string channelType;



                                                            if (sensor.Equations.Count() == 0)
                                                            {
                                                                continue;
                                                            }

                                                            if (sensor.CurrentEquation.LoggerChannel == null)
                                                            {
                                                                continue;
                                                            }


                                                         

                                                            //Determine the type of each chanel in the chanel list
                                                            if (sensor.CurrentEquation.Equation.Contains("RG 1"))
                                                            {
                                                                channelType = "RG 1";
                                                            }
                                                            else if (sensor.CurrentEquation.Equation.Contains("RG 2"))
                                                            {
                                                                channelType = "RG 2";
                                                            }
                                                            else if (sensor.CurrentEquation.Equation.Contains("Battery Voltage"))
                                                            {
                                                                channelType = "Battery Voltage";
                                                            }
                                                            else if (sensor.CurrentEquation.Equation.Contains("PCB Temp"))
                                                            {
                                                                channelType = "PCB Temp";
                                                            }
                                                            else if (sensor.CurrentEquation.Equation.Contains("Ultrasonic1"))
                                                            {
                                                                channelType = "Ultrasonic1";
                                                            }
                                                            else if (sensor.CurrentEquation.Equation.Contains("RS-A"))
                                                            {//A is to small to use part of it, include the units because of this, 
                                                                channelType = "A(0)";
                                                            }
                                                            else if (sensor.CurrentEquation.Equation.Contains("4-20"))
                                                            {
                                                                channelType = "4-20";
                                                            }
                                                            else if (sensor.CurrentEquation.Equation.Contains("Clamp 1"))
                                                            {
                                                                channelType = "Clamp 1";
                                                            }
                                                            else if (sensor.CurrentEquation.Equation.Contains("Clamp 2"))
                                                            {
                                                                channelType = "Clamp 2";
                                                            }
                                                            else if (sensor.CurrentEquation.Equation.Contains("Clamp 3"))
                                                            {
                                                                channelType = "Clamp 3";
                                                            }
                                                            else
                                                            {
                                                                throw new System.NotSupportedException("The sensor : " + sensor.Id + " does not contain valid chanel in its equation for RS Loggers");
                                                            }

                                                            var newsmap = new SensorMap();

                                                            //Create a new chanel map record
                                                            newsmap.Sensor = sensor;
                                                            newsmap.Chanel = sensor.CurrentEquation.LoggerChannel.Channel;

                                                            //For each chanel in the chanel list, circle through the chale headers in the second line of the file, to determine the position.
                                                            for (int i = 1; i <= sensorsInFile.Length - 1; i++)
                                                            {
                                                                var sn = sensorsInFile[i].ToUpper();

                                                                if (sn.Contains(channelType.ToUpper()))
                                                                {
                                                                    newsmap.Position = i;
                                                                    break;
                                                                }    
                                                            }

                                                            smapList.Add(newsmap);
                                                        }
                                                        //Read the datalines
                                                        if (smapList.Count() > 0)
                                                        {

                                                            while (file.Peek() >= 0)
                                                            {
                                                                var dataline = file.ReadLine();
                                                                if (dataline == null) { break; }
                                                                lineCount++;

                                                                var dataValues = dataline.Split(',');
                                                                if (dataValues.Length > 1)
                                                                {
                                                                    var datestamp = dataValues[0];
                                                                    foreach (var sen in smapList)
                                                                    {
                                                                        double value;
                                                                        if (!double.TryParse(dataValues[sen.Position], out value))
                                                                        {
                                                                            value = 0;
                                                                        }

                                                                        var calculated = processData.EvaluateMSensor(sen.Sensor.CurrentEquation.Equation, sen.Sensor.CurrentEquation.LoggerChannel.Channel, value);
                                                                        if (calculated != null)
                                                                        {
                                                                            value = (double)calculated;
                                                                        }

                                                                        SensorData d = new SensorData();
                                                                        ttime = datestamp;


                                                                        if (!DateTime.TryParse(datestamp, out d.timestamp))
                                                                        {
                                                                            if (!DateTime.TryParseExact(datestamp, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out d.timestamp))
                                                                            {
                                                                                response.ErrorResults.Add("Unrecognized Date Format", "Unrecognized datetime format was encountered : " + datestamp);
                                                                                response.DTO = false;
                                                                                break;
                                                                            }
                                                                        }



                                                                        var dateOffset = new DateTimeOffset(d.timestamp, station.TimeZoneOffset);
                                                                        sen.table.Rows.Add(sen.Sensor.Id, value, dateOffset, 0);
                                                                        sen.tableraw.Rows.Add(sen.Sensor.Id, value, dateOffset);

                                                                        updateTimestamps.Add(dateOffset);

                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    if (file.ReadLine() != null)
                                                                    {
                                                                        throw new System.NotSupportedException("The the file : " + name + " has problematic line at line position :" + lineCount);
                                                                    }
                                                                    else
                                                                    {
                                                                        response.AddServiceLog("Warning: ", "The the file : " + name + " has problematic last line at line position :" + lineCount);

                                                                    }
                                                                }
                                                         

                                                            }  // End of for loop for


                                                        } // End of for loop for  

                                                        if (response.DTO)
                                                        {

                                                            updateTimestamps.Sort();
                                                            var ts = updateTimestamps.Distinct().ToList();

                                                            var msensorupdate = processData.BulckUploadPhysicalSensors(smapList);

                                                            if (!msensorupdate.DTO)
                                                            {
                                                                var m = String.Empty;
                                                                foreach (var s in msensorupdate.ErrorResults)
                                                                {
                                                                    m = m + "Key : " + s.Key + " , Value : " + s.Value + ", ";
                                                                }
                                                                response.ErrorResults.Add("Could Not Import Data", "Problem with Builk Import : " + m);
                                                                response.DTO = false;

                                                            }
                                                            else
                                                            {
                                                                var calculatedSensorsMap = new CalculatedSensorMap();

                                                                calculatedSensorsMap.Sensors = station.Sensors.Where(x => x.Equations.Count() > 0).ToList();

                                                                calculatedSensorsMap.TimeSeries = ts;

                                                                var calsResult = processData.BulckUploadCalculatedsensors(calculatedSensorsMap);
                                                                var m = String.Empty;
                                                                if (!calsResult.DTO)
                                                                {
                                                                    m = String.Empty;
                                                                    foreach (var s in calsResult.ErrorResults)
                                                                    {
                                                                        m = m + "Key : " + s.Key + " , Value : " + s.Value + ", ";
                                                                    }
                                                                    response.ErrorResults.Add("Could Not Import Data", "Problem with Builk Import Calculated Sensors : " + m);
                                                                    response.DTO = false;

                                                                }

                                                                m = String.Empty;
                                                                if (!calsResult.DTO)
                                                                {

                                                                    foreach (var s in calsResult.ErrorResults)
                                                                    {
                                                                        m = m + "Key : " + s.Key + " , Value : " + s.Value + ", ";
                                                                    }
                                                                    response.ErrorResults.Add("Could Not Import Data", "Problem with Builk Import Calculated Sensors : " + m);

                                                                    response.DTO = false;
                                                                }

                                                                m = String.Empty;
                                                                if (calsResult.ServiceLogs.Count > 0)
                                                                {
                                                                    foreach (var l in calsResult.ServiceLogs)
                                                                    {
                                                                        m = m + "Key : " + l.Key + " , Value : " + l.Value + ", ";
                                                                    }
                                                                    response.ServiceLogs.Add("Warnings while Importing", "There were problems while importing data : " + m);
                                                                }


                                                            }

                                                            var updatedStation = SensorDBMethods.RecalculateStationLastTimeStamp(station.Id);

                                                        } // if the parsing of the files has errors, do not do the upload.

                                                    } // End of  if (station.Sensors.Count > 0) 

                                                } // End of   if (station != null) 

                                            } // End of If stetement where we check if station with the Unique ID exist - in case it does not - either ignor the file or move if older than 30 days.
                                        }
                                        else
                                        {
                                            response.ErrorResults.Add("Pars Error", "Could Not Find the Unique ID in the first two lines");
                                            response.DTO = false;
                                        }
                                     

                                        //Common data upload for physical and calculated sensors
                                        // need to be enclosed in as many if{} statments, as need, for checking the correctness of 
                                        // the data in the file while reading.  Here the reading of the file has tobe done and sensor maps  
                                        // and timestamp intervals collected

                                  

                                        file.Close();
                                    } // End of   Using a specific file

                                    //Move the file if it has been in the root directory for more than 30 days
                                    if (response.ToBeMoved)
                                    {
                                        var archivepath = f.RootDirectory + "\\ErrorArchive\\" + filename;

                                        if (File.Exists(archivepath))
                                        {
                                            File.Delete(archivepath);
                                        }
                                        File.Move(f.FilePath, archivepath);
                                        ImportLog.AddLogEntry("RemoteSense", "Warning", stationId, filename, uniqueId, "File : " + filename + " was moved to ErrorArchive because it is oldern than30 days", "", DateTimeOffset.Now.ToUniversalTime());
                                        continue;
                                    }
                                }
                                catch (IOException ioe)
                                {
                                    var m = ioe.Message;
                                    continue;
                                }
                                catch (Exception e)
                                {
                                    var archivepath = f.RootDirectory + "\\ErrorArchive\\" + filename;

                                    if (File.Exists(archivepath))
                                    {
                                        File.Delete(archivepath);
                                    }
                                    File.Move(f.FilePath, archivepath);
                                    ImportLog.AddLogEntry("RemoteSense", "Error", stationId, filename, uniqueId, e.Message, e.StackTrace, DateTimeOffset.Now.ToUniversalTime());
                                    continue;
                                }


                                if (response.DTO)
                                {
                                    var archivepath = f.RootDirectory + "\\Archive\\" + filename;

                                    if (File.Exists(archivepath))
                                    {
                                        File.Delete(archivepath);
                                    }
                                    File.Move(f.FilePath, archivepath);
                                    if (response.ServiceLogs != null)
                                    {

                                        if (response.ServiceLogs.Count > 0)
                                        {
                                            var message = String.Empty;
                                            foreach (var ms in response.ServiceLogs)
                                            {
                                                message = message + " / " + ms.Key + " : " + ms.Value;

                                            }
                                            ImportLog.AddLogEntry("RemoteSense", "Warning", stationId, filename, uniqueId, message, "", DateTimeOffset.Now.ToUniversalTime());
                                        }
                                    }
                                }
                                else
                                {
                                    var archivepath = f.RootDirectory + "\\ErrorArchive\\" + filename;

                                    if (File.Exists(archivepath))
                                    {
                                        File.Delete(archivepath);
                                    }
                                    File.Move(f.FilePath, archivepath);
                                    var message = String.Empty;
                                    if (response.ErrorResults != null)
                                    {
                                        foreach (var ms in response.ErrorResults)
                                        {
                                            message = message + " / " + ms.Key + " : " + ms.Value;

                                        }
                                    }
                                    ImportLog.AddLogEntry("RemoteSense", "Error", stationId, filename, uniqueId, message, "", DateTimeOffset.Now.ToUniversalTime());
                                }

                            } // End of   foreach (var f in res.Files)
                        } // End of   if (res.Files.Count > 0)
                    } // End of   if (res.Files != null)
                } // End of   if (res != null)
            }
            catch (IOException e)
            {
                // bc.LogWriter.Log(LogLevel.Error, "IO Exception", e);
                var m = e.Message;
            }
            catch (Exception e)
            {
                //  bc.LogWriter.Log(LogLevel.Error, "General Exception", e);
                var m = e.Message;
            }

            return t;
        }

    }

}
