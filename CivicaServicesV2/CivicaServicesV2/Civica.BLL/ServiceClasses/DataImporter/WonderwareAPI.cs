﻿using System;
using System.Collections.Generic;
using System.Linq;
using Civica.BLL.BaseClasses;
using System.IO;
using NLog;
using Civica.BLL.Models;
using Civica.DBL.DBClasses;
using System.Collections;
using System.Globalization;
using Civica.Infrastructure.BaseClasses;
using Civica.BLL.CommonClasses;
using Civica.BLL.ServiceClasses.DataImporter.Interfaces;
using Civica.BLL.ServiceClasses.DataImporter;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;
using System.Runtime.Serialization.Json;
using System.Threading;

namespace Civica.BLL.ServiceClasses.DataImporter
{
    public class WonderwareAPI : LoggerBase, IWonderwareAPI
    {
        private static string[] formats = new string[]
       {
            "dd/MM/yyyy h:mm:ss tt",
            "d/MM/yyyy h:mm:ss tt"
       };
        public bool Transfer(ref DCLogger logger)
        {
            var ttime = string.Empty;
            var bc = new LoggerBase();

            var response = new ServiceResult<bool>(true);
            var sectionsize = 10;
            var sectioncount = 0;

            List<List<StationDTO>> stationsections = new List<List<StationDTO>>();
            List<Civica.Domain.CivicaDomain.MonitoringLoggerChannel> channels = SensorEquationDBMethods.GetMonitoringLoggerChannels((int)BaseEnum.LoggerType.WonderwareAPI);

            try
            {
                var stations = StationDBMethods.GetListOfStationsByLoggerType((int)BaseEnum.LoggerType.WonderwareAPI);

                if (stations != null)
                {
                    if (stations.Count() > 0)
                    {
                        var section = new List<StationDTO>();

                        //choping the list of stations in chuncks, so we can parallelize the process
                        foreach (var station in stations)
                        {
                            if (sectioncount < sectionsize)
                            {
                                section.Add(StationDTO.Create(station));
                                sectioncount++;
                            }
                            else
                            {
                                section.Add(StationDTO.Create(station));
                                stationsections.Add(section);
                                section = new List<StationDTO>();
                                sectioncount = 0;
                            }
                        }

                        stationsections.Add(section);

                        if (stationsections != null)
                        {
                            if (stationsections.Count() > 0)
                            {
                                foreach (var s in stationsections)
                                {
                                    var re = ProcessSection(s, channels);
                                }
                            }
                        }
                    }
                    else
                    {
                        response.AddError("Process Error", "No Stations Found");
                    }
                }
                else
                {
                    response.AddError("Process Error", "No Stations Found");
                }

            }
            catch (Exception e)
            {
                response.AddError("System Error", e.Message);
            }
            return response.DTO;
        }

        private ServiceResult<bool> ProcessSection(List<StationDTO> section, List<Civica.Domain.CivicaDomain.MonitoringLoggerChannel> channels)
        {
            var t = true;
            var response = new ServiceResult<bool>(t);

            foreach (var station in section)
            {
                var loggerId = station.Logger.UniqueId;

                if (station != null && station.Sensors.Count > 0)
                {
                    foreach (var sensor in station.Sensors)
                    {
                        var smapList = new List<SensorMap>();
                        double v = 0.0;
                        var processData = new DataProcessing();
                        var updateTimestamps = new List<DateTimeOffset>();
                        var aggregate = string.Empty;
                        var lt = DateTime.Now;

                        //if there are no equations, the automated importer of data won't do anything
                        if (sensor.Equations.Count() == 0)
                        {
                            continue;
                        }

                        bool wonderChannel = false;

                        //just verify that the current channel is actually a digi logger. 
                        for (int i = 0; i < channels.Count; i++)
                        {
                            if (channels[i].Channel.ToLower() == sensor.CurrentEquation.LoggerChannel.Channel.ToLower())
                            {
                                wonderChannel = true;
                                break;
                            }
                        }

                        if (wonderChannel == false)
                        {
                            //next sensor
                            continue;
                        }

                        var newsmap = new SensorMap();

                        double value;
                        //Create a new chanel map record
                        newsmap.Sensor = sensor;
                        newsmap.Chanel = sensor.CurrentEquation.LoggerChannel.Channel;

                        var Lasttimestamp = SensorDBMethods.GetLastSensorTime(sensor.Id);

                        List<SensorData> res;

                        if (Lasttimestamp.DTO != null)
                        {
                            var dt = (DateTimeOffset)Lasttimestamp.DTO;
                            lt = dt.DateTime;
                            res = GetWonderwareData(station.Logger.UniqueId, dt);
                        }
                        else
                        {
                            if (station.StartDate.HasValue)
                            {
                                //fix this up and generalize later to use timezones timezones
                                res = GetWonderwareData(station.Logger.UniqueId, station.StartDate.Value);
                            }
                            else
                            {
                                res = GetWonderwareData(station.Logger.UniqueId, new DateTimeOffset(2017, 07, 13, 0, 0, 0, 0, new TimeSpan(0)));
                            }
                        }

                        ImportLog.SaveMonitoringDataToCSV("Data From wonderware Server", res, sensor.MonitorStationId, sensor.Id);

                        foreach (var sv in res)
                        {
                            v = sv.value;
                            var calculated = processData.EvaluateMSensor(sensor.CurrentEquation.Equation, sensor.CurrentEquation.LoggerChannel.Channel, v);
                            if (calculated != null)
                            {
                                value = (double)calculated;
                            }
                            else
                            {
                                value = v;
                            }

                            //according to the 
                            var dateOffset = new DateTimeOffset( DateTime.SpecifyKind(sv.timestamp, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0));
                            newsmap.table.Rows.Add(sensor.Id, value, dateOffset, 0);
                            newsmap.tableraw.Rows.Add(sensor.Id, value, dateOffset);
                            updateTimestamps.Add(dateOffset);
                        }
                        smapList.Add(newsmap);

                        updateTimestamps.Sort();
                        var ts = updateTimestamps.Distinct().ToList();

                        ImportLog.SaveTimestampListToCSV("Data From Digi Server", updateTimestamps);

                        var msensorupdate = processData.BulckUploadPhysicalSensors(smapList);

                        if (!msensorupdate.DTO)
                        {
                            var m = String.Empty;
                            foreach (var s in msensorupdate.ErrorResults)
                            {
                                m = m + "Key : " + s.Key + " , Value : " + s.Value + ", ";
                                ImportLog.AddLogEntryDebugging("Digi", "Error", station.Id, "", loggerId, s.Key + " " + s.Value, "There has been an error with the Digi Import", DateTimeOffset.UtcNow);
                            }

                            response.ErrorResults.Add("Could Not Import Data for Station" + station.Id, "Problem with Builk Import : " + m);
                            response.DTO = false;
                        }
                        else
                        {
                            var calculatedSensorsMap = new CalculatedSensorMap();

                            List<SensorDTO> toCalc = new List<SensorDTO>();
                            toCalc.Add(sensor);
                            calculatedSensorsMap.Sensors = toCalc;
                            calculatedSensorsMap.TimeSeries = ts;

                            var calsResult = processData.BulckUploadCalculatedsensors(calculatedSensorsMap);

                            var m = String.Empty;
                            if (!calsResult.DTO)
                            {
                                foreach (var s in calsResult.ErrorResults)
                                {
                                    m = m + "Key : " + s.Key + " , Value : " + s.Value + ", ";
                                }
                                response.ErrorResults.Add("Could Not Import Data for Sensor" + sensor.Id, "Problem with Builk Import Calculated Sensors : " + m);
                                response.DTO = false;
                            }

                            m = String.Empty;
                            if (calsResult.ServiceLogs.Count > 0)
                            {
                                foreach (var l in calsResult.ServiceLogs)
                                {
                                    m = m + "Key : " + l.Key + " , Value : " + l.Value + ", ";
                                }
                                response.ServiceLogs.Add("Warnings while Importing Sensor" + sensor.Id, "There were problems while importing data : " + m);
                            }
                            var updatedStation = SensorDBMethods.RecalculateStationLastTimeStamp(station.Id);
                        }
                    }
                }
            }
            return response;
        }

        /// <summary>
        /// The web request used in this revision is primarly meant for collecting aggregate data. I surmised another query after the fact, and this could be improved further
        /// https://online.wonderware.com/s/raghf7/apis/Historian/v2/ProcessValues?$filter=FQN+eq+%27Halton%20Rain%20Gauge%20Data.SCADA2.BSXTPB00-5L%27+and+DateTime+ge+2017-01-10T00:00:00Z+and+DateTime+lt+2017-08-01T10:00:00Z&Resolution=10
        /// 
        /// </summary>
        /// <param name="UniqueId"></param>
        /// <param name="DataStart"></param>
        /// <returns></returns>
        public static List<SensorData> GetWonderwareData(String UniqueId, DateTimeOffset DataStart)
        {

            List<SensorData> result = new List<SensorData>();
            String responseString;
            HttpWebResponse resp;
            WebRequest req;

            DataStart = DataStart.AddSeconds(1);

            req = WebRequest.Create(@"https://online.wonderware.com/s/raghf7/apis/historian/v2/AnalogSummary?$filter="  +
                UniqueId + "+ and+StartDateTime+ge+" + DataStart.ToUniversalTime().ToString("yyyy-MM-ddTHH\\:mm\\:ssZ") + "&Resolution=30000");

            req.Method = "GET";
            req.Headers["Authorization"] = "Basic " + Convert.ToBase64String(Encoding.Default.GetBytes("wmerritt@civi.ca:GWnM7Ybgmgm47F0JWPJw"));

            try { 
                resp = req.GetResponse() as HttpWebResponse;
            }
            catch (Exception ex)
            {   //just fail silently, add logging the future
                return new List<SensorData>();
            }

            using (Stream stream = resp.GetResponseStream())
            {
                StreamReader reader = new StreamReader(stream, Encoding.UTF8);
                responseString = reader.ReadToEnd();
            }

            var serializer = new JavaScriptSerializer();
            serializer.MaxJsonLength = Int32.MaxValue;

            var deserialized = serializer.DeserializeObject(responseString) as Dictionary<string, object>;

            result = DoRequestWork(deserialized, new List<SensorData>(),DataStart.ToUniversalTime().DateTime);

            bool hasPageNextValue = deserialized.Keys.Contains("@odata.nextLink");

            while(hasPageNextValue)
            {
                var nextPageValue = deserialized["@odata.nextLink"];
                req = WebRequest.Create(nextPageValue.ToString());

                req.Method = "GET";
                req.Headers["Authorization"] = "Basic " + Convert.ToBase64String(Encoding.Default.GetBytes("wmerritt@civi.ca:GWnM7Ybgmgm47F0JWPJw"));
                resp = req.GetResponse() as HttpWebResponse;

                using (Stream stream = resp.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(stream, Encoding.UTF8);
                    responseString = reader.ReadToEnd();
                }

                deserialized = serializer.DeserializeObject(responseString) as Dictionary<string, object>;
                result = DoRequestWork(deserialized,result,DataStart.ToUniversalTime().DateTime);

                hasPageNextValue = deserialized.Keys.Contains("@odata.nextLink");
            }
            //just return the non 0 values, or the last 0 value so the importer knows where to start next time
            if(result.Count > 0)
            {
                //the data start check is needed, the api treats the start date as more of a suggestion
                List<SensorData> resultNoZero = result.Where(x => x.value != 0).OrderBy(y => y.timestamp).ToList();

                if(resultNoZero.Count() == 0)
                {
                    resultNoZero.Add(result.OrderBy(y => y.timestamp).Last());
                }
                return resultNoZero;
            }
            return result;
        }

        private static List<SensorData> DoRequestWork(Dictionary<string, object> deserialized, List<SensorData> Existing,DateTime DataStart)
        {
            bool hasValue = false;

            hasValue = deserialized.Keys.Contains("value");

            if (hasValue)
            {
                object[] valueHolder = (object[])deserialized["value"];

                for (int i = 0; i < valueHolder.Count(); i++)
                {
                    SensorData y = new SensorData();
                    SensorData y2 = new SensorData();

                    //double check the to UTC
                    y.value = Convert.ToDouble(((Dictionary<string, object>)valueHolder[i])["First"]);
                    y.timestamp = DateTime.Parse(((Dictionary<string, object>)valueHolder[i])["FirstDateTime"].ToString()).ToUniversalTime();

                    y2.value = Convert.ToDouble(((Dictionary<string, object>)valueHolder[i])["Last"]);
                    y2.timestamp = DateTime.Parse(((Dictionary<string, object>)valueHolder[i])["LastDateTime"].ToString()).ToUniversalTime();

                    if (!Existing.Any(z => z.timestamp == y.timestamp) && y.timestamp > DataStart)
                    {
                        if (!(y.timestamp < new DateTime(2000, 1, 1)) || !double.IsNaN(y.value))
                        {
                            y.value = Math.Round(y.value, 3);
                            Existing.Add(y);
                        }
                    }
                    if (!Existing.Any(z => z.timestamp == y2.timestamp) && y2.timestamp > DataStart)
                    {
                        if (!(y2.timestamp < new DateTime(2000, 1, 1)) || !double.IsNaN(y2.value))
                        {
                            y2.value = Math.Round(y2.value, 3);
                            Existing.Add(y2);
                        }
                    }
                }
            }
            return Existing;
        }
    }
}
