﻿using System;
using System.Collections.Generic;
using System.Linq;
using Civica.BLL.BaseClasses;
using System.IO;
using NLog;
using Civica.BLL.Models;
using Civica.DBL.DBClasses;
using System.Collections;
using System.Globalization;
using Civica.Infrastructure.BaseClasses;
using Civica.BLL.CommonClasses;
using Civica.BLL.ServiceClasses.DataImporter.Interfaces;
using Civica.BLL.ServiceClasses.DataImporter;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Threading;

namespace Civica.BLL.ServiceClasses.DataImporter
{
    public class DigiConnectSensor : LoggerBase, IDigiConnectSensor
    {
        private static string[] formats = new string[]
       {
            "dd/MM/yyyy h:mm:ss tt",
            "d/MM/yyyy h:mm:ss tt"
       };


        [XmlRoot(ElementName = "DataPoint")]
        public class DataPoint
        {
            [XmlElement(ElementName = "id")]
            public string Id { get; set; }
            [XmlElement(ElementName = "cstId")]
            public string CstId { get; set; }
            [XmlElement(ElementName = "streamId")]
            public string StreamId { get; set; }
            [XmlElement(ElementName = "timestamp")]
            public string Timestamp { get; set; }
            [XmlElement(ElementName = "timestampISO")]
            public string TimestampISO { get; set; }
            [XmlElement(ElementName = "serverTimestamp")]
            public string ServerTimestamp { get; set; }
            [XmlElement(ElementName = "serverTimestampISO")]
            public string ServerTimestampISO { get; set; }
            [XmlElement(ElementName = "data")]
            public double Data { get; set; }
            [XmlElement(ElementName = "description")]
            public string Description { get; set; }
            [XmlElement(ElementName = "quality")]
            public string Quality { get; set; }
        }

        [XmlRoot(ElementName = "result")]
        public class Result
        {
            [XmlElement(ElementName = "resultSize")]
            public string ResultSize { get; set; }
            [XmlElement(ElementName = "requestedSize")]
            public string RequestedSize { get; set; }
            [XmlElement(ElementName = "pageCursor")]
            public string PageCursor { get; set; }
            [XmlElement(ElementName = "requestedStartTime")]
            public string RequestedStartTime { get; set; }
            [XmlElement(ElementName = "requestedEndTime")]
            public string RequestedEndTime { get; set; }
            [XmlElement(ElementName = "DataPoint")]

            //the important one
            public List<DataPoint> DataPoint { get; set; }
        }

        public bool Transfer(ref DCLogger logger)
        {
            var ttime = string.Empty;
            var bc = new LoggerBase();

            var response = new ServiceResult<bool>(true);
            var sectionsize = 10;
            var sectioncount = 0;

            List<List<StationDTO>> stationsections = new List<List<StationDTO>>();
            List<Civica.Domain.CivicaDomain.MonitoringLoggerChannel> channels = SensorEquationDBMethods.GetMonitoringLoggerChannels((int)BaseEnum.LoggerType.DigiConnectSensor);
            try
            {
                var stations = StationDBMethods.GetListOfStationsByLoggerType((int)BaseEnum.LoggerType.DigiConnectSensor);

                if (stations != null)
                {
                    if (stations.Count() > 0)
                    {
                        var section = new List<StationDTO>();

                        //choping the list of stations in chuncks, so we can parallelize the process
                        foreach (var station in stations)
                        {
                            if (sectioncount < sectionsize)
                            {
                                section.Add(StationDTO.Create(station));
                                sectioncount++;
                            }
                            else
                            {
                                section.Add(StationDTO.Create(station));
                                stationsections.Add(section);
                                section = new List<StationDTO>();
                                sectioncount = 0;
                            }
                        }

                        stationsections.Add(section);

                        if (stationsections != null)
                        {
                            if (stationsections.Count() > 0)
                            {
                                foreach (var s in stationsections)
                                {
                                    var re = ProcessSection(s, channels);
                                }
                            }
                        }
                    }
                    else
                    {
                        response.AddError("Process Error", "No Stations Found");
                    }
                }
                else
                {
                    response.AddError("Process Error", "No Stations Found");
                }

            }
            catch (Exception e)
            {
                response.AddError("System Error", e.Message);
            }
            return response.DTO;
        }
        private ServiceResult<bool> ProcessSection(List<StationDTO> section, List<Civica.Domain.CivicaDomain.MonitoringLoggerChannel> channels)
        {
            var t = true;
            var response = new ServiceResult<bool>(t);
            
            foreach (var station in section)
            {
                var loggerId = station.Logger.UniqueId;

                if (station != null && station.Sensors.Count > 0)
                {
                    foreach (var sensor in station.Sensors)
                    {
                        var smapList = new List<SensorMap>();
                        double v = 0.0;
                        var processData = new DataProcessing();
                        var updateTimestamps = new List<DateTimeOffset>();
                        var aggregate = string.Empty;
                        var lt = DateTime.Now;

                        //if there are no equations, the automated importer of data won't do anything
                        if (sensor.Equations.Count() == 0)
                        {
                            continue;
                        }

                        bool digiChannel = false;

                        //just verify that the current channel is actually a digi logger. 
                        for (int i = 0; i < channels.Count; i++)
                        {
                            if (channels[i].Channel.ToLower() == sensor.CurrentEquation.LoggerChannel.Channel.ToLower())
                            {
                                digiChannel = true;
                                break;
                            }
                        }

                        if (digiChannel == false)
                        {
                            //next sensor
                            continue;
                        }

                        var newsmap = new SensorMap();

                        double value;
                        //Create a new chanel map record
                        newsmap.Sensor = sensor;
                        newsmap.Chanel = sensor.CurrentEquation.LoggerChannel.Channel;

                        var Lasttimestamp = SensorDBMethods.GetLastSensorTime(sensor.Id);

                        if (Lasttimestamp.DTO != null)
                        {
                            ImportLog.AddLogEntryDebugging("FTSTCP", "Information", station.Id, "", station.Logger.UniqueId, "Getting Data From " + Lasttimestamp.DTO.Value.ToString(), "", DateTimeOffset.UtcNow);
                        }
                        else
                        {
                            ImportLog.AddLogEntryDebugging("FTSTCP", "Information", station.Id, "", station.Logger.UniqueId, "Getting Data From beginning of time", "", DateTimeOffset.UtcNow);
                        }

                        List<SensorData> res;

                        if (Lasttimestamp.DTO != null)
                        {
                            var dt = (DateTimeOffset)Lasttimestamp.DTO;
                            lt = dt.ToOffset(station.TimeZoneOffset).DateTime;
                            res = GetDigiData(station.Logger.UniqueId, newsmap.Chanel.ToLower(), dt, sensor.TimeInterval.Value);
                        }
                        else
                        {
                            if (station.StartDate.HasValue)
                            {
                                //fix this up and generalize later to use timezones timezones
                                res = GetDigiData(station.Logger.UniqueId, newsmap.Chanel.ToLower(), station.StartDate.Value, sensor.TimeInterval.Value);
                            }
                            else
                            {
                                res = GetDigiData(station.Logger.UniqueId, newsmap.Chanel.ToLower(), DateTimeOffset.MinValue, sensor.TimeInterval.Value);
                            }
                        }
                        if (res.Count < 3)
                            continue;

                        ImportLog.SaveMonitoringDataToCSV("Data From Digi Server", res, sensor.MonitorStationId, sensor.Id);

                        foreach (var sv in res)
                        {
                            v = sv.value;
                            var calculated = processData.EvaluateMSensor(sensor.CurrentEquation.Equation, sensor.CurrentEquation.LoggerChannel.Channel, v);
                            if (calculated != null)
                            {
                                value = (double)calculated;
                            }
                            else
                            {
                                value = v;
                            }
                            //according to the 
                            var dateOffset = new DateTimeOffset(sv.timestamp, new TimeSpan(0, 0, 0));
                            newsmap.table.Rows.Add(sensor.Id, value, dateOffset, 0);
                            newsmap.tableraw.Rows.Add(sensor.Id, value, dateOffset);
                            updateTimestamps.Add(dateOffset);
                        }
                        smapList.Add(newsmap);

                        updateTimestamps.Sort();
                        var ts = updateTimestamps.Distinct().ToList();

                        ImportLog.SaveTimestampListToCSV("Data From Digi Server", updateTimestamps);

                        var msensorupdate = processData.BulckUploadPhysicalSensors(smapList);

                        if (!msensorupdate.DTO)
                        {
                            var m = String.Empty;
                            foreach (var s in msensorupdate.ErrorResults)
                            {
                                m = m + "Key : " + s.Key + " , Value : " + s.Value + ", ";
                                ImportLog.AddLogEntryDebugging("Digi", "Error", station.Id, "", loggerId, s.Key + " " + s.Value, "There has been an error with the Digi Import", DateTimeOffset.UtcNow);
                            }

                            response.ErrorResults.Add("Could Not Import Data for Station" + station.Id, "Problem with Builk Import : " + m);
                            response.DTO = false;
                        }
                        else
                        {
                            var calculatedSensorsMap = new CalculatedSensorMap();

                            List<SensorDTO> toCalc = new List<SensorDTO>();
                            toCalc.Add(sensor);
                            calculatedSensorsMap.Sensors = toCalc;
                            calculatedSensorsMap.TimeSeries = ts;

                            var calsResult = processData.BulckUploadCalculatedsensors(calculatedSensorsMap);

                            var m = String.Empty;
                            if (!calsResult.DTO)
                            {

                                foreach (var s in calsResult.ErrorResults)
                                {
                                    m = m + "Key : " + s.Key + " , Value : " + s.Value + ", ";
                                }
                                response.ErrorResults.Add("Could Not Import Data for Sensor" + sensor.Id, "Problem with Builk Import Calculated Sensors : " + m);
                                response.DTO = false;
                            }

                            m = String.Empty;
                            if (calsResult.ServiceLogs.Count > 0)
                            {
                                foreach (var l in calsResult.ServiceLogs)
                                {
                                    m = m + "Key : " + l.Key + " , Value : " + l.Value + ", ";
                                }
                                response.ServiceLogs.Add("Warnings while Importing Sensor" + sensor.Id , "There were problems while importing data : " + m);
                            }
                            var updatedStation = SensorDBMethods.RecalculateStationLastTimeStamp(station.Id);
                        }
                    }
                }
            }
            return response;
        }

        public static List<SensorData> GetDigiData(String UniqueId, string ChannelId, DateTimeOffset DataStart, decimal SensorTimestamp)
        {

            ImportLog.AddLogEntryDebugging("Digi", "Debug", 0, "", UniqueId,"Beginning fetch data for " + UniqueId.ToString() + " fetch from " + DataStart.ToString(), "", DateTimeOffset.Now.ToUniversalTime());
            Thread.Sleep(1000 * 5);
            //add one second becasue the API does >=, and the first returned result would be dupliacate otherwise
            DataStart = DataStart.AddSeconds(1);
            WebRequest req;
            if (DataStart.Offset.Duration().TotalSeconds == 0)
            {
                //it is in UTC, a hack cause the digi api is broken,
                req = WebRequest.Create(@"https://devicecloud.digi.com/ws/DataPoint/" + UniqueId + "/" + ChannelId + "?startTime=" + DataStart.ToString("yyyy-MM-ddTHH:mm:ssZ"));
            }
            else
            {
                req = WebRequest.Create(@"https://devicecloud.digi.com/ws/DataPoint/" + UniqueId + "/" + ChannelId + "?startTime=" + DataStart.ToString("yyyy-MM-ddTHH\\:mm\\:ss.fffffffzzz"));
            }
           
            req.Method = "GET";
            req.Headers["Authorization"] = "Basic " + Convert.ToBase64String(Encoding.Default.GetBytes("wmerritt:APHOsINkbThV7CkRStbx!"));
            //req.Credentials = new NetworkCredential("username", "password");
            HttpWebResponse resp = req.GetResponse() as HttpWebResponse;
            XmlSerializer serializer = new XmlSerializer(typeof(Result));
            Result deserialized = (Result)serializer.Deserialize(resp.GetResponseStream());

            List<SensorData> result = new List<SensorData>();

            foreach (DataPoint x in deserialized.DataPoint)
            {
                SensorData y = new SensorData();
                y.value = x.Data;
                y.timestamp = DateTime.Parse(x.TimestampISO).ToUniversalTime();
                y.timestamp = Civica.Infrastructure.DateExtensions.Round(y.timestamp, new TimeSpan(0, (int)SensorTimestamp, 0));

                if (result.Count > 0 && result[result.Count - 1].timestamp != y.timestamp)
                {
                    result.Add(y);
                }
                else if (result.Count == 0)
                {
                    result.Add(y);
                }
            }

            //api only returns chunks of 1000, need to get the next page
            while (deserialized.DataPoint.Count == 1000)
            {
                Thread.Sleep(1000 * 5);
                req = WebRequest.Create(@"https://devicecloud.digi.com/ws/DataPoint/" + UniqueId + "/" + ChannelId + "?pageCursor=" + deserialized.PageCursor);
                req.Method = "GET";
                req.Headers["Authorization"] = "Basic " + Convert.ToBase64String(Encoding.Default.GetBytes("wmerritt:APHOsINkbThV7CkRStbx!"));
                //req.Credentials = new NetworkCredential("username", "password");
                resp = req.GetResponse() as HttpWebResponse;
                serializer = new XmlSerializer(typeof(Result));
                deserialized = (Result)serializer.Deserialize(resp.GetResponseStream());

                foreach (DataPoint x in deserialized.DataPoint)
                {
                    SensorData y = new SensorData();
                    y.value = x.Data;
                    y.timestamp = DateTime.Parse(x.TimestampISO).ToUniversalTime();
                    y.timestamp = Civica.Infrastructure.DateExtensions.Round(y.timestamp, new TimeSpan(0, (int)SensorTimestamp, 0));

                    if (result.Count > 0 && result[result.Count - 1].timestamp != y.timestamp) { 
                        result.Add(y);
                    }
                    else if (result.Count == 0)
                    {
                        result.Add(y);
                    }
                }
            }
            return result;
        }
    }
}
