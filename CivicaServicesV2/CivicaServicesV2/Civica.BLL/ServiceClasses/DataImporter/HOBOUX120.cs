﻿using System;
using System.Collections.Generic;
using System.Linq;
using Civica.BLL.BaseClasses;
using System.IO;
using NLog;
using Civica.BLL.Models;
using Civica.DBL.DBClasses;
using System.Collections;
using System.Globalization;
using Civica.Infrastructure.BaseClasses;
using Civica.BLL.CommonClasses;
using Civica.BLL.ServiceClasses.DataImporter.Interfaces;

namespace Civica.BLL.ServiceClasses.DataImporter
{

    public class HOBOUX120 : LoggerBase, IHOBOUX120
    {

        private static string[] formats = new string[]
        {
            "dd/MM/yyyy h:mm:ss tt",
            "d/MM/yyyy h:mm:ss tt",
            "d/MM/yyyy HH:mm:ss",
            "MM/dd/yy hh:mm:ss tt",
            "yy-MM-dd HH:mm:ss"
        };

        public bool StartHOBOUX120(string path, ref DCLogger logger)
        {
            var t = true;
            var ttime = string.Empty;
            string monitorid = String.Empty;
            int lineCount = 0;
            var bc = new LoggerBase();
            var processData = new DataProcessing();

            try
            {
                var res = bc.GetFilesInDirectory(path);

                if (res != null)
                {
                    if (res.Files != null)
                    {
                        if (res.Files.Count > 0)
                        {
                            foreach (var f in res.Files)
                            {
                                bool r = true;
                                var response = new ServiceResult<bool>(r);
                                string filename = String.Empty;
                                string extension = String.Empty;
                                int stationId = 0;
                                string uniqueId = String.Empty;

                                var updateTimestamps = new List<DateTimeOffset>();
                                var p = f.FilePath.LastIndexOf("\\");
                                filename = f.FilePath.Substring(p + 1);

                                var c = filename.LastIndexOf(".");
                                extension = filename.Substring(c + 1);

                                if (extension.ToUpper() != "CSV")
                                {
                                    var archivepath = f.RootDirectory + "\\Other\\" + filename;

                                    if (File.Exists(archivepath))
                                    {
                                        File.Delete(archivepath);
                                    }
                                    File.Move(f.FilePath, archivepath);

                                    continue;
                                }


                                try
                                {
                                    using (var file = System.IO.File.OpenText(f.FilePath))
                                    {
                                        var name = ((FileStream)file.BaseStream).Name;

                                        // Parse file here
                                        var smapList = new List<SensorMap>();

                                        String line1 = file.ReadLine();
                                        if (line1 == null)
                                        {
                                            response.DTO = false;
                                            response.AddError("FileError", "The File : " + name + " has no lines.");
                                            continue;
                                        }

                                        //Sample Header Line
                                        //    "#","Date Time, GMT-04:00","Scaled Series, M (LGR S/N: 10903444)"

                                        String line2 = file.ReadLine();
                                        if (line2 == null)
                                        {
                                            response.DTO = false;
                                            response.AddError("FileError", "The File : " + name + " has no header line.");
                                            continue;
                                        }

                                        line2 =  line2.Replace("\",\"", "|");

                                        var headerFields = line2.Split('|');

                                        if (headerFields.Count() < 3)
                                        {
                                            response.DTO = false;
                                            response.AddError("FileError", "The File : " + name + " has less than three fields in its header Line.");
                                            continue;
                                        }

                                        var offsetValue = 0.0;

                                        for (int i = 1; i <= headerFields.Length - 1; i++)
                                        {
                                            if (headerFields[i].Contains(""))
                                            {
                                                break;
                                            }
                                        }

                                        var datetimeOffset = headerFields[1].Replace(" ", "").Split(',');
                                        var dtof = datetimeOffset[1].Remove(0, 4);
                                        dtof = dtof.Replace(":", ".");

                                        if (!Double.TryParse(dtof, out offsetValue))
                                        {
                                            response.DTO = false;
                                            response.AddError("FileError", "The Date Field does not have properly defined time offset.");
                                            continue;
                                        }

                                        var indexofLGR = headerFields[2].LastIndexOf("LGR S/N:");

                                        var str = headerFields[2].Remove(0, indexofLGR + 9);
                                        var lastBracket = str.IndexOf(")");


                                        monitorid = str.Substring(0, lastBracket);

                                        // Retriving the station information based on unique ID
                                        var stres = StationDBMethods.GetStationByUniqueId(monitorid);
                                        if (stres == null)
                                        {
                                            continue;
                                        }

                                        stationId = stres.Id;

                                        var station = StationDTO.Create(stres);

                                        foreach (var sen in station.Sensors)
                                        {
                                            var newsmap = new SensorMap();
                                            newsmap.Sensor = sen;
                                            var lsttstres = SensorDBMethods.GetLastSensorTime(sen.Id);
                                            if(lsttstres != null)
                                            {
                                                if (lsttstres.IsSuccess)
                                                {
                                                    if(lsttstres.DTO != null)
                                                    {
                                                        newsmap.LasTimeStamp = lsttstres.DTO;
                                                    } 
                                                }
                                            }                                       
                                            

                                            switch (sen.CurrentEquation.MonitoringLoggerChannelId)
                                            {
                                                case (int)BaseEnum.HOBOChanels.Scaled_Series_1:
                                                    newsmap.Position = 2;
                                                    break;

                                                case (int)BaseEnum.HOBOChanels.Scaled_Series_2:
                                                    newsmap.Position = 3;
                                                    break;

                                                case (int)BaseEnum.HOBOChanels.Scaled_Series_3:
                                                    newsmap.Position = 4;
                                                    break;

                                                case (int)BaseEnum.HOBOChanels.Scaled_Series_4:
                                                    newsmap.Position = 5;
                                                    break;
                                            }

                                            smapList.Add(newsmap);
                                        }

                                        if (smapList.Count > 0)
                                        {
                                            string filedataline = String.Empty;
                                            var breakout = false;
                                            while (file.Peek() >= 0)  // loop through the file lines
                                            {
                                                filedataline = file.ReadLine();
                                                if (filedataline == null) { break; }
                                                lineCount++;
                                                var dataLine = filedataline.Split(',');
                                                double val = 0;
                                                
                                                 
                                                //ProcessData The First Line
                                                foreach (var sen in smapList)
                                                {
                                                    if (sen.Position > dataLine.Count() - 1)
                                                    {
                                                        continue;
                                                    }

                                                    SensorData d = new SensorData();

                                                    var timeInterval = sen.Sensor.TimeInterval;



                                                    var datestamp = dataLine[1];
                                                    if (datestamp == " 16 - 07 - 31 12:38:30")
                                                    {
                                                        var s = datestamp;
                                                    }
                                                    var value = dataLine[sen.Position];

                                                    if ((datestamp == null || datestamp == String.Empty) && (value == null || value == String.Empty))
                                                    {
                                                        continue;
                                                    }

                                                    if ((value == null) || (!double.TryParse(value, out val)))
                                                    {
                                                        continue;
                                                    }

                                                    var calculated = processData.EvaluateMSensor(sen.Sensor.CurrentEquation.Equation, sen.Sensor.CurrentEquation.LoggerChannel.Channel, val);
                                                    if (calculated != null)
                                                    {
                                                        val = (double)calculated;
                                                    }
                                                    if (!DateTime.TryParse(datestamp, out d.timestamp))
                                                    {
                                                        if (!DateTime.TryParseExact(datestamp, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out d.timestamp))
                                                        {
                                                            continue;
                                                        }
                                                    }


                                                    if (d.timestamp.Second != 0)
                                                    {
                                                        continue;
                                                    }

                                                    var minutes = d.timestamp.Minute;
                                                    if (minutes % timeInterval != 0)
                                                    {
                                                        continue;
                                                    }

                                                    var dateOffset = new DateTimeOffset(d.timestamp, station.TimeZoneOffset);

                                                    if (dateOffset < sen.LasTimeStamp)
                                                    {
                                                        continue;
                                                    }

                                                    sen.table.Rows.Add(sen.Sensor.Id, Math.Round(val,6), dateOffset, 0);
                                                    sen.tableraw.Rows.Add(sen.Sensor.Id, Math.Round(val,6), dateOffset);

                                                    updateTimestamps.Add(dateOffset);
                                                }


                                                if(breakout)
                                                {
                                                    break;
                                                }
                                            }
                                        }


                                        //Common data upload for physical and calculated sensors
                                        // need to be enclosed in as many if{} statments, as need, for checking the correctness of 
                                        // the data in the file while reading.  Here the reading of the file has tobe done and sensor maps  
                                        // and timestamp intervals collected

                                        if (response.DTO)
                                        {

                                            updateTimestamps.Sort();
                                            var ts = updateTimestamps.Distinct().ToList();

                                            var msensorupdate = processData.BulckUploadPhysicalSensors(smapList);

                                            if (!msensorupdate.DTO)
                                            {
                                                var m = String.Empty;
                                                foreach (var s in msensorupdate.ErrorResults)
                                                {
                                                    m = m + "Key : " + s.Key + " , Value : " + s.Value + ", ";
                                                }
                                                response.ErrorResults.Add("Could Not Import Data", "Problem with Builk Import : " + m);
                                                response.DTO = false;

                                            }
                                            else
                                            {
                                                var calculatedSensorsMap = new CalculatedSensorMap();

                                                calculatedSensorsMap.Sensors = station.Sensors.Where(x => x.Equations.Count() > 0).ToList();

                                                calculatedSensorsMap.TimeSeries = ts;

                                                var calsResult = processData.BulckUploadCalculatedsensors(calculatedSensorsMap);
                                                var m = String.Empty;
                                                if (!calsResult.DTO)
                                                {
                                                    m = String.Empty;
                                                    foreach (var s in calsResult.ErrorResults)
                                                    {
                                                        m = m + "Key : " + s.Key + " , Value : " + s.Value + ", ";
                                                    }
                                                    response.ErrorResults.Add("Could Not Import Data", "Problem with Builk Import Calculated Sensors : " + m);
                                                    response.DTO = false;

                                                }

                                                m = String.Empty;
                                                if (!calsResult.DTO)
                                                {

                                                    foreach (var s in calsResult.ErrorResults)
                                                    {
                                                        m = m + "Key : " + s.Key + " , Value : " + s.Value + ", ";
                                                    }
                                                    response.ErrorResults.Add("Could Not Import Data", "Problem with Builk Import Calculated Sensors : " + m);

                                                    response.DTO = false;
                                                }

                                                m = String.Empty;
                                                if (calsResult.ServiceLogs.Count > 0)
                                                {
                                                    foreach (var l in calsResult.ServiceLogs)
                                                    {
                                                        m = m + "Key : " + l.Key + " , Value : " + l.Value + ", ";
                                                    }
                                                    response.ServiceLogs.Add("Warnings while Importing", "There were problems while importing data : " + m);
                                                }

                                                var updatedStation = SensorDBMethods.RecalculateStationLastTimeStamp(station.Id);
                                            }
                                        }
                                        file.Close();
                                    } // End of   Using a specific file

                                }
                                catch (IOException ioe)
                                {
                                    var m = ioe.Message;
                                    continue;
                                }
                                catch (Exception e)
                                {
                                    var archivepath = f.RootDirectory + "\\ErrorArchive\\" + DateTime.Now.ToString("yyyy_MM_dd_hh_mm_ss") + "_" + filename;

                                    if (File.Exists(archivepath))
                                    {
                                        File.Delete(archivepath);
                                    }
                                    File.Move(f.FilePath, archivepath);
                                    ImportLog.AddLogEntry("HOBOware", "Error", stationId, filename, uniqueId, e.Message, e.StackTrace, DateTimeOffset.Now.ToUniversalTime());
                                    continue;
                                }


                                if (response.DTO)
                                {
                                    var archivepath = f.RootDirectory + "\\Archive\\" +  DateTime.Now.ToString("yyyy_MM_dd_hh_mm_ss") + "_" + filename; 

                                    if (File.Exists(archivepath))
                                    {
                                        File.Delete(archivepath);
                                    }
                                    File.Move(f.FilePath, archivepath);
                                    if (response.ServiceLogs != null)
                                    {

                                        if (response.ServiceLogs.Count > 0)
                                        {
                                            var message = String.Empty;
                                            foreach (var ms in response.ServiceLogs)
                                            {
                                                message = message + " / " + ms.Key + " : " + ms.Value;

                                            }
                                            ImportLog.AddLogEntry("HOBOware", "Warning", stationId, filename, uniqueId, message, "", DateTimeOffset.Now.ToUniversalTime());
                                        }
                                    }

                                }
                                else
                                {
                                    var archivepath = f.RootDirectory + "\\ErrorArchive\\" + DateTime.Now.ToString("yyyy_MM_dd_hh_mm_ss") + "_" + filename; 

                                    if (File.Exists(archivepath))
                                    {
                                        File.Delete(archivepath);
                                    }
                                    File.Move(f.FilePath, archivepath);
                                    var message = String.Empty;
                                    if (response.ErrorResults != null)
                                    {
                                        foreach (var ms in response.ErrorResults)
                                        {
                                            message = message + " / " + ms.Key + " : " + ms.Value;

                                        }


                                    }

                                    ImportLog.AddLogEntry("HOBOware", "Error", stationId, filename, uniqueId, message, "", DateTimeOffset.Now.ToUniversalTime());
                                }

                            } // End of   foreach (var f in res.Files)
                        } // End of   if (res.Files.Count > 0)
                    } // End of   if (res.Files != null)
                } // End of   if (res != null)

            }
            catch (IOException e)
            {
                var m = e.Message;
            }
            catch (Exception e)
            {
                var m = e.Message;
            }

            return t;
        }

    }

}
