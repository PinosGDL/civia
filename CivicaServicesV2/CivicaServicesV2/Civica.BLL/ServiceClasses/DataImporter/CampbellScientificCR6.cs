﻿using System;
using System.Collections.Generic;
using System.Linq;
using Civica.BLL.BaseClasses;
using System.IO;
using NLog;
using Civica.BLL.Models;
using Civica.DBL.DBClasses;
using System.Collections;
using System.Globalization;
using Civica.Infrastructure.BaseClasses;
using Civica.BLL.CommonClasses;
using Civica.BLL.ServiceClasses.DataImporter.Interfaces;
using Civica.BLL.ServiceClasses.DataImporter;
using System.Net;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using HtmlAgilityPack;

namespace Civica.BLL.ServiceClasses.DataImporter
{
    public class CampbellScientificCR6 : LoggerBase, ICampbellScientificCR6
    {
        private static string[] formats = new string[]
       {
            "dd/MM/yyyy h:mm:ss tt",
            "d/MM/yyyy h:mm:ss tt"
       };

        public bool Transfer(ref DCLogger logger)
        {
            var ttime = string.Empty;
            var bc = new LoggerBase();

            var response = new ServiceResult<bool>(true);
            var sectionsize = 10;
            var sectioncount = 0;

            List<List<StationDTO>> stationsections = new List<List<StationDTO>>();
            List<Civica.Domain.CivicaDomain.MonitoringLoggerChannel> channels = SensorEquationDBMethods.GetMonitoringLoggerChannels((int)BaseEnum.LoggerType.CampbellScientificCR6);
            try
            {
                var stations = StationDBMethods.GetListOfStationsByLoggerType((int)BaseEnum.LoggerType.CampbellScientificCR6);

                if (stations != null)
                {
                    if (stations.Count() > 0)
                    {
                        var section = new List<StationDTO>();

                        //choping the list of stations in chuncks, so we can parallelize the process
                        foreach (var station in stations)
                        {
                            section.Add(StationDTO.Create(station));

                            if (sectioncount < sectionsize)
                            {
                                sectioncount++;
                            }
                            else
                            {
                                stationsections.Add(section);
                                section = new List<StationDTO>();
                                sectioncount = 0;
                            }
                        }

                        stationsections.Add(section);

                        if (stationsections != null)
                        {
                            if (stationsections.Count() > 0)
                            {
                                foreach (var s in stationsections)
                                {
                                    var re = ProcessSection(s, channels);
                                }
                            }
                        }
                    }
                    else
                    {
                        response.AddError("Process Error", "No Stations Found");
                    }
                }
                else
                {
                    response.AddError("Process Error", "No Stations Found");
                }

            }
            catch (Exception e)
            {
                response.AddError("System Error", e.Message);
            }
            return response.DTO;
        }

        private ServiceResult<bool> ProcessSection(List<StationDTO> section, List<Civica.Domain.CivicaDomain.MonitoringLoggerChannel> channels)
        {
            var t = true;

            double v = 0.0;
            var response = new ServiceResult<bool>(t);
            var processData = new DataProcessing();
            var updateTimestamps = new List<DateTimeOffset>();
            var aggregate = string.Empty;
            var lt = DateTime.Now;

            foreach (var station in section)
            {
                var loggerId = station.Logger.UniqueId;

                if (station != null && station.Sensors.Count > 0)
                {
                    var smapList = new List<SensorMap>();

                    foreach (var sensor in station.Sensors)
                    {
                        //if there are no equations, the automated importer of data won't do anything
                        if (sensor.Equations.Count() == 0)
                        {
                            continue;
                        }

                        bool CS6Channel = false;

                        //just verify that the current channel is actually a CR6 logger. 
                        for (int i = 0; i < channels.Count; i++)
                        {
                            if (channels[i].Channel.ToLower() == sensor.CurrentEquation.LoggerChannel.Channel.ToLower())
                            {
                                CS6Channel = true;
                                break;
                            }
                        }

                        if (CS6Channel == false)
                        {
                            //next sensor
                            continue;
                        }

                        var newsmap = new SensorMap();

                        double value;
                        //Create a new chanel map record
                        newsmap.Sensor = sensor;

                        newsmap.Chanel = sensor.CurrentEquation.LoggerChannel.Channel;


                        var Lasttimestamp = SensorDBMethods.GetLastSensorTime(sensor.Id);

                        List<SensorData> res;

                        if (Lasttimestamp.DTO != null)
                        {
                            var dt = (DateTimeOffset)Lasttimestamp.DTO;
                            lt = dt.ToOffset(station.TimeZoneOffset).DateTime;
                            res = GetCS6Data(station.Logger.UniqueId, newsmap.Chanel.ToLower(), dt,sensor.TimeInterval.Value);
                        }
                        else
                        {
                            if (station.StartDate.HasValue)
                            {
                                //fix this up and generalize later to use timezones timezones
                                res = GetCS6Data(station.Logger.UniqueId, newsmap.Chanel.ToLower(), station.StartDate.Value,sensor.TimeInterval.Value);
                            }
                            else {
                                res = GetCS6Data(station.Logger.UniqueId, newsmap.Chanel.ToLower(), DateTimeOffset.MinValue,sensor.TimeInterval.Value);
                            }
                        }

                        foreach (var sv in res)
                        {
                            v = sv.value;
                            var calculated = processData.EvaluateMSensor(sensor.CurrentEquation.Equation, sensor.CurrentEquation.LoggerChannel.Channel, v);
                            if (calculated != null)
                            {
                                value = (double)calculated;
                            }
                            else
                            {
                                value = v;
                            }
                            //according to the 
                            var dateOffset = new DateTimeOffset(sv.timestamp, station.TimeZoneOffset);
                            newsmap.table.Rows.Add(sensor.Id, value, dateOffset, 0);
                            newsmap.tableraw.Rows.Add(sensor.Id, value, dateOffset);
                            updateTimestamps.Add(dateOffset);
                        }

                        smapList.Add(newsmap);
                    }

                    updateTimestamps.Sort();
                    var ts = updateTimestamps.Distinct().ToList();

                    var msensorupdate = processData.BulckUploadPhysicalSensors(smapList);

                    if (!msensorupdate.DTO)
                    {
                        var m = String.Empty;
                        foreach (var s in msensorupdate.ErrorResults)
                        {
                            m = m + "Key : " + s.Key + " , Value : " + s.Value + ", ";
                        }

                        try
                        { 
                            response.ErrorResults.Add("Could Not Import Data", "Problem with Builk Import : " + m);
                            response.DTO = false;
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    else
                    {
                        var calculatedSensorsMap = new CalculatedSensorMap();

                        calculatedSensorsMap.Sensors = station.Sensors.Where(x => x.Equations.Count() > 0).ToList();
                        calculatedSensorsMap.TimeSeries = ts;

                        var calsResult = processData.BulckUploadCalculatedsensors(calculatedSensorsMap);

                        var m = String.Empty;
                        if (!calsResult.DTO)
                        {

                            foreach (var s in calsResult.ErrorResults)
                            {
                                m = m + "Key : " + s.Key + " , Value : " + s.Value + ", ";
                            }
                            try
                            { 
                                response.ErrorResults.Add("Could Not Import Data", "Problem with Builk Import Calculated Sensors : " + m);
                                response.DTO = false;
                            }
                            catch (Exception ex)
                            {

                            }
                        }

                        m = String.Empty;
                        if (calsResult.ServiceLogs.Count > 0)
                        {
                            foreach (var l in calsResult.ServiceLogs)
                            {
                                m = m + "Key : " + l.Key + " , Value : " + l.Value + ", ";
                            }
                            try
                            { 
                                response.ServiceLogs.Add("Warnings while Importing", "There were problems while importing data : " + m);
                            }
                            catch(Exception Ex)
                            {

                            }
                        }
                        var updatedStation = SensorDBMethods.RecalculateStationLastTimeStamp(station.Id);

                    }
                }
            }

            return response;
        }

        public static List<SensorData> GetCS6Data(String UniqueId, string ChannelId, DateTimeOffset DataStart, decimal SensorTimestamp)
        {
            //add one second becasue the API does >=, and the first returned result would be dupliacate otherwise
            List<SensorData> result = new List<SensorData>();
            DataStart = DataStart.AddSeconds(1);
            WebRequest req;

            int indexNumber = Convert.ToInt32(ChannelId.Substring(ChannelId.LastIndexOf('_') + 1));

            string options = "";

            if (ChannelId.ToLower().Contains("min_60"))
            {
                int numberOfRecords = (int) DateTime.UtcNow.Subtract(DataStart.UtcDateTime).TotalMinutes / 60;

                options = "/?command=TableDisplay&table=Min_60&records=" + numberOfRecords.ToString();
            }
            else if (ChannelId.ToLower().Contains("min_15"))
            {
                int numberOfRecords = (int)DateTime.UtcNow.Subtract(DataStart.UtcDateTime).TotalMinutes / 15;
                options = "/?command=TableDisplay&table=Min_15&records=" + numberOfRecords.ToString(); 
            }
            else if (ChannelId.ToLower().Contains("min_5"))
            {
                int numberOfRecords = (int)DateTime.UtcNow.Subtract(DataStart.UtcDateTime).TotalMinutes / 5;
                options = "/?command=TableDisplay&table=Min_5&records=" + numberOfRecords;
            }
            else if (ChannelId.ToLower().Contains("berm_pz"))
            {
                int numberOfRecords = (int)DateTime.UtcNow.Subtract(DataStart.UtcDateTime).TotalMinutes / 60;
                options = "/?command=TableDisplay&table=BERM_PZ&records=" + numberOfRecords;
            }
            req = WebRequest.Create(@"http://" + UniqueId + options);
       
           
            req.Method = "GET";
            //req.Credentials = new NetworkCredential("username", "password");
            HttpWebResponse resp = req.GetResponse() as HttpWebResponse;

            if (resp.StatusCode == HttpStatusCode.OK)
            {
                Stream receiveStream = resp.GetResponseStream();
                StreamReader readStream = null;

                if (resp.CharacterSet == null)
                {
                    readStream = new StreamReader(receiveStream);
                }
                else
                {
                    readStream = new StreamReader(receiveStream, Encoding.GetEncoding(resp.CharacterSet));
                }

                string data = readStream.ReadToEnd();
                HtmlAgilityPack.HtmlDocument doc = new HtmlAgilityPack.HtmlDocument();

                doc.LoadHtml(data);

                foreach (HtmlNode table in doc.DocumentNode.SelectNodes("//table"))
                {
                    foreach (HtmlNode row in table.SelectNodes("tr"))
                    {
                        HtmlNodeCollection cells = row.SelectNodes("td");
                        
                        if(cells != null)
                        {
                            try { 
                                SensorData temp = new SensorData();
                                temp.value = Convert.ToDouble(cells[indexNumber].InnerText.ToString());
                                temp.timestamp = Convert.ToDateTime(cells[0].InnerText.ToString());
                                result.Add(temp);
                            }
                            catch
                            {
                                //assume a bad value in the table, no action
                            }
                        }
                    }
                }
                resp.Close();
                readStream.Close();
            }
            return result;
        }
    }
}
