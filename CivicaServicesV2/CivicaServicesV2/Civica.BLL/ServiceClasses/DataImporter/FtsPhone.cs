﻿using System;
using System.Collections.Generic;
using System.Linq;
using Civica.BLL.BaseClasses;
using System.IO;
using NLog;
using Civica.BLL.Models;
using Civica.DBL.DBClasses;
using System.Collections;
using System.Globalization;
using Civica.Infrastructure.BaseClasses;
using Civica.BLL.CommonClasses;
using Civica.BLL.ServiceClasses.DataImporter.Interfaces;

namespace Civica.BLL.ServiceClasses.DataImporter
{
    public class FTSPHONE : LoggerBase, IFTSPHONE
    {

        private static string[] formats = new string[]
        {
            "yyyy/MM/dd HH:mm:ss",
            "d/MM/yyyy h:mm:ss",
            "d/MM/yyyy HH:mm:ss"
        };

        public bool Start(string path, ref DCLogger logger)
        {

            var t = true;

            var ttime = string.Empty;
            string _monitorid = String.Empty;
            int lineCount = 0;
            var bc = new LoggerBase();
            var processData = new DataProcessing();
            var recievingpath = path + "\\Receiving";

            try
            {

                var res = bc.GetFilesInDirectory(recievingpath);

                if (res != null)
                {

                    if (res.Files != null)
                    {
                        if (res.Files.Count > 0)
                        {
                            foreach (var f in res.Files)
                            {
                                bool r = true;
                                var response = new ServiceResult<bool>(r);
                                string filename = String.Empty;
                                string extension = String.Empty;
                                int stationId = 0;
                                string uniqueId = String.Empty;

                                var filePath = f.FilePath;
                                if (filePath.Contains("capital water systems"))
                                {
                                    var s = f.FilePath;
                                }

                                var updateTimestamps = new List<DateTimeOffset>();
                                var p = f.FilePath.LastIndexOf("\\");

                                filename = f.FilePath.Substring(p + 1);
                                var c = filename.LastIndexOf(".");

                                try
                                {
                                    var creationDate = File.GetCreationTime(f.FilePath);

                                    using (var file = System.IO.File.OpenText(f.FilePath))
                                    {
                                        var name = ((FileStream)file.BaseStream).Name;

                                        while (file.Peek() >= 0)
                                        {
                                            string header1 = file.ReadLine();
                                            if (header1.ToLower().Contains("data from:")) {
                                                int firstColonIndex = header1.IndexOf(':');
                                                int secondColonIndex = header1.IndexOf(':', firstColonIndex + 1);
                                                string roughSection = header1.Substring(firstColonIndex + 1, secondColonIndex - firstColonIndex - 1);
                                                string refinedSection = roughSection.Replace("Date", "");

                                                _monitorid = refinedSection.Trim();
                                                 break;
                                           }
                                        }
                                            
                                        uniqueId = _monitorid;


                                        String line2 = file.ReadLine();
                                        var sensorsInFile = line2.Split(',');

                                        String line3 = file.ReadLine();
                                        var units = line3.Split(',');

                                        lineCount = 3;

                                        //this is probably not needed, maybe file is malformed...
                                        if (sensorsInFile.Length > 1)
                                        {
                                            var stres = StationDBMethods.GetStationByUniqueId(_monitorid,BaseEnum.LoggerType.FtsAxiomPhone);
                                            if (stres == null)
                                            {
                                                if (creationDate.AddDays(30) < DateTime.Now)
                                                {
                                                    response.ToBeMoved = true;
                                                }
                                                else
                                                {
                                                    continue;
                                                }
                                            }
                                            else
                                            {
                                                stationId = stres.Id;

                                                var station = StationDTO.Create(stres);

                                                if (station != null)
                                                {
                                                    if (station.Sensors.Count > 0)
                                                    {
                                                        var smapList = new List<SensorMap>();

                                                        foreach (var sensor in station.Sensors)
                                                        {
                                                            if (sensor.CurrentEquation.LoggerChannel == null)
                                                                continue;

                                                            bool ftsChannel = false;
                                                            List<Civica.Domain.CivicaDomain.MonitoringLoggerChannel> channels = SensorEquationDBMethods.GetMonitoringLoggerChannels((int)BaseEnum.LoggerType.FtsAxiomPhone);

                                                            //just verify that the current channel is actually a digi logger. 
                                                            for (int i = 0; i < channels.Count; i++)
                                                            {
                                                                if (channels[i].Channel.ToLower() == sensor.CurrentEquation.LoggerChannel.Channel.ToLower())
                                                                {
                                                                    ftsChannel = true;
                                                                    break;
                                                                }
                                                            }

                                                            if (sensor.CurrentEquation.LoggerChannel == null || ftsChannel == false || sensor.Equations.Count() == 0 )
                                                            {
                                                                //next sensor
                                                                continue;
                                                            }

                                                            //Check the monitoring Point line - needed for the "MP1"/"MP2" match in the sensor chanel line
                                                            
                                                            var newsmap = new SensorMap();

                                                            //Create a new chanel map record
                                                            newsmap.Sensor = sensor;
                                                            newsmap.Chanel = sensor.CurrentEquation.LoggerChannel.Channel;

                                                            //For each chanel in the chanel list, circle through the chale headers in the second line of the file, to determine the position.
                                                            int indexOfLastUnderScore = sensor.CurrentEquation.LoggerChannel.Channel.LastIndexOf('_');

                                                            //plus one because the date and time are separated by a comma
                                                            //and i've kept the convention of channel_1 = the first data column
                                                            newsmap.Position = Convert.ToInt32(sensor.CurrentEquation.LoggerChannel.Channel.Substring(indexOfLastUnderScore + 1)) + 1;

                                                            smapList.Add(newsmap);
                                                        }
                                                        //Read the datalines
                                                        if (smapList.Count() > 0)
                                                        {

                                                            while (file.Peek() >= 0)
                                                            {
                                                                var dataline = file.ReadLine();
                                                                if (dataline == null || dataline.ToLower().Contains("data")) { break; }
                                                                lineCount++;

                                                                var dataValues = dataline.Split(',');
                                                                if ((dataValues.Length > 1) && (sensorsInFile.Length == dataValues.Length))
                                                                {
                                                                    var datestamp = dataValues[0] + ' ' + dataValues[1];
                                                                    foreach (var sen in smapList)
                                                                    {
                                                                        double value;

                                                                        if (sen.Position >= dataValues.Length)
                                                                            continue;

                                                                        if (double.TryParse(dataValues[sen.Position], out value))
                                                                        {
                                                                            var calculated = processData.EvaluateMSensor(sen.Sensor.CurrentEquation.Equation, sen.Sensor.CurrentEquation.LoggerChannel.Channel, value);
                                                                            if (calculated != null)
                                                                            {
                                                                                value = (double)calculated;
                                                                            }

                                                                            SensorData d = new SensorData();
                                                                            ttime = datestamp;

                                                                            if (!DateTime.TryParseExact(datestamp, units[0], CultureInfo.InvariantCulture, DateTimeStyles.None, out d.timestamp))
                                                                            {
                                                                                if (!DateTime.TryParseExact(datestamp, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out d.timestamp))
                                                                                {
                                                                                    response.ErrorResults.Add("Unrecognized Date Format", "Unrecognized datetime format was encountered : " + datestamp);
                                                                                    response.DTO = false;
                                                                                    break;
                                                                                }
                                                                            }
                                                                            //fts error or missing value, don't include
                                                                            if(value != -99999) { 
                                                                                var dateOffset = new DateTimeOffset(d.timestamp, station.TimeZoneOffset);
                                                                                sen.table.Rows.Add(sen.Sensor.Id, value, dateOffset, 0);
                                                                                sen.tableraw.Rows.Add(sen.Sensor.Id, value, dateOffset);
                                                                                updateTimestamps.Add(dateOffset);
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    if (file.ReadLine() != null)
                                                                    {
                                                                       // response.AddServiceLog("Warning: ", "The the file : " + name + " has problematic last line at line position :" + lineCount);
                                                                    }
                                                                    else
                                                                    {
                                                                       // response.AddServiceLog("Warning: ", "The the file : " + name + " has problematic last line at line position :" + lineCount);
                                                                    }
                                                                }

                                                            }  // End of for loop for

                                                        } // End of for loop for  

                                                        if (response.DTO)
                                                        {

                                                            updateTimestamps.Sort();
                                                            var ts = updateTimestamps.Distinct().ToList();

                                                            var msensorupdate = processData.BulckUploadPhysicalSensors(smapList);

                                                            if (!msensorupdate.DTO)
                                                            {
                                                                var m = String.Empty;
                                                                foreach (var s in msensorupdate.ErrorResults)
                                                                {
                                                                    m = m + "Key : " + s.Key + " , Value : " + s.Value + ", ";
                                                                }
                                                                response.ErrorResults.Add("Could Not Import Data", "Problem with Builk Import : " + m);
                                                                response.DTO = false;

                                                            }
                                                            else
                                                            {
                                                                var calculatedSensorsMap = new CalculatedSensorMap();

                                                                calculatedSensorsMap.Sensors = station.Sensors.Where(x => x.Equations.Count() > 0).ToList();

                                                                calculatedSensorsMap.TimeSeries = ts;

                                                                var calsResult = processData.BulckUploadCalculatedsensors(calculatedSensorsMap);
                                                                var m = String.Empty;
                                                                if (!calsResult.DTO)
                                                                {
                                                                    m = String.Empty;
                                                                    foreach (var s in calsResult.ErrorResults)
                                                                    {
                                                                        m = m + "Key : " + s.Key + " , Value : " + s.Value + ", ";
                                                                    }
                                                                    response.ErrorResults.Add("Could Not Import Data", "Problem with Builk Import Calculated Sensors : " + m);
                                                                    response.DTO = false;

                                                                }

                                                                m = String.Empty;
                                                                if (!calsResult.DTO)
                                                                {

                                                                    foreach (var s in calsResult.ErrorResults)
                                                                    {
                                                                        m = m + "Key : " + s.Key + " , Value : " + s.Value + ", ";
                                                                    }
                                                                    response.ErrorResults.Add("Could Not Import Data", "Problem with Builk Import Calculated Sensors : " + m);

                                                                    response.DTO = false;
                                                                }

                                                                m = String.Empty;
                                                                if (calsResult.ServiceLogs.Count > 0)
                                                                {
                                                                    foreach (var l in calsResult.ServiceLogs)
                                                                    {
                                                                        m = m + "Key : " + l.Key + " , Value : " + l.Value + ", ";
                                                                    }
                                                                    response.ServiceLogs.Add("Warnings while Importing", "There were problems while importing data : " + m);
                                                                }


                                                            }

                                                            var updatedStation = SensorDBMethods.RecalculateStationLastTimeStamp(station.Id);

                                                        } // if the parsing of the files has errors, do not do the upload.

                                                    } // End of  if (station.Sensors.Count > 0) 



                                                } // End of   if (station != null) 
                                            }



                                        } //if (sensorsInFile.Length > 1)  


                                        file.Close();
                                    } //using (var file = System.IO.File.OpenText(_filePath))

                                    //Move the file if it has been in the root directory for more than 30 days
                                    if (response.ToBeMoved)
                                    {
                                        var archivepath = f.RootDirectory + "\\ErrorArchive\\" + filename;

                                        if (File.Exists(archivepath))
                                        {
                                            File.Delete(archivepath);
                                        }
                                        File.Move(f.FilePath, archivepath);
                                        ImportLog.AddLogEntry("ADSPath", "Warning", stationId, filename, uniqueId, "File : " + filename + " was moved to ErrorArchive because it is oldern than30 days", "", DateTimeOffset.Now.ToUniversalTime());
                                        continue;

                                    }

                                }
                                catch (IOException ioe)
                                {
                                    var m = ioe.Message;
                                    continue;
                                }
                                catch (Exception e)
                                {
                                    var archivepath = f.RootDirectory + "\\ErrorArchive\\" + filename;

                                    if (File.Exists(archivepath))
                                    {
                                        File.Delete(archivepath);
                                    }
                                    File.Move(f.FilePath, archivepath);
                                    ImportLog.AddLogEntry("ADSPath", "Error", stationId, filename, uniqueId, e.Message, e.StackTrace, DateTimeOffset.Now.ToUniversalTime());
                                    continue;
                                }


                                if (response.DTO)
                                {
                                    var archivepath = f.RootDirectory + "\\Archive\\" + filename;

                                    if (File.Exists(archivepath))
                                    {
                                        File.Delete(archivepath);
                                    }
                                    File.Move(f.FilePath, archivepath);
                                    if (response.ServiceLogs != null)
                                    {

                                        if (response.ServiceLogs.Count > 0)
                                        {
                                            var message = String.Empty;
                                            foreach (var ms in response.ServiceLogs)
                                            {
                                                message = message + " / " + ms.Key + " : " + ms.Value;

                                            }
                                            ImportLog.AddLogEntry("ADSPath", "Warning", stationId, filename, uniqueId, message, "", DateTimeOffset.Now.ToUniversalTime());
                                        }
                                    }

                                }
                                else
                                {
                                    var archivepath = f.RootDirectory + "\\ErrorArchive\\" + filename;

                                    if (File.Exists(archivepath))
                                    {
                                        File.Delete(archivepath);
                                    }
                                    File.Move(f.FilePath, archivepath);
                                    var message = String.Empty;
                                    if (response.ErrorResults != null)
                                    {
                                        foreach (var ms in response.ErrorResults)
                                        {
                                            message = message + " / " + ms.Key + " : " + ms.Value;

                                        }


                                    }

                                    ImportLog.AddLogEntry("ADSPath", "Error", stationId, filename, uniqueId, message, "", DateTimeOffset.Now.ToUniversalTime());
                                }

                            } //foreach (var f in fileList)

                        }

                    }
                }
            }
            catch (IOException e)
            {
                var m = e.Message;
            }
            catch (Exception e)
            {
                var m = e.Message;
            }

            return t;
        }

    }
}
