﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.BLL.ServiceClasses.Reactor.Interfaces;
using System.Net;
using System.Net.Sockets;
using Civica.DBL.DBClasses;
using Civica.Infrastructure.BaseClasses;
using System.Net.NetworkInformation;
using System.Collections;

namespace Civica.BLL.ServiceClasses.Reactor
{

    public class ReactorServices : IReactorServices
    {


        public async void StartIPReg(int port, string endpoit, EventLog eLog)
        {
            try
            {

                eLog.WriteEntry("Starting Listener for IP - end poin is :" + endpoit);
                IPAddress ipAddress = IPAddress.Parse(endpoit);
                TcpListener listener = new TcpListener(ipAddress, port);

                eLog.WriteEntry("Starting Listener for IP");
                listener.Start();

                while (true)
                {
                    try
                    {
                        var tcpClient = await listener.AcceptTcpClientAsync();
                        eLog.WriteEntry("Recieved something on port 1233");
                        HandleConnectionAsync(tcpClient, 100);
                    }
                    catch (Exception exp)
                    {
                        eLog.WriteEntry("Error wile listening for IP : " + exp.Message);
                        break;
                    }

                }


            }
            catch (Exception e)
            {
                eLog.WriteEntry("Error Starting Listener for IP :" + e.Message);

            }






            eLog.WriteEntry("End of Listener for IP");
        }

        public async void StartBEvent(int port, string endpoit, EventLog eLog)
        {
            try
            {
                eLog.WriteEntry("Starting Listener for BEven - end poin is :" + endpoit);
                IPAddress ipAddress = IPAddress.Parse(endpoit);
                TcpListener listener = new TcpListener(ipAddress, port);

                eLog.WriteEntry("Starting Listener for BEven");
                listener.Start();

                while (true)
                {
                    try
                    {
                        var tcpClient = await listener.AcceptTcpClientAsync();
                        eLog.WriteEntry("Recieved something on port 1244");
                        HandleConnectionAsync(tcpClient, 1024);
                    }
                    catch (Exception exp)
                    {
                        eLog.WriteEntry("Error wile listening for BEven : " + exp.Message);
                        break;
                    }

                }

            }
            catch (Exception e)
            {
                eLog.WriteEntry("Error Starting Listener for IP :" + e.Message);

            }

            eLog.WriteEntry("End of Listener for BEvent");
        }

        private async void HandleConnectionAsync(TcpClient tcpClient, int messageSize)
        {
            String clientInfo = tcpClient.Client.RemoteEndPoint.ToString();
            byte[] myReadBuffer = new byte[messageSize];
            try
            {

                using (var networkStream = tcpClient.GetStream())
                {
                    if (networkStream.CanRead)
                    {
                        int numberOfBytesRead = 0;
                        do
                        {
                            numberOfBytesRead = await networkStream.ReadAsync(myReadBuffer, 0, myReadBuffer.Length);
                        }
                        while (networkStream.DataAvailable);

                    }
                    else {

                    }

                    if (messageSize == 100)  // update the logger IP
                    {
                        var modemInfo = getModemInfo(myReadBuffer);
                        var result = ReactorDbMethods.UpdateReactorIP(modemInfo);
                    }
                    else  // update the event
                    {
                        var modemData = getModemData(myReadBuffer);
                        var result = ReactorDbMethods.UpdateReactorBEvent(modemData);
                    }


                }
            }
            catch (Exception ex)
            {
                //LogMessage(exp.Message);
            }
            finally
            {
                tcpClient.Close();
            }

        }


        protected static ModemData getModemData(byte[] bytes)
        {

            /// The first 45 bytes map
            /// 
            ///  IMEI =  [0] -[7] 
            ///  DI1,DI2,DI3,DI4   = [25]
            ///  Power =  [29[ - [32])
            ///  AI1 =  [33:36])
            ///  AI2 =  [37:40])
            ///  AI3 =  [41:44])

            //  var bt = bytes[25];

            ModemData result = new ModemData();

            result.IMEI = BitConverter.ToInt64(bytes, 0);

            var digitalByte = bytes[25];

            var d1 = digitalByte & 1;
            var d2 = digitalByte & 2;
            var d4 = digitalByte & 4;
            var d8 = digitalByte & 8;

            result.DI1 = (int)d1;

            if ((int)d2 == 2)
            {
                result.DI2 = 1;
            }
            else
            {
                result.DI2 = 0;
            }

            if ((int)d4 == 4)
            {
                result.DI3 = 1;
            }
            else
            {
                result.DI3 = 0;
            }

            if ((int)d8 == 8)
            {
                result.DI4 = 1;
            }
            else
            {
                result.DI4 = 0;
            }


            List<byte> bitsAI1 = new List<byte>();
            bitsAI1.Add(bytes[33]);
            bitsAI1.Add(bytes[34]);
            bitsAI1.Add(bytes[35]);
            bitsAI1.Add(bytes[36]);
            result.AI1 = BitsToDecimal(bitsAI1.ToArray());

            List<byte> bitsAI2 = new List<byte>();
            bitsAI2.Add(bytes[37]);
            bitsAI2.Add(bytes[38]);
            bitsAI2.Add(bytes[39]);
            bitsAI2.Add(bytes[40]);
            result.AI2 = BitsToDecimal(bitsAI2.ToArray());

            List<byte> bitsAI3 = new List<byte>();
            bitsAI3.Add(bytes[41]);
            bitsAI3.Add(bytes[42]);
            bitsAI3.Add(bytes[43]);
            bitsAI3.Add(bytes[44]);
            result.AI3 = BitsToDecimal(bitsAI3.ToArray());


            List<byte> bitsP = new List<byte>();
            bitsP.Add(bytes[29]);
            bitsP.Add(bytes[30]);
            bitsP.Add(bytes[31]);
            bitsP.Add(bytes[32]);
            result.Power = BitsToDecimal(bitsP.ToArray());

            return result;
        }



        public static double BitsToDecimal(byte[] TestByte)
        {
            BitArray TestBitArray = new BitArray(TestByte);
            double Mantissa = 0;
            double Exponent;
            int Counter;
            double Result;

            for (Counter = 0; Counter <= 22; Counter++)
            {
                Mantissa = Mantissa + (TestBitArray[Counter] ? 1 : 0) / (Math.Pow(2, (-(Counter - 23))));
            }
            Mantissa = Mantissa + 1;
            Exponent = 0;
            for (Counter = 23; Counter <= 30; Counter++)
            {
                Exponent = Exponent + (TestBitArray[Counter] ? 1 : 0) * Math.Pow(2, (Counter - 23));
            }
            Exponent = Exponent - 127;

            Result = Mantissa * Math.Pow(2, Exponent);
            if (Result < 0.0001)
            {
                return 0;
            }
            else
            {
                return Result;
            }
        }





        protected static ModemInfo getModemInfo(byte[] bytes)
        {
            /* bytes should look something like:
             *    "602A0F11\r,Bathurst 6A Bell,206.47.177.124\n"
             */

            ModemInfo result = new ModemInfo();

            string txt = Encoding.UTF8.GetString(bytes);

            // remove CR and LF
            txt = txt.Replace("\r", "");
            txt = txt.Replace("\n", "");
            txt = txt.Replace("\0", "");

            string[] items = txt.Split(',');

            /***** get modem id *****/

            // modem id is the first element
            string modem_id = items[0].Trim();

            if (modem_id.Length == 8)
            {
                // modem id is only 8 characters, it must be hex, so convert it to decimal
                result.IMEI = Int64.Parse(modem_id, System.Globalization.NumberStyles.HexNumber);
            }
            else {
                result.IMEI = Int64.Parse(modem_id);
            }

            /***** get modem id *****/

            // IP address is the 3rd element
            result.ip_addr = IPAddress.Parse(items[2].Trim());

            return result;
        }

    }


}
