﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.BLL.Models.AnalysisTools;
using Civica.BLL.Models;

namespace Civica.BLL.ServiceClasses.Interfaces
{
    public interface IStormEventDataService
    {
       
        List<StormEvent> SeperateEvents(int sensorId, double interDryPeriod, double stormSize, DateTime? fromDate, DateTime? toDate);
        List<StormEvent> SeperateEvents(List<MonitoringDataDTO> sensorData, double interDryPeriod, double stormSize);

        List<StormEvent> SeperateEvents(int sensorId, double interDryPeriod, double stormSize, DateTime? fromDate, DateTime? toDate, out List<DateTime>[] outStormList
            , bool forStormSep = false, int flowId = 0);
        Dictionary<int, Dictionary<int, decimal?>> GetIDFCurves(int id, int intensityUnitId = 0);

        void PrecipitationAlign(IEnumerable<MonitoringDataDTO> dt, ref List<DateTime> at, ref List<double> av, double interval, DateTime beginDate, DateTime endDate);
        void PrecipitationAlign(IEnumerable<SensorDataDTO> dt, ref List<DateTime> at, ref List<double> av, double interval, DateTime beginDate, DateTime endDate);

        IEnumerable<KeyValuePair<int, decimal?>> GetEventCurve(int sensorId, DateTime startDate, DateTime endDate, int rainIntensityUnitId);
        IEnumerable<DesignStormDTO> GetDesignStorms(int sensorId);
        IEnumerable<DesignStormDTO> GetDesignStormsByOrganizationId(int Id);
        //List<MonitoringData> GetEventData(int sensorId, ref List<DateTime>[] stormList,  int interDryPeriod,  DateTime? startDate, DateTime? endDate);
        List<MonitoringDataDTO> GetEventData(int sensorId, DateTime? startDate, DateTime? endDate);
        List<MonitoringDataDTO> GetMoniotirngDate(int sensorId, DateTime? startDate, DateTime? endDate);
        Dictionary<int, List<MonitoringDataDTO>> GetEventData(int[] sensorId, DateTime? startDate, DateTime? endDate);
        Dictionary<int, List<MonitoringDataDTO>> GetEventData(int[] sensorId, DateTime?[] fromDate, DateTime?[] toDate);

        List<DateTime>[] StormSeparate(List<DateTime> at, List<double> av, double interDryPeriod);
        long IdealizeTime(long start, double interval, long time);
        long getseconds(DateTime t1);
        DateTime getdates(long s1);
        double GetTcPrecipIntensity(List<MonitoringDataDTO> eventData, double tc);
        void FlowAlign(MonitoringDataDTO[] eventData, int sensorId, decimal flowInt, ref List<DateTime> measuredFlowTime, ref List<double> measuredFlowValue, DateTime? startDate, DateTime? endDate);

        MonitoringDataDTO[] Flow_Seperate(MonitoringDataDTO[] flowData, int sensorId, decimal mIntFlow, int interDryPeriod, DateTime? fromDate, DateTime? toDate, DateTime[][] stormList, int stormIndex
              , int StartCount = 0, int EndCount = 0, int selection = 0, bool isPassed = false);

        void PrecipAlign(List<MonitoringDataDTO> eventData, int sensorId, double tc, double timestep, ref List<DateTime> arrayLTimeP, ref List<double> arrayLValueP, bool forStormSep, bool forAnalysis, DateTime? fromDate, DateTime? toDate);

        Dictionary<String, Double> Analysis(Boolean step, double tc, double catcharea, double tcPrecipInten, decimal mIntFlow, double[] WWFSeries, double[] DWFSeries, Dictionary<DateTime, Double> PrecipSeries, Dictionary<DateTime, Double> IISeries, DateTime? fromDate, DateTime? toDate);
        void InitFlowData(MonitoringDataDTO[] dt, MonitoringDataDTO[] rg, decimal flowInt, double interDryPeriod, ref Periods[] dryFlowArray, ref Periods[] flowArray, ref List<DateTime>[] stormList, DateTime startDate, DateTime endDate, out int numDays, out int numPeriods);
        void InitFlowData(Dictionary<int, MonitoringDataDTO[]> dt, Dictionary<int, MonitoringDataDTO[]> rg, decimal[] flowInt, double interDryPeriod, ref Dictionary<int, List<DateTime>[]> stormList, DateTime[] startDate, DateTime[] endDate, out int numDays, out int numPeriods);
        void GetValueDynamic(ref List<Dictionary<String, List<Decimal>>> mValueDynamic, Dictionary<int, List<DateTime>[]> stormList, int[] stationId, decimal[] catchment, decimal[] tc);
        List<EventStatRow>[] CalculateEventStats(List<Dictionary<String, List<Decimal>>> mValueDynamic, Dictionary<int, MonitoringDataDTO[]> flowData, Dictionary<int, MonitoringDataDTO[]> rainData, Dictionary<int, DateTime[][]> stormList, decimal[] flowInt, int[] flowId, int[] rainId, double interDryPeriod, double minStormSize);
        void GetValueDynamic(ref Dictionary<String, List<Decimal>> mValueDynamic, List<DateTime>[] stormList, int stationId, decimal catchment, decimal tc);
        List<EventStatRow> CalculateEventStats(Dictionary<String, List<Decimal>> mValueDynamic, MonitoringDataDTO[] flowData, MonitoringDataDTO[] rainData, DateTime[][] stormList, decimal flowInt, int flowId, int rainId, double interDryPeriod, double minStormSize);

        Dictionary<String, List<String>>[] PlotFittedCurve(ref List<SeriesPoints>[] s_Design, List<EventStatRow>[] Series, Dictionary<string, Dictionary<int, Dictionary<int, Decimal?>>> designStorm, string[] designStormNames, decimal[] tc, int axis, bool linear, bool log);
        Dictionary<int, Dictionary<int, Decimal?>> LoadDesignStorms(ref Dictionary<int, Dictionary<int, Decimal?>> designStorms, int idfSources);
        Dictionary<string, Dictionary<int, Dictionary<int, Decimal?>>> LoadDesignStorms(ref Dictionary<string, Dictionary<int, Dictionary<int, Decimal?>>> designStorms, Dictionary<int, string> idfSources);
        double PlotValue(List<EventStatRow> events, Dictionary<int, Dictionary<int, Decimal?>> designStorms, decimal tc, int returnIntervalIndex, int plotType);
        void CalculateFlowStats(ref Dictionary<string, FlowStatRow> FlowStats, Periods[] flowArray, Periods[] dryFlowArray, int numDays, int numPeriods, int population, int GWISelect, double GWIEst, decimal catchment, out Nullable<double>[] flowAvgDayArray);
        Dictionary<string, List<Point>> PopulateAvgDwfChart(Nullable<double>[] flowAvgDayArray, Periods[] flowDwfArray, double mConfBand, double flowTimeInt, int numDays, int numPeriods);

        List<Point> ManningsCalculation(int flowDepthPoints, double flowDepthMax, int XsensorId, int YsensorId, double slopeGradient, double frictionCoef, int PipeType, double dimensionOne, double dimensionTwo = 0, double dimensionThree = 0, double dimensionFour = 0);
        List<decimal> CalculateCoeficientOfDetermination(List<EventStatRow>[] Series, int axis);
        //Dictionary<string, List<Point>> scatterConfBand(List<Point> chartSeries, double confInterval);
        Dictionary<string, List<MonitoringDataDTO>[]> CalculateDryWeatherFlowPatterns(string DOW, int flowId, int rainId, DateTime[][] stormList, DateTime startDate, DateTime endDate);

        double[] CleanData(double[] noisy, int range, double decay);
        IEnumerable<SensorDTO> GetMonitoringStationSensorsByStationAdmin(int stationId);
      //  BoundaryDTO GetFlowMonitoringMappingToolBoundary(int serviceId);
    }
}
