﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.Domain.CivicaDomain;
using System.Data.Entity;
using Civica.Infrastructure.BaseClasses;

namespace Civica.BLL.ServiceClasses.Interfaces
{
    public interface IAlarmDataService
    {
        List<MonitoringStationAlarm> GetAllAlarms();
        List<MonitoringStationAlarmCondition> GetAllAlarmConidtions(int alarmId);

    }
}
