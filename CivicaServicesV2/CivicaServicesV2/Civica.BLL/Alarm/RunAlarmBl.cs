﻿using Civica.BLL.Models;

namespace Civica.BLL.Alarm
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Civica.BLL.ServiceClasses;
    using Civica.Domain;
    using Civica.Domain.CivicaDomain;
    using Civica.Infrastructure.BaseClasses;

    public class RunAlarmBl
    {
        public AlarmDataService alarmDataService;

        private IDatacurrrentEntitiesContext _model;
        private ISender _sender;

        //public RunAlarmBl() : this(new DatacurrrentEntitiesContext())
        //{
        //    // this.alarmDataService = new AlarmDataService();
        //}

        public RunAlarmBl(ISender sender) : this(new DatacurrrentEntitiesContext(), sender)
        {
        }

        public RunAlarmBl(IDatacurrrentEntitiesContext model, ISender sender)
        {
            _model = model;
            this.alarmDataService = new AlarmDataService(_model);
            _sender = sender;
        }

        public void ProcessAlarm(System.Diagnostics.EventLog log)
        {
            var allstationAlarmIds = alarmDataService.GetAllAlarms().Select(x => x.Id);

            try
            {
                Parallel.ForEach(allstationAlarmIds, new ParallelOptions { MaxDegreeOfParallelism = 4 },
                                  alarmId => { runOneAlarm(alarmId, log); });

                //ThreadPool.SetMaxThreads(4, 4);      
                //List<Thread> threads = allstationAlarmIds.Select(x => new Thread(() => runOneAlarm(x, log))).ToList();
                //foreach (var thread in threads)
                //{
                //    thread.Start();
                //}
            }
            catch (Exception ex)
            {
                log.WriteEntry("Exception : " + ex.Message);
            }
        }

        public Models.Alarm.AlarmResultModel runOneAlarm(int alarmId, System.Diagnostics.EventLog log)
        {
            var statutsResult = new List<BaseEnum.AlarmTriggerType>();
            var results = new Dictionary<int, string>();
            var checkresult = new List<string>();
            var LastcheckTimes = new List<DateTimeOffset?>();
            var CheckedToTime = new List<DateTimeOffset?>();
            DateTimeOffset? lastRun = null;
            var result = new Civica.BLL.Models.Alarm.AlarmResultModel(alarmId);
            var alarm = alarmDataService.GetMonitoringStationAlarmById(alarmId);
            var alarmServity = alarmDataService.stationAlarmSeverity(alarmId);

            if (alarm.Enabled)
            {
                BaseEnum.AlarmTriggerType res = BaseEnum.AlarmTriggerType.ALARMRES_OK;
                BaseEnum.AlarmTriggerType phomsgType = BaseEnum.AlarmTriggerType.ALARMRES_OK;
                var conditions = alarmDataService.GetAllAlarmConidtions(alarmId);
                var alarmSensorDataTypeIds = alarmDataService.GetAllAlarmSensorDataType(alarmId);
                bool hasContinousSensor = false;

                foreach (var alarmSensorDataType in alarmSensorDataTypeIds)
                {
                    if (alarmSensorDataType == 2)
                    {
                        hasContinousSensor = true;
                        break;
                    }
                }

                if (hasContinousSensor == false)
                {
                    alarmDataService.UpdateAlarmUsePointSensorCalculateLastRun(alarmId, true);
                }

                try
                {
                    foreach (var condition in conditions)
                    {
                        var conidtionId = condition.Id;
                        var monitoringstationAlarmCondition = alarmDataService.GetMonitoringStationAlarmConditionById(conidtionId);
                        var condtionTypeId = monitoringstationAlarmCondition.AlarmConditionTypeId;
                        CondChecker check = null;

                        switch (condtionTypeId)
                        {
                            case 1:
                                check = new Max(conidtionId, _model);
                                break;
                            case 2:
                                check = new Min(conidtionId, _model);
                                break;
                            case 3:
                                check = new OutOfRange(conidtionId, _model);
                                break;
                            case 4:
                                check = new NoData(conidtionId, _model);
                                break;
                            case 5:
                                check = new TimeAvgMax(conidtionId, _model);
                                break;
                            case 6:
                                check = new TimeAvgMin(conidtionId, _model);
                                break;
                            case 7:
                                check = new TimeSumMax(conidtionId, _model);
                                break;
                            case 8:
                                check = new TimeSumMin(conidtionId, _model);
                                break;
                            case 9:
                                check = new NewData(conidtionId, _model);
                                break;
                            case 10:
                                check = new Discrepancy(conidtionId, _model);
                                break;

                            default:
                                throw new Exception("Condition Type is not valid");
                        }

                        var alarmTriggerType = check.Check(null, null, log);
                        
                        if (alarmTriggerType == BaseEnum.AlarmTriggerType.ALARMRES_ERRORS)
                        {
                            continue;
                        }
                        else
                        {
                            result.ConditionResults.Add( new Civica.BLL.Models.Alarm.AlarmConditionResultModel
                            {
                                CheckFrom = check.LastRun,
                                CheckTo = check.CheckToTime,
                                ConditionStatus = alarmTriggerType
                            });

                            statutsResult.Add(alarmTriggerType);
                            checkresult.Add(check.result);
                            LastcheckTimes.Add(check.TestTime);
                            CheckedToTime.Add(check.CheckToTime);
                            lastRun = check.LastRun;
                        }
                    }
                }
                catch (Exception ex)
                {
                    log.WriteEntry("Exception : " + ex.Message);
                }
                var sensorNames = new List<string>();
                string msg;
                var sendEmail = false;
                msg = "Alarm Name : " + alarm.Name + "\n";
                msg = msg + "Severity: " + alarmServity + "\n";
                msg = msg + "Service: " + alarmDataService.serviceName(alarmId) + "\n";
                msg = msg + "Station Name: " + alarmDataService.GetStationName(alarmId) + "\n";
                int m = 1;
                msg = msg + "Alarm Description: " + "\n";

                foreach (var condition in conditions)
                {
                    var tempcondtionTypeLongDescription = alarmDataService.GetAlarmConditionTypeLongDescription(condition.AlarmConditionTypeId.Value);
                    var conidtionLogic = condition.MonitoringStationAlarmConditionLogic;
                    msg = msg + m + ": " + tempcondtionTypeLongDescription;
                    if (m < conditions.Count)
                    {
                        if (conidtionLogic)
                        {
                            msg = msg + " AND";
                        }
                        else
                        {
                            msg = msg + " OR";
                        }
                    }
                    msg = msg + "\n";
                    m++;
                }

                msg = msg + "Where" + "\n";
                m = 1;
                foreach (var condition in conditions)
                {
                    var paramTypes = getParameters(condition);
                    var conditionPara = getConditionParams(condition);
                    var sensors = alarmDataService.GetMonitoringConditionMonitoringSensors(condition.Id);
                    sensorNames = new List<string>();
                    msg = msg + "Condition " + m + ": " + "\n";
                    foreach (var sensor in sensors)
                    {
                        sensorNames.Add(sensor.Name);
                    }

                    msg = msg + paramTypes[0] + " is: " + String.Join(", or ", sensorNames) + "\n";

                    for (var j = 1; j < paramTypes.Count; j++)
                    {
                        msg = msg + paramTypes[j] + " is: " + conditionPara[j] + "\n";
                    }
                    if (lastRun != null && CheckedToTime[m - 1] != null)
                    {
                        msg = msg + "Check From " + lastRun.ToString() + " To " + CheckedToTime[m - 1].ToString() + "\n";
                    }
                    msg = msg + "\n";
                    m++;
                }
                DateTimeOffset? testTime = null;
                foreach (var dateTime in LastcheckTimes)
                {
                    if (dateTime != null)
                    {
                        if (testTime == null)
                        {
                            testTime = dateTime;
                        }
                        else
                        {
                            if (testTime.Value > dateTime.Value)
                            {
                                testTime = dateTime;
                            }
                        }
                    }
                }

                if (alarm.IsTriggered)
                {
                    if (CheckAlarmStatus(BaseEnum.AlarmTriggerType.ALARMRES_NODATA, statutsResult, conditions))
                    {
                        res = BaseEnum.AlarmTriggerType.ALARMRES_NODATA;
                        if (alarm.AlarmNoNewData)
                        {
                            if (alarm.LastRunCurrentTime == null)
                            {
                                alarmDataService.UpdateLastRunCurrentTime(alarmId);
                            }
                            if (alarm.NoDataNotification == false)
                            {
                                var currentTime = Civica.Infrastructure.Providers.TimeProvider.Current.Now;
                                if (Convert.ToDecimal(currentTime.Subtract(alarm.LastRunCurrentTime.Value).TotalHours) > alarm.OutOfDataHours)
                                {
                                    msg = msg + "alarm has not been running in last  " + alarm.OutOfDataHours + "hours" + "\n";
                                    sendEmail = true;
                                    alarmDataService.UpdateNoDataNotificationToTrue(alarmId);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (alarm.NoDataNotification == true)
                            alarmDataService.UpdateNoDataNotificationToFalse(alarmId);
                        if (CheckAlarmStatus(BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION, statutsResult, conditions))
                        {
                            res = BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION;
                            if (alarm.AckRequired && alarm.AckStatus == false)
                            {
                                var currentTime = Civica.Infrastructure.Providers.TimeProvider.Current.Now;
                                if (Convert.ToDecimal(currentTime.Subtract(alarm.LastRunCurrentTime.Value).TotalMinutes) > AlarmServiceBaseParameter.NextVoilateEmailTime)
                                {
                                    sendEmail = true;
                                    alarmDataService.UpdateAlarmSth(true, false, Civica.Infrastructure.Providers.TimeProvider.Current.Now, alarmId);
                                    msg = msg + "Message: " + "********Alarm has triggered ********" + "\n";
                                    m = 1;
                                    for (var j = 0; j < statutsResult.Count; j++)
                                    {
                                        if (checkresult[j] != "" && checkresult[j] != null)
                                        {
                                            msg = msg + "Violation Condition " + m + ":" + "\n" + checkresult[j] + "\n";
                                        }
                                        msg = msg + "\n";
                                        m++;
                                    }
                                    msg = msg + " ";
                                    results.Add(alarmId, msg);
                                    phomsgType = BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION;

                                    if (alarmDataService.AlarmConditionCounts(alarmId) == 1)
                                    {
                                        if (alarmDataService.GetAlarmFirstConditionTypeId(alarmId) == 4)
                                        {
                                            var sensorIds = alarmDataService.GetMonitoringSensorIdsByAlarmId(alarmId);
                                            alarmDataService.UpdateSensorStatus(BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION, sensorIds);
                                        }
                                    }
                                    res = BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION;
                                    alarmDataService.insertintoLog(alarmId, "Alarm", msg);
                                }
                            }
                        }
                        else if (CheckAlarmStatus(BaseEnum.AlarmTriggerType.ALARMRES_OK, statutsResult, conditions) || CheckAlarmStatus(BaseEnum.AlarmTriggerType.ALARMRES_VIOL_BACK2NORMAL, statutsResult, conditions))
                        {
                            alarmDataService.UpdateAlarmSth(false, true, null, alarmId);
                            alarmDataService.UpdateEmailGroupIndex(alarmId, 0);
                            alarmDataService.UpdatePhoneGroupIndex(alarmId, 0);
                            alarmDataService.UpdatePhoneTextGroupIndex(alarmId, 0);
                            alarmDataService.UpdateAlarmReaptedTimes(alarmId, 0);
                            if (alarm.BackToNormalNotification)
                            {
                                sendEmail = true;
                                msg = msg + "Message: " + "Data has returned to pre-alarm levels" + "\n";
                                msg = msg + "Message_trun: " + "Data BACK TO NORMAL" + "\n";
                                //msg.Add("MESSAGE_trun", "Data back to normal");
                                results.Add(alarmId, msg);
                                phomsgType = BaseEnum.AlarmTriggerType.ALARMRES_VIOL_BACK2NORMAL;
                            }
                            if (alarmDataService.AlarmConditionCounts(alarmId) == 1)
                            {
                                if (alarmDataService.GetAlarmFirstConditionTypeId(alarmId) == 4)
                                {
                                    var sensorIds = alarmDataService.GetMonitoringSensorIdsByAlarmId(alarmId);
                                    alarmDataService.UpdateSensorStatus(BaseEnum.AlarmTriggerType.ALARMRES_OK, sensorIds);
                                }
                            }
                            res = BaseEnum.AlarmTriggerType.ALARMRES_VIOL_BACK2NORMAL;
                            alarmDataService.insertintoLog(alarmId, "BACK TO NORMAL", msg);
                            ////write log
                        }
                    }
                }
                else
                {
                    if (CheckAlarmStatus(BaseEnum.AlarmTriggerType.ALARMRES_NODATA, statutsResult, conditions))
                    {
                        res = BaseEnum.AlarmTriggerType.ALARMRES_NODATA;
                        if (alarm.AlarmNoNewData)
                        {
                            if (alarm.LastRunCurrentTime == null)
                            {
                                alarmDataService.UpdateLastRunCurrentTime(alarmId);
                            }
                            if (alarm.NoDataNotification == false)
                            {
                                var currentTime = Civica.Infrastructure.Providers.TimeProvider.Current.Now;
                                if (Convert.ToDecimal(currentTime.Subtract(alarm.LastRunCurrentTime.Value).TotalHours) > alarm.OutOfDataHours)
                                {
                                    msg = msg + "alarm has not been running in last  " + alarm.OutOfDataHours + "hours" + "\n";
                                    sendEmail = true;
                                    alarmDataService.UpdateNoDataNotificationToTrue(alarmId);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (alarm.NoDataNotification == true)
                            alarmDataService.UpdateNoDataNotificationToFalse(alarmId);
                        if (CheckAlarmStatus(BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION, statutsResult, conditions))
                        {
                            sendEmail = true;
                            alarmDataService.UpdateAlarmSth(true, false, Civica.Infrastructure.Providers.TimeProvider.Current.Now, alarmId);
                            msg = msg + "Message: " + "********Alarm has TRIGGERED ********" + "\n";
                            m = 1;
                            for (var j = 0; j < statutsResult.Count; j++)
                            {
                                if (checkresult[j] != "" && checkresult[j] != null)
                                {
                                    msg = msg + "Violation Condition " + m + ":" + "\n" + checkresult[j] + "\n";
                                }
                                msg = msg + "\n";
                                m++;
                            }
                            msg = msg + " ";
                            results.Add(alarmId, msg);
                            phomsgType = BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION;
                            if (alarmDataService.AlarmConditionCounts(alarmId) == 1)
                            {
                                if (alarmDataService.GetAlarmFirstConditionTypeId(alarmId) == 4)
                                {
                                    var sensorIds = alarmDataService.GetMonitoringSensorIdsByAlarmId(alarmId);
                                    alarmDataService.UpdateSensorStatus(BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION, sensorIds);
                                }
                            }
                            res = BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION;
                            alarmDataService.insertintoLog(alarmId, "Alarm", msg);
                        }
                        else if (CheckAlarmStatus(BaseEnum.AlarmTriggerType.ALARMRES_OK, statutsResult, conditions))
                        {
                            res = BaseEnum.AlarmTriggerType.ALARMRES_OK;
                        }
                        else if (CheckAlarmStatus(BaseEnum.AlarmTriggerType.ALARMRES_VIOL_BACK2NORMAL, statutsResult, conditions))
                        {
                            msg = msg + "Message: " + "Alarm has triggered and data has since returned to pre-alarm levels" + "\n";

                            msg = msg + " ";
                            results.Add(alarmId, msg);

                            phomsgType = BaseEnum.AlarmTriggerType.ALARMRES_VIOL_BACK2NORMAL;
                            res = BaseEnum.AlarmTriggerType.ALARMRES_VIOL_BACK2NORMAL;
                            alarmDataService.insertintoLog(alarmId, "BACK TO NORMAL", msg);
                        }
                        else
                        {
                            //res = getEvalStr(statutsResult,conditions);
                        }
                    }
                }
                result.Messages = msg;


                int repeatedIndex = alarmDataService.GetAlarmReaptedTimes(alarmId) == null ? 0 : alarmDataService.GetAlarmReaptedTimes(alarmId).Value;

                if (!CheckAlarmStatus(BaseEnum.AlarmTriggerType.ALARMRES_NODATA, statutsResult, conditions))
                    alarmDataService.UpdateLastRun(alarmId, testTime);

                List<List<string>> phoneList = new List<List<string>>();
                List<List<string>> textphoneList = new List<List<string>>();
                var phoneNotification = alarm.PhoneNotification;
                var phoneTextNotification = alarm.textNotification;
                if (phoneNotification != null && phoneNotification != "")
                {
                    if (phoneNotification.Contains(";"))
                    {
                        var phonnums = phoneNotification.Split(';');
                        for (int i = 0; i < phonnums.Length; i++)
                        {
                            var nums = phonnums[i].Split(',');
                            List<String> phonnum = new List<string>();
                            for (int j = 0; j < nums.Length; j++)
                            {
                                phonnum.Add(nums[j]);
                            }
                            phoneList.Add(phonnum);
                        }
                    }
                    else
                    {
                        var phonnums = phoneNotification.Split(',');
                        List<String> phonnum = new List<string>();
                        for (int i = 0; i < phonnums.Length; i++)
                        {
                            phonnum.Add(phonnums[i]);
                        }
                        phoneList.Add(phonnum);
                    }
                }
                if (phoneTextNotification != null && phoneTextNotification != "")
                {
                    if (phoneTextNotification.Contains(";"))
                    {
                        var phonnums = phoneTextNotification.Split(';');
                        for (int i = 0; i < phonnums.Length; i++)
                        {
                            var nums = phonnums[i].Split(',');
                            List<String> phonnum = new List<string>();
                            for (int j = 0; j < nums.Length; j++)
                            {
                                phonnum.Add(nums[j]);
                            }
                            textphoneList.Add(phonnum);
                        }
                    }
                    else
                    {
                        var phonnums = phoneTextNotification.Split(',');
                        List<String> phonnum = new List<string>();
                        for (int i = 0; i < phonnums.Length; i++)
                        {
                            phonnum.Add(phonnums[i]);
                        }
                        textphoneList.Add(phonnum);
                    }
                }
                if (phomsgType != BaseEnum.AlarmTriggerType.ALARMRES_OK)
                {
                    int phoneIndex = alarmDataService.getPhoneGroupIndex(alarmId) == null ? 0 : alarmDataService.getPhoneGroupIndex(alarmId).Value;
                    if (alarm.PhoneNotification != null && phoneIndex < phoneList.Count)
                    {
                        var phonums = phoneList.ElementAt(phoneIndex).ToArray();
                        var phonmessage = alarm.PhoneMessage.Split(':');
                        doPhone(phonums, phonmessage, phomsgType, alarmId, repeatedIndex);
                    }
                }

                if (phomsgType != BaseEnum.AlarmTriggerType.ALARMRES_OK)
                {
                    int phoneTextIndex = alarmDataService.getPhoneTextGroupIndex(alarmId) == null ? 0 : alarmDataService.getPhoneTextGroupIndex(alarmId).Value;
                    if (alarm.textNotification != null && phoneTextIndex < textphoneList.Count)
                    {
                        var phonums = textphoneList.ElementAt(phoneTextIndex).ToArray();
                        var phonmessage = alarm.textmessage.Split(':');
                        String ackMessage = "";
                        if (phomsgType == BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION && alarm.AckRequired && alarm.AckStatus == false)
                        {
                            ackMessage = ackMessage + "Acknowledge alarm:" + AlarmServiceBaseParameter.DataCurrentBaseUrl + "AlarmAck/Index/?guid=";
                        }

                        doPhoneText(phonums, phonmessage, ackMessage, phomsgType, alarmId, repeatedIndex);
                    }
                }
                if (sendEmail)
                {
                    var alarmEmail = new Civica.BLL.Models.Alarm.AlarmEmailMessge();
                    alarmEmail.alarmId = alarmId;
                    alarmEmail.alarmServity = alarmServity;
                    if (alarm.CustomizedEmailMessage == "" || alarm.CustomizedEmailMessage == null)
                        alarmEmail.msg = msg;
                    else
                        alarmEmail.msg = alarm.CustomizedEmailMessage;
                    alarmEmail.trigger = res;
                    var emailNotifications = alarm.EmailNotification;
                    List<List<string>> emailList = new List<List<string>>();
                    if (emailNotifications != null && emailNotifications != "")
                    {
                        if (emailNotifications.Contains(";"))
                        {
                            var emails = emailNotifications.Split(';');
                            for (int i = 0; i < emails.Length; i++)
                            {
                                var email = emails[i].Split(',');
                                List<String> emailaddress = new List<string>();
                                for (int j = 0; j < email.Length; j++)
                                {
                                    emailaddress.Add(email[j]);
                                }
                                emailList.Add(emailaddress);
                            }
                        }
                        else
                        {
                            var emails = emailNotifications.Split(',');
                            List<String> emailaddress = new List<string>();
                            for (int i = 0; i < emails.Length; i++)
                            {
                                emailaddress.Add(emails[i]);
                            }
                            emailList.Add(emailaddress);
                        }
                    }
                    int emailGroupIndex = alarmDataService.geEmailGroupIndex(alarmId) == null ? 0 : alarmDataService.geEmailGroupIndex(alarmId).Value;
                    var serviceId = alarmDataService.getServiceIdByAlarmId(alarmId);
                    var stationName = alarmDataService.GetStationName(alarm.Id);
                    var subject = "";
                    if (alarm.CustomizedEmailSubject == "" || alarm.CustomizedEmailSubject == null)
                        subject = "DataCurrent Alarm [" + alarmServity + "]: " + stationName + " - " + alarm.Name;
                    else
                        subject = alarm.CustomizedEmailSubject;
                    if (alarmEmail.trigger == BaseEnum.AlarmTriggerType.ALARMRES_VIOL_BACK2NORMAL)
                        subject = subject + " (BACK TO NORMAL)";

                    else if (alarmEmail.trigger == BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION)
                    {
                        if (alarm.AckRequired && alarm.AckStatus == false)
                        {
                            alarmEmail.msg = alarmEmail.msg + "\n";
                            alarmEmail.msg = alarmEmail.msg + "*** Alarm requires immediate acknowledgement ***" + "\n";
                            alarmEmail.msg = alarmEmail.msg + "Acknowledge alarm:" + AlarmServiceBaseParameter.DataCurrentBaseUrl + "AlarmAck/Index/?guid=";
                        }
                        subject = subject + " (triggered)";
                    }
                    if (emailGroupIndex < emailList.Count)
                    {
                        var emailReceipts = emailList.ElementAt(emailGroupIndex);
                        foreach (var emailreceipt in emailReceipts)
                        {
                            if (alarmEmail.msg != "" && alarmEmail.msg != null)
                            {
                                if (alarmEmail.trigger == BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION && alarm.AckRequired && alarm.AckStatus == false)
                                {
                                    var guid = Guid.NewGuid();
                                    var Emailmsg = alarmEmail.msg + guid + "\n" + "\n";
                                    Emailmsg = Emailmsg + "View Chart: " + AlarmServiceBaseParameter.DataCurrentBaseUrl + "/Chart/TestChart/Index/" + alarm.MonitoringStationId + "\n";
                                    Emailmsg = Emailmsg + "Edit Alarm: " + AlarmServiceBaseParameter.DataCurrentBaseUrl + "Monitoring/MonitoringService/MonitoringStationDetails/" + alarm.MonitoringStationId + "?serviceId=" + serviceId + "&alarmId=" + alarm.Id + "\n";
                                    alarmDataService.insertInfoIntoAlarmAckinfo(guid.ToString(), alarmId, emailreceipt);
                                    _sender.SendMail(alarmId, emailreceipt, subject, Emailmsg, repeatedIndex);
                                }
                                else
                                {
                                    var Emailmsg = alarmEmail.msg;
                                    Emailmsg = Emailmsg + "View Chart: " + AlarmServiceBaseParameter.DataCurrentBaseUrl + "Chart/TestChart/Index/" + alarm.MonitoringStationId + "\n";
                                    Emailmsg = Emailmsg + "Edit Alarm: " + AlarmServiceBaseParameter.DataCurrentBaseUrl + "Monitoring/MonitoringService/MonitoringStationDetails/" + alarm.MonitoringStationId + "?serviceId=" + serviceId + "&alarmId=" + alarm.Id + "\n";
                                    _sender.SendMail(alarmId, emailreceipt, subject, alarmEmail.msg, repeatedIndex);
                                }
                            }
                        };
                    }
                }

                if (sendEmail && res == BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION && alarm.AckRequired && alarm.AckStatus == false)
                {
                    if (repeatedIndex == AlarmServiceBaseParameter.moveToNext - 1)
                    {
                        int emailGroupIndex = alarmDataService.geEmailGroupIndex(alarmId) == null ? 0 : alarmDataService.geEmailGroupIndex(alarmId).Value;
                        int phoneIndex = alarmDataService.getPhoneGroupIndex(alarmId) == null ? 0 : alarmDataService.getPhoneGroupIndex(alarmId).Value;
                        int phoneTextIndex = alarmDataService.getPhoneTextGroupIndex(alarmId) == null ? 0 : alarmDataService.getPhoneTextGroupIndex(alarmId).Value;
                        alarmDataService.UpdateEmailGroupIndex(alarmId, emailGroupIndex + 1);
                        alarmDataService.UpdatePhoneGroupIndex(alarmId, phoneIndex + 1);
                        alarmDataService.UpdatePhoneGroupIndex(alarmId, phoneTextIndex + 1);
                        alarmDataService.UpdateAlarmReaptedTimes(alarmId, 0);
                    }
                    else
                    {
                        alarmDataService.UpdateAlarmReaptedTimes(alarmId, repeatedIndex + 1);
                    }
                }
                result.AlarmStatus = res;
            }
            return result;
        }

        public List<String> getParameters(MonitoringStationAlarmCondition condition)
        {
            var parameter = alarmDataService.GetAlarmConditionTypeParameters(condition.AlarmConditionTypeId.Value);
            var parameterTypes = new List<String>();
            if (parameter != null)
            {
                parameterTypes = parameter.Split(';').ToList();
            }
            return parameterTypes;

        }

        public List<String> getConditionParams(MonitoringStationAlarmCondition condition)
        {
            var parameter = condition.Params;
            var parameterTypes = new List<String>();
            if (parameter != null)
            {
                parameterTypes = parameter.Split(';').ToList();
            }
            return parameterTypes;

        }

        public void doPhone(string[] phonenumbers, string[] message, BaseEnum.AlarmTriggerType status, int alarmId, int repeatedIndex)
        {
            if (phonenumbers.Length == 0)
                return;
            if (status == BaseEnum.AlarmTriggerType.ALARMRES_VIOL_BACK2NORMAL)
            {
                _sender.makePhoneCall(phonenumbers, message[1], alarmId, repeatedIndex);
            }
            else
            {
                _sender.makePhoneCall(phonenumbers, message[0], alarmId, repeatedIndex);
            }
        }

        public void doPhoneText(string[] phonenumbers, string[] message, string ackMessage, BaseEnum.AlarmTriggerType status, int alarmId, int repeatedIndex)
        {
            if (phonenumbers.Length == 0)
                return;
            if (status == BaseEnum.AlarmTriggerType.ALARMRES_VIOL_BACK2NORMAL)
            {
                _sender.SendPhoneMessage(phonenumbers, message[1], ackMessage, alarmId, repeatedIndex);
            }
            else
            {

                _sender.SendPhoneMessage(phonenumbers, message[0], ackMessage, alarmId, repeatedIndex);
            }
        }

        //public void SendPhoneMessage(string[] recipients, string message, string ackMessage, int alarmId, int repeatedIndex)
        //{
        //    bool sent = false;
        //    string license = ConfigurationManager.AppSettings["SMSNotifyAPIKey"];
        //    message = "This is a datacurrent alarm text message" + "\n" + message;
        //    TextMessageService.IsmsClient client = new TextMessageService.IsmsClient("sms2wsHttpBinding");
        //    foreach (var recipient in recipients)
        //    {
        //        TextMessageService.SMSResponse resp = client.SimpleSMSsend(recipient, message, new Guid(license));
        //        DateTime sentTime = DateTime.Now;
        //        TimeSpan span = DateTime.Now - sentTime;
        //        while (!sent && span.TotalSeconds < 5)
        //        {
        //            Thread.Sleep(5000);
        //            resp = client.GetMessageStatus(resp.MessageID);
        //            sent = resp.Sent;
        //            span = DateTime.Now - sentTime;
        //        }
        //        Console.WriteLine("Message Sent: " + resp.Sent + "\n" + "Error: " + resp.SMSError + "\n" + "Message ID: " + resp.MessageID);
        //        Console.ReadLine();
        //        String newackMessage = ackMessage;
        //        if (ackMessage != null && ackMessage != "")
        //        {
        //            var guid = Guid.NewGuid();
        //            newackMessage = newackMessage + guid;
        //            TextMessageService.SMSResponse resp1 = client.SimpleSMSsend(recipient, newackMessage, new Guid(license));
        //            alarmDataService.insertInfoIntoAlarmAckinfo(guid.ToString(), alarmId, recipient);
        //            alarmDataService.insertintoLog(alarmId, "notification-text", "text message send to " + recipient + "; repeated times : " + (repeatedIndex + 1));
        //        }
        //    }
        //    client.Close();
        //}

        //public long makePhoneCall(string[] recipients, string message, int alarmId, int repeatedIndex)
        //{
        //    try
        //    {
        //        WSDL.PhoneNotify PN = new WSDL.PhoneNotify();
        //        WSDL.AdvancedNotifyRequest[] ANRs = new WSDL.AdvancedNotifyRequest[recipients.Count()];
        //        for (int i = 0; i < recipients.Length; i++)
        //        {
        //            ANRs[i] = new WSDL.AdvancedNotifyRequest();

        //            ANRs[i].CallerIDName = ConfigurationManager.AppSettings["CallerIDName"];
        //            ANRs[i].CallerIDNumber = ConfigurationManager.AppSettings["CallerIDNumber"];
        //            ANRs[i].PhoneNumberToDial = recipients[i];
        //            ANRs[i].TextToSay = message;
        //            ANRs[i].VoiceID = Convert.ToInt32(ConfigurationManager.AppSettings["VoiceID"]);
        //            ANRs[i].StatusChangePostUrl = "https://www.example.com/callback-URL/"; //Optional
        //            ANRs[i].UTCScheduledDateTime = DateTime.UtcNow;
        //            ANRs[i].NextTryInSeconds = Convert.ToInt32(ConfigurationManager.AppSettings["NextTryInSeconds"]);
        //            ANRs[i].MaxCallLength = 120;
        //            ANRs[i].TryCount = Convert.ToInt32(ConfigurationManager.AppSettings["TryCount"]);
        //            ANRs[i].TTSvolume = 100;
        //            ANRs[i].TTSrate = 25;

        //            //ANR.UTCScheduledDateTime = new DateTime(2012, 12, 18, 11, 36, 0).ToUniversalTime(); 
        //            //If scheduling calls, the local time will automatically be converted to UTC time when using the line of code above.
        //            ANRs[i].LicenseKey = ConfigurationManager.AppSettings["License"];
        //        }
        //        WSDL.NotifyReturn[] NR = PN.NotifyMultiplePhoneAdvanced(ANRs);
        //        Thread.Sleep(60000);
        //        foreach (var recipient in recipients)
        //        {
        //            var status = PN.GetQueueIDStatusesByPhoneNumber(recipient, ConfigurationManager.AppSettings["License"]);
        //            if (status[0].CallAnswered)
        //            {
        //                var msg = "phone call:" + recipient + " has been answered at " + DateTimeOffset.Now;
        //                alarmDataService.insertintoLog(alarmId, "Phone answered", msg);
        //            }
        //            alarmDataService.insertintoLog(alarmId, "notification-phone", "phone call made to " + recipient + "; repeated times : " + (repeatedIndex + 1));
        //        }


        //        return NR[0].QueueID;

        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);

        //    }
        //}


        //public void SendMail(int alarmId, string recipient, string subject, string message, int repeatedIndex)
        //{
        //    Configuration oConfig = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
        //    var mailSettings = oConfig.GetSectionGroup("system.net/mailSettings") as MailSettingsSectionGroup;
        //    message = message.Replace("\n", "<br/>");
        //    if (mailSettings != null)
        //    {
        //        int port = mailSettings.Smtp.Network.Port;
        //        string from = mailSettings.Smtp.From;
        //        string host = mailSettings.Smtp.Network.Host;
        //        string pwd = mailSettings.Smtp.Network.Password;
        //        string uid = mailSettings.Smtp.Network.UserName;

        //        var mailMessage = new MailMessage
        //        {
        //            From = new MailAddress(@from)
        //        };

        //        mailMessage.To.Add(new MailAddress(recipient));
        //        mailMessage.CC.Add(new MailAddress(from));
        //        mailMessage.Subject = subject;
        //        mailMessage.IsBodyHtml = true;
        //        mailMessage.Body = message;

        //        var client = new SmtpClient
        //        {
        //            Host = host,
        //            Port = port,
        //            Credentials = new NetworkCredential(uid, pwd),
        //            EnableSsl = true
        //        };

        //        try
        //        {
        //            client.Send(mailMessage);
        //            alarmDataService.insertintoLog(alarmId, "notification-email", "email send to " + recipient + "; repeated times : " + (repeatedIndex + 1));
        //        }
        //        catch (Exception ex)
        //        {
        //            throw new Exception(ex.Message);
        //        }
        //    }
        //}

        private bool CheckAlarmStatus(BaseEnum.AlarmTriggerType type, List<BaseEnum.AlarmTriggerType> results, List<MonitoringStationAlarmCondition> conditions)
        {

            if (type == BaseEnum.AlarmTriggerType.ALARMRES_NODATA && results.Contains(type))
                return true;
            Boolean tempLogic;
            Boolean logic = true;
            for (var i = 0; i < results.Count; i++)
            {
                if (type == BaseEnum.AlarmTriggerType.ALARMRES_OK)
                {
                    if (results[i] == type || results[i] == BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION)
                    {
                        tempLogic = true;
                    }
                    else
                        tempLogic = false;
                }
                else if (type == BaseEnum.AlarmTriggerType.ALARMRES_VIOL_BACK2NORMAL)
                {
                    if (results[i] == type || results[i] == BaseEnum.AlarmTriggerType.ALARMRES_OK || results[i] == BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION)
                        tempLogic = true;
                    else
                        tempLogic = false;
                }
                else
                {
                    if (results[i] == type)
                        tempLogic = true;
                    else
                        tempLogic = false;

                }
                if (i == 0)
                    logic = tempLogic;
                else
                {
                    if (i < conditions.Count)
                    {
                        if (conditions[i - 1].MonitoringStationAlarmConditionLogic)
                        {
                            logic = logic && tempLogic;
                        }
                        else
                            logic = logic || tempLogic;
                    }
                }

            }

            return logic;
        }

        public string getEvalStr(List<BaseEnum.AlarmTriggerType> results, List<MonitoringStationAlarmCondition> conditions)
        {
            string evalStr = "";
            for (int i = 0; i < results.Count; i++)
            {
                evalStr = evalStr + results[i];
                if (i < conditions.Count)
                {
                    if (conditions[i].MonitoringStationAlarmConditionLogic)
                    {
                        evalStr = evalStr + "&&";
                    }
                    else
                    {
                        evalStr = evalStr + "||";
                    }
                }
            }
            return evalStr;
        }


    }
}
