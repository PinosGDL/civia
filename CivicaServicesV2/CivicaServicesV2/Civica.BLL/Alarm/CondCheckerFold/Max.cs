﻿namespace Civica.BLL.Alarm
{
    using System;
    using System.Linq;
    using Civica.Domain.CivicaDomain;
    using Civica.Infrastructure.BaseClasses;
    using Civica.Domain;

    public class Max : CondChecker
    {
        public Max(int condIdx, IDatacurrrentEntitiesContext model) : base(condIdx, model)
        {

        }

        public Max(int condIdx) :
         base(condIdx)
        {
        }

        public override string CondType()
        {
            return "Max";
        }
        /**
         * Trigger if there are any values (since we last checked) which are 
         * greater than the threshold value.
         * @return ALARMRES
        */
        public override BaseEnum.AlarmTriggerType _Check(DateTimeOffset testTime, DateTimeOffset testLastRun)
        {
            using (var model = Model)
            {
                var monitoringStation = model.MonitoringStationAlarmConditions.FirstOrDefault(x => x.Id == this.condIdx);
                var stationId = monitoringStation.MonitoringStationAlarm.MonitoringStationId;
                var alarm =
                    model.MonitoringStationAlarmConditions.Where(x => x.Id == this.condIdx)
                        .First()
                        .MonitoringStationAlarm;
                var stationTimeOffSet =
                    model.MonitoringStations.Where(x => x.Id == stationId).FirstOrDefault().TimeZone.Offset;
                var stationTimeZoneSpan = new TimeSpan(Convert.ToInt32(stationTimeOffSet), 0, 0);
                var monitoringSensors =
                    model.MonitoringSensorAlarms.Where(x => x.MonitoringStationAlarmConditionId == this.condIdx)
                        .Where(x => x.Active == true)
                        .Select(x => x.MonitoringSensor)
                        .ToArray();
                var monitoringSensorIds = monitoringSensors.Select(x => x.Id);
                var sensorData = Model.MonitoringDatas.Where(x => monitoringSensorIds.Contains(x.MonitoringSensorId));
                var data =sensorData
                        .Where(x => x.TimeStamp <= testTime)
                        .Where(x => x.TimeStamp > testLastRun);

                var conditionparameter =
                    model.MonitoringStationAlarmConditions.Where(x => x.Id == this.condIdx)
                        .Select(x => x.Params)
                        .FirstOrDefault();
                var value = Convert.ToDecimal(conditionparameter.Split(';')[1]);
                if (data.Count() == 0)
                {
                    bool points = true;
                    foreach (var monitoringsensor in monitoringSensors)
                    {
                        if (monitoringsensor.SensorDataTypeId != 1)
                        {
                            points = false;
                        }
                    }
                    if (points)
                    {
                        if (value > 0)
                        {
                            var newAlarmConditionLog3 = new MonitoringStationAlarmConditionLog();
                            newAlarmConditionLog3.AlarmId = alarm.Id;
                            newAlarmConditionLog3.TimeStamp = DateTime.Now;
                            newAlarmConditionLog3.ConditonType = "MAX";
                            newAlarmConditionLog3.description = "ALARMRES_OK";
                            newAlarmConditionLog3.note = this.result;
                            model.MonitoringStationAlarmConditionLogs.Add(newAlarmConditionLog3);
                            model.SaveChanges();
                            return BaseEnum.AlarmTriggerType.ALARMRES_OK;
                        }
                        else
                        {
                            var monitoringsensor = monitoringSensors.FirstOrDefault();
                            // build the result
                            this.result = "Sensors:";
                            for (var j = 0; j < monitoringSensors.Count(); j++)
                            {
                                if (j != monitoringSensors.Count() - 1)
                                {
                                    this.result = this.result + monitoringSensors[j].Name + "(" +
                                                  monitoringSensors[j].Id + ")" + ", ";
                                }
                                else
                                {
                                    this.result = this.result + monitoringSensors[j].Name + "(" +
                                                  monitoringSensors[j].Id + ")";
                                }
                            }

                            this.result = this.result + "\n" + "No New Data " + "From " + testLastRun.ToString() +
                                          " To " + testTime.ToString();

                            var newAlarmConditionLog3 = new MonitoringStationAlarmConditionLog();
                            newAlarmConditionLog3.AlarmId = alarm.Id;
                            newAlarmConditionLog3.TimeStamp = DateTime.Now;
                            newAlarmConditionLog3.ConditonType = "MAX";
                            newAlarmConditionLog3.description = "ALARMRES_VIOLATION";
                            newAlarmConditionLog3.note = this.result;
                            model.MonitoringStationAlarmConditionLogs.Add(newAlarmConditionLog3);
                            model.SaveChanges();
                            return BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION;
                        }

                    }
                    var newAlarmConditionLog2 = new MonitoringStationAlarmConditionLog();
                    newAlarmConditionLog2.AlarmId = alarm.Id;
                    newAlarmConditionLog2.TimeStamp = DateTime.Now;
                    newAlarmConditionLog2.ConditonType = "MAX";
                    newAlarmConditionLog2.description = "ALARMRES_NODATA";
                    newAlarmConditionLog2.note = this.result;
                    model.MonitoringStationAlarmConditionLogs.Add(newAlarmConditionLog2);
                    model.SaveChanges();
                    return BaseEnum.AlarmTriggerType.ALARMRES_NODATA;
                }
                if (value < 0)
                {
                    foreach (var monitoringsensor in monitoringSensors)
                    {
                        if (monitoringsensor.SensorDataTypeId == 1)
                        {
                            var PointSensorData = data.Where(x => x.MonitoringSensorId == monitoringsensor.Id);
                            if (PointSensorData.Count() == 0)
                            {

                                this.result = this.result + monitoringsensor.Name + "(" + monitoringsensor.Id + ") " +
                                              "No New Data From" + testLastRun.ToString() + " To " + testTime.ToString();
                                var newAlarmConditionLog3 = new MonitoringStationAlarmConditionLog();
                                newAlarmConditionLog3.AlarmId = alarm.Id;
                                newAlarmConditionLog3.TimeStamp = DateTime.Now;
                                newAlarmConditionLog3.ConditonType = "MAX";
                                newAlarmConditionLog3.description = "ALARMRES_VIOLATION";
                                newAlarmConditionLog3.note = this.result;
                                model.MonitoringStationAlarmConditionLogs.Add(newAlarmConditionLog3);
                                model.SaveChanges();
                                return BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION;
                            }
                        }
                    }
                }

                var values = data.Select(x => x.Value);
                var maxValue = values.Max();
                if (maxValue <= Convert.ToDecimal(value))
                {
                    var newAlarmConditionLogtmp = new MonitoringStationAlarmConditionLog();
                    newAlarmConditionLogtmp.AlarmId = alarm.Id;
                    newAlarmConditionLogtmp.TimeStamp = DateTime.Now;
                    newAlarmConditionLogtmp.ConditonType = "MAX";
                    newAlarmConditionLogtmp.description = "ALARMRES_OK";
                    newAlarmConditionLogtmp.note = this.result;
                    model.MonitoringStationAlarmConditionLogs.Add(newAlarmConditionLogtmp);
                    model.SaveChanges();
                    return BaseEnum.AlarmTriggerType.ALARMRES_OK;
                }

                var time =
                    Model.MonitoringDatas.Where(x => monitoringSensorIds.Contains(x.MonitoringSensorId))
                        .Where(x => x.TimeStamp <= testTime)
                        .Where(x => x.TimeStamp > testLastRun)
                        .Where(x => x.Value > value)
                        .OrderBy(x => x.TimeStamp)
                        .FirstOrDefault();
                var violTime = time;
                maxValue =
                    Model.MonitoringDatas.Where(x => monitoringSensorIds.Contains(x.MonitoringSensorId))
                        .Where(x => x.TimeStamp <= testTime)
                        .Where(x => x.TimeStamp > testLastRun)
                        .Where(x => x.Value > value)
                        .OrderByDescending(x => x.TimeStamp)
                        .Select(x => x.Value)
                        .FirstOrDefault();
                bool backToNormal = false;
                if (maxValue < value)
                {
                    backToNormal = true;
                }
                var sensor = monitoringSensors.FirstOrDefault();
                this.result = "Sensors:";
                for (var j = 0; j < monitoringSensors.Count(); j++)
                {
                    if (j != monitoringSensors.Count() - 1)
                    {
                        this.result = this.result + monitoringSensors[j].Name + "(" + monitoringSensors[j].Id + ")" +
                                      ", ";
                    }
                    else
                    {
                        this.result = this.result + monitoringSensors[j].Name + "(" + monitoringSensors[j].Id + ")";
                    }
                }
                this.result = this.result + "\n";
                this.result = this.result + "Time of Violation " +
                              violTime.TimeStamp.ToOffset(stationTimeZoneSpan).DateTime.ToString() + "\n" +
                              " Highest Value = " + maxValue + " " + sensor.Unit;
                var newAlarmConditionLog = new MonitoringStationAlarmConditionLog();
                newAlarmConditionLog.AlarmId = alarm.Id;
                newAlarmConditionLog.TimeStamp = DateTime.Now;
                newAlarmConditionLog.ConditonType = "MAX";
                if (backToNormal)
                {
                    newAlarmConditionLog.description = "ALARMRES_VIOL_BACK2NORMAL";
                }
                else
                {
                    newAlarmConditionLog.description = "ALARMRES_VIOLATION";
                }
                newAlarmConditionLog.note = this.result;
                model.MonitoringStationAlarmConditionLogs.Add(newAlarmConditionLog);
                model.SaveChanges();
                if (backToNormal)
                {
                    return BaseEnum.AlarmTriggerType.ALARMRES_VIOL_BACK2NORMAL;
                }
                else
                {
                    return BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION;
                }
            }
        }
        protected override string _GetErrors()
        {
            return "";
        }

    }
}
