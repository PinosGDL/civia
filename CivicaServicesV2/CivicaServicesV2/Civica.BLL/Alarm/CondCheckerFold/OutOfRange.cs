﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.Domain.CivicaDomain;
using System.Data.Entity;
using Civica.Domain;
using Civica.Infrastructure.BaseClasses;

namespace Civica.BLL.Alarm
{
    public class OutOfRange : CondChecker
    {
        public OutOfRange(int condIdx, IDatacurrrentEntitiesContext model) : base(condIdx, model)
        {

        }
        public OutOfRange(int condIdx) :
         base(condIdx)
        {
        }

        public override string CondType()
        {
            return "OUT_OF_RANGE";
        }
        public override BaseEnum.AlarmTriggerType _Check(DateTimeOffset testTime, DateTimeOffset testLastRun)
        {
            using (var model = Model)
            {
                var stationId = model.MonitoringStationAlarmConditions.Where(x => x.Id == this.condIdx).FirstOrDefault().MonitoringStationAlarm.MonitoringStationId;
                var stationTimeOffSet = model.MonitoringStations.Where(x => x.Id == stationId).FirstOrDefault().TimeZone.Offset;
                var stationTimeZoneSpan = new TimeSpan(Convert.ToInt32(stationTimeOffSet), 0, 0);
                var monitoringSensorIds = model.MonitoringSensorAlarms.Where(x => x.MonitoringStationAlarmConditionId == this.condIdx).Where(x => x.Active == true).Select(x => x.MonitoringSensorId);
                var monitoringSensors = model.MonitoringSensorAlarms.Where(x => x.MonitoringStationAlarmConditionId == this.condIdx).Where(x => x.Active == true).Select(x => x.MonitoringSensor).ToArray();
                var conditionparameter = model.MonitoringStationAlarmConditions.Where(x => x.Id == this.condIdx).Select(x => x.Params).FirstOrDefault();
                var alarm = model.MonitoringStationAlarmConditions.Where(x => x.Id == this.condIdx).First().MonitoringStationAlarm;
                var minValueFromCon = Convert.ToDecimal(conditionparameter.Split(';')[1]);
                var maxValueFromCon = Convert.ToDecimal(conditionparameter.Split(';')[2]);
                var data = model.MonitoringDatas.Where(x => monitoringSensorIds.Contains(x.MonitoringSensorId)).Where(x => x.TimeStamp <= testTime).Where(x => x.TimeStamp > testLastRun).OrderBy(x => x.TimeStamp);
                if (data.Count() == 0)
                {
                    bool points = true;
                    foreach (var monitoringsensor in monitoringSensors)
                    {
                        if (monitoringsensor.SensorDataTypeId != 1)
                        {
                            points = false;
                        }
                    }
                    if (points)
                    {
                        if (minValueFromCon < 0 && maxValueFromCon > 0)
                        {
                            var newAlarmConditionLog3 = new MonitoringStationAlarmConditionLog();
                            newAlarmConditionLog3.AlarmId = alarm.Id;
                            newAlarmConditionLog3.TimeStamp = DateTime.Now;
                            newAlarmConditionLog3.ConditonType = "OUT_OF_RANGE";
                            newAlarmConditionLog3.description = "ALARMRES_OK";
                            newAlarmConditionLog3.note = this.result;
                            model.MonitoringStationAlarmConditionLogs.Add(newAlarmConditionLog3);
                            model.SaveChanges();
                            return BaseEnum.AlarmTriggerType.ALARMRES_OK;
                        }
                        else
                        {
                            var monitoringsensor = monitoringSensors.FirstOrDefault();
                            // build the result
                            this.result = "Sensors:";
                            for (var j = 0; j < monitoringSensors.Count(); j++)
                            {
                                if (j != monitoringSensors.Count() - 1)
                                {
                                    this.result = this.result + monitoringSensors[j].Name + "(" + monitoringSensors[j].Id + ")" + ", ";
                                }
                                else
                                {
                                    this.result = this.result + monitoringSensors[j].Name + "(" + monitoringSensors[j].Id + ")";
                                }
                            }

                            this.result = this.result + "\n" + "No New Data " + "From " + testLastRun.ToString() + " To " + testTime.ToString();
                            var newAlarmConditionLog3 = new MonitoringStationAlarmConditionLog();
                            newAlarmConditionLog3.AlarmId = alarm.Id;
                            newAlarmConditionLog3.TimeStamp = DateTime.Now;
                            newAlarmConditionLog3.ConditonType = "OUT_OF_RANGE";
                            newAlarmConditionLog3.description = "ALARMRES_VIOLATION";
                            newAlarmConditionLog3.note = this.result;
                            model.MonitoringStationAlarmConditionLogs.Add(newAlarmConditionLog3);
                            model.SaveChanges();
                            return BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION;
                        }

                    }

                    var newAlarmConditionLog1 = new MonitoringStationAlarmConditionLog();
                    newAlarmConditionLog1.AlarmId = alarm.Id;
                    newAlarmConditionLog1.TimeStamp = DateTime.Now;
                    newAlarmConditionLog1.ConditonType = "OUT_OF_RANGE";
                    newAlarmConditionLog1.description = "ALARMRES_NODATA";
                    newAlarmConditionLog1.note = this.result;
                    model.MonitoringStationAlarmConditionLogs.Add(newAlarmConditionLog1);
                    model.SaveChanges();
                    return BaseEnum.AlarmTriggerType.ALARMRES_NODATA;
                    // there are no new rows since the last check

                }
                if (maxValueFromCon < 0 && minValueFromCon < 0)
                {
                    foreach (var monitoringsensor in monitoringSensors)
                    {
                        if (monitoringsensor.SensorDataTypeId == 1)
                        {
                            var PointSensorData = data.Where(x => x.MonitoringSensorId == monitoringsensor.Id);
                            if (PointSensorData.Count() == 0)
                            {

                                this.result = this.result + monitoringsensor.Name + "(" + monitoringsensor.Id + ") " + "No New Data From" + testLastRun.ToString() + " To " + testTime.ToString();
                                var newAlarmConditionLog1 = new MonitoringStationAlarmConditionLog();
                                newAlarmConditionLog1.AlarmId = alarm.Id;
                                newAlarmConditionLog1.TimeStamp = DateTime.Now;
                                newAlarmConditionLog1.ConditonType = "OUT_OF_RANGE";
                                newAlarmConditionLog1.description = "ALARMRES_VIOLATION";
                                newAlarmConditionLog1.note = this.result;
                                model.MonitoringStationAlarmConditionLogs.Add(newAlarmConditionLog1);
                                model.SaveChanges();
                                return BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION;
                            }
                        }
                    }

                }
                var dataList = data.Select(x => x.Value);
                var minValue = dataList.Min();
                var maxValue = dataList.Max();


                if (minValue >= minValueFromCon && maxValue <= maxValueFromCon)
                {
                    // the max and min values are within the range, so there could
                    //never be any violations
                    var newAlarmConditionLog1 = new MonitoringStationAlarmConditionLog();
                    newAlarmConditionLog1.AlarmId = alarm.Id;
                    newAlarmConditionLog1.TimeStamp = DateTime.Now;
                    newAlarmConditionLog1.ConditonType = "OUT_OF_RANGE";
                    newAlarmConditionLog1.description = "ALARMRES_OK";
                    newAlarmConditionLog1.note = this.result;
                    model.MonitoringStationAlarmConditionLogs.Add(newAlarmConditionLog1);
                    model.SaveChanges();
                    return BaseEnum.AlarmTriggerType.ALARMRES_OK;
                }

                var monitoringData = model.MonitoringDatas.Where(x => monitoringSensorIds.Contains(x.MonitoringSensorId)).Where(x => x.TimeStamp <= testTime).Where(x => x.TimeStamp > testLastRun).Where(x => x.Value < minValueFromCon || x.Value > maxValueFromCon).Select(x => new { TimeStamp = x.TimeStamp, Value = x.Value }).FirstOrDefault();

                var violTime = monitoringData.TimeStamp;
                var violValue = monitoringData.Value;

                // See if the violation goes away at the end of the data
                var value = data.Select(x => x.Value).FirstOrDefault();

                bool backToNormal = false;
                if (value > minValueFromCon && value < minValueFromCon)
                {
                    backToNormal = true;
                }
                this.result = "Sensors:";
                for (var j = 0; j < monitoringSensors.Count(); j++)
                {
                    if (j != monitoringSensors.Count() - 1)
                    {
                        this.result = this.result + monitoringSensors[j].Name + "(" + monitoringSensors[j].Id + ")" + ", ";
                    }
                    else
                    {
                        this.result = this.result + monitoringSensors[j].Name + "(" + monitoringSensors[j].Id + ")";
                    }
                }
                this.result = this.result + "\n" + "Time of Violation :" + violTime.ToOffset(stationTimeZoneSpan).DateTime + "\n" + "Violation value: " + violValue;
                var newAlarmConditionLog = new MonitoringStationAlarmConditionLog();
                newAlarmConditionLog.AlarmId = alarm.Id;
                newAlarmConditionLog.TimeStamp = DateTime.Now;
                newAlarmConditionLog.ConditonType = "MIN";
                if (backToNormal)
                {
                    newAlarmConditionLog.description = "ALARMRES_VIOL_BACK2NORMAL";
                    newAlarmConditionLog.note = this.result;
                    model.MonitoringStationAlarmConditionLogs.Add(newAlarmConditionLog);
                    model.SaveChanges();
                    return BaseEnum.AlarmTriggerType.ALARMRES_VIOL_BACK2NORMAL;
                }
                else
                {
                    newAlarmConditionLog.description = "ALARMRES_VIOLATION";
                    newAlarmConditionLog.note = this.result;
                    model.MonitoringStationAlarmConditionLogs.Add(newAlarmConditionLog);
                    model.SaveChanges();
                    return BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION;
                }
            }
        }
        protected override string _GetErrors()
        {
            return "";
        }
    }
}
