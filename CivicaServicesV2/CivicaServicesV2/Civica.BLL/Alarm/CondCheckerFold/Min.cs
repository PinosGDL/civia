﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.Domain.CivicaDomain;
using System.Data.Entity;
using Civica.Domain;
using Civica.Infrastructure.BaseClasses;

namespace Civica.BLL.Alarm
{
    public class Min : CondChecker
    {
        public Min(int condIdx, IDatacurrrentEntitiesContext model) : base(condIdx, model)
        {

        }
        public Min(int condIdx) :
         base(condIdx)
        {
        }

        public override String CondType()
        {
            return "MIN";
        }


        /**
     * Trigger if there are any values (since we last checked) which are 
     * less than the threshold value.
     * @return ALARMRES
     */
        public override BaseEnum.AlarmTriggerType _Check(DateTimeOffset testTime, DateTimeOffset testLastRun)
        {
            using (var model = Model)
            {
                var stationId = model.MonitoringStationAlarmConditions.Where(x => x.Id == this.condIdx).FirstOrDefault().MonitoringStationAlarm.MonitoringStationId;
                var stationTimeOffSet = model.MonitoringStations.Where(x => x.Id == stationId).FirstOrDefault().TimeZone.Offset;
                var stationTimeZoneSpan = new TimeSpan(Convert.ToInt32(stationTimeOffSet), 0, 0);
                var monitoringSensorIds = model.MonitoringSensorAlarms.Where(x => x.MonitoringStationAlarmConditionId == this.condIdx).Where(x => x.Active == true).Select(x => x.MonitoringSensorId);
                var monitoringSensors = model.MonitoringSensorAlarms.Where(x => x.MonitoringStationAlarmConditionId == this.condIdx).Where(x => x.Active == true).Select(x => x.MonitoringSensor).ToArray();
                var data = model.MonitoringDatas.Where(x => monitoringSensorIds.Contains(x.MonitoringSensorId)).Where(x => x.TimeStamp <= testTime).Where(x => x.TimeStamp > testLastRun);
                var alarm = model.MonitoringStationAlarmConditions.Where(x => x.Id == this.condIdx).First().MonitoringStationAlarm;

                var conditionparameter = model.MonitoringStationAlarmConditions.Where(x => x.Id == this.condIdx).Select(x => x.Params).FirstOrDefault();
                var value = Convert.ToDecimal(conditionparameter.Split(';')[1]);
                if (data.Count() == 0)
                {
                    bool points = true;
                    foreach (var monitoringsensor in monitoringSensors)
                    {
                        if (monitoringsensor.SensorDataTypeId != 1)
                        {
                            points = false;
                        }
                    }
                    if (points)
                    {
                        if (value < 0)
                        {
                            var newAlarmConditionLog3 = new MonitoringStationAlarmConditionLog();
                            newAlarmConditionLog3.AlarmId = alarm.Id;
                            newAlarmConditionLog3.TimeStamp = DateTime.Now;
                            newAlarmConditionLog3.ConditonType = "MIN";
                            newAlarmConditionLog3.description = "ALARMRES_OK";
                            newAlarmConditionLog3.note = this.result;
                            model.MonitoringStationAlarmConditionLogs.Add(newAlarmConditionLog3);
                            model.SaveChanges();
                            return BaseEnum.AlarmTriggerType.ALARMRES_OK;
                        }
                        else
                        {
                            var monitoringsensor = monitoringSensors.FirstOrDefault();
                            // build the result
                            this.result = "Sensors:";
                            for (var j = 0; j < monitoringSensors.Count(); j++)
                            {
                                if (j != monitoringSensors.Count() - 1)
                                {
                                    this.result = this.result + monitoringSensors[j].Name + "(" + monitoringSensors[j].Id + ")" + ", ";
                                }
                                else
                                {
                                    this.result = this.result + monitoringSensors[j].Name + "(" + monitoringSensors[j].Id + ")";
                                }
                            }

                            this.result = this.result + "\n" + "No New Data " + "From " + testLastRun.ToString() + " To " + testTime.ToString(); ;
                            var newAlarmConditionLog2 = new MonitoringStationAlarmConditionLog();
                            newAlarmConditionLog2.AlarmId = alarm.Id;
                            newAlarmConditionLog2.TimeStamp = DateTime.Now;
                            newAlarmConditionLog2.ConditonType = "MIN";
                            newAlarmConditionLog2.description = "ALARMRES_VIOLATION";
                            newAlarmConditionLog2.note = this.result;
                            model.MonitoringStationAlarmConditionLogs.Add(newAlarmConditionLog2);
                            model.SaveChanges();
                            return BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION;
                        }

                    }
                    var newAlarmConditionLog1 = new MonitoringStationAlarmConditionLog();
                    newAlarmConditionLog1.AlarmId = alarm.Id;
                    newAlarmConditionLog1.TimeStamp = DateTime.Now;
                    newAlarmConditionLog1.ConditonType = "MIN";
                    newAlarmConditionLog1.description = "ALARMRES_NODATA";
                    newAlarmConditionLog1.note = this.result;
                    model.MonitoringStationAlarmConditionLogs.Add(newAlarmConditionLog1);
                    model.SaveChanges();
                    return BaseEnum.AlarmTriggerType.ALARMRES_NODATA;
                }
                if (value > 0)
                {
                    foreach (var monitoringsensor in monitoringSensors)
                    {
                        if (monitoringsensor.SensorDataTypeId == 1)
                        {
                            var PointSensorData = data.Where(x => x.MonitoringSensorId == monitoringsensor.Id);
                            if (PointSensorData.Count() == 0)
                            {
                                this.result = this.result + monitoringsensor.Name + "(" + monitoringsensor.Id + ") " + "No New Data From" + testLastRun.ToString() + " To " + testTime.ToString();
                                var newAlarmConditionLog3 = new MonitoringStationAlarmConditionLog();
                                newAlarmConditionLog3.AlarmId = alarm.Id;
                                newAlarmConditionLog3.TimeStamp = DateTime.Now;
                                newAlarmConditionLog3.ConditonType = "MIN";
                                newAlarmConditionLog3.description = "ALARMRES_VIOLATION";
                                newAlarmConditionLog3.note = this.result;
                                model.MonitoringStationAlarmConditionLogs.Add(newAlarmConditionLog3);
                                model.SaveChanges();
                                return BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION;
                            }
                        }
                    }

                }
                var values = data.Select(x => x.Value);
                var minValue = values.Min();
                if (minValue >= Convert.ToDecimal(value))
                {
                    var newAlarmConditionLogtmp = new MonitoringStationAlarmConditionLog();
                    newAlarmConditionLogtmp.AlarmId = alarm.Id;
                    newAlarmConditionLogtmp.TimeStamp = DateTime.Now;
                    newAlarmConditionLogtmp.ConditonType = "MIN";
                    newAlarmConditionLogtmp.description = "ALARMRES_OK";
                    newAlarmConditionLogtmp.note = this.result;
                    model.MonitoringStationAlarmConditionLogs.Add(newAlarmConditionLogtmp);
                    model.SaveChanges();
                    return BaseEnum.AlarmTriggerType.ALARMRES_OK;
                }
                var time = model.MonitoringDatas.Where(x => monitoringSensorIds.Contains(x.MonitoringSensorId)).Where(x => x.TimeStamp <= testTime).Where(x => x.TimeStamp > testLastRun).Where(x => x.Value < value).OrderBy(x => x.TimeStamp).FirstOrDefault();
                var violTime = time;
                minValue = model.MonitoringDatas.Where(x => monitoringSensorIds.Contains(x.MonitoringSensorId)).Where(x => x.TimeStamp <= testTime).Where(x => x.TimeStamp > testLastRun).Where(x => x.Value < value).OrderByDescending(x => x.TimeStamp).Select(x => x.Value).FirstOrDefault();
                bool backToNormal = false;
                if (minValue > Convert.ToDecimal(value))
                {
                    backToNormal = true;
                }
                var sensor = model.MonitoringSensors.Where(x => x.Id == monitoringSensorIds.FirstOrDefault()).FirstOrDefault();
                this.result = "Sensors:";
                for (var j = 0; j < monitoringSensors.Count(); j++)
                {
                    if (j != monitoringSensors.Count() - 1)
                    {
                        this.result = this.result + monitoringSensors[j].Name + "(" + monitoringSensors[j].Id + ")" + ", ";
                    }
                    else
                    {
                        this.result = this.result + monitoringSensors[j].Name + "(" + monitoringSensors[j].Id + ")";
                    }
                }
                this.result = this.result + "\n";
                this.result = this.result + "Time of Violation " + violTime.TimeStamp.ToOffset(stationTimeZoneSpan).DateTime.ToString() + "\n" + " Lowest Value = " + minValue + " " + sensor.Unit;
                var newAlarmConditionLog = new MonitoringStationAlarmConditionLog();
                newAlarmConditionLog.AlarmId = alarm.Id;
                newAlarmConditionLog.TimeStamp = DateTime.Now;
                newAlarmConditionLog.ConditonType = "MIN";
                if (backToNormal)
                {
                    newAlarmConditionLog.description = "ALARMRES_VIOL_BACK2NORMAL";
                    newAlarmConditionLog.note = this.result;
                    model.MonitoringStationAlarmConditionLogs.Add(newAlarmConditionLog);
                    model.SaveChanges();
                    return BaseEnum.AlarmTriggerType.ALARMRES_VIOL_BACK2NORMAL;
                }
                else
                {
                    newAlarmConditionLog.description = "ALARMRES_VIOLATION";
                    newAlarmConditionLog.note = this.result;
                    model.MonitoringStationAlarmConditionLogs.Add(newAlarmConditionLog);
                    model.SaveChanges();
                    return BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION;
                }
            }
        }
        protected override string _GetErrors()
        {
            return "";
        }
    }
}
