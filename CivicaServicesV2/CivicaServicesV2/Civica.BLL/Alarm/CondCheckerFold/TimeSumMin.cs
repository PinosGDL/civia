﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.Domain.CivicaDomain;
using System.Data.Entity;
using Civica.Domain;
using Civica.Infrastructure.BaseClasses;

namespace Civica.BLL.Alarm
{
    public class TimeSumMin : CondChecker
    {
        public TimeSumMin(int condIdx, IDatacurrrentEntitiesContext model) : base(condIdx, model)
        {

        }
        public TimeSumMin(int condIdx) :
         base(condIdx)
        {
        }

        public override string CondType()
        {
            return "TIMESUM_MIN";
        }

        /**
   * Trigger if the sum of all values over the specified number of hours is 
   * less than the specified minimum
   * @return ALARMRES
   */
        public override BaseEnum.AlarmTriggerType _Check(DateTimeOffset testTime, DateTimeOffset testLastRun)
        {
            using (var model = Model)
            {
                var monitoringSensorIds = model.MonitoringSensorAlarms.Where(x => x.MonitoringStationAlarmConditionId == this.condIdx).Where(x => x.Active == true).Select(x => x.MonitoringSensorId);
                var monitoringSensors = model.MonitoringSensorAlarms.Where(x => x.MonitoringStationAlarmConditionId == this.condIdx).Where(x => x.Active == true).Select(x => x.MonitoringSensor).ToArray();
                var conditionparameter = model.MonitoringStationAlarmConditions.Where(x => x.Id == this.condIdx).Select(x => x.Params).FirstOrDefault();
                var sumHours = Convert.ToDouble(conditionparameter.Split(';')[2]);
                var sumValue = Convert.ToDecimal(conditionparameter.Split(';')[1]);
                var alarm = model.MonitoringStationAlarmConditions.Where(x => x.Id == this.condIdx).First().MonitoringStationAlarm;
                List<decimal> sumValueList = new List<decimal>();
                var newTestTime = testTime;
                var newStartTime = testTime;
                while (DateTimeOffset.Compare(newStartTime, testLastRun) > 0)
                {
                    newStartTime = newStartTime.AddHours((-1) * sumHours);
                }
                var data = model.MonitoringDatas.Where(x => monitoringSensorIds.Contains(x.MonitoringSensorId)).Where(x => x.TimeStamp <= testTime).Where(x => x.TimeStamp > newStartTime);

                if (data.Count() == 0)
                {

                    bool points = true;
                    foreach (var monitoringsensor in monitoringSensors)
                    {
                        if (monitoringsensor.SensorDataTypeId != 1)
                        {
                            points = false;
                        }
                    }
                    if (points)
                    {
                        if (sumValue > 0)
                        {
                            var sensor = model.MonitoringSensors.Where(x => x.Id == monitoringSensorIds.FirstOrDefault()).FirstOrDefault();
                            // build the result
                            this.result = "Sensors:";
                            for (var j = 0; j < monitoringSensors.Count(); j++)
                            {
                                if (j != monitoringSensors.Count() - 1)
                                {
                                    this.result = this.result + monitoringSensors[j].Name + "(" + monitoringSensors[j].Id + ")" + ", ";
                                }
                                else
                                {
                                    this.result = this.result + monitoringSensors[j].Name + "(" + monitoringSensors[j].Id + ")";
                                }
                            }
                            this.result = this.result + "\n";
                            this.result = this.result + "No New Data " + "From " + testLastRun.ToString() + " To " + testTime.ToString();
                            var newAlarmConditionLog1 = new MonitoringStationAlarmConditionLog();
                            newAlarmConditionLog1.AlarmId = alarm.Id;
                            newAlarmConditionLog1.TimeStamp = DateTime.Now;
                            newAlarmConditionLog1.ConditonType = "TIMESUM_MIN";
                            newAlarmConditionLog1.description = "ALARMRES_VIOLATION";
                            newAlarmConditionLog1.note = this.result;
                            model.MonitoringStationAlarmConditionLogs.Add(newAlarmConditionLog1);
                            model.SaveChanges();
                            return BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION;

                        }
                        else
                        {
                            var newAlarmConditionLog1 = new MonitoringStationAlarmConditionLog();
                            newAlarmConditionLog1.AlarmId = alarm.Id;
                            newAlarmConditionLog1.TimeStamp = DateTime.Now;
                            newAlarmConditionLog1.ConditonType = "TIMESUM_MIN";
                            newAlarmConditionLog1.description = "ALARMRES_VIOLATION";
                            newAlarmConditionLog1.note = this.result;
                            model.MonitoringStationAlarmConditionLogs.Add(newAlarmConditionLog1);
                            model.SaveChanges();
                            return BaseEnum.AlarmTriggerType.ALARMRES_OK;
                        }

                    }
                    var newAlarmConditionLog2 = new MonitoringStationAlarmConditionLog();
                    newAlarmConditionLog2.AlarmId = alarm.Id;
                    newAlarmConditionLog2.TimeStamp = DateTime.Now;
                    newAlarmConditionLog2.ConditonType = "TIMESUM_MIN";
                    newAlarmConditionLog2.description = "ALARMRES_NODATA";
                    newAlarmConditionLog2.note = this.result;
                    model.MonitoringStationAlarmConditionLogs.Add(newAlarmConditionLog2);
                    model.SaveChanges();
                    return BaseEnum.AlarmTriggerType.ALARMRES_NODATA;
                }
                if (sumValue > 0)
                {
                    foreach (var monitoringsensor in monitoringSensors)
                    {
                        if (monitoringsensor.SensorDataTypeId == 1)
                        {
                            var PointSensorData = data.Where(x => x.MonitoringSensorId == monitoringsensor.Id);
                            if (PointSensorData.Count() == 0)
                            {
                                this.result = this.result + monitoringsensor.Name + "(" + monitoringsensor.Id + ") " + "No New Data From" + testLastRun.ToString() + " To " + testTime.ToString();
                                var newAlarmConditionLog2 = new MonitoringStationAlarmConditionLog();
                                newAlarmConditionLog2.AlarmId = alarm.Id;
                                newAlarmConditionLog2.TimeStamp = DateTime.Now;
                                newAlarmConditionLog2.ConditonType = "TIMESUM_MIN";
                                newAlarmConditionLog2.description = "ALARMRES_VIOLATION";
                                newAlarmConditionLog2.note = this.result;
                                model.MonitoringStationAlarmConditionLogs.Add(newAlarmConditionLog2);
                                model.SaveChanges();
                                return BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION;
                            }
                        }
                    }
                }
                var checkFromTimes = new List<DateTimeOffset>();
                var checkToTimes = new List<DateTimeOffset>();
                var dictonarySumValue = new Dictionary<int, List<decimal>>();
                int index = 0;
                while (newTestTime > testLastRun)
                {
                    testTime = newTestTime;
                    newTestTime = newTestTime.AddHours((-1) * sumHours);
                    sumValueList = data.Where(x => x.TimeStamp <= testTime).Where(x => x.TimeStamp > newTestTime).GroupBy(x => x.MonitoringSensorId).Select(x => new { sensorId = x.Key, sumValue = x.Sum(row => row.Value) }).Select(x => x.sumValue).ToList();
                    checkFromTimes.Add(newTestTime);
                    checkToTimes.Add(testTime);
                    dictonarySumValue.Add(index, sumValueList);
                    index++;
                }
                bool allValueOk = true;
                decimal VIOLATIONValue = 0;
                DateTimeOffset? ViolationFromTime = null;
                DateTimeOffset? ViolationToTime = null;

                foreach (var key in dictonarySumValue)
                {
                    var sums = dictonarySumValue[key.Key];
                    foreach (var sum in sums)
                    {
                        if (sum <= sumValue)
                        {
                            allValueOk = false;
                            VIOLATIONValue = sum;
                            ViolationFromTime = checkFromTimes[key.Key];
                            ViolationToTime = checkToTimes[key.Key];
                            break;
                        }
                    }
                }

                if (allValueOk)
                {
                    var newAlarmConditionLog2 = new MonitoringStationAlarmConditionLog();
                    newAlarmConditionLog2.AlarmId = alarm.Id;
                    newAlarmConditionLog2.TimeStamp = DateTime.Now;
                    newAlarmConditionLog2.ConditonType = "TIMESUM_MIN";
                    newAlarmConditionLog2.description = "ALARMRES_OK";
                    newAlarmConditionLog2.note = this.result;
                    model.MonitoringStationAlarmConditionLogs.Add(newAlarmConditionLog2);
                    model.SaveChanges();
                    return BaseEnum.AlarmTriggerType.ALARMRES_OK;
                }
                var Unitsensor = model.MonitoringSensors.Where(x => x.Id == monitoringSensorIds.FirstOrDefault()).FirstOrDefault();
                // build the result
                this.result = "Sensors:";
                for (var j = 0; j < monitoringSensors.Count(); j++)
                {
                    if (j != monitoringSensors.Count() - 1)
                    {
                        this.result = this.result + monitoringSensors[j].Name + "(" + monitoringSensors[j].Id + ")" + ", ";
                    }
                    else
                    {
                        this.result = this.result + monitoringSensors[j].Name + "(" + monitoringSensors[j].Id + ")";
                    }
                }
                this.result = this.result + "\n";
                this.result = this.result + "From " + ViolationFromTime.Value.ToString() + " To " + ViolationToTime.Value.ToString() + " Sum Value is " + VIOLATIONValue + " " + Unitsensor.Unit;
                var newAlarmConditionLog3 = new MonitoringStationAlarmConditionLog();
                newAlarmConditionLog3.AlarmId = alarm.Id;
                newAlarmConditionLog3.TimeStamp = DateTime.Now;
                newAlarmConditionLog3.ConditonType = "TIMESUM_MIN";
                newAlarmConditionLog3.description = "ALARMRES_VIOLATION";
                newAlarmConditionLog3.note = this.result;
                model.MonitoringStationAlarmConditionLogs.Add(newAlarmConditionLog3);
                model.SaveChanges();
                return BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION;
            }
        }

        protected override string _GetErrors()
        {
            return "";
        }
    }
}
