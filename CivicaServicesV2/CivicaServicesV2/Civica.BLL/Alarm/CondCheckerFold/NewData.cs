﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.Domain.CivicaDomain;
using System.Data.Entity;
using Civica.Domain;
using Civica.Infrastructure.BaseClasses;

namespace Civica.BLL.Alarm
{
    public class NewData : CondChecker
    {
        public NewData(int condIdx, IDatacurrrentEntitiesContext model) : base(condIdx, model)
        {

        }
        public NewData(int condIdx) :
         base(condIdx)
        {
        }


        public override string CondType()
        {
            return "NewData";
        }
        /**
         * Trigger if there are any values (since we last checked) which are 
         * greater than the threshold value.
         * @return ALARMRES
        */
        public override BaseEnum.AlarmTriggerType _Check(DateTimeOffset testTime, DateTimeOffset testLastRun)
        {
            using (var model = Model)
            {
                var monitoringSensorIds = model.MonitoringSensorAlarms.Where(x => x.MonitoringStationAlarmConditionId == this.condIdx).Where(x => x.Active == true).Select(x => x.MonitoringSensorId);
                var conditionparameter = model.MonitoringStationAlarmConditions.Where(x => x.Id == this.condIdx).Select(x => x.Params).FirstOrDefault();
                var timebuffer = Convert.ToDouble(conditionparameter.Split(';')[1]);
                var newTestLastRun = testTime.AddHours((-1) * timebuffer);
                var alarm = model.MonitoringStationAlarmConditions.Where(x => x.Id == this.condIdx).First().MonitoringStationAlarm;
                var dataList = model.MonitoringDatas.Where(x => monitoringSensorIds.Contains(x.MonitoringSensorId)).Where(x => x.TimeStamp <= testTime).Where(x => x.TimeStamp > newTestLastRun);
                if (dataList.Count() == 0)
                {
                    var newAlarmConditionLog1 = new MonitoringStationAlarmConditionLog();
                    newAlarmConditionLog1.AlarmId = alarm.Id;
                    newAlarmConditionLog1.TimeStamp = DateTime.Now;
                    newAlarmConditionLog1.ConditonType = "NewData";
                    newAlarmConditionLog1.description = "ALARMRES_OK";
                    newAlarmConditionLog1.note = this.result;
                    model.MonitoringStationAlarmConditionLogs.Add(newAlarmConditionLog1);
                    model.SaveChanges();
                    return BaseEnum.AlarmTriggerType.ALARMRES_OK;
                }
                this.result = "SensorId =" + String.Join(",", monitoringSensorIds) + "\n" + "Number of data points seen = " + dataList.Count();
                var newAlarmConditionLog = new MonitoringStationAlarmConditionLog();
                newAlarmConditionLog.AlarmId = alarm.Id;
                newAlarmConditionLog.TimeStamp = DateTime.Now;
                newAlarmConditionLog.ConditonType = "NewData";
                newAlarmConditionLog.description = "ALARMRES_VIOLATION";
                newAlarmConditionLog.note = this.result;
                model.MonitoringStationAlarmConditionLogs.Add(newAlarmConditionLog);
                model.SaveChanges();
                return BaseEnum.AlarmTriggerType.ALARMRES_VIOLATION;
            }
        }
        protected override string _GetErrors()
        {
            return "";
        }
    }
}
