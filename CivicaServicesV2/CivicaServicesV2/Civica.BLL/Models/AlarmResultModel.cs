﻿using Civica.Infrastructure.BaseClasses;

namespace Civica.BLL.Models
{
    public class AlarmResultModel
    {
        public int AlarmId { get; set; }
        public BaseEnum.AlarmTriggerType AlarmStatus { get; set; }
        public string Messages { get; set; }
    }
}
