﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civica.BLL.Models.AnalysisTools
{
    public class Point
    {

        public Point()
        {

        }
        public Point(double _x, double _y)
        {
            this.X = _x;
            this.Y = _y;
        }
        public double X { get; set; }
        public double Y { get; set; }
    }
}
