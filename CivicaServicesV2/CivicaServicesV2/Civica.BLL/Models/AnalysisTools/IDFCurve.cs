﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civica.BLL.Models.AnalysisTools
{
    public class IDFCurve
    {
        public Decimal intensity;
        public Decimal duration;
    }
}
