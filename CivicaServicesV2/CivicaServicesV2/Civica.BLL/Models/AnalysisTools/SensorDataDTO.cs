﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civica.BLL.Models.AnalysisTools
{
    public class SensorDataDTO
    {
        public int Id { get; set; }
        public decimal? value { get; set; }
        public DateTime timestamp { get; set; }
        public short Processed { get; set; }
        public bool IsRaw { get; set; }
        public string ChartType { get; set; }
    }
}
