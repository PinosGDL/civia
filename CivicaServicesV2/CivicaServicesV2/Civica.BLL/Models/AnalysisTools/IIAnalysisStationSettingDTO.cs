﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civica.BLL.Models.AnalysisTools
{
    public class IIAnalysisStationSettingDTO
    {
        public int Id { get; set; }
        public int StationId { get; set; }
        public int FlowSensorId { get; set; }
        public int RainGuageSensorId { get; set; }
        public decimal DrainageArea { get; set; }
        public decimal Population { get; set; }
        public decimal tc { get; set; }
        public decimal InterDryPeriod { get; set; }
        public decimal MinumStormSize { get; set; }
        public int DesignStromId {get;set;}
        public int MinumNumberOfEvent { get; set; }
        public double MinumRSquare { get; set; }
        public bool isLeaner { get; set; }
        public int returnInterval { get; set; }
    }
}
