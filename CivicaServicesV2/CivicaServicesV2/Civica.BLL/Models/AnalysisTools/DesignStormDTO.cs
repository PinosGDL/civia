﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.Domain.CivicaDomain;

namespace Civica.BLL.Models.AnalysisTools
{
    public class DesignStormDTO
    {

        public int Id { get; set; }
        public int OrganizationID { get; set; }
        public string FullName { get; set; }
        public string ShortName { get; set; }
        public bool Public { get; set; }
        public string Comments { get; set; }
        public List<DesignStormIntervalDTO> DesignStormIntervals { get; set; }
        public int? UnitId { get; set; }
        public string UnitSymbol { get; set; }
        public bool Active { get; set; }

        public static DesignStormDTO Create(DesignStorm map)
        {
            var returnvalue = new DesignStormDTO();
            returnvalue.Id = map.Id;
            returnvalue.OrganizationID = map.OrganizationID;
            returnvalue.FullName = map.FullName;
            returnvalue.ShortName = map.ShortName;
            returnvalue.Public = map.Public;
            returnvalue.Comments = map.Comments;
            returnvalue.UnitId = map.UnitId;
            if (returnvalue.UnitId != null)
            {
                returnvalue.UnitSymbol = map.Unit.UnitSymbol;
            }
            else
            {
                returnvalue.UnitSymbol = "N/A";
            }
            return returnvalue;
        }
    }
}
