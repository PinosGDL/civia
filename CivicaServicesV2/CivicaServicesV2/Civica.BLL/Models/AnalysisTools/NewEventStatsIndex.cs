﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civica.BLL.Models.AnalysisTools
{
    public class NewEventStatsIndex
    {
        public int startIndex { get; set; }
        public int endIndex { get; set; }
    }
}
