﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civica.BLL.Models.AnalysisTools
{
    public class TimeValue
    {
        public DateTime Timestamp { get; set; }
        public Decimal Value { get; set; }
    }
}
