﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Civica.BLL.Models.AnalysisTools
{
    public class ScattterReportChartSeriesModel
    {
        public decimal X { get; set; }
        public decimal Y { get; set; }
        public int ProjectId { get; set; }
        public int ServiceId { get; set; }
        public int SiteId { get; set; }
        public int IndependentId { get; set; }
        public int DependentId { get; set; }
        public string errorMessage { get; set; }
        public bool error { get; set; }
    }

    public enum ColorOptions
    {
        [Display(Name = "Rainbow")]
        Rainbow = 2,
        [Display(Name = "Simple")]
        Simple = 0,
        [Display(Name = "Two Color")]
        twoColor = 1,
    }

}
