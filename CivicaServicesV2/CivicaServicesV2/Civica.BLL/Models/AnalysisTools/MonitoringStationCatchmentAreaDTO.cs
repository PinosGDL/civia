﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.Domain.CivicaDomain;

namespace Civica.BLL.Models.AnalysisTools
{
    public class MonitoringStationCatchmentAreaDTO
    {
        public int Id { get; set; }
        public int MonitoringStationId { get; set; }
        public decimal CatchmentArea { get; set; }
        public System.DateTime TimeStamp { get; set; }
        public bool Active { get; set; }
        public DateTimeOffset Created { get; set; }
        public DateTimeOffset Modified { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public int? UnitId { get; set; }
        public string UnitSymbol { get; set; }

        public static MonitoringStationCatchmentAreaDTO Create(MonitoringStationCatchmentArea catchment)
        {
            MonitoringStationCatchmentAreaDTO dto = new MonitoringStationCatchmentAreaDTO();

            dto.Id = catchment.Id;

            dto.MonitoringStationId = catchment.MonitoringStationId;
            dto.CatchmentArea = catchment.CatchmentArea;
            dto.Active = catchment.Active;
            dto.UnitId = catchment.UnitId;
            if (catchment.UnitId != null)
            {
                dto.UnitSymbol = catchment.Unit.UnitSymbol;
            }

            if ((catchment.MonitoringStation.TimeZone.TZICode == "") || (catchment.MonitoringStation.TimeZone.TZICode == null))
            {

                dto.Created = catchment.Created;
                dto.Modified = catchment.Modified;
                dto.TimeStamp = catchment.TimeStamp;
            }
            else
            {
                TimeZoneInfo tsi = TimeZoneInfo.FindSystemTimeZoneById(catchment.MonitoringStation.TimeZone.TZICode);
                dto.Created = TimeZoneInfo.ConvertTime(catchment.Created, tsi);
                dto.Modified = TimeZoneInfo.ConvertTime(catchment.Modified, tsi);
                dto.TimeStamp = TimeZoneInfo.ConvertTime(catchment.TimeStamp, tsi);
            }

            dto.CreatedBy = catchment.CreatedBy;
            dto.ModifiedBy = catchment.ModifiedBy;

            return dto;
        }
    }
}
