﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civica.BLL.Models.AnalysisTools
{
    public class MonitoringDataDTO
    {
        public long Id { get; set; }
        public int MonitoringSensorId { get; set; }
        public decimal Value { get; set; }
        public DateTime TimeStamp { get; set; }
        public short Processed { get; set; }
        public int Occurences { get; set; } // Temporary fix for DWF Analysis, need to make new DTO with this attribute, but im lazy..... maybe later
        public string TimeStampString { get; set; }
        public DateTime UTCTimeStamp { get; set; }
    }
}
