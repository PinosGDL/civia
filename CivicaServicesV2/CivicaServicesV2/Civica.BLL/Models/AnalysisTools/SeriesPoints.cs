﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civica.BLL.Models.AnalysisTools
{
    public class SeriesPoints
    {
        public SeriesPoints(decimal rX, decimal rY)
        {
            xValue = rX;
            yValue = rY;
        }
        public SeriesPoints()
        {
        }
        public Decimal xValue { get; set; }
        public Decimal yValue { get; set; }
    }
}
