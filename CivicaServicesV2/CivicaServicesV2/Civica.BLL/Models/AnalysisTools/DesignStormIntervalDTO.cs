﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Civica.BLL.Models.AnalysisTools
{
    public class DesignStormIntervalDTO
    {
        public int DesignStormId { get; set; }
        public int DesignStormReturnIntervalId { get; set; }
        public List<decimal?> minA { get; set; }
        public List<decimal?> minB { get; set; }
        public List<decimal?> minC { get; set; }
    }
}
