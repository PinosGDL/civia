﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.Domain.CivicaDomain;

namespace Civica.BLL.Models.AnalysisTools
{
    public class EventStatRow
    {
        public string Site { get; set; }
        public string Id { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Decimal DrainageArea { get; set; }
        public Decimal Tc { get; set; }
        public Decimal TotalPrecip { get; set; }
        // peak intensity over 1 h (mm/h)
        public double PeakIntensity { get; set; }
        // peak intensity over Tc (mm/h)
        public Double PeakIntensityOverTc { get; set; }
        public Decimal PeakIIFlow { get; set; }
        public Decimal PeakIIRate { get; set; }
        public Decimal VolumeOfII { get; set; }
        public Decimal VolumetricRunoffCoef { get; set; }
        public Decimal PeakIiCoef { get; set; }
        public DateTime TimeOfPeakII { get; set; }
        public DateTime TimeOfPeakWWF { get; set; }
        public Decimal MeasuredPeakFlow { get; set; }
        public Decimal EstDWFAtTd { get; set; }
        public Decimal PeakIIOverPeakDWF { get; set; }
        public bool PossibleUpdate { get; set; }

        public static EventStatRow Create(IIAnalysisSummaryEventStat summaryEventStat)
        {
            return new EventStatRow()
            {
                Id = summaryEventStat.Id + "",
                Site = summaryEventStat.MonitoringStation.Name,
                StartDate = summaryEventStat.StartDate,
                EndDate = summaryEventStat.EndDate,
                TotalPrecip = summaryEventStat.TotalPercip,
                PeakIntensity = Decimal.ToDouble(summaryEventStat.PeakIntensity),
                PeakIIFlow = summaryEventStat.PeakIIFlow,
                PeakIIRate = summaryEventStat.PeakIIRate,
                VolumeOfII = summaryEventStat.VolumeOfII,
                VolumetricRunoffCoef = summaryEventStat.VolumetricRunoffCoef,
                PeakIiCoef = summaryEventStat.PeakIICoef,
                TimeOfPeakII = summaryEventStat.TimeofPeakII,
                TimeOfPeakWWF = summaryEventStat.TimeofPeakWWF,
                MeasuredPeakFlow = summaryEventStat.MeasuredPeakFlow,
                EstDWFAtTd = summaryEventStat.EstDWFAtId,
                PeakIIOverPeakDWF = summaryEventStat.PeakIIOverPeakDWF,
                PeakIntensityOverTc = Decimal.ToDouble(summaryEventStat.PeakIntensityOverTc),
            };
        }
    }
}
