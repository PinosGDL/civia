﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civica.BLL.Models.AnalysisTools
{
    public class Periods
    {
        public Periods()
        {
            this.Lines = new List<DateValueLine>();
        }

    public List<DateValueLine> Lines { get; set; }
}
}
