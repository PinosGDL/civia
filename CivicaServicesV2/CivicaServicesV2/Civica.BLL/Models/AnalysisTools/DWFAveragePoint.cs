﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civica.BLL.Models.AnalysisTools
{
    public class DWFAveragePoint
    {
        public decimal Value { get; set; }
        public DateTime? TimeStamp { get; set; }
        public int Occurence { get; set; }
    }
}
