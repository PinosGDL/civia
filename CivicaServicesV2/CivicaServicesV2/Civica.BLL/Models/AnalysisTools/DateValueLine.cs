﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civica.BLL.Models.AnalysisTools
{
    public class DateValueLine
    {
        public DateValueLine()
        {
            this.Pairs = new List<TimeValue>();
        }

        public List<TimeValue> Pairs { get; set; }
    }
}
