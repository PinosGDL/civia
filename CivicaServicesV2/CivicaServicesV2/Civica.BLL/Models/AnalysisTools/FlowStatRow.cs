﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civica.BLL.Models.AnalysisTools
{
    public class FlowStatRow
    {
        public string Name;
        public string Variable;
        public string AverageUnits;
        public string NormUnits;
        public Nullable<double> Average;
        public Nullable<double> Normalized;
        public int AverageNumSigDigs;

        public int NormNumSigDigs;
        public FlowStatRow(string Name, string Variable, string AverageUnits, string NormUnits, int AverageNumSigDigs = 4, int NormNumSigDigs = 4)
        {
            this.Name = Name;
            this.Variable = Variable;
            this.AverageUnits = AverageUnits;
            this.NormUnits = NormUnits;
            this.Average = null;
            this.Normalized = null;
            this.AverageNumSigDigs = AverageNumSigDigs;
            this.NormNumSigDigs = NormNumSigDigs;
        }
    }
}
