﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civica.BLL.Models.AnalysisTools
{
    public class MonitoringStationTCDTO
    {
        public int Id { get; set; }
        public int MonitoringStationId { get; set; }
        public decimal TC { get; set; }
        public System.DateTime TimeStamp { get; set; }
        public bool Active { get; set; }
    }
}
