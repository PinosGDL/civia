﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civica.BLL.Models.AnalysisTools
{
    public class ProjectStationDTO
    {
        public int ProjectId { get; set; }
        public int StationId { get; set; }
        public string ProjectName { get; set; }
        public string StationName { get; set; }
    }
}
