﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civica.BLL.Models.AnalysisTools
{
    public class MonitoringAnalysisTaskDTO
    {
        public int Id { get; set; }
        public int? StationId { get; set; }
        public DateTime? DataStartTime { get; set; }
        public DateTime? DataEndTime { get; set; }
    }
}
