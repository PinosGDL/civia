﻿using System;
using System.Collections.Generic;
using System.Linq;
using Civica.Domain.CivicaDomain;


namespace Civica.BLL.Models
{
    public class MonitoringTaskDTO
    {
        public MonitoringTaskDTO()
        {


        }
        public int Id { get; set; }
        public int? MonitoringStationId { get; set; }
        public int TypeId { get; set; }
        public int StatusId { get; set; }
        public int PoriorityId { get; set; }
        public int FormatId { get; set; }
        public string FilePath { get; set; }
        public string OriginalPath { get; set; }
        public string FilePathUndo { get; set; }
        public DateTimeOffset? ProcessStartTime { get; set; }
        public DateTimeOffset? ProcessEndTime { get; set; }
        public bool UploadRaw { get; set; }
        public bool Unprocessed { get; set; }
        public bool? CancelProcess { get; set; }
        public int? PercentCompleted { get; set; }
        public bool? Confirm { get; set; }
        public int? Retries { get; set; }
        public string ResultMessage { get; set; }
        public string Comments { get; set; }
        public DateTimeOffset Created { get; set; }
        public DateTimeOffset Modified { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public bool Active { get; set; }



        public static MonitoringTaskDTO Create(MonitoringTask task)
        {
            MonitoringTaskDTO dto = new MonitoringTaskDTO();

            dto.Id = task.Id;
            dto.MonitoringStationId = task.MonitoringStationId;
            dto.TypeId = task.TypeId;
            dto.StatusId = task.StatusId;
            dto.PoriorityId = task.PoriorityId;
            dto.FormatId = task.FormatId;
            dto.FilePath = task.FilePath;
            dto.FilePathUndo = task.FilePathUndo;
            dto.ProcessStartTime = task.ProcessStartTime;
            dto.ProcessEndTime = task.ProcessEndTime;
            dto.UploadRaw = task.UploadRaw;
            dto.Unprocessed = task.Unprocessed;
            dto.CancelProcess = task.CancelProcess;
            dto.PercentCompleted = task.PercentCompleted;
            dto.Confirm = task.Confirm;
            dto.Retries = task.Retries;
            dto.ResultMessage = task.ResultMessage;
            dto.Comments = task.Comments;
            dto.Created = task.Created;
            dto.Modified = task.Modified;
            dto.CreatedBy = task.CreatedBy;
            dto.ModifiedBy = task.ModifiedBy;
            dto.Active = task.Active;
            dto.OriginalPath = task.OriginalFileName;

            return dto;
        }


    }
}
