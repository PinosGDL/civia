﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civica.BLL.Models
{
    public class MonitoringTaskStartTimeAndEndTimeDTO
    {
        public MonitoringTaskStartTimeAndEndTimeDTO()
        {
            this.Passed = true;
        }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public bool Passed { get; set; }
    }
}
