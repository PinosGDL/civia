﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.Domain.CivicaDomain;

namespace Civica.BLL.Models
{
    public class LoggerDTO
    {
        public int Id { get; set; }
        public int LoggerTypeId { get; set; }
        public string UniqueId { get; set; }
        public int? MonitoringLoggerServerId { get; set; }
        public MonitoringLoggerServerDTO Server { get; set; }
        public static LoggerDTO Create(MonitoringLogger logger)
        {
            LoggerDTO dto = new LoggerDTO();
            dto.Id = logger.Id;
            dto.LoggerTypeId = logger.LoggerTypeId;
            dto.MonitoringLoggerServerId = logger.MonitoringLoggerServerId;
            dto.UniqueId = logger.UniqueId;

            if(logger.MonitoringLoggerServer != null)
            {
                dto.Server = MonitoringLoggerServerDTO.Create(logger.MonitoringLoggerServer);
            }

            return dto;
        }

    }


    public class MonitoringLoggerServerDTO
    {
        public int Id { get; set; }
        public int ServerTypeId { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public string URL { get; set; }
        public string Username { get; set; }
        public string Paassword { get; set; }
        public System.DateTimeOffset Created { get; set; }
        public System.DateTimeOffset Modified { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public bool Active { get; set; }

        public static MonitoringLoggerServerDTO Create(MonitoringLoggerServer logger)
        {
            MonitoringLoggerServerDTO dto = new MonitoringLoggerServerDTO();

            dto.Id = logger.Id;
            dto.ServerTypeId = logger.ServerTypeId;
            dto.Name = logger.Name;
            dto.Path = logger.Path;
            dto.URL = logger.URL;
            dto.Username = logger.Username;
            dto.Paassword = logger.Paassword;
            dto.Created = logger.Created;
            dto.Modified = logger.Modified;
            dto.CreatedBy = logger.CreatedBy;
            dto.ModifiedBy = logger.ModifiedBy;
            dto.Active = logger.Active;
            return dto;
        }

    }





}
