﻿using System;
using Civica.Infrastructure.BaseClasses;
using System.Collections.Generic;

namespace Civica.BLL.Models.Alarm
{
    public class AlarmResultModel
    {
        public int AlarmId { get; set; }
        public BaseEnum.AlarmTriggerType AlarmStatus { get; set; }
        public List<AlarmConditionResultModel> ConditionResults {get;set;}
        public string Messages { get; set; }

        public AlarmResultModel(int AlarmId)
        {
            this.AlarmId = AlarmId;
            this.ConditionResults = new List<AlarmConditionResultModel>();
        }
    }

    public class AlarmConditionResultModel
    {
        public DateTimeOffset? CheckFrom { get; set; }
        public DateTimeOffset? CheckTo { get; set; }
        public BaseEnum.AlarmTriggerType ConditionStatus { get; set; }
    }
}
