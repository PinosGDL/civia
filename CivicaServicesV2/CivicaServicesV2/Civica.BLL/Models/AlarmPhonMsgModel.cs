﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.Infrastructure.BaseClasses;

namespace Civica.BLL
{
    public class AlarmPhonMsg
    {
        public int alarmId { get; set; }
        public BaseEnum.AlarmTriggerType trigger { get; set; }
        public string msg;
    }

    public class AlarmEmailMessge
    {
        public int alarmId { get; set; }
        public string msg { get; set; }
        public string alarmServity { get; set; }
        public BaseEnum.AlarmTriggerType trigger { get; set; }
    }
}
