﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.Infrastructure.BaseClasses;
using Civica.BLL.Models;
using Civica.DBL.DBClasses;
using System.Globalization;
using LumenWorks.Framework.IO.Csv;
using System.IO;
using System.Data;

namespace Civica.BLL.CommonClasses
{
    public class CivicaCSV
    {

        private static string[] formats = new string[]
        {
            "yyyy-MM-dd HH:mm:ss"
        };

        public ServiceResult<bool> ExportStationCSVFile(int stationId)
        {

            return null;

        }

        public ServiceResult<MonitoringTaskStartTimeAndEndTimeDTO> ProcessFileData(FileResult fileRes, MonitoringTaskDTO task, StationDTO station, string undopath)
        {
            int lineCount = 0;
            int sensorid = 0;
            var sensorList = new List<int>();
            var processData = new DataProcessing();
            var updateTimestamps = new List<DateTimeOffset>();
            DateTime minTime = DateTime.MaxValue;
            DateTime maxTime = DateTime.MinValue;
            String[] sensorline = null ;
            String firstdataline = String.Empty;

            String line2 = String.Empty;

            var res = new ServiceResult<MonitoringTaskStartTimeAndEndTimeDTO>(new MonitoringTaskStartTimeAndEndTimeDTO());

            while (fileRes.TextStream.Peek() >= 0)  // loop through the file lines
            {
                var ln = fileRes.TextStream.ReadLine();

                if (ln.Substring(0,1) == "!")
                {
                    if (ln.Contains("!Sensorids"))
                    {
                        if (sensorline == null)
                        {
                            line2 = ln;
                            sensorline = ln.Split(',');
                            if (sensorline[0].Trim() != "!Sensorids")
                            {
                                res.ErrorResults.Add("Bad File", "The Sensor line has bad syntax ; [" + sensorline[0] + "]");
                                res.DTO.Passed = false;
                                return res;
                            }
                        }
                        else
                        {
                            res.ErrorResults.Add("Bad File", "There are more than one !Sensor line in the file ");
                            res.DTO.Passed = false;
                            return res;
                        }
                    }
                }
                else
                {
                    firstdataline = fileRes.TextStream.ReadLine();
                    lineCount++;
                    break;
                }
                lineCount++;
            }


            if (firstdataline == String.Empty)
            {
                res.ErrorResults.Add("Bad File", "There are no data lines in the file.");
                res.DTO.Passed = false;
                return res;
            }

            var dataLine = firstdataline.Split(',');

            if (sensorline != null)
            {
                if (sensorline.Length > 1)
                {
                    for (var j = 1; j < sensorline.Length; j++)
                    {
                        if (sensorline[j] == "")
                        {
                            continue;
                        }
                        else if (Int32.TryParse(sensorline[j], out sensorid))
                        {
                            sensorList.Add(sensorid);
                        }
                        else
                        {
                            res.ErrorResults.Add("Bad File", "There are values in the sensor line : [" + line2 + "],  that are not valid sensor Ids.  SensorIds canonly be integers.");
                            res.DTO.Passed = false;
                            return res;
                        }
                    }


                    if(sensorList.Count() == 0)
                    {

                        res.ErrorResults.Add("Bad File", "There are no sensors in the sensor line : [" + line2 + "]");
                        res.DTO.Passed = false;
                        return res;
                    }
                }
                else
                {
                    res.ErrorResults.Add("Bad File", "There are no sensors in the sensor line : [" + line2 + "]");
                    res.DTO.Passed = false;
                    return res;
                }
            }
            else
            {
                res.ErrorResults.Add("Bad File", "There is no sensor line :");
                res.DTO.Passed = false;
                return res;
            }

            var senorListArray = sensorList.ToArray();

            for (int i = 0; i < senorListArray.Length; i++)
            {
                for (int j = i + 1; j < senorListArray.Length; j++)
                {
                    if (senorListArray[i] == senorListArray[j])
                    {
                        res.ErrorResults.Add("Bad File", "There are duplicate entries in the sensor line : [" + line2 + "] and position: [" + i + "] and position [" + j + "]");
                        res.DTO.Passed = false;
                        return res;
                    }                
                }
            }

            if ((sensorList.Count()) * 2 != dataLine.Length)
            {
                if ((dataLine.Length % 2) != 0)
                {
                    if (dataLine[dataLine.Length - 1] != "")
                    {
                        res.ErrorResults.Add("Bad File", "There is mismatch in number of sensor decleared in the sensor line : [" + line2 + "] and the colums in the data line : [" + sensorline + "].  The number of columns is an odd number and the last column is not empty.");
                        res.DTO.Passed = false;
                        return res;
                    }
                }
      
            }


            if (sensorList.Count < 1)
            {
                res.ErrorResults.Add("Bad File", "There are no sensors the sensor line : [" + line2 + "]");
                res.DTO.Passed = false;
                return res;
            }

            var sensors = SensorDBMethods.GetSensorsInLists(sensorList);

            if (sensors.Count() < 1)
            {
                res.ErrorResults.Add("Bad File", "No Matching sensors found in the database for the sensors in the sensor line : [" + line2 + "]");
                res.DTO.Passed = false;
                return res;
            }
            else if (sensors.Count() != sensorList.Count())
            {
                var msg = "There is mismatch in the number of sensors found in the database and the sensors in the sensor line : [" + line2 + "].  Sensors found : ";
                foreach (var s in sensors)
                {
                    msg = msg + s.Id + ", ";
                }
                res.DTO.Passed = false;
                res.ErrorResults.Add("Bad File", msg);
                return res;
            }

            var smapList = new List<SensorMap>();

            foreach (var s in sensors)
            {
                var newsmap = new SensorMap();
                newsmap.Sensor = SensorDTO.Create(s);

                if (task.MonitoringStationId != s.MonitorStationId)
                {
                    res.ErrorResults.Add("Bad File", "The sensor : " + s.Id + " which is in the sensor line : [" + line2 + "], is not a site sensor for Monitoring Station : " + task.MonitoringStationId);
                    res.DTO.Passed = false;
                    return res;
                }

                if (s.Calculated)
                {
                    res.ErrorResults.Add("Bad File", "The sensor : " + s.Id + " which is in the sensor line : [" + line2 + "], is calculated sensor.");
                    res.DTO.Passed = false;
                    return res;
                }

                var sl = sensorList.ToArray();

                for (var j = 1; j < sl.Length ; j++)
                {
                    if (sl[j] == s.Id)
                    {
                        newsmap.Position = j;
                        break;
                    }
                }

                smapList.Add(newsmap);
            }


            double val = 0;
            // We have greate accurate sensor map and can procced to read thefile
            if (smapList.Count > 0)
            {
                //ProcessData The First Line
                foreach (var sen in smapList)
                {
                    SensorData d = new SensorData();

                    if (dataLine.Length < sen.Position * 2 + 2)
                    {
                        continue;
                    }

                    var datestamp = dataLine[sen.Position * 2];
                    var value = dataLine[sen.Position * 2 + 1];


                    if ((datestamp == null || datestamp == String.Empty) && (value == null || value == String.Empty))
                    {
                        continue;
                    }

                    if (!DateTime.TryParseExact(datestamp, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out d.timestamp))
                    {
                        res.ErrorResults.Add("Bad Data", "Bad Date At timestamp:  : " + datestamp + ", line : " + lineCount + ", for sensor :  " + sen.Sensor.Id + ". The only acceptable date format is ( yyyy-MM-dd HH:mm:ss ). Ex. ( 2016-03-23 09:04:00 )");
                        res.DTO.Passed = false;
                        return res;
                    }

                    if ((value == null) || (!double.TryParse(value, out val)))
                    {
                        res.ErrorResults.Add("Bad File", "Null or empty value at line : " + lineCount + " for sensor :  " + sen.Sensor.Id);
                        res.DTO.Passed = false;
                        return res;
                    }
                    if (d.timestamp < minTime)
                    {
                        minTime = d.timestamp;
                    }
                    if (d.timestamp > maxTime)
                    {
                        maxTime = d.timestamp;
                    }
                    var dateOffset = new DateTimeOffset(d.timestamp, station.TimeZoneOffset);

                    sen.table.Rows.Add(sen.Sensor.Id, val, dateOffset, 0);

                    if (task.UploadRaw == true)
                    {
                        sen.tableraw.Rows.Add(sen.Sensor.Id, val, dateOffset);
                    }

                }// End of first data line processing

                string filedataline = String.Empty;

                while (fileRes.TextStream.Peek() >= 0)  // loop through the file lines
                {
                    filedataline = fileRes.TextStream.ReadLine();
                    if (filedataline == null) { break; }
                    lineCount++;
                    dataLine = filedataline.Split(',');

                    foreach (var sen in smapList)
                    {
                        SensorData d = new SensorData();

                        if (dataLine.Length < sen.Position * 2 + 2)
                        {
                            continue;
                        }


                        var datestamp = dataLine[sen.Position * 2];
                        var value = dataLine[sen.Position * 2 + 1];

                      
                        if ((datestamp == null || datestamp == String.Empty) && (value == null || value == String.Empty))
                        {
                            continue;
                        }

                        if ((value == null) || (!double.TryParse(value, out val)))
                        {
                            res.ErrorResults.Add("Bad File", "Null or empty value at line : " + lineCount + " for sensor :  " + sen.Sensor.Id);
                            res.DTO.Passed = false;
                            return res;
                        }


                        if (!DateTime.TryParseExact(datestamp, formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out d.timestamp))
                        {
                            res.ErrorResults.Add("Bad Data", "Bad Date At timestamp:  : " + datestamp + ", line : " + lineCount + ", for sensor :  " + sen.Sensor.Id + ". The only acceptable date format is ( yyyy-MM-dd HH:mm:ss ). Ex. ( 2016-03-23 09:04:00 )");

                            res.DTO.Passed = false;
                            return res;
                        }
                        if (d.timestamp < minTime)
                        {
                            minTime = d.timestamp;
                        }
                        if (d.timestamp > maxTime)
                        {
                            maxTime = d.timestamp;
                        }

                        var dateOffset = new DateTimeOffset(d.timestamp, station.TimeZoneOffset);

                        sen.table.Rows.Add(sen.Sensor.Id, val, dateOffset, 0);
                        if (task.UploadRaw == true)
                        {
                            sen.tableraw.Rows.Add(sen.Sensor.Id, val, dateOffset);
                        }
                    }
                }
            } //  End of looping through the file and producing file maps with data

            fileRes.TextStream.Close();

            DataTable selected = new DataTable();
            var prime1 = selected.Columns.Add("TimeStamp", typeof(DateTimeOffset));
            selected.PrimaryKey = new DataColumn[] { prime1 };

            foreach (var sm in smapList)
            {
                

                DataView view = new System.Data.DataView(sm.table);
                var tg = view.ToTable("Selected", false, "TimeStamp");
                var duplicates = tg.AsEnumerable().GroupBy(r1 => r1[0]).Where(gr => gr.Count() > 1).ToList();

                if (duplicates != null)
                {

                    if (duplicates.Count()  > 0)
                    {

                        var duplicatesList = String.Empty;
                        int z = 0;

                        foreach (var d in duplicates)
                        {
                            if (z > 40)
                            {
                                break;
                            }

                            duplicatesList = duplicatesList + d.FirstOrDefault()[0].ToString() + " ;";

                            z++;

                        }



                        res.ErrorResults.Add("Duplicate Timestamps in file", "There are duplicate timestamps in the file:  : " + duplicatesList);
                        res.DTO.Passed = false;
                        return res;

                    }


                }


                selected.Merge(tg);
            }
            res.DTO.StartTime = minTime;
            res.DTO.EndTime = maxTime;
            var r  = (from row in selected.AsEnumerable() select row["TimeStamp"]).ToList();
            updateTimestamps = r.Cast<DateTimeOffset>().ToList();
            var ts = updateTimestamps.ToArray();

            var undomap = new List<UndoMap>();

            foreach (var s in smapList)
            {
                var undos = new UndoMap();
                undos.Sensor = s.Sensor;
                List<DateTimeOffset> levels = s.table.AsEnumerable().Select(al => al.Field<DateTimeOffset>("TimeStamp")).Distinct().ToList();
                undos.BeginTimeStamp = levels.Min();
                undos.EndTimeStamp = levels.Max();
                undomap.Add(undos);
            }

            var msensorupdate = processData.BulckUploadPhysicalSensors(smapList);
            var m = String.Empty;
            if (!msensorupdate.DTO)
            {
                  m = String.Empty;
                foreach (var s in msensorupdate.ErrorResults)
                {
                    m = m + "Key : " + s.Key + " , Value : " + s.Value + ", ";
                }
                res.ErrorResults.Add("Could Not Import Data", "Problem with Builk Import : " + m );
                res.DTO.Passed = false;
                return res;
            }

            var calculatedSensorsMap = new CalculatedSensorMap();
            foreach (var sen in smapList)
            {
                calculatedSensorsMap.Sensors.Add(sen.Sensor);
            }

            calculatedSensorsMap.TimeSeries = updateTimestamps;
            var calsResult = processData.BulckUploadCalculatedsensors(calculatedSensorsMap);

            if (!calsResult.DTO)
            {
                 m = String.Empty;
                foreach (var s in calsResult.ErrorResults)
                {
                    m = m + "Key : " + s.Key + " , Value : " + s.Value + ", ";
                }
                res.ErrorResults.Add("Could Not Import Data", "Problem with Builk Import Calculated Sensors : " + m);
                res.DTO.Passed = false;
            }

            m = String.Empty;
            if(calsResult.ServiceLogs.Count > 0)
            { 
                foreach (var l in calsResult.ServiceLogs)
                {
                    m = m + "Key : " + l.Key + " , Value : " + l.Value + ", ";
                }
                res.ServiceLogs.Add("Warnings while Importing", "There were problems while importing data : " + m);
            }

            var updatedStation = SensorDBMethods.RecalculateStationLastTimeStamp(station.Id);

            return res;

        }

    }
}
