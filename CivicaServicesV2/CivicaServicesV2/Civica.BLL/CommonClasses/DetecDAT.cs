﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Civica.Infrastructure.BaseClasses;
using Civica.BLL.Models;
using System.Data;
using Civica.DBL.DBClasses;

namespace Civica.BLL.CommonClasses
{
    public class DetecDAT
    {

        public DetecDAT()
        {
            this.Data = new List<SensorData>();
            this.CurrentCursorPosition = 0;
            this.EndOfFile = false;

        }


        public int Rate { get; private set; }
        public int SampleSize { get; private set; }
        public int Strategy { get; private set; }
        public long NumSamples { get; private set; }
        public float Range { get; private set; }
        public float Offset { get; private set; }
        public String Measureand { get; private set; }
        public String Units { get; private set; }
        public String Name { get; private set; }
        public DateTime StartDate { get; private set; }
        public int CurrentCursorPosition { get; private set; }
        public List<SensorData> Data { get; set; }
        public bool EndOfFile { get; set; }
        private FileStream DataFile { get; set; }


        public DateTime GetMaxDateInFile()
        {
            //rate in minutes = rate / 600
            // so rate is measured in 10ths of seconds. 
            return ReadBaseTime().DTO.AddMinutes(Rate * NumSamples / 600);
        }


        public ServiceResult<MonitoringTaskStartTimeAndEndTimeDTO> ProcessFileData( MonitoringTaskDTO task, StationDTO station, string undopath)
        {

            var sensorList = new List<int>();
            var processData = new DataProcessing();
            var updateTimestamps = new List<DateTimeOffset>();
            String firstdataline = String.Empty;
            DateTime minTime = DateTime.MaxValue;
            DateTime maxTime = DateTime.MinValue;
            String line2 = String.Empty;
            var sensors = new List<SensorDTO>();
            var res = new ServiceResult<MonitoringTaskStartTimeAndEndTimeDTO>(new MonitoringTaskStartTimeAndEndTimeDTO());
            var smapList = new List<SensorMap>();

            string filechanel = String.Empty;
            var filename = task.OriginalPath;

            var c = filename.LastIndexOf("(");
            var chanel = filename.Remove(0, c + 1);
            c = chanel.IndexOf(")");
            filechanel = chanel.Substring(0, c);

            if (filechanel == null) // the first type of file name is not
            {
                res.ErrorResults.Add("Can not pars the Chanel", "The File Name : " + task.OriginalPath + " does not specify the chanel name correctly.  The two supported format are : ZZZ_02.dat or zzz(02).dat");
                res.DTO.Passed = false;
                return res;

            }

            switch (filechanel)
            {
                case "01":
                    sensors = station.Sensors.Where(x => x.CurrentEquation.LoggerChannel.Channel == "DTC1" && x.CurrentEquation != null).ToList();
                    break;
                case "02":
                    sensors = station.Sensors.Where(x => (x.CurrentEquation.LoggerChannel.Channel == "DTC2" || x.CurrentEquation.LoggerChannel.Channel == "DTCU" || x.CurrentEquation.LoggerChannel.Channel == "DTCR") && x.CurrentEquation != null).ToList();
                    break;
                case "03":
                    sensors = station.Sensors.Where(x => x.CurrentEquation.LoggerChannel.Channel == "DTC3" && x.CurrentEquation != null).ToList();
                    break;
                case "04":
                    sensors = station.Sensors.Where(x => x.CurrentEquation.LoggerChannel.Channel == "DTC4" && x.CurrentEquation != null).ToList();
                    break;
                case "05":
                    sensors = station.Sensors.Where(x => x.CurrentEquation.LoggerChannel.Channel == "DTC5" && x.CurrentEquation != null).ToList();
                    break;
                case "06":
                    sensors = station.Sensors.Where(x => (x.CurrentEquation.LoggerChannel.Channel == "DTC6" || x.CurrentEquation.LoggerChannel.Channel == "DTCD") && x.CurrentEquation != null).ToList();
                    break;
                case "07":
                    sensors = station.Sensors.Where(x => (x.CurrentEquation.LoggerChannel.Channel == "DTC7" || x.CurrentEquation.LoggerChannel.Channel == "DTCV") && x.CurrentEquation != null).ToList();
                    break;
                case "08":
                    sensors = station.Sensors.Where(x => (x.CurrentEquation.LoggerChannel.Channel == "DTC8" || x.CurrentEquation.LoggerChannel.Channel == "DTCB") && x.CurrentEquation != null).ToList();
                    break;
                default:
                    break;
            }

            if (sensors != null)
            {
                if (sensors.Count() > 0)
                {
                    var smaps = ParseFile(sensors, task.FilePath, station.TimeZoneOffset, ref updateTimestamps, ref minTime, ref maxTime);
                    if (smaps != null)
                    {
                        foreach (var newsmap in smaps)
                        {
                            smapList.Add(newsmap);
                        }
                    }
                }
            }
            else
            {
                res.ErrorResults.Add("Station Set Up Error", "The Station : " + station.Name + " does not have a sensor set with a proper equation for this file. ");
                res.DTO.Passed = false;
                return res;
            }
            res.DTO.StartTime = minTime;
            res.DTO.EndTime = maxTime;
            DataTable selected = new DataTable();
            var prime1 = selected.Columns.Add("TimeStamp", typeof(DateTimeOffset));
            selected.PrimaryKey = new DataColumn[] { prime1 };

            foreach (var sm in smapList)
            {
                DataView view = new System.Data.DataView(sm.table);
                var tg = view.ToTable("Selected", false, "TimeStamp");
                var duplicates = tg.AsEnumerable().GroupBy(r1 => r1[0]).Where(gr => gr.Count() > 1).ToList();

                if (duplicates != null)
                {
                    if (duplicates.Count() > 0)
                    {
                        var duplicatesList = String.Empty;
                        int z = 0;

                        foreach (var d in duplicates)
                        {
                            if (z > 40)
                            {
                                break;
                            }

                            duplicatesList = duplicatesList + d.FirstOrDefault()[0].ToString() + " ;";
                            z++;
                        }

                        res.ErrorResults.Add("Duplicate Timestamps in file", "There are duplicate timestamps in the file:  : " + duplicatesList);
                        res.DTO.Passed = false;
                        return res;
                    }
                }
                selected.Merge(tg);
            }

            var r = (from row in selected.AsEnumerable() select row["TimeStamp"]).ToList();
            updateTimestamps = r.Cast<DateTimeOffset>().ToList();
            var ts = updateTimestamps.ToArray();

            var undomap = new List<UndoMap>();

            foreach( var s in smapList)
            {
                var undos = new UndoMap();
                undos.Sensor = s.Sensor;

                var p = s.table.Rows.Count;

                if (p > 0)
                {
                    List<DateTimeOffset> levels = s.table.AsEnumerable().Select(al => al.Field<DateTimeOffset>("TimeStamp")).Distinct().ToList();
                    undos.BeginTimeStamp = levels.Min();
                    undos.EndTimeStamp = levels.Max();
                    undomap.Add(undos);
                } 
            }

            var msensorupdate = processData.BulckUploadPhysicalSensors(smapList);
            var m = String.Empty;
            if (!msensorupdate.DTO)
            {
                m = String.Empty;
                foreach (var s in msensorupdate.ErrorResults)
                {
                    m = m + "Key : " + s.Key + " , Value : " + s.Value + ", ";
                }
                res.ErrorResults.Add("Could Not Import Data", "Problem with Builk Import : " + m);
                res.DTO.Passed = false;
                return res;
            }

            var calculatedSensorsMap = new CalculatedSensorMap();
            foreach (var sen in smapList)
            {
                calculatedSensorsMap.Sensors.Add(sen.Sensor);
            }
            //  updateTimestamps.Sort();
            calculatedSensorsMap.TimeSeries = updateTimestamps;
            var calsResult = processData.BulckUploadCalculatedsensors(calculatedSensorsMap);


            if (!calsResult.DTO)
            {
                m = String.Empty;
                foreach (var s in calsResult.ErrorResults)
                {
                    m = m + "Key : " + s.Key + " , Value : " + s.Value + ", ";
                }
                res.ErrorResults.Add("Could Not Import Data", "Problem with Builk Import Calculated Sensors : " + m);
                res.DTO.Passed = false;
            }

            m = String.Empty;
            if (calsResult.ServiceLogs.Count > 0)
            {
                foreach (var l in calsResult.ServiceLogs)
                {
                    m = m + "Key : " + l.Key + " , Value : " + l.Value + ", ";
                }
                res.ServiceLogs.Add("Warnings while Importing", "There were problems while importing data : " + m);
            }

            var updatedStation = SensorDBMethods.RecalculateStationLastTimeStamp(station.Id);

            return res;

        }


        public List<SensorMap> ParseFile(List<SensorDTO> sensors, string filepath, TimeSpan timeZoneOffset, ref List<DateTimeOffset> timestamps, ref DateTime minTime, ref DateTime maxTime)
        {
            var sensormaps = new List<SensorMap>();
            var processData = new DataProcessing();
           
            if (sensors != null)
            {
                if (sensors.Count() > 0)
                {
                    var dfile = new DetecDAT();
                    var res = dfile.Load(filepath);

                    if(!res.IsSuccess)
                    {
                        return sensormaps;
                    }

                    foreach (var s in sensors)
                    {
                        var sm = new SensorMap();
                        sm.Chanel = s.CurrentEquation.LoggerChannel.Channel;
                        sm.Sensor = s;
                        sensormaps.Add(sm);
                    }

                    foreach (var sm in sensormaps)
                    {

                        var Lasttimestamp = SensorDBMethods.GetLastSensorTime(sm.Sensor.Id);


                        if (Lasttimestamp.DTO != null)
                        {
                            var dt = (DateTimeOffset)Lasttimestamp.DTO;
                            var lt = dt.ToOffset(timeZoneOffset).DateTime;
                              dfile.SetCurrentCursorPosition(lt);
                        }


                        while (!dfile.EndOfFile)
                        {
                            var data = dfile.GetNext();

                            if (data != null)
                            {
                                if (data.timestamp < minTime)
                                {
                                    minTime = data.timestamp;
                                }
                                if (data.timestamp > maxTime)
                                {
                                    maxTime = data.timestamp;
                                }
                                var dateOffset = new DateTimeOffset(data.timestamp, timeZoneOffset);
                                timestamps.Add(dateOffset);

                                var calculated = processData.EvaluateMSensor(sm.Sensor.CurrentEquation.Equation, sm.Sensor.CurrentEquation.LoggerChannel.Channel, data.value);
                                if (calculated != null)
                                {
                                    data.value = (double)calculated;
                                }

                                sm.table.Rows.Add(sm.Sensor.Id, data.value, dateOffset, 0);

                            }

                        }
                    }
                    

                    dfile.DataFile.Close();
                }
            }

            return sensormaps;

        }


        public ServiceResult<bool> Load(String Path)
        {
            var res = new ServiceResult<bool>(true);

            try
            {
                this.DataFile = File.OpenRead(Path);
                this.Name = DataFile.Name;


                var r = ReadBaseTime();
                if (!r.IsSuccess)
                {
                    foreach(var er in r.ErrorResults)
                    {
                        res.AddError(er.Key, er.Value);
                    }
                    foreach (var er in r.ServiceLogs)
                    {
                        res.AddServiceLog(er.Key, er.Value);
                    }
                    return res;
                }

                this.StartDate = r.DTO;
                byte[] buffer;

                buffer = new byte[10];
                DataFile.Position = 60;
                DataFile.Read(buffer, 0, 10);
                this.Units = System.Text.Encoding.ASCII.GetString(buffer);

                buffer = new byte[10];
                DataFile.Position = 45;
                DataFile.Read(buffer, 0, 10);
                this.Measureand = System.Text.Encoding.ASCII.GetString(buffer);

                DataFile.Position = 30;
                DataFile.Read(buffer, 0, 1);
                this.Strategy = (int)buffer[0];

                if (Strategy == 8 || Strategy == 10)
                {
                    this.SampleSize = 2;
                }
                else if (Strategy == 2)
                {
                    this.SampleSize = 1;
                }
                DataFile.Position = 43;
                buffer = new byte[2];
                DataFile.Read(buffer, 0, 2);

                this.Rate = buffer[1] << 8 | buffer[0];

                DataFile.Position = 70;
                buffer = new byte[4];
                DataFile.Read(buffer, 0, 4);

                this.Range = System.BitConverter.ToSingle(buffer, 0);

                buffer = new byte[4];
                DataFile.Read(buffer, 0, 4);

                this.Offset = System.BitConverter.ToSingle(buffer, 0);

                this.NumSamples = (DataFile.Length - 78) / SampleSize;
            }
            catch(Exception ex)
            {
                res.AddError("System Error", ex.Message);
            }

            return res;
        }

        public void SetCurrentCursorPosition(DateTime lastTimeStamp)
        {
            if (StartDate < lastTimeStamp)
            {
                var dateinterval = lastTimeStamp - StartDate;
                var timeintervals = (decimal)dateinterval.TotalMinutes / ((decimal)Rate / (decimal)600);
                this.CurrentCursorPosition = (int) Math.Round(timeintervals,0);
            }
        }

        public SensorData GetNext()
        {
            if(CurrentCursorPosition >= NumSamples ) 
            {
                this.EndOfFile = true;
                return null;
            }

            var sdata = new SensorData();
            CurrentCursorPosition++;

            sdata = GetSample(CurrentCursorPosition);

            return sdata;
        }

        private SensorData GetSample(long index)
        {
            SensorData Result = new SensorData();
            long numTenthSeconds = Rate * index;
            byte[] buffer;
            buffer = new byte[SampleSize];

            Result.timestamp = StartDate.AddMilliseconds(numTenthSeconds * 100);
            //round to 5 seconds
            DateTime rounded = new DateTime(((Result.timestamp.Ticks + 25000000) / 50000000) * 50000000);
            Result.timestamp = rounded;

            DataFile.Position = 78 + SampleSize * index;
            DataFile.Read(buffer, 0, SampleSize);

            if (SampleSize == 2)
            {
                float value = buffer[1] << 8 | buffer[0];

                if (value == 32767)
                {
                    Result.value = 0;
                }
                else
                {
                    Result.value = (Range / 32766 * value) + Offset;
                }
            }
            if (SampleSize == 1)
            {
                float value = buffer[0];

                if (value == 255)
                {
                    Result.value = 0;
                }
                else
                {
                    Result.value = (Range / 254 * value) + Offset;
                }
            }
            Result.value = (float)Math.Round(Result.value, 4);
            return Result;
        }

        private ServiceResult<DateTime> ReadBaseTime()
        {
            //DateTime Result;

            var dto = new DateTime();
            var Result = new ServiceResult<DateTime>(dto);

            byte[] buffer;

            DataFile.Position = 31;
            int[] rawDate = new int[6];

            for (int i = 0; i < 6; i++)
            {
                buffer = new byte[2];
                DataFile.Read(buffer, 0, 2);
                rawDate[i] = buffer[1] << 8 | buffer[0];
            }

            if (rawDate[0] < 1 || rawDate[0] > 9999)
            {
                Result.AddError("Detect File Error:", "Year is out of range. Year must be 1 - 9999");
            }

            if (rawDate[1] < 1 || rawDate[1] > 12)
            {
                Result.AddError("Detect File Error:", "Month is out of range. Month must be 1 - 12");
            }

            if (rawDate[2] < 1 || rawDate[2] > 31)
            {
                Result.AddError("Detect File Error:", "Month is out of range. Month must be 1 - day of the month");
            }

            if (rawDate[3] < 0 || rawDate[3] > 23)
            {
                Result.AddError("Detect File Error:", "Hour is out of range. Hour must be 0 - 23");
            }

            if (rawDate[4] < 0 || rawDate[4] > 59)
            {
                Result.AddError("Detect File Error:", "Minutes is out of range. Minutes must be 0 - 59");
            }

            if (rawDate[5] < 0 || rawDate[5] > 59)
            {
                Result.AddError("Detect File Error:", "Seconds is out of range. Month must be 0 - 59");
            }

            if (Result.ErrorResults.Count() == 0)
            {  
                Result.DTO = new DateTime(rawDate[0], rawDate[1], rawDate[2], rawDate[3], rawDate[4], rawDate[5], DateTimeKind.Utc);
            }

            return Result;
        }

    }
}

