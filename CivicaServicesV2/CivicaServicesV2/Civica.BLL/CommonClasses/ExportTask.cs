﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.Infrastructure.BaseClasses;
using Civica.DBL.DBClasses;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Configuration;
using System.IO.Compression;
using Civica.Domain.CivicaDomain;

namespace Civica.BLL.CommonClasses
{
    public class ExportTask
    {

        public static ServiceResult<bool> ExportMonitoringSerivceData(int taskId, int formatId, int? MonitoringTaskExportDataTypeId, int serviceId, DateTimeOffset? startTime, DateTimeOffset? endTime)
        {
            var result = new ServiceResult<bool>(true);

            try
            {
                Dictionary<string, StringWriter> stringList = new Dictionary<string, StringWriter>();
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(
              ConfigurationManager.AppSettings["StorageConnectionString"]);
                CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
                // Retrieve a reference to a container.
                CloudBlobContainer container = blobClient.GetContainerReference("datacurrentfilestorage");
                container.CreateIfNotExists();
                var exportSensor = MonitoringTaskDBMethods.GetExportSensorsByTaskId(taskId);
                var serviceName = MonitoringServiceDBMethod.GetServiceName(serviceId);
                if (exportSensor.Count > 0)
                {
                    var exportSensorGroupByStation = exportSensor.GroupBy(x => x.MonitorStationId).ToDictionary(g => g.Key, g => g.ToList());
                    foreach (KeyValuePair<int, List<MonitoringSensor>> entry in exportSensorGroupByStation)
                    {
                        var sw = new StringWriter();
                        var stationId = entry.Key;

                        var stationTimeZoneOffset = StationDBMethods.GetStationTimeZoneSpan(stationId);
                        var stationName = StationDBMethods.GetStationById(stationId).Name;
                        var sensors = entry.Value;
                        var stationSensorData = new Dictionary<DateTimeOffset, Decimal?[]>();
                        if (formatId == (int)Civica.Infrastructure.BaseClasses.BaseEnum.MonitoringTaskFormat.CustomerFormat1)
                        {
                            stationSensorData = StationDBMethods.GetStationSensorData(sensors, MonitoringTaskExportDataTypeId.Value, startTime, endTime);
                            if (stationSensorData.Count > 0)
                            {
                                foreach (KeyValuePair<DateTimeOffset, Decimal?[]> sensorDataentry in stationSensorData)
                                {
                                    DateTime dateTime = sensorDataentry.Key.ToOffset(stationTimeZoneOffset).DateTime;
                                    var values = sensorDataentry.Value;
                                    sw.Write(string.Format("{0}", dateTime.ToString("yyyy-MM-dd HH:mm")));
                                    for (var i = 0; i < values.Length; i++)
                                    {
                                        if (values[i] == null)
                                        {
                                            sw.Write(",");
                                        }
                                        else
                                        {
                                            sw.Write(string.Format(",{0}", values[i]));
                                        }
                                    }
                                    sw.WriteLine();
                                }
                                var fileName = stationName + " ( " + stationId + " ).CSV";
                                stringList.Add(fileName, sw);
                            }

                        }
                        else if (formatId == (int)Civica.Infrastructure.BaseClasses.BaseEnum.MonitoringTaskFormat.DefaultExport)
                        {
                            var stationSensorDatatime = StationDBMethods.GetDefaultFormatStationSensorData(sensors, MonitoringTaskExportDataTypeId.Value, startTime, endTime, stationTimeZoneOffset);
                            if (stationSensorDatatime.Count > 0)
                            {
                                sw.WriteLine("!DataCurrent" + "," + "  Downloaded CSV file from A DataCurrent Website.");
                                sw.Write("!Sensorids");
                                foreach (var sensor in sensors)
                                {
                                    var sensorType = sensor.SensorTypeId;
                                    var sensorDataType = sensor.SensorDataTypeId;
                                    if (sensorType == 2 && sensorDataType == 1)
                                    {
                                        sw.Write(String.Format(",Calculated Precipitation ({0})", sensor.Id));
                                    }
                                    else
                                    {
                                        sw.Write(String.Format(",{0}", sensor.Id));
                                    }
                                }
                                sw.WriteLine();
                                sw.Write("!SensorNames");
                                foreach (var sensor in sensors)
                                {
                                    String unit = "";
                                    if (sensor.Unit1 != null)
                                    {
                                        unit = sensor.Unit1.UnitName;
                                    }
                                    else
                                        unit = sensor.Unit;
                                    sw.Write(String.Format(",{0}", sensor.Name + " (" + unit + ")"));
                                }
                                sw.WriteLine();
                                foreach (KeyValuePair<DateTime, Decimal?[]> sensorDataentry in stationSensorDatatime)
                                {
                                    DateTime dateTime = sensorDataentry.Key;
                                    var values = sensorDataentry.Value;
                                    sw.Write(string.Format("{0}", dateTime.ToString("yyyy-MM-dd HH:mm")));
                                    for (var i = 0; i < values.Length; i++)
                                    {
                                        if (values[i] == null)
                                        {
                                            sw.Write(",");
                                        }
                                        else
                                        {
                                            sw.Write(string.Format(",{0}", values[i]));
                                        }
                                    }
                                    sw.WriteLine();
                                }
                                var fileName = stationName + " ( " + stationId + " ).CSV";
                                stringList.Add(fileName, sw);
                            }
                        }
                         
                    }
                }
                using (var memoryStream = new MemoryStream())
                {

                    using (var archive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                    {
                        foreach (var item in stringList)
                        {
                            var file = archive.CreateEntry(item.Key);
                            using (var entryStream = file.Open())
                            using (var streamWriter = new StreamWriter(entryStream))
                            {
                                streamWriter.Write(item.Value.ToString());
                            }
                        }
                    }
                    memoryStream.Seek(0, SeekOrigin.Begin);
                    CloudBlockBlob blockblob = container.GetBlockBlobReference(serviceName + " ( " + serviceId + " ).zip");
                    blockblob.Properties.ContentType = "zip";
                    blockblob.UploadFromStream(memoryStream);
                }

            }
            catch (Exception e)
            {
                result.AddError("Error: ", e.Message);
                result.DTO = false;
            }

            return result;
        }
    }
}
