﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civica.DBL.SPModels
{
    public class SensorType
    {
        public String SensorTypeName { get; set; }
        public int SensorTypeId { get; set; }

        public static List<SensorType> GetDefaultSensorTypeOrder()
        {
            List<SensorType> DefaultSensorTypeList = new List<SensorType>();
            SensorType PrecipationSensorType = new SensorType()
            {
                SensorTypeId = 2,
                SensorTypeName = "Precipitation",
            };
            DefaultSensorTypeList.Add(PrecipationSensorType);
            SensorType LevelSensorType = new SensorType()
            {
                SensorTypeId = 1,
                SensorTypeName = "Level",
            };
            DefaultSensorTypeList.Add(LevelSensorType);
            SensorType VelocitySensorType = new SensorType()
            {
                SensorTypeId = 3,
                SensorTypeName = "Velocity",
            };
            DefaultSensorTypeList.Add(VelocitySensorType);
            SensorType FlowSensorType = new SensorType()
            {
                SensorTypeId = 4,
                SensorTypeName = "Flow",
            };
            DefaultSensorTypeList.Add(FlowSensorType);
            return DefaultSensorTypeList;
        }
    }

}
