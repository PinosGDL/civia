﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civica.DBL.SPModels
{
    public class MonitoringSensorEquationSP
    {
      public int sensorId { get; set; }
      public DateTimeOffset ValidUntil { get; set; }
      public string Equation { get; set; }
    }
}
