﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civica.DBL.SPModels
{
    public class MonitoringDataSP
    {
        public DateTime TimeStamp;
        public decimal Value;
    }
}
