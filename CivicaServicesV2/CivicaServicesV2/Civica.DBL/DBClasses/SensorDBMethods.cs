﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.Domain.CivicaDomain;
using Civica.DBL.SPModels;
using System.Data;
using System.Data.SqlClient;
using Civica.Infrastructure.BaseClasses;
using Civica.Domain.TelogDomain;

namespace Civica.DBL.DBClasses
{

    public class SensorDBMethods
    {

        public static SensorDataOffsets[] GetSensorDataRange(int sensorsId, DateTimeOffset fromts, DateTimeOffset tots)
        {
            var ccf = new datacurrentEntities();
            return ccf.MonitoringDatas.AsQueryable().Where(x => x.MonitoringSensorId == sensorsId && x.TimeStamp >= fromts && x.TimeStamp <= tots).Select(x => new SensorDataOffsets() { value = (double)x.Value, timestamp = x.TimeStamp }).ToArray();
        }


        public static List<MonitoringSensor> GetSensorsInLists(List<int> sensorsId)
        {
            var ccf = new datacurrentEntities();
            return ccf.MonitoringSensors.AsQueryable().Where(x => x.Active == true).Where(x => sensorsId.Contains(x.Id)).ToList();
        }

        public static  MonitoringSensor  GetSensorsBYIds( int sensorsId)
        {
            var ccf = new datacurrentEntities();
            return ccf.MonitoringSensors.AsQueryable().Where(x => x.Active == true).Where(x =>  x.Id == sensorsId).FirstOrDefault();
        }


        public static List<DateTimeOffset> GetSensorTimeRange(int sensorsId, DateTimeOffset fromts, DateTimeOffset tots)
        {
            var ccf = new datacurrentEntities();
            return ccf.MonitoringDatas.AsQueryable().Where(x => x.MonitoringSensorId == sensorsId && x.TimeStamp >= fromts && x.TimeStamp <= tots ).Select(x => x.TimeStamp).ToList();
        }




        public static List<MonitoringSensor> GetSensorDependencies(int sensorId)
        {
            var ccf = new datacurrentEntities();
            return ccf.MonitoringSensorCalculatedDependencies.AsQueryable().Where(x => x.Active == true).Where(x => x.DependsOnSensorId == sensorId).Select(x => x.MonitoringSensor).Distinct().ToList();
        }

        public static List<int> GetSensorParents(int sensorId)
        {
            var ccf = new datacurrentEntities();
            return ccf.MonitoringSensorCalculatedDependencies.AsQueryable().Where(x => x.Active == true).Where(x => x.CalculatedSensorId == sensorId).Select(x => x.DependsOnSensorId).Distinct().ToList();
        }

        public static  ServiceResult<bool> DeleteTimeRange(int sensorId, DateTimeOffset start, DateTimeOffset end)
        {

            var res = new ServiceResult<bool>(true);
            var ccf = new datacurrentEntities();
            try
            {
               var    resultList = ccf.MonitoringDatas.Where(c => c.MonitoringSensorId == sensorId && c.TimeStamp >= start && c.TimeStamp <= end).ToList();
                ccf.MonitoringDatas.RemoveRange(resultList);
                ccf.SaveChanges();
            }
            catch (Exception ex)
            {

                res.DTO = false;
                res.AddError("System Error", ex.Message);
                return res;
            }


            return res;
        }

        public static ServiceResult<bool> DeleteTimeRangeRaw(int sensorId, DateTimeOffset start, DateTimeOffset end)
        {
            var res = new ServiceResult<bool>(true);
            var ccf = new datacurrentEntities();
            try
            {
               var resultList = ccf.MonitoringDataRaws.Where(c => c.MonitoringSensorId == sensorId && c.Timestamp >= start && c.Timestamp <= end).ToList();
                ccf.MonitoringDataRaws.RemoveRange(resultList);
                ccf.SaveChanges();
            }
            catch (Exception ex)
            {
                res.DTO = false;
                res.AddError("System Error", ex.Message);
                return res;
            }

            return res;
        }

        public static ServiceResult<bool> UpdateStationLastTimeStamp(int stationId, DateTimeOffset lastTimestamp)
        {
            var res = new ServiceResult<bool>(true);
            var ccf = new datacurrentEntities();
            try
            {

                var station = ccf.MonitoringStations.Where(c => c.Id == stationId).FirstOrDefault();

                station.LastTimeStamp = lastTimestamp;
                station.LastTimeStampAdded = DateTimeOffset.Now.ToUniversalTime();
                ccf.SaveChanges();
            }
            catch (Exception ex)
            {
                res.DTO = false;
                res.AddError("System Error", ex.Message);
                return res;
            }


            return res;
        }



        public static ServiceResult<bool> RecalculateStationLastTimeStamp(int stationId)
        {
            var res = new ServiceResult<bool>(true);
            var ccf = new datacurrentEntities();
            try
            {

                var station = ccf.MonitoringStations.Where(c => c.Id == stationId).FirstOrDefault();

                if(station != null)
                {
                    var sensorIds = ccf.MonitoringSensors.Where(x => x.MonitorStationId == stationId).Select(x => x.Id).ToArray();
                    var lastTimestampRecord = ccf.MonitoringDatas.Where(c => sensorIds.Contains(c.MonitoringSensorId)).OrderByDescending(x => x.TimeStamp).FirstOrDefault();
                    station.LastTimeStamp = lastTimestampRecord.TimeStamp;
                    station.LastTimeStampAdded = DateTimeOffset.Now.ToUniversalTime();
                    ccf.SaveChanges();
                }
             
            }
            catch (Exception ex)
            {
                res.DTO = false;
                res.AddError("System Error", ex.Message);
                return res;
            }


            return res;
        }

        public static List<MonitoringSensorEquationSP> GetMonitoringSensorEquations(int sensorId)
        {
            var ccf = new datacurrentEntities();
            var sensorEquations = ccf.MonitoringSensorEquations.Where(x => x.SensorId == sensorId && x.Active == true).ToList();
            var sensorEquationSPList = new List<MonitoringSensorEquationSP>();
            foreach (var sensorEquation in sensorEquations)
            {
                var monitoringSensorEquationsp = new MonitoringSensorEquationSP();
                monitoringSensorEquationsp.sensorId = sensorEquation.SensorId;
                monitoringSensorEquationsp.ValidUntil = sensorEquation.ValidUntil == null ? DateTimeOffset.MaxValue : sensorEquation.ValidUntil.Value;
                monitoringSensorEquationsp.Equation = sensorEquation.Equation;
                sensorEquationSPList.Add(monitoringSensorEquationsp);
            }
            return sensorEquationSPList;
        }


        public static List<SensorDataSP> GetParentSensorData(List<int> sensorIds, List<DateTimeOffset> timeblock)
        {
            var ccf = new datacurrentEntities();
            List<SqlParameter> parameters = new List<SqlParameter>();
            List<SensorDataSP> res = new List<SensorDataSP>();
            var sensorTable = new DataTable();
            sensorTable.Columns.Add("id", typeof(int));
            foreach (var ids in sensorIds)
            {
                sensorTable.Rows.Add(ids);
            }

            var timeStampsTable = new DataTable();
            timeStampsTable.Columns.Add("TimeStamp", typeof(DateTimeOffset));
            foreach (var ts in timeblock)
            {
                timeStampsTable.Rows.Add(ts);
            }


            var idParam0 = new SqlParameter("ParentSensors", SqlDbType.Structured);
            idParam0.TypeName = "dbo.IntegerList";
            idParam0.Value = sensorTable;

            parameters.Add(idParam0);

            var idParam1 = new SqlParameter("TimeInterval", SqlDbType.Structured);
            idParam1.TypeName = "dbo.TimeStampList";
            idParam1.Value = timeStampsTable;

            parameters.Add(idParam1);

            try
            {
                var r = ccf.Database.SqlQuery<SensorDataSP>("EXEC SP_MonitoringDataParentTimeBlocks @ParentSensors, @TimeInterval", parameters.ToArray()).ToList();

                if (r != null)
                {
                    if (r.Count() > 0)
                    {
                        res = r.ToList();
                        parameters.Clear();
                        return res;
                    }
                }

                parameters.Clear();
                return null;
            }
            catch (Exception ex)
            {
                parameters.Clear();
                return null;
            }

            //parameters.Clear();
            //return res;
        }

        public static List<SensorDataWithReplacmentStringSP> GetParentDelayedSensorData(List<DelayedSensorWithOffset> sensors, List<DateTimeOffset> timeblock)
        {
            var ccf = new datacurrentEntities();
            List<SensorDataWithReplacmentStringSP> res = new List<SensorDataWithReplacmentStringSP>();
            var sensorTable = new DataTable();
            sensorTable.Columns.Add("Id", typeof(int));
            sensorTable.Columns.Add("Offset", typeof(double));
            foreach (var ids in sensors)
            {
                sensorTable.Rows.Add(ids.Id, ids.Offset);
            }

            var timeStampsTable = new DataTable();
            timeStampsTable.Columns.Add("TimeStamp", typeof(DateTimeOffset));
            foreach (var ts in timeblock)
            {
                timeStampsTable.Rows.Add(ts);
            }

            try
            {
                res = ccf.Database.SqlQuery<SensorDataWithReplacmentStringSP>("EXEC SP_MonitoringDataParentTimeBlocks {0},{1}", sensorTable, timeStampsTable).ToList();

            }
            catch (Exception ex)
            {

                return null;
            }


            return res;
        }



        public static ServiceResult<DateTimeOffset?> GetLastSensorTime(int id)
        {
            var dtof = new DateTimeOffset?();
            var res = new ServiceResult<DateTimeOffset?>(dtof);
            var ccf = new datacurrentEntities();

            try
            {
                var lastentry = ccf.MonitoringDatas.Where(x => x.MonitoringSensorId == id).OrderByDescending(x => x.TimeStamp).FirstOrDefault();

                if (lastentry != null)
                {
                    res.DTO = lastentry.TimeStamp;
                }
                else
                {
                    var sensor = ccf.MonitoringSensors.Where(x => x.Id == id).FirstOrDefault();
                    res.DTO = sensor.MonitoringStation.StartDate;
                }
            }
            catch (Exception ex)
            {
                res.AddError("System Error", ex.Message);
                return res;
            }

            return res;
        }

        public static ServiceResult<List<TelogSensorData>> GetTelogSensorData(string uniqueId, DateTime lasttimestamp, string channel, bool retriveall)
        {

            var list = new List<TelogSensorData>();
            var ctel = new TelogEntities();
            var res = new ServiceResult<List<TelogSensorData>>(list);

            try
            {

                if (!retriveall)
                {


                    //var set = ctel.trend_data.Where(x => x.trend_data_time > lasttimestamp && x.measurement.measurement_name == channel && x.measurement.site.site_name == uniqueId).ToList();

                    //if (set != null)
                    //{
                    //    foreach (var s in set)
                    //    {

                    //    }
                    //}

                    var valuesList = (from m in ctel.measurements
                                      join t in ctel.trend_data on m.measurement_id equals t.measurement_id
                                      join s in ctel.sites on m.site_id equals s.site_id
                                      where m.measurement_name == channel
                                          && s.site_name == uniqueId
                                          && t.trend_data_time > lasttimestamp
                                      orderby t.trend_data_time ascending
                                      select new
                                      {
                                          MeasurementID = m.measurement_id,
                                          MeasurementName = m.measurement_name,
                                          Timestamp = t.trend_data_time,
                                          Avg = t.trend_data_avg,
                                          Max = t.trend_data_max,
                                          Min = t.trend_data_min
                                      }).ToList();

                    if (valuesList != null)
                    {
                        if (valuesList.Count() > 0)
                        {
                            foreach (var v in valuesList)
                            {
                                var t = new TelogSensorData();

                                t.avgValue = v.Avg;
                                t.maxValue = v.Max;
                                t.minValue = v.Min;
                                t.timestamp = v.Timestamp;
                                t.measurmentId = v.MeasurementID;
                                t.measurmentName = v.MeasurementName;
                                list.Add(t);
                            }
                        }
                        else
                        {
                            res.AddError("Operation Error", "UniqueID : " + uniqueId + ", Chanel : " + channel + ", TimeStamp : " + lasttimestamp + ".  No Records Found");
                            return res;
                        }
                    }
                    else
                    {
                        res.AddError("Operation Error", "UniqueID : " + uniqueId + ", Chanel : " + channel + ", TimeStamp : " + lasttimestamp + ".  No Records Found");
                        return res;
                    }
                }
                else
                {
                    var valuesList = (from m in ctel.measurements
                                      join t in ctel.trend_data on m.measurement_id equals t.measurement_id
                                      join s in ctel.sites on m.site_id equals s.site_id
                                      where m.measurement_name == channel
                                          && s.site_name == uniqueId
                                      orderby t.trend_data_time ascending
                                      select new
                                      {
                                          MeasurementID = m.measurement_id,
                                          MeasurementName = m.measurement_name,
                                          Timestamp = t.trend_data_time,
                                          Avg = t.trend_data_avg,
                                          Max = t.trend_data_max,
                                          Min = t.trend_data_min
                                      }).ToList();

                    if (valuesList != null)
                    {
                        if (valuesList.Count() > 0)
                        {
                            foreach (var v in valuesList)
                            {
                                var t = new TelogSensorData();

                                t.avgValue = v.Avg;
                                t.maxValue = v.Max;
                                t.minValue = v.Min;
                                t.timestamp = v.Timestamp;
                                t.measurmentId = v.MeasurementID;
                                t.measurmentName = v.MeasurementName;
                                list.Add(t);
                            }
                        }
                        else
                        {
                            res.AddError("Operation Error", "UniqueID : " + uniqueId + ", Chanel : " + channel + ", TimeStamp : " + lasttimestamp + ".  No Records Found");
                            return res;
                        }
                    }
                    else
                    {
                        res.AddError("Operation Error", "UniqueID : " + uniqueId + ", Chanel : " + channel + ", TimeStamp : " + lasttimestamp + ".  No Records Found");

                    }

                }

            }
            catch (Exception ex)
            {
                res.AddError("System Error", ex.Message);
                return res;
            }

            res.DTO = list;
            return res;

        }



    }
}
