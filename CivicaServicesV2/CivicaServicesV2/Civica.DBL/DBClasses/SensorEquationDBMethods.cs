﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Civica.Domain.CivicaDomain;
using Civica.Infrastructure.BaseClasses;

namespace Civica.DBL.DBClasses
{
    public class SensorEquationDBMethods
    {
        public static List<MonitoringLoggerChannel> GetMonitoringLoggerChannels(int loggerId)
        {
            if (loggerId != 0)
            {
                var ccf = new datacurrentEntities();


                ccf.MonitoringLoggerChannels.FirstOrDefault();
                var logger = ccf.MonitoringLoggerChannels.AsQueryable().Where(x => x.LoggerTypeId == loggerId).ToList();

                if (logger != null)
                {
                    return logger;
                }
            }
            return null;
        }
    }
}
