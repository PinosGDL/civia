﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.Domain.CivicaDomain;
using Civica.DBL.SPModels;
using System.Data;
using Civica.Infrastructure.BaseClasses;


namespace Civica.DBL.DBClasses
{
    public class ImportLog
    {
        public static void AddLogEntry(string importType, string messageType, int stationId, string fileName, string uniqueId, string errorMEssage, string stack, DateTimeOffset timestamp)
        {
            try
            {
                var ccf = new datacurrentEntities();
                var newlog = new MonitoringDataImportLog();

                newlog.MessageType = messageType;
                newlog.ImportType = importType;
                newlog.StationId = stationId;
                newlog.FileMane = fileName;
                newlog.UniqueId = uniqueId;
                newlog.ErrorMessage = errorMEssage;
                newlog.Stack = stack;
                newlog.TimeStamp = timestamp;

                ccf.MonitoringDataImportLogs.Add(newlog);
                ccf.SaveChanges();

            }
            catch (Exception e)
            {
               
            }
        }

        public static void AddLogEntryDebugging(string importType, string messageType, int stationId, string fileName, string uniqueId, string errorMEssage, string stack, DateTimeOffset timestamp)
        {
            try
            {
                if (!(string.IsNullOrEmpty(ConfigurationManager.AppSettings["CsvDebugPath"])))
                {
                    var ccf = new datacurrentEntities();
                    var newlog = new MonitoringDataImportLog();

                    newlog.MessageType = messageType;
                    newlog.ImportType = importType;
                    newlog.StationId = stationId;
                    newlog.FileMane = fileName;
                    newlog.UniqueId = uniqueId;
                    newlog.ErrorMessage = errorMEssage;
                    newlog.Stack = stack;
                    newlog.TimeStamp = timestamp;

                    ccf.MonitoringDataImportLogs.Add(newlog);
                    ccf.SaveChanges();

                }
            }
            catch (Exception e)
            {

            }
        }

        public static void SaveMonitoringDataToCSV(string Type, IEnumerable<SensorData> InputData , int StationId = 0, int SensorId = 0)
        {
            SensorData[] inputDataArray = InputData.ToArray();

            if (!(string.IsNullOrEmpty(ConfigurationManager.AppSettings["CsvDebugPath"])))
            {
                string fileName =  Path.Combine(ConfigurationManager.AppSettings["CsvDebugPath"], Path.GetRandomFileName() + ".csv");

                using (FileStream tempFileStream = File.Open(fileName, FileMode.OpenOrCreate))
                { 
                    StreamWriter writer = new StreamWriter(tempFileStream);

                    for(int i = 0; i < inputDataArray.Length;i++)
                    {
                        writer.WriteLine("Station Id , "+ StationId +",Sensorid , " + SensorId + "," + inputDataArray[i].timestamp.ToString() + "," + inputDataArray[i].value);
                    }
                    writer.Close();
                    ImportLog.AddLogEntry("Data Log","Degug",StationId,fileName,"", Type, "",DateTimeOffset.Now.UtcDateTime);
                }
            }
        }

        public static void SaveTimestampListToCSV(string Type, IEnumerable<DateTimeOffset> InputData, int StationId = 0, int SensorId = 0)
        {
            DateTimeOffset[] inputDataArray = InputData.ToArray();

            if (!(string.IsNullOrEmpty(ConfigurationManager.AppSettings["CsvDebugPath"])))
            {
                string fileName = Path.Combine(ConfigurationManager.AppSettings["CsvDebugPath"], Path.GetRandomFileName() + ".csv");

                using (FileStream tempFileStream = File.Open(fileName, FileMode.OpenOrCreate))
                {
                    StreamWriter writer = new StreamWriter(tempFileStream);

                    for (int i = 0; i < inputDataArray.Length; i++)
                    {
                        writer.WriteLine("Station Id , " + StationId + ", Sensorid , " + SensorId + "," + inputDataArray[i].ToString() + ", timezone," + inputDataArray[i].Offset.TotalHours.ToString());
                    }
                    writer.Close();
                    ImportLog.AddLogEntry("Data Log", "Debug", StationId, fileName, "", Type, "", DateTimeOffset.Now.UtcDateTime);
                }
            }
        }

        public static void SaveSensorData(DataTable table)
        {
            if (!(string.IsNullOrEmpty(ConfigurationManager.AppSettings["CsvDebugPath"])))
            {
                string fileName = Path.Combine(ConfigurationManager.AppSettings["CsvDebugPath"], Path.GetRandomFileName() + ".csv");

                using (FileStream tempFileStream = File.Open(fileName, FileMode.OpenOrCreate))
                {
                    StreamWriter writer = new StreamWriter(tempFileStream);

                    for (int i = 0; i < table.Rows.Count; i++)
                    {
                        string line = "";
                        for(int j=0;j < table.Columns.Count;j++)
                        {
                            line += table.Rows[i][j].ToString();
                            line += ",";
                        }
                        writer.WriteLine(line);
                    }
                    ImportLog.AddLogEntry("Data Log", "Debug", 0, fileName, "", "Bulk Upload Data", "", DateTimeOffset.Now.UtcDateTime);
                }
            }
        }
    }
}