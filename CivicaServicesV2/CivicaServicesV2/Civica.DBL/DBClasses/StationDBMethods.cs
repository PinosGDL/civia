﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Civica.Domain.CivicaDomain;
using Civica.Infrastructure.BaseClasses;
using Civica.DBL.SPModels;

namespace Civica.DBL.DBClasses
{
    public class StationDBMethods
    {

        public static MonitoringStation GetStationByUniqueId(string uniqueId)
        {

            var ccf = new datacurrentEntities();

            var station = ccf.MonitoringStations.AsQueryable().Where(x => x.Active == true && x.StateId == (int)BaseEnum.StationState.Receiving && x.MonitoringLogger.UniqueId == uniqueId && x.MonitoringLogger.Active == true && x.MaintanaceMode == false).FirstOrDefault();

            return station;
        }

        public static MonitoringStation GetStationByUniqueId(string uniqueId, BaseEnum.LoggerType type)
        {

            var ccf = new datacurrentEntities();

            var station = ccf.MonitoringStations.AsQueryable().Where(x => x.Active == true && x.StateId == (int)BaseEnum.StationState.Receiving && x.MonitoringLogger.UniqueId == uniqueId && x.MaintanaceMode == false && x.MonitoringLogger.LoggerTypeId == (int)type ).FirstOrDefault();

            return station;
        }

        public static MonitoringStation GetStationById(int Id)
        {
            var ccf = new datacurrentEntities();
            var station = ccf.MonitoringStations.Where(x => x.Id == Id && x.MaintanaceMode == false && x.StateId == (int)BaseEnum.StationState.Receiving && x.Active == true).FirstOrDefault();
            return station;
        }

        public static MonitoringStation GetInactiveStationByStationById(int Id)
        {
            var ccf = new datacurrentEntities();
            var station = ccf.MonitoringStations.Where(x => x.Id == Id && x.Active == true).FirstOrDefault();
            return station;
        }

        public static List<MonitoringStation> GetListOfStationsByLoggerType(int loggerTypeId)
        {
            var ccf = new datacurrentEntities();

            var station = ccf.MonitoringStations.Where(x => x.MonitoringLogger.LoggerTypeId == loggerTypeId && x.Active == true && x.MaintanaceMode == false && x.StateId == (int)BaseEnum.StationState.Receiving).ToList();

            return station;
        }
        public static Dictionary<DateTime, decimal?[]> GetDefaultFormatStationSensorData(List<MonitoringSensor> sensors, int ExportDatType, DateTimeOffset? startTime, DateTimeOffset? endTime, TimeSpan timeZoneOffset)
        {
            var ccf = new datacurrentEntities();

            Dictionary<DateTime, decimal?[]> dictionary = new Dictionary<DateTime, decimal?[]>();

            var sensorDataArraySize = sensors.Count;
            for (int i = 0; i < sensors.Count; i++)
            {
                var sensorType = sensors.ElementAt(i).SensorTypeId;
                var sensorDataType = sensors.ElementAt(i).SensorDataTypeId;
                bool Group = false;
                if (sensorType == 2 && sensorDataType == 1)
                    Group = true;
                var monitoringSensorId = sensors.ElementAt(i).Id;
                if (!Group)
                {
                    List<MonitoringData> sensorDataList = null;
                    List<MonitoringDataRaw> sensorDataRawList = null;
                    if (ExportDatType == 3)
                    {
                        sensorDataRawList = new List<MonitoringDataRaw>();
                    }
                    else
                    {
                        sensorDataList = new List<MonitoringData>();
                    }
                    sensorDataList = new List<MonitoringData>();
                    if (ExportDatType == 3)
                    {
                        if (startTime == null && endTime == null)
                        {
                            sensorDataRawList = ccf.MonitoringDataRaws.Where(x => x.MonitoringSensorId == monitoringSensorId).ToList();
                        }

                        else
                        {
                            if (startTime != null && endTime == null)
                            {
                                sensorDataRawList = ccf.MonitoringDataRaws.Where(x => x.MonitoringSensorId == monitoringSensorId).Where(x => x.Timestamp >= startTime).ToList();
                            }
                            else if (startTime == null && endTime != null)
                            {
                                sensorDataRawList = ccf.MonitoringDataRaws.Where(x => x.MonitoringSensorId == monitoringSensorId).Where(x => x.Timestamp <= endTime).ToList();
                            }
                            else
                            {
                                sensorDataRawList = ccf.MonitoringDataRaws.Where(x => x.MonitoringSensorId == monitoringSensorId).Where(x => x.Timestamp >= startTime).Where(x => x.Timestamp <= endTime).ToList();
                            }
                        }
                    }
                    else
                    {
                        if (ExportDatType == 1)
                        {
                            if (startTime == null && endTime == null)
                                sensorDataList = ccf.MonitoringDatas.Where(x => x.MonitoringSensorId == monitoringSensorId).ToList();
                            else
                            {
                                if (startTime != null && endTime == null)
                                {
                                    sensorDataList = ccf.MonitoringDatas.Where(x => x.MonitoringSensorId == monitoringSensorId).Where(x => x.TimeStamp >= startTime).ToList();
                                }
                                else if (startTime == null && endTime != null)
                                {
                                    sensorDataList = ccf.MonitoringDatas.Where(x => x.MonitoringSensorId == monitoringSensorId).Where(x => x.TimeStamp <= endTime).ToList();
                                }
                                else
                                {
                                    sensorDataList = ccf.MonitoringDatas.Where(x => x.MonitoringSensorId == monitoringSensorId).Where(x => x.TimeStamp >= startTime).Where(x => x.TimeStamp <= endTime).ToList();
                                }
                            }
                        }
                        else
                        {
                            if (startTime == null && endTime == null)

                                sensorDataList = ccf.MonitoringDatas.Where(x => x.Processed == 3).Where(x => x.MonitoringSensorId == monitoringSensorId).ToList();
                            else
                            {
                                if (startTime != null && endTime == null)
                                {
                                    sensorDataList = ccf.MonitoringDatas.Where(x => x.Processed == 3).Where(x => x.MonitoringSensorId == monitoringSensorId).Where(x => x.TimeStamp >= startTime).ToList();
                                }
                                else if (startTime == null && endTime != null)
                                {
                                    sensorDataList = ccf.MonitoringDatas.Where(x => x.Processed == 3).Where(x => x.MonitoringSensorId == monitoringSensorId).Where(x => x.TimeStamp <= endTime).ToList();
                                }
                                else
                                {
                                    sensorDataList = ccf.MonitoringDatas.Where(x => x.Processed == 3).Where(x => x.MonitoringSensorId == monitoringSensorId).Where(x => x.TimeStamp >= startTime).Where(x => x.TimeStamp <= endTime).ToList();
                                }
                            }
                        }

                    }

                    if (sensorDataList != null)
                    {
                        foreach (var sensorData in sensorDataList)
                        {
                            if (dictionary.ContainsKey(sensorData.TimeStamp.ToOffset(timeZoneOffset).DateTime))
                            {
                                var values = dictionary[sensorData.TimeStamp.ToOffset(timeZoneOffset).DateTime];
                                values[i] = sensorData.Value;
                                dictionary[sensorData.TimeStamp.ToOffset(timeZoneOffset).DateTime] = values;
                            }
                            else
                            {
                                Decimal?[] values = new Decimal?[sensorDataArraySize];
                                for (int q = 0; q < sensorDataArraySize; q++)
                                {
                                    values[q] = null;
                                }
                                values[i] = sensorData.Value;
                                dictionary.Add(sensorData.TimeStamp.ToOffset(timeZoneOffset).DateTime, values);

                            }
                        }
                    }
                    else
                    {
                        foreach (var sensorData in sensorDataRawList)
                        {
                            if (dictionary.ContainsKey(sensorData.Timestamp.ToOffset(timeZoneOffset).DateTime))
                            {
                                var values = dictionary[sensorData.Timestamp.ToOffset(timeZoneOffset).DateTime];
                                values[i] = sensorData.Value;
                                dictionary[sensorData.Timestamp.ToOffset(timeZoneOffset).DateTime] = values;
                            }
                            else
                            {
                                Decimal?[] values = new Decimal?[sensorDataArraySize];
                                for (int q = 0; q < sensorDataArraySize; q++)
                                {
                                    values[q] = null;
                                }
                                values[i] = sensorData.Value;
                                dictionary.Add(sensorData.Timestamp.ToOffset(timeZoneOffset).DateTime, values);
                            }
                        }
                    }
                }
                else
                {
                    List<MonitoringDataSP> sensorDataList = new List<MonitoringDataSP>();
                    if (ExportDatType == 3)
                    {
                        sensorDataList = GetGroupSensorRawData(monitoringSensorId, startTime, endTime, 5, timeZoneOffset);
                    }
                    else if (ExportDatType == 2)
                    {
                        sensorDataList = GetGroupSensorProcessedData(monitoringSensorId, startTime, endTime, 5, timeZoneOffset);
                    }
                    else
                    {
                        sensorDataList = GetGroupSensorData(monitoringSensorId, startTime, endTime, 5, timeZoneOffset);
                    }
                    foreach (var sensorData in sensorDataList)
                    {
                        if (dictionary.ContainsKey(sensorData.TimeStamp))
                        {
                            var values = dictionary[sensorData.TimeStamp];
                            values[i] = sensorData.Value;
                            dictionary[sensorData.TimeStamp] = values;
                        }
                        else
                        {
                            Decimal?[] values = new Decimal?[sensorDataArraySize];
                            for (int q = 0; q < sensorDataArraySize; q++)
                            {
                                values[q] = null;
                            }
                            values[i] = sensorData.Value;
                            dictionary.Add(sensorData.TimeStamp, values);

                        }
                    }
                }

            }
            return dictionary.OrderBy(x => x.Key).ToDictionary(x => x.Key, x => x.Value);
        }


        public static Dictionary<DateTimeOffset, decimal?[]> GetStationSensorData(List<MonitoringSensor> sensors, int ExportDatType, DateTimeOffset? startTime, DateTimeOffset? endTime)
        {
            var ccf = new datacurrentEntities();

            Dictionary<DateTimeOffset, decimal?[]> dictionary = new Dictionary<DateTimeOffset, decimal?[]>();
            var selectSensors = new List<MonitoringSensor>();
            var mandatorySensorTypeIds = SensorType.GetDefaultSensorTypeOrder().Select(x => x.SensorTypeId).ToList();
            for (var i = 0; i < mandatorySensorTypeIds.Count; i++)
            {
                var sensorTypeId = mandatorySensorTypeIds[i];
                var sensor = sensors.Where(x => x.SensorTypeId == sensorTypeId).FirstOrDefault();
                if (sensor != null)
                {
                    selectSensors.Add(sensor);
                }

            }
            var notMadatorySensors = sensors.Where(x => !mandatorySensorTypeIds.Contains(x.SensorTypeId)).ToList();
            selectSensors.AddRange(notMadatorySensors);
            var sensorDataArraySize = selectSensors.Count;
            for (int i = 0; i < selectSensors.Count; i++)
            {

                var monitoringSensorId = selectSensors.ElementAt(i).Id;

                List<MonitoringData> sensorDataList = null;
                List<MonitoringDataRaw> sensorDataRawList = null;
                if (ExportDatType == 3)
                {
                    sensorDataRawList = new List<MonitoringDataRaw>();
                }
                else
                {
                    sensorDataList = new List<MonitoringData>();
                }
                sensorDataList = new List<MonitoringData>();
                if (ExportDatType == 3)
                {
                    if (startTime == null && endTime == null)

                        sensorDataRawList = ccf.MonitoringDataRaws.Where(x => x.MonitoringSensorId == monitoringSensorId).ToList();
                    else
                    {
                        if (startTime != null && endTime == null)
                        {
                            sensorDataRawList = ccf.MonitoringDataRaws.Where(x => x.MonitoringSensorId == monitoringSensorId).Where(x => x.Timestamp >= startTime).ToList();
                        }
                        else if (startTime == null && endTime != null)
                        {
                            sensorDataRawList = ccf.MonitoringDataRaws.Where(x => x.MonitoringSensorId == monitoringSensorId).Where(x => x.Timestamp <= endTime).ToList();
                        }
                        else
                        {
                            sensorDataRawList = ccf.MonitoringDataRaws.Where(x => x.MonitoringSensorId == monitoringSensorId).Where(x => x.Timestamp >= startTime).Where(x => x.Timestamp <= endTime).ToList();
                        }
                    }
                }
                else
                {
                    if (ExportDatType == 1)
                    {
                        if (startTime == null && endTime == null)

                            sensorDataList = ccf.MonitoringDatas.Where(x => x.MonitoringSensorId == monitoringSensorId).ToList();
                        else
                        {
                            if (startTime != null && endTime == null)
                            {
                                sensorDataList = ccf.MonitoringDatas.Where(x => x.MonitoringSensorId == monitoringSensorId).Where(x => x.TimeStamp >= startTime).ToList();
                            }
                            else if (startTime == null && endTime != null)
                            {
                                sensorDataList = ccf.MonitoringDatas.Where(x => x.MonitoringSensorId == monitoringSensorId).Where(x => x.TimeStamp <= endTime).ToList();
                            }
                            else
                            {
                                sensorDataList = ccf.MonitoringDatas.Where(x => x.MonitoringSensorId == monitoringSensorId).Where(x => x.TimeStamp >= startTime).Where(x => x.TimeStamp <= endTime).ToList();
                            }
                        }
                    }
                    else
                    {
                        if (startTime == null && endTime == null)

                            sensorDataList = ccf.MonitoringDatas.Where(x => x.Processed == 3).Where(x => x.MonitoringSensorId == monitoringSensorId).ToList();
                        else
                        {
                            if (startTime != null && endTime == null)
                            {
                                sensorDataList = ccf.MonitoringDatas.Where(x => x.Processed == 3).Where(x => x.MonitoringSensorId == monitoringSensorId).Where(x => x.TimeStamp >= startTime).ToList();
                            }
                            else if (startTime == null && endTime != null)
                            {
                                sensorDataList = ccf.MonitoringDatas.Where(x => x.Processed == 3).Where(x => x.MonitoringSensorId == monitoringSensorId).Where(x => x.TimeStamp <= endTime).ToList();
                            }
                            else
                            {
                                sensorDataList = ccf.MonitoringDatas.Where(x => x.Processed == 3).Where(x => x.MonitoringSensorId == monitoringSensorId).Where(x => x.TimeStamp >= startTime).Where(x => x.TimeStamp <= endTime).ToList();
                            }
                        }
                    }

                }

                if (sensorDataList != null)
                {
                    foreach (var sensorData in sensorDataList)
                    {
                        if (dictionary.ContainsKey(sensorData.TimeStamp))
                        {
                            var values = dictionary[sensorData.TimeStamp];
                            values[i] = sensorData.Value;
                            dictionary[sensorData.TimeStamp] = values;
                        }
                        else
                        {
                            Decimal?[] values = new Decimal?[sensorDataArraySize];
                            for (int q = 0; q < sensorDataArraySize; q++)
                            {
                                values[q] = null;
                            }
                            values[i] = sensorData.Value;
                            dictionary.Add(sensorData.TimeStamp, values);

                        }
                    }
                }
                else
                {
                    foreach (var sensorData in sensorDataRawList)
                    {
                        if (dictionary.ContainsKey(sensorData.Timestamp))
                        {
                            var values = dictionary[sensorData.Timestamp];
                            values[i] = sensorData.Value;
                            dictionary[sensorData.Timestamp] = values;
                        }
                        else
                        {
                            Decimal?[] values = new Decimal?[sensorDataArraySize];
                            for (int q = 0; q < sensorDataArraySize; q++)
                            {
                                values[q] = null;
                            }
                            values[i] = sensorData.Value;
                            dictionary.Add(sensorData.Timestamp, values);

                        }
                    }

                }


            }

            return dictionary.OrderBy(x => x.Key).ToDictionary(x => x.Key, x => x.Value);
        }

        public static TimeSpan GetStationTimeZoneSpan(int stationId)
        {
            var ccf = new datacurrentEntities();
            var stationTimeZoneOffset = ccf.MonitoringStations.Where(x => x.Id == stationId).FirstOrDefault().TimeZone.Offset;
            TimeSpan timeSpan = new TimeSpan((int)stationTimeZoneOffset, 0, 0);
            return timeSpan;
        }

        public static List<int> GetStationIdsByServiceId(int serviceId)
        {
            var ccf = new datacurrentEntities();
            var stationIds = ccf.MonitoringServiceStations.Where(x => x.MonitoringServiceId == serviceId).Where(x => x.Active == true).Select(x => x.MonitoringStationId).ToList();
            return stationIds;
        }

        public static List<MonitoringDataSP> GetGroupSensorData(int sensorid, DateTimeOffset? startTime, DateTimeOffset? endTime, int groupInterval, TimeSpan timeZoneOffset)
        {
            if (startTime == null && endTime == null)
            {
                var ccf = new datacurrentEntities();
                var monitoringDataList = ccf.MonitoringDatas.Where(x => x.MonitoringSensorId == sensorid).ToList();
                long ticksInFiveMinutes = TimeSpan.TicksPerMinute * groupInterval;
                var query = from r in monitoringDataList
                            group r by new { ticks = r.TimeStamp.ToOffset(timeZoneOffset).DateTime.Ticks / ticksInFiveMinutes }
                            into g
                            let key = new DateTime(g.Key.ticks * ticksInFiveMinutes)
                            select new MonitoringDataSP
                            {
                                TimeStamp = key,
                                Value = g.Sum(r => r.Value)
                            };
                return query.ToList();

            }
            else if (startTime != null && endTime == null)
            {
                var ccf = new datacurrentEntities();
                var monitoringDataList = ccf.MonitoringDatas.Where(x => x.MonitoringSensorId == sensorid).Where(x => x.TimeStamp >= startTime).ToList();
                long ticksInFiveMinutes = TimeSpan.TicksPerMinute * groupInterval;
                var query = from r in monitoringDataList         
                            group r by new { ticks = r.TimeStamp.ToOffset(timeZoneOffset).DateTime.Ticks / ticksInFiveMinutes }
                            into g
                            let key = new DateTime(g.Key.ticks * ticksInFiveMinutes)
                            select new MonitoringDataSP
                            {
                                TimeStamp = key,
                                Value = g.Sum(r => r.Value)
                            };
                return query.ToList();
            }
            else if (startTime == null && endTime != null)
            {
                var ccf = new datacurrentEntities();
                var monitoringDataList = ccf.MonitoringDatas.Where(x => x.MonitoringSensorId == sensorid).Where(x => x.TimeStamp <= endTime).ToList();
                long ticksInFiveMinutes = TimeSpan.TicksPerMinute * groupInterval;
                var query = from r in monitoringDataList
                            group r by new { ticks = r.TimeStamp.ToOffset(timeZoneOffset).DateTime.Ticks / ticksInFiveMinutes }
                            into g
                            let key = new DateTime(g.Key.ticks * ticksInFiveMinutes)
                            select new MonitoringDataSP
                            {
                                TimeStamp = key,
                                Value = g.Sum(r => r.Value)
                            };
                return query.ToList();
            }
            else
            {
                var ccf = new datacurrentEntities();
                var monitoringDataList = ccf.MonitoringDatas.Where(x => x.MonitoringSensorId == sensorid).Where(x => x.TimeStamp >= startTime).Where(x => x.TimeStamp <= endTime).ToList();
                long ticksInFiveMinutes = TimeSpan.TicksPerMinute * groupInterval;
                var query = from r in monitoringDataList
                            group r by new { ticks = r.TimeStamp.ToOffset(timeZoneOffset).DateTime.Ticks / ticksInFiveMinutes }
                            into g
                            let key = new DateTime(g.Key.ticks * ticksInFiveMinutes)
                            select new MonitoringDataSP
                            {
                                TimeStamp = key,
                                Value = g.Sum(r => r.Value)
                            };
                return query.ToList();

            }


        }

        public static List<MonitoringDataSP> GetGroupSensorProcessedData(int sensorid, DateTimeOffset? startTime, DateTimeOffset? endTime, int groupInterval, TimeSpan timeZoneOffset)
        {
            if (startTime == null && endTime == null)
            {
               
                var ccf = new datacurrentEntities();
                var monitoringDataList = ccf.MonitoringDatas.Where(x => x.MonitoringSensorId == sensorid).Where(r =>r.Processed == 3).ToList();
                long ticksInFiveMinutes = TimeSpan.TicksPerMinute * groupInterval;
                var query = from r in monitoringDataList
                            group r by new { ticks = r.TimeStamp.ToOffset(timeZoneOffset).Ticks / ticksInFiveMinutes }
                            into g
                            let key = new DateTime(g.Key.ticks * ticksInFiveMinutes)
                            select new MonitoringDataSP
                            {
                                TimeStamp = key,
                                Value = g.Sum(r => r.Value)
                            };
                return query.ToList();

            }
            else if (startTime != null && endTime == null)
            {
                var ccf = new datacurrentEntities();
                var monitoringDataList = ccf.MonitoringDatas.Where(x => x.MonitoringSensorId == sensorid).Where(r => r.Processed == 3).Where(x => x.TimeStamp >= startTime).ToList();
                long ticksInFiveMinutes = TimeSpan.TicksPerMinute * groupInterval;
                var query = from r in monitoringDataList

                            group r by new { ticks = r.TimeStamp.ToOffset(timeZoneOffset).Ticks / ticksInFiveMinutes }
                            into g
                            let key = new DateTime(g.Key.ticks * ticksInFiveMinutes)
                            select new MonitoringDataSP
                            {
                                TimeStamp = key,
                                Value = g.Sum(r => r.Value)
                            };
                return query.ToList();
            }
            else if (startTime == null && endTime != null)
            {
                var ccf = new datacurrentEntities();
                var monitoringDataList = ccf.MonitoringDatas.Where(x => x.MonitoringSensorId == sensorid).Where(r => r.Processed == 3).Where(x => x.TimeStamp <= endTime).ToList();
                long ticksInFiveMinutes = TimeSpan.TicksPerMinute * groupInterval;
                var query = from r in ccf.MonitoringDatas
                            group r by new { ticks = r.TimeStamp.ToOffset(timeZoneOffset).Ticks / ticksInFiveMinutes }
                            into g
                            let key = new DateTime(g.Key.ticks * ticksInFiveMinutes)
                            select new MonitoringDataSP
                            {
                                TimeStamp = key,
                                Value = g.Sum(r => r.Value)
                            };
                return query.ToList();
            }
            else
            {
                var ccf = new datacurrentEntities();
                var monitoringDataList = ccf.MonitoringDatas.Where(x => x.MonitoringSensorId == sensorid).Where(r => r.Processed == 3).Where(x => x.TimeStamp >= startTime).Where(x => x.TimeStamp <= endTime).ToList();
                long ticksInFiveMinutes = TimeSpan.TicksPerMinute * groupInterval;
                var query = from r in monitoringDataList

                            group r by new { ticks = r.TimeStamp.ToOffset(timeZoneOffset).Ticks / ticksInFiveMinutes }
                            into g
                            let key = new DateTime(g.Key.ticks * ticksInFiveMinutes)
                            select new MonitoringDataSP
                            {
                                TimeStamp = key,
                                Value = g.Sum(r => r.Value)
                            };
                return query.ToList();

            }


        }

        public static List<MonitoringDataSP> GetGroupSensorRawData(int sensorid, DateTimeOffset? startTime, DateTimeOffset? endTime, int groupInterval, TimeSpan timeZoneOffset)
        {

            if (startTime == null && endTime == null)
            {
                var ccf = new datacurrentEntities();
                var monitoringDataList = ccf.MonitoringDataRaws.Where(x => x.MonitoringSensorId == sensorid).ToList();
                long ticksInFiveMinutes = TimeSpan.TicksPerMinute * groupInterval;
                var query = from r in monitoringDataList
                            group r by new { ticks = r.Timestamp.ToOffset(timeZoneOffset).DateTime.Ticks / ticksInFiveMinutes }
                            into g
                            let key = new DateTime(g.Key.ticks * ticksInFiveMinutes)
                            select new MonitoringDataSP
                            {
                                TimeStamp = key,
                                Value = g.Sum(r => r.Value)
                            };
                return query.ToList();

            }
            else if (startTime != null && endTime == null)
            {
                var ccf = new datacurrentEntities();
                var monitoringDataList = ccf.MonitoringDataRaws.Where(x => x.MonitoringSensorId == sensorid).Where(x => x.Timestamp >= startTime).ToList();
                long ticksInFiveMinutes = TimeSpan.TicksPerMinute * groupInterval;
                var query = from r in monitoringDataList
                            group r by new { ticks = r.Timestamp.ToOffset(timeZoneOffset).DateTime.Ticks / ticksInFiveMinutes }
                            into g
                            let key = new DateTime(g.Key.ticks * ticksInFiveMinutes)
                            select new MonitoringDataSP
                            {
                                TimeStamp = key,
                                Value = g.Sum(r => r.Value)
                            };
                return query.ToList();
            }
            else if (startTime == null && endTime != null)
            {
                var ccf = new datacurrentEntities();
                var monitoringDataList = ccf.MonitoringDataRaws.Where(x => x.MonitoringSensorId == sensorid).Where(x => x.Timestamp <= endTime).ToList();
                long ticksInFiveMinutes = TimeSpan.TicksPerMinute * groupInterval;
                var query = from r in monitoringDataList
                            group r by new { ticks = r.Timestamp.ToOffset(timeZoneOffset).DateTime.Ticks / ticksInFiveMinutes }
                            into g
                            let key = new DateTime(g.Key.ticks * ticksInFiveMinutes)
                            select new MonitoringDataSP
                            {
                                TimeStamp = key,
                                Value = g.Sum(r => r.Value)
                            };
                return query.ToList();
            }
            else
            {
                var ccf = new datacurrentEntities();
                var monitoringDataList = ccf.MonitoringDataRaws.Where(x => x.MonitoringSensorId == sensorid).Where(x => x.Timestamp >= startTime).Where(x => x.Timestamp <= endTime).ToList();
                long ticksInFiveMinutes = TimeSpan.TicksPerMinute * groupInterval;
                var query = from r in monitoringDataList
                            group r by new { ticks = r.Timestamp.ToOffset(timeZoneOffset).DateTime.Ticks / ticksInFiveMinutes }
                            into g
                            let key = new DateTime(g.Key.ticks * ticksInFiveMinutes)
                            select new MonitoringDataSP
                            {
                                TimeStamp = key,
                                Value = g.Sum(r => r.Value)
                            };
                return query.ToList();

            }


        }

    }
}
