﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.Domain.CivicaDomain;
using Civica.DBL.SPModels;
using System.Data;
using Civica.Infrastructure.BaseClasses;

namespace Civica.DBL.DBClasses
{

    public class MonitoringTaskDBMethods
    {
        public static List<MonitoringTask> GetPendingTasks()
        {
            try
            {
                var ccf = new datacurrentEntities();
                var tasks = ccf.MonitoringTasks.AsQueryable().Where(x => x.Active == true).Where(x => x.StatusId == 1).ToList();

                foreach (var t in tasks)
                {
                    t.StatusId = 2;
                }
                ccf.SaveChanges();
                return tasks;
            }
            catch (Exception e)
            {
                return null;
            }

        }


        public static ServiceResult<bool> UpdateTaskUndoPath(int taskId,   string path)
        {
            ServiceResult<bool> res = new ServiceResult<bool>(true);

            var ccf = new datacurrentEntities();

            try
            {
                var task = ccf.MonitoringTasks.AsQueryable().Where(x => x.Id == taskId).FirstOrDefault();

                if (task != null)
                {
                    task.FilePathUndo = path;
     
                    ccf.SaveChanges();

                    res.DTO = true;
                    res.ServiceLogs.Add("Update Task", "Success updating the Undo file path");
                }
                else
                {
                    res.ErrorResults.Add("Error Updating Task", "Task : " + taskId + " not found.");
                    res.DTO = false;
                    return res;
                }

            }
            catch (Exception ex)
            {
                res.ErrorResults.Add("Error Updating Task", "General error thrown while updating task : " + taskId + " : Message :  " + ex.Message);
                res.DTO = false;
                return res;
            }



            return res;
        }

        public static List<MonitoringSensor> GetExportSensorsByTaskId(int taskId)
        {
            var ccf = new datacurrentEntities();
            var monitoringTaskSensors = ccf.MonitoringTaskSensors.AsQueryable().Where(x => x.MonitoringTaskId == taskId).Select(x => x.MonitoringSensor).ToList();
            return monitoringTaskSensors;
        }

        public static ServiceResult<bool> UpdateTask(int taskId, int status, string message, DateTimeOffset start, DateTimeOffset end, DateTime? dataStartTime = null, DateTime? dataEndTime = null)
        {
            ServiceResult<bool> res = new ServiceResult<bool>(true);

            var ccf = new datacurrentEntities();

            try
            {
                var task = ccf.MonitoringTasks.AsQueryable().Where(x => x.Id == taskId).FirstOrDefault();

                if (task != null)
                {
                    task.StatusId = status;
                    task.ResultMessage = message;
                    task.ProcessStartTime = start;
                    task.ProcessEndTime = end;
                    task.DataStartDate = dataStartTime;
                    task.DataSEndDate = dataEndTime;
                    ccf.SaveChanges();

                    res.DTO = true;
                    res.ServiceLogs.Add("Update Task", "Success");
                }
                else
                {
                    res.ErrorResults.Add("Error Updating Task", "Task : " + taskId + " not found.");
                    res.DTO = false;
                    return res;
                }

            }
            catch (Exception ex)
            {
                res.ErrorResults.Add("Error Updating Task", "General error thrown while updating task : " + taskId + " : Message :  " + ex.Message );
                res.DTO = false;
                return res;
            }



            return res;
        }

    }

}