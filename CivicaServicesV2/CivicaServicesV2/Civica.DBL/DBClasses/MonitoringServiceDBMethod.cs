﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Civica.Domain.CivicaDomain;

namespace Civica.DBL.DBClasses
{
    public class MonitoringServiceDBMethod
    {
        public static string GetServiceName(int monitoringServiceId)
        {
            var ccf = new datacurrentEntities();
            var serviceName = ccf.MonitoringServices.Where(x => x.Id == monitoringServiceId).FirstOrDefault().Name;
            return serviceName;
        }
    }
}
