//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Civica.Domain.CivicaDomain
{
    using System;
    using System.Collections.Generic;
    
    public partial class MonitoringStationActivity
    {
        public int Id { get; set; }
        public int MonitoringStationId { get; set; }
        public System.DateTime ActiveMonth { get; set; }
        public System.DateTimeOffset Created { get; set; }
    
        public virtual MonitoringStation MonitoringStation { get; set; }
    }
}
