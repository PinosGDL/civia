//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Civica.Domain.CivicaDomain
{
    using System;
    using System.Collections.Generic;
    
    public partial class ProjectLotInspectionResult
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ProjectLotInspectionResult()
        {
            this.ProjectLotInspectionResultDocuments = new HashSet<ProjectLotInspectionResultDocument>();
            this.TestEntityLotInspectionResults = new HashSet<TestEntityLotInspectionResult>();
        }
    
        public int Id { get; set; }
        public Nullable<int> ProjectId { get; set; }
        public int TypeId { get; set; }
        public int DischargeId { get; set; }
        public Nullable<int> DrainageDirectionId { get; set; }
        public string Code { get; set; }
        public int GeometryTypeID { get; set; }
        public System.Data.Entity.Spatial.DbGeography Geometry { get; set; }
        public System.Data.Entity.Spatial.DbGeometry FlatGeometry { get; set; }
        public bool FurtherReview { get; set; }
        public Nullable<System.DateTimeOffset> InspectionDate { get; set; }
        public string Comment { get; set; }
        public System.DateTimeOffset Created { get; set; }
        public System.DateTimeOffset Modified { get; set; }
        public int CreatedBy { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public bool Active { get; set; }
        public string Adress { get; set; }
        public System.Data.Entity.Spatial.DbGeometry FlatBoundary { get; set; }
        public System.Data.Entity.Spatial.DbGeography Boundary { get; set; }
    
        public virtual DefectDischarge DefectDischarge { get; set; }
        public virtual DefectDrainageDirection DefectDrainageDirection { get; set; }
        public virtual DefectType DefectType { get; set; }
        public virtual Project Project { get; set; }
        public virtual ProjectLotInspectionResult ProjectLotInspectionResult1 { get; set; }
        public virtual ProjectLotInspectionResult ProjectLotInspectionResult2 { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ProjectLotInspectionResultDocument> ProjectLotInspectionResultDocuments { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TestEntityLotInspectionResult> TestEntityLotInspectionResults { get; set; }
    }
}
