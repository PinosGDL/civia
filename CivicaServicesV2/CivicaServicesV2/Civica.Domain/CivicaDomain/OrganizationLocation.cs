//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Civica.Domain.CivicaDomain
{
    using System;
    using System.Collections.Generic;
    
    public partial class OrganizationLocation
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OrganizationLocation()
        {
            this.AppUsers = new HashSet<AppUser>();
        }
    
        public int Id { get; set; }
        public int OrganizationId { get; set; }
        public string AddressId { get; set; }
        public string LocationName { get; set; }
        public string LocationCode { get; set; }
        public System.Data.Entity.Spatial.DbGeography Location { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string PostalCode { get; set; }
        public Nullable<int> CountryId { get; set; }
        public bool IsHeadquarters { get; set; }
        public bool Active { get; set; }
    
        public virtual Country Country { get; set; }
        public virtual Organization Organization { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AppUser> AppUsers { get; set; }
    }
}
