//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Civica.Domain.CivicaDomain
{
    using System;
    using System.Collections.Generic;
    
    public partial class MonitoringStationAlarmConditionLog
    {
        public int Id { get; set; }
        public int AlarmId { get; set; }
        public string ConditonType { get; set; }
        public System.DateTimeOffset TimeStamp { get; set; }
        public string description { get; set; }
        public string note { get; set; }
        public bool Active { get; set; }
        public Nullable<int> AppUserId { get; set; }
    }
}
