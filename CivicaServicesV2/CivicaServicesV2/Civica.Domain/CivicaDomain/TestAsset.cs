//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Civica.Domain.CivicaDomain
{
    using System;
    using System.Collections.Generic;
    
    public partial class TestAsset
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TestAsset()
        {
            this.TestAssetDefectOutlets = new HashSet<TestAssetDefectOutlet>();
            this.TestAssetDocuments = new HashSet<TestAssetDocument>();
            this.TestAssetGroups = new HashSet<TestAssetGroup>();
            this.TestAssetNotes = new HashSet<TestAssetNote>();
            this.TestEntities = new HashSet<TestEntity>();
            this.TestGeometryAssetLayerDatas = new HashSet<TestGeometryAssetLayerData>();
        }
    
        public int Id { get; set; }
        public int TestId { get; set; }
        public Nullable<int> TestAssetProgressId { get; set; }
        public Nullable<int> TestAssetStateId { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public Nullable<System.DateTimeOffset> TestDate { get; set; }
        public Nullable<bool> Result { get; set; }
        public string Weather { get; set; }
        public string Comment { get; set; }
        public System.DateTimeOffset Created { get; set; }
        public System.DateTimeOffset Modified { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
        public bool Active { get; set; }
        public Nullable<int> ParentTestAssetID { get; set; }
        public string Color { get; set; }
        public int Progress { get; set; }
        public Nullable<bool> FollowUp { get; set; }
        public Nullable<int> ProjectDefectId { get; set; }
        public System.Data.Entity.Spatial.DbGeometry FlatGeometry { get; set; }
        public System.Data.Entity.Spatial.DbGeography Geometry { get; set; }
        public Nullable<int> TestAssetWeatherId { get; set; }
        public Nullable<int> AssetId { get; set; }
        public bool Approved { get; set; }
        public Nullable<int> ApprovedBy { get; set; }
        public Nullable<System.DateTime> ApprovedDate { get; set; }
        public string ApprovedComments { get; set; }
        public Nullable<bool> OmitFromReport { get; set; }
    
        public virtual Asset Asset { get; set; }
        public virtual ProjectDefect ProjectDefect { get; set; }
        public virtual Test Test { get; set; }
        public virtual TestAssetProgress TestAssetProgress { get; set; }
        public virtual TestAssetState TestAssetState { get; set; }
        public virtual TestAssetWeather TestAssetWeather { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TestAssetDefectOutlet> TestAssetDefectOutlets { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TestAssetDocument> TestAssetDocuments { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TestAssetGroup> TestAssetGroups { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TestAssetNote> TestAssetNotes { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TestEntity> TestEntities { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TestGeometryAssetLayerData> TestGeometryAssetLayerDatas { get; set; }
    }
}
