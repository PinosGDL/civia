//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Civica.Domain.CivicaDomain
{
    using System;
    using System.Collections.Generic;
    
    public partial class IMPUserReference
    {
        public string OldId { get; set; }
        public int NewId { get; set; }
        public string NewName { get; set; }
        public string InstanceName { get; set; }
    }
}
