//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Civica.Domain.CivicaDomain
{
    using System;
    using System.Collections.Generic;
    
    public partial class TestAssetGroup
    {
        public int Id { get; set; }
        public int TestAssetId { get; set; }
        public bool Active { get; set; }
        public bool IsSmoker { get; set; }
        public bool IsBlocker { get; set; }
        public int AssetId { get; set; }
    
        public virtual Asset Asset { get; set; }
        public virtual TestAsset TestAsset { get; set; }
    }
}
