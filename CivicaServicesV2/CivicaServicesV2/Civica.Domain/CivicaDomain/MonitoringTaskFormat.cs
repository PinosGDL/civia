//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Civica.Domain.CivicaDomain
{
    using System;
    using System.Collections.Generic;
    
    public partial class MonitoringTaskFormat
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MonitoringTaskFormat()
        {
            this.MonitoringTasks = new HashSet<MonitoringTask>();
        }
    
        public int Id { get; set; }
        public string Format { get; set; }
        public string Code { get; set; }
        public string DateFormats { get; set; }
        public int SortOrder { get; set; }
        public bool Active { get; set; }
        public Nullable<int> MonitoringTaskTypeId { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MonitoringTask> MonitoringTasks { get; set; }
        public virtual MonitoringTaskType MonitoringTaskType { get; set; }
    }
}
