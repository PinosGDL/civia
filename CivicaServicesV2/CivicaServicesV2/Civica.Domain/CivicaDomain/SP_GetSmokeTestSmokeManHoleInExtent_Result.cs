//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Civica.Domain.CivicaDomain
{
    using System;
    
    public partial class SP_GetSmokeTestSmokeManHoleInExtent_Result
    {
        public int Id { get; set; }
        public int TestAssetId { get; set; }
        public string TestAssetGroupName { get; set; }
        public int GeometryTypeID { get; set; }
        public string Name { get; set; }
        public string ProgressName { get; set; }
        public string StateName { get; set; }
        public string Type { get; set; }
    }
}
