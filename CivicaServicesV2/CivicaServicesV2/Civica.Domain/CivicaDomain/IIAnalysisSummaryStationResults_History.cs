//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Civica.Domain.CivicaDomain
{
    using System;
    using System.Collections.Generic;
    
    public partial class IIAnalysisSummaryStationResults_History
    {
        public int hId { get; set; }
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int ServiceId { get; set; }
        public int StationId { get; set; }
        public Nullable<decimal> ProjectedPeakIIFlow { get; set; }
        public Nullable<decimal> ProjectedPeakIIRate { get; set; }
        public Nullable<decimal> AvgFlow { get; set; }
        public Nullable<decimal> AvgFlowRate { get; set; }
        public Nullable<decimal> AvgDryWeatherFlow { get; set; }
        public Nullable<decimal> AvgDryWeatherFlowRate { get; set; }
        public Nullable<decimal> AvgpopulationDWF { get; set; }
        public Nullable<decimal> AvgpopulationDWFRate { get; set; }
        public Nullable<decimal> AvgdailypeakDWF { get; set; }
        public Nullable<decimal> AvgdailypeakDWFRate { get; set; }
        public Nullable<decimal> GroundwaterInfiltration { get; set; }
        public Nullable<decimal> GroundwaterInfiltrationRate { get; set; }
        public Nullable<decimal> MeasuredHarmonpeakfactor { get; set; }
        public Nullable<decimal> MeasuredHarmonpeakfactorRate { get; set; }
        public Nullable<decimal> TheoreticalHarmonpeakfactor { get; set; }
        public Nullable<decimal> TheoreticalHarmonpeakfactorRate { get; set; }
        public Nullable<decimal> Harmoncorrectionfactor { get; set; }
        public Nullable<decimal> HarmoncorrectionfactorRate { get; set; }
        public Nullable<decimal> Peakwetweatherflow { get; set; }
        public Nullable<decimal> PeakwetweatherflowRate { get; set; }
        public Nullable<decimal> ObservedPeakII { get; set; }
        public Nullable<decimal> ObservedPeakRateIIRate { get; set; }
        public Nullable<decimal> Peakpercentageofprecipitationinsanitary { get; set; }
        public Nullable<System.DateTimeOffset> LastRun { get; set; }
        public bool Active { get; set; }
        public Nullable<decimal> AvgDailyMinDWF { get; set; }
        public Nullable<decimal> AvgDailyMinDWFRate { get; set; }
        public Nullable<decimal> DrainageArea { get; set; }
        public Nullable<decimal> Population { get; set; }
        public Nullable<decimal> TC { get; set; }
        public Nullable<System.DateTimeOffset> LastRunStormEventStartTime { get; set; }
        public Nullable<System.DateTimeOffset> LastRunCurrentTime { get; set; }
        public int FlowSensorId { get; set; }
        public int RainGaugeSensorId { get; set; }
        public decimal InterDryPeriod { get; set; }
        public decimal MinStormSize { get; set; }
    }
}
