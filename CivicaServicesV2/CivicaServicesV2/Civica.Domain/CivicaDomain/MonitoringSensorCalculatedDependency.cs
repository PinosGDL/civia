//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Civica.Domain.CivicaDomain
{
    using System;
    using System.Collections.Generic;
    
    public partial class MonitoringSensorCalculatedDependency
    {
        public int Id { get; set; }
        public int CalculatedSensorId { get; set; }
        public int DependsOnSensorId { get; set; }
        public bool Active { get; set; }
        public System.DateTimeOffset Created { get; set; }
        public System.DateTimeOffset Modified { get; set; }
        public int CreatedBy { get; set; }
        public int ModifiedBy { get; set; }
    
        public virtual MonitoringSensor MonitoringSensor { get; set; }
        public virtual MonitoringSensor MonitoringSensor1 { get; set; }
    }
}
