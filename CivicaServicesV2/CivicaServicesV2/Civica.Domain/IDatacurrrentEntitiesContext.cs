﻿using Civica.Domain.CivicaDomain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Civica.Domain
{
    public interface IDatacurrrentEntitiesContext : IDisposable
    {
        int SaveChanges();

        DbSet<AppUserLog> AppUserLogs { get; }
        DbSet<Address> Addresses { get; }
        DbSet<AddressType> AddressTypes { get; }
        DbSet<AppUser_History> AppUser_History { get; }
        DbSet<AppUserDocument> AppUserDocuments { get; }
        DbSet<AppUserNote> AppUserNotes { get; }
        DbSet<AppUserToken> AppUserTokens { get; }
        DbSet<AreaUnit> AreaUnits { get; }
        DbSet<Asset> Assets { get; }
        DbSet<Asset_History> Asset_History { get; }
        DbSet<AssetAttributeValue> AssetAttributeValues { get; }
        DbSet<AssetCatchBasin> AssetCatchBasins { get; }
        DbSet<AssetCatchBasin_History> AssetCatchBasin_History { get; }
        DbSet<AssetCatchBasinCoverType> AssetCatchBasinCoverTypes { get; }
        DbSet<AssetCatchBasinStructureType> AssetCatchBasinStructureTypes { get; }
        DbSet<AssetCondition> AssetConditions { get; }
        DbSet<AssetCulvert> AssetCulverts { get; }
        DbSet<AssetCulvert_History> AssetCulvert_History { get; }
        DbSet<AssetCulvertType> AssetCulvertTypes { get; }
        DbSet<AssetDepreciationModel> AssetDepreciationModels { get; }
        DbSet<AssetDocument> AssetDocuments { get; }
        DbSet<AssetEquipment> AssetEquipments { get; }
        DbSet<AssetEquipmentLog> AssetEquipmentLogs { get; }
        DbSet<AssetEquipmentManufacturer> AssetEquipmentManufacturers { get; }
        DbSet<AssetEquipmentModel> AssetEquipmentModels { get; }
        DbSet<AssetEquipmentStatu> AssetEquipmentStatus { get; }
        DbSet<AssetEquipmentType> AssetEquipmentTypes { get; }
        DbSet<AssetLot> AssetLots { get; }
        DbSet<AssetLotType> AssetLotTypes { get; }
        DbSet<AssetManHole> AssetManHoles { get; }
        DbSet<AssetManHole_History> AssetManHole_History { get; }
        DbSet<AssetManHolePipeConection> AssetManHolePipeConections { get; }
        DbSet<AssetManHoleType> AssetManHoleTypes { get; }
        DbSet<AssetMaterial> AssetMaterials { get; }
        DbSet<AssetOwnershipStatu> AssetOwnershipStatus { get; }
        DbSet<AssetPond> AssetPonds { get; }
        DbSet<AssetPond_History> AssetPond_History { get; }
        DbSet<AssetPondFunction> AssetPondFunctions { get; }
        DbSet<AssetPondHydrology> AssetPondHydrologies { get; }
        DbSet<AssetPondSedimentAccumulation> AssetPondSedimentAccumulations { get; }
        DbSet<AssetPondStageDischarge> AssetPondStageDischarges { get; }
        DbSet<AssetPondType> AssetPondTypes { get; }
        DbSet<AssetSanitaryPumpStation> AssetSanitaryPumpStations { get; }
        DbSet<AssetSanitaryPumpStation_History> AssetSanitaryPumpStation_History { get; }
        DbSet<AssetSewerSegment> AssetSewerSegments { get; }
        DbSet<AssetSewerSegment_History> AssetSewerSegment_History { get; }
        DbSet<AssetSewerSegmentType> AssetSewerSegmentTypes { get; }
        DbSet<AssetShape> AssetShapes { get; }
        DbSet<AssetsNote> AssetsNotes { get; }
        DbSet<AssetStatu> AssetStatus { get; }
        DbSet<AssetSurfaceType> AssetSurfaceTypes { get; }
        DbSet<AssetType> AssetTypes { get; }
        DbSet<AssetWebCam> AssetWebCams { get; }
        DbSet<AssetWebCamLease> AssetWebCamLeases { get; }
        DbSet<CoordinatSystem> CoordinatSystems { get; }
        DbSet<Country> Countries { get; }
        DbSet<DataExportHost> DataExportHosts { get; }
        DbSet<DataExportSensor> DataExportSensors { get; }
        DbSet<DefaultLayerStyle> DefaultLayerStyles { get; }
        DbSet<DefaultLayerType> DefaultLayerTypes { get; }
        DbSet<DefaultMunicipalityBoundaryLayerData> DefaultMunicipalityBoundaryLayerDatas { get; }
        DbSet<DefaultMunicipalityBoundaryLayer> DefaultMunicipalityBoundaryLayers { get; }
        DbSet<DefectDischarge> DefectDischarges { get; }
        DbSet<DefectDrainageDirection> DefectDrainageDirections { get; }
        DbSet<DefectProgress> DefectProgresses { get; }
        DbSet<DefectSourceOutlet> DefectSourceOutlets { get; }
        DbSet<DefectSourceOutletDyeIntensity> DefectSourceOutletDyeIntensities { get; }
        DbSet<DefectSourceOutletType> DefectSourceOutletTypes { get; }
        DbSet<DefectState> DefectStates { get; }
        DbSet<DefectType> DefectTypes { get; }
        DbSet<DesignStorm> DesignStorms { get; }
        DbSet<DesignStormDuration> DesignStormDurations { get; }
        DbSet<DesignStormEquation> DesignStormEquations { get; }
        DbSet<DesignStormEquationType> DesignStormEquationTypes { get; }
        DbSet<DesignStormReturnInterval> DesignStormReturnIntervals { get; }
        DbSet<DocumentStorageType> DocumentStorageTypes { get; }
        DbSet<DocumentType> DocumentTypes { get; }
        DbSet<FlowVolumeRelation> FlowVolumeRelations { get; }
        DbSet<GeometryLayerCategory> GeometryLayerCategories { get; }
        DbSet<GeometryLayerData> GeometryLayerDatas { get; }
        DbSet<GeometryLayer> GeometryLayers { get; }
        DbSet<GeometryLayerType> GeometryLayerTypes { get; }
        DbSet<HomePageType> HomePageTypes { get; }
        DbSet<IIAnalysisSummaryEventStat> IIAnalysisSummaryEventStats { get; }
        DbSet<IIAnalysisSummaryEventStats_Hisotry> IIAnalysisSummaryEventStats_Hisotry { get; }
        DbSet<IIAnalysisSummaryGWIEOption> IIAnalysisSummaryGWIEOptions { get; }
        DbSet<IIAnalysisSummaryProjectSetting> IIAnalysisSummaryProjectSettings { get; }
        DbSet<IIAnalysisSummaryProjectSettings_History> IIAnalysisSummaryProjectSettings_History { get; }
        DbSet<IIAnalysisSummaryReturnPeriod> IIAnalysisSummaryReturnPeriods { get; }
        DbSet<IIAnalysisSummaryStationResult> IIAnalysisSummaryStationResults { get; }
        DbSet<IIAnalysisSummaryStationResults_History> IIAnalysisSummaryStationResults_History { get; }
        DbSet<IIAnalysisSummaryStationSetting> IIAnalysisSummaryStationSettings { get; }
        DbSet<IIAnalysisSummaryStationSettings_History> IIAnalysisSummaryStationSettings_History { get; }
        DbSet<IIAnalysisSummaryStormEvent> IIAnalysisSummaryStormEvents { get; }
        DbSet<IIAnalysisSummaryStormEvent_History> IIAnalysisSummaryStormEvent_History { get; }
        DbSet<IMPClientReference> IMPClientReferences { get; }
        DbSet<IMPProjectReference> IMPProjectReferences { get; }
        DbSet<IMPSensorReference> IMPSensorReferences { get; }
        DbSet<IMPSensorStormReference> IMPSensorStormReferences { get; }
        DbSet<IMPSiteStationReference> IMPSiteStationReferences { get; }
        DbSet<IMPSiteVisitReference> IMPSiteVisitReferences { get; }
        DbSet<IMPUserReference> IMPUserReferences { get; }
        DbSet<LayerSymbologyType> LayerSymbologyTypes { get; }
        DbSet<MeasuringStationAlarmLog> MeasuringStationAlarmLogs { get; }
        DbSet<MenuItem> MenuItems { get; }
        DbSet<MenuItemType> MenuItemTypes { get; }
        DbSet<MonitoringData> MonitoringDatas { get; }
        DbSet<MonitoringDataImportLog> MonitoringDataImportLogs { get; }
        DbSet<MonitoringDataRaw> MonitoringDataRaws { get; }
        DbSet<MonitoringDataReactorBlueTreeLog> MonitoringDataReactorBlueTreeLogs { get; }
        DbSet<MonitoringDataReactorBlueTreeMessageType> MonitoringDataReactorBlueTreeMessageTypes { get; }
        DbSet<MonitoringLogger> MonitoringLoggers { get; }
        DbSet<MonitoringLoggerChannel> MonitoringLoggerChannels { get; }
        DbSet<MonitoringLoggerServer> MonitoringLoggerServers { get; }
        DbSet<MonitoringLoggerServerType> MonitoringLoggerServerTypes { get; }
        DbSet<MonitoringLoggerServiceData> MonitoringLoggerServiceDatas { get; }
        DbSet<MonitoringLoggerType> MonitoringLoggerTypes { get; }
        DbSet<MonitoringSensor> MonitoringSensors { get; }
        DbSet<MonitoringSensor_History> MonitoringSensor_History { get; }
        DbSet<MonitoringSensorAlarm> MonitoringSensorAlarms { get; }
        DbSet<MonitoringSensorAnalysisSetting> MonitoringSensorAnalysisSettings { get; }
        DbSet<MonitoringSensorAnotation> MonitoringSensorAnotations { get; }
        DbSet<MonitoringSensorCalculatedDependency> MonitoringSensorCalculatedDependencies { get; }
        DbSet<MonitoringSensorCleaningSetting> MonitoringSensorCleaningSettings { get; }
        DbSet<MonitoringSensorDataType> MonitoringSensorDataTypes { get; }
        DbSet<MonitoringSensorDesignStorm> MonitoringSensorDesignStorms { get; }
        DbSet<MonitoringSensorEquation> MonitoringSensorEquations { get; }
        DbSet<MonitoringSensorImportExportMap> MonitoringSensorImportExportMaps { get; }
        DbSet<MonitoringSensorOffsitePoint> MonitoringSensorOffsitePoints { get; }
        DbSet<MonitoringSensorStaticData> MonitoringSensorStaticDatas { get; }
        DbSet<MonitoringSensorStatu> MonitoringSensorStatus { get; }
        DbSet<MonitoringSensorType> MonitoringSensorTypes { get; }
        DbSet<MonitoringSensorUser> MonitoringSensorUsers { get; }
        DbSet<MonitoringSensorUsers_History> MonitoringSensorUsers_History { get; }
        DbSet<MonitoringService> MonitoringServices { get; }
        DbSet<MonitoringService_History> MonitoringService_History { get; }
        DbSet<MonitoringServiceBoundary> MonitoringServiceBoundaries { get; }
        DbSet<MonitoringServiceGeometryLayerData> MonitoringServiceGeometryLayerDatas { get; }
        DbSet<MonitoringServiceGeometryLayer> MonitoringServiceGeometryLayers { get; }
        DbSet<MonitoringServiceLayerGroup> MonitoringServiceLayerGroups { get; }
        DbSet<MonitoringServiceStation> MonitoringServiceStations { get; }
        DbSet<MonitoringStation> MonitoringStations { get; }
        DbSet<MonitoringStation_History> MonitoringStation_History { get; }
        DbSet<MonitoringStationActivity> MonitoringStationActivities { get; }
        DbSet<MonitoringStationAlarmCondition> MonitoringStationAlarmConditions { get; }
        DbSet<MonitoringStationAlarmConditionType> MonitoringStationAlarmConditionTypes { get; }
        DbSet<MonitoringStationAlarmLog> MonitoringStationAlarmLogs { get; }
        DbSet<MonitoringStationAlarmSeverity> MonitoringStationAlarmSeverities { get; }
        DbSet<MonitoringStationAlarmType> MonitoringStationAlarmTypes { get; }
        DbSet<MonitoringStationCatchmentArea> MonitoringStationCatchmentAreas { get; }
        DbSet<MonitoringStationDocument> MonitoringStationDocuments { get; }
        DbSet<MonitoringStationManualMeasurment> MonitoringStationManualMeasurments { get; }
        DbSet<MonitoringStationNote> MonitoringStationNotes { get; }
        DbSet<MonitoringStationPopulation> MonitoringStationPopulations { get; }
        DbSet<MonitoringStationsMap> MonitoringStationsMaps { get; }
        DbSet<MonitoringStationsOffSiteSensor> MonitoringStationsOffSiteSensors { get; }
        DbSet<MonitoringStationState> MonitoringStationStates { get; }
        DbSet<MonitoringStationTC> MonitoringStationTCs { get; }
        DbSet<MonitoringStationTrafficLevel> MonitoringStationTrafficLevels { get; }
        DbSet<MonitoringStationType> MonitoringStationTypes { get; }
        DbSet<MonitoringTaskFormat> MonitoringTaskFormats { get; }
        DbSet<MonitoringTaskPriority> MonitoringTaskPriorities { get; }
        DbSet<MonitoringTaskStatu> MonitoringTaskStatus { get; }
        DbSet<MonitoringTaskType> MonitoringTaskTypes { get; }
        DbSet<MonitoringUnitConversion> MonitoringUnitConversions { get; }
        DbSet<ObservationType> ObservationTypes { get; }
        DbSet<Organization> Organizations { get; }
        DbSet<Organization_History> Organization_History { get; }
        DbSet<OrganizationDefaultUnitSet> OrganizationDefaultUnitSets { get; }
        DbSet<OrganizationDefectType> OrganizationDefectTypes { get; }
        DbSet<OrganizationDocument> OrganizationDocuments { get; }
        DbSet<OrganizationGeometryAssetLayerData> OrganizationGeometryAssetLayerDatas { get; }
        DbSet<OrganizationGeometryLayer> OrganizationGeometryLayers { get; }
        DbSet<OrganizationLayerGroup> OrganizationLayerGroups { get; }
        DbSet<OrganizationLocation> OrganizationLocations { get; }
        DbSet<OrganizationPermission> OrganizationPermissions { get; }
        DbSet<OrganizationPermission_History> OrganizationPermission_History { get; }
        DbSet<OrganizationPersonelType> OrganizationPersonelTypes { get; }
        DbSet<OrganizationRemediationType> OrganizationRemediationTypes { get; }
        DbSet<OrganizationRolePermission> OrganizationRolePermissions { get; }
        DbSet<OrganizationRole> OrganizationRoles { get; }
        DbSet<OrganizationService> OrganizationServices { get; }
        DbSet<OrganizationSiteVisitDefaultTemplate> OrganizationSiteVisitDefaultTemplates { get; }
        DbSet<OrganizationSiteVisitTemplate> OrganizationSiteVisitTemplates { get; }
        DbSet<OrganizationSiteVisitTemplateField> OrganizationSiteVisitTemplateFields { get; }
        DbSet<OrganizationSiteVisitTemplateFieldOption> OrganizationSiteVisitTemplateFieldOptions { get; }
        DbSet<OrganizationSiteVisitTemplateFieldType> OrganizationSiteVisitTemplateFieldTypes { get; }
        DbSet<OrganizationTask> OrganizationTasks { get; }
        DbSet<OrganizationTestType> OrganizationTestTypes { get; }
        DbSet<OrganizationTool> OrganizationTools { get; }
        DbSet<OrganizationType> OrganizationTypes { get; }
        DbSet<OrganizationUserRole> OrganizationUserRoles { get; }
        DbSet<OrganizationUserRoles_History> OrganizationUserRoles_History { get; }
        DbSet<PartnerOrganization> PartnerOrganizations { get; }
        DbSet<Permission> Permissions { get; }
        DbSet<Project> Projects { get; }
        DbSet<Project_History> Project_History { get; }
        DbSet<ProjectAsset> ProjectAssets { get; }
        DbSet<ProjectBoundary> ProjectBoundaries { get; }
        DbSet<ProjectDefect> ProjectDefects { get; }
        DbSet<ProjectDefect_History> ProjectDefect_History { get; }
        DbSet<ProjectDefectDocument> ProjectDefectDocuments { get; }
        DbSet<ProjectDefectNote> ProjectDefectNotes { get; }
        DbSet<ProjectDefectOutlet> ProjectDefectOutlets { get; }
        DbSet<ProjectDocument> ProjectDocuments { get; }
        DbSet<ProjectGeometryAssetLayerData> ProjectGeometryAssetLayerDatas { get; }
        DbSet<ProjectGeometryLayer> ProjectGeometryLayers { get; }
        DbSet<ProjectLayerGroup> ProjectLayerGroups { get; }
        DbSet<ProjectLotInspectionResult> ProjectLotInspectionResults { get; }
        DbSet<ProjectLotInspectionResult_History> ProjectLotInspectionResult_History { get; }
        DbSet<ProjectLotInspectionResultDocument> ProjectLotInspectionResultDocuments { get; }
        DbSet<ProjectLotInspectionResultNote> ProjectLotInspectionResultNotes { get; }
        DbSet<ProjectNote> ProjectNotes { get; }
        DbSet<ProjectObservation> ProjectObservations { get; }
        DbSet<ProjectOrganization> ProjectOrganizations { get; }
        DbSet<ProjectResource> ProjectResources { get; }
        DbSet<ProjectServiceDocument> ProjectServiceDocuments { get; }
        DbSet<ProjectServiceNote> ProjectServiceNotes { get; }
        DbSet<ProjectService> ProjectServices { get; }
        DbSet<ProjectServices_History> ProjectServices_History { get; }
        DbSet<ProjectServiceUser> ProjectServiceUsers { get; }
        DbSet<ProjectType> ProjectTypes { get; }
        DbSet<ProjectUser> ProjectUsers { get; }
        DbSet<Province> Provinces { get; }
        DbSet<RemediationType> RemediationTypes { get; }
        DbSet<ResourceType> ResourceTypes { get; }
        DbSet<ResourceTypeUnit> ResourceTypeUnits { get; }
        DbSet<ServiceLayerType> ServiceLayerTypes { get; }
        DbSet<ServiceLayerTypeConditionRanx> ServiceLayerTypeConditionRanges { get; }
        DbSet<ServiceLayerTypeCondition> ServiceLayerTypeConditions { get; }
        DbSet<ServiceProgress> ServiceProgresses { get; }
        DbSet<Service> Services { get; }
        DbSet<ServiceType> ServiceTypes { get; }
        DbSet<ShapeFileAttributeMapp> ShapeFileAttributeMapps { get; }
        DbSet<ShapeFileTask> ShapeFileTasks { get; }
        DbSet<SiteVisit> SiteVisits { get; }
        DbSet<SiteVisitDocument> SiteVisitDocuments { get; }
        DbSet<SiteVisitField> SiteVisitFields { get; }
        DbSet<SiteVisitNote> SiteVisitNotes { get; }
        DbSet<SiteVisitUser> SiteVisitUsers { get; }
        DbSet<StreetClass> StreetClasses { get; }
        DbSet<TaskType> TaskTypes { get; }
        DbSet<Test> Tests { get; }
        DbSet<Test_History> Test_History { get; }
        DbSet<TestAccessibilityType> TestAccessibilityTypes { get; }
        DbSet<TestAsset> TestAssets { get; }
        DbSet<TestAsset_History> TestAsset_History { get; }
        DbSet<TestAssetDefectOutLetPercentage> TestAssetDefectOutLetPercentages { get; }
        DbSet<TestAssetDefectOutlet> TestAssetDefectOutlets { get; }
        DbSet<TestAssetDefectOutlets_History> TestAssetDefectOutlets_History { get; }
        DbSet<TestAssetDocument> TestAssetDocuments { get; }
        DbSet<TestAssetGroup> TestAssetGroups { get; }
        DbSet<TestAssetLot> TestAssetLots { get; }
        DbSet<TestAssetNote> TestAssetNotes { get; }
        DbSet<TestAssetProgress> TestAssetProgresses { get; }
        DbSet<TestAssetState> TestAssetStates { get; }
        DbSet<TestAssetWeather> TestAssetWeathers { get; }
        DbSet<TestAssetWeather_History> TestAssetWeather_History { get; }
        DbSet<TestBoundary> TestBoundaries { get; }
        DbSet<TestContact> TestContacts { get; }
        DbSet<TestEnityWeather> TestEnityWeathers { get; }
        DbSet<TestEnityWeather_History> TestEnityWeather_History { get; }
        DbSet<TestEntity> TestEntities { get; }
        DbSet<TestEntities_History> TestEntities_History { get; }
        DbSet<TestEntityDefect> TestEntityDefects { get; }
        DbSet<TestEntityDefects_History> TestEntityDefects_History { get; }
        DbSet<TestEntityDocument> TestEntityDocuments { get; }
        DbSet<TestEntityLotInspectionResult> TestEntityLotInspectionResults { get; }
        DbSet<TestEntityNote> TestEntityNotes { get; }
        DbSet<TestGeometryAssetLayerData> TestGeometryAssetLayerDatas { get; }
        DbSet<TestGeometryBaseLayerData> TestGeometryBaseLayerDatas { get; }
        DbSet<TestGeometryLayer> TestGeometryLayers { get; }
        DbSet<TestLayerGroup> TestLayerGroups { get; }
        DbSet<TestType> TestTypes { get; }
        DbSet<TimeInterval> TimeIntervals { get; }
        //DbSet<TimeZone> TimeZones { get; }
        DbSet<Tool> Tools { get; }
        DbSet<ToolType> ToolTypes { get; }
        DbSet<Toronto_Area39_Projected_LatLong> Toronto_Area39_Projected_LatLong { get; }
        DbSet<TorontoRainfallTask> TorontoRainfallTasks { get; }
        DbSet<TrafficLevel> TrafficLevels { get; }
        DbSet<Unit> Units { get; }
        DbSet<UnitType> UnitTypes { get; }
        DbSet<WeatherType> WeatherTypes { get; }
        DbSet<WindDirection> WindDirections { get; }
        DbSet<WindType> WindTypes { get; }
        DbSet<WorkOrderPriority> WorkOrderPriorities { get; }
        DbSet<WorkOrderStatu> WorkOrderStatus { get; }
        DbSet<WorkOrderType> WorkOrderTypes { get; }
        DbSet<Toronto_SAN_Sewer> Toronto_SAN_Sewer { get; }
        DbSet<Toronto_SAN_MH> Toronto_SAN_MH { get; }
        DbSet<tempView_KawarthaMonitoringStations_Boundary> tempView_KawarthaMonitoringStations_Boundary { get; }
        DbSet<tempView_KawarthaMonitoringStations_Points> tempView_KawarthaMonitoringStations_Points { get; }
        DbSet<ViewLotInspectionLotPolygon> ViewLotInspectionLotPolygons { get; }
        DbSet<ViewLotsInspectionPointsOfInterest> ViewLotsInspectionPointsOfInterests { get; }
        DbSet<ViewManholeInspectionProgress> ViewManholeInspectionProgresses { get; }
        DbSet<ViewMenuItemAccess> ViewMenuItemAccesses { get; }
        DbSet<ViewMonitoringStationBoundary> ViewMonitoringStationBoundaries { get; }
        DbSet<ViewMonitoringStationPoint> ViewMonitoringStationPoints { get; }
        DbSet<ViewMonitoringTaskList> ViewMonitoringTaskLists { get; }
        DbSet<ViewPointsOfInterestCentroid> ViewPointsOfInterestCentroids { get; }
        DbSet<ViewSmokeTestDefectBoundary> ViewSmokeTestDefectBoundaries { get; }
        DbSet<ViewSmokeTestDefectCentroid> ViewSmokeTestDefectCentroids { get; }
        DbSet<ViewSmokeTestGroupBoundary> ViewSmokeTestGroupBoundaries { get; }
        DbSet<ViewSmokeTestGroupManhole> ViewSmokeTestGroupManholes { get; }
        DbSet<ViewSmokeTestGroupSewerLine> ViewSmokeTestGroupSewerLines { get; }
        DbSet<ViewTestDefec> ViewTestDefecs { get; }
        DbSet<ViewTestDefecsOutlet> ViewTestDefecsOutlets { get; }
        DbSet<MonitoringTask> MonitoringTasks { get; }
        DbSet<MonitoringTaskExportDataType> MonitoringTaskExportDataTypes { get; }
        DbSet<MonitoringTaskSensor> MonitoringTaskSensors { get; }
        DbSet<AppUser> AppUsers { get; }
        DbSet<AlarmAckinfo> AlarmAckinfoes { get; }
        DbSet<MonitoringStationAlarm> MonitoringStationAlarms { get; }
        DbSet<MonitoringStationAlarmConditionLog> MonitoringStationAlarmConditionLogs { get; }
    }

    public class DatacurrrentEntitiesContext : DbContext, IDatacurrrentEntitiesContext
    {
        datacurrentEntities model = new datacurrentEntities();

        public DbSet<AppUserLog> AppUserLogs
        {
            get
            {
                return model.AppUserLogs;
            }
        }
        public DbSet<Address> Addresses
        {
            get
            {
                return model.Addresses;
            }
        }
        public DbSet<AddressType> AddressTypes
        {
            get
            {
                return model.AddressTypes;
            }
        }
        public DbSet<AppUser_History> AppUser_History
        {
            get
            {
                return model.AppUser_History;
            }
        }
        public DbSet<AppUserDocument> AppUserDocuments
        {
            get { return model.AppUserDocuments; }
        }
        public DbSet<AppUserNote> AppUserNotes { get { return model.AppUserNotes; } }
        public DbSet<AppUserToken> AppUserTokens { get { return model.AppUserTokens; } }
        public DbSet<AreaUnit> AreaUnits { get { return model.AreaUnits; } }
        public DbSet<Asset> Assets { get { return model.Assets; } }
        public DbSet<Asset_History> Asset_History { get { return model.Asset_History; } }
        public DbSet<AssetAttributeValue> AssetAttributeValues { get { return model.AssetAttributeValues; } }
        public DbSet<AssetCatchBasin> AssetCatchBasins { get { return model.AssetCatchBasins; } }
        public DbSet<AssetCatchBasin_History> AssetCatchBasin_History { get { return model.AssetCatchBasin_History; } }
        public DbSet<AssetCatchBasinCoverType> AssetCatchBasinCoverTypes { get { return model.AssetCatchBasinCoverTypes; } }
        public DbSet<AssetCatchBasinStructureType> AssetCatchBasinStructureTypes { get { return model.AssetCatchBasinStructureTypes; } }
        public DbSet<AssetCondition> AssetConditions { get { return model.AssetConditions; } }
        public DbSet<AssetCulvert> AssetCulverts { get { return model.AssetCulverts; } }
        public DbSet<AssetCulvert_History> AssetCulvert_History { get { return model.AssetCulvert_History; } }
        public DbSet<AssetCulvertType> AssetCulvertTypes { get { return model.AssetCulvertTypes; } }
        public DbSet<AssetDepreciationModel> AssetDepreciationModels { get { return model.AssetDepreciationModels; } }
        public DbSet<AssetDocument> AssetDocuments { get { return model.AssetDocuments; } }
        public DbSet<AssetEquipment> AssetEquipments { get { return model.AssetEquipments; } }
        public DbSet<AssetEquipmentLog> AssetEquipmentLogs { get { return model.AssetEquipmentLogs; } }
        public DbSet<AssetEquipmentManufacturer> AssetEquipmentManufacturers { get { return model.AssetEquipmentManufacturers; } }
        public DbSet<AssetEquipmentModel> AssetEquipmentModels { get { return model.AssetEquipmentModels; } }
        public DbSet<AssetEquipmentStatu> AssetEquipmentStatus { get { return model.AssetEquipmentStatus; } }
        public DbSet<AssetEquipmentType> AssetEquipmentTypes { get { return model.AssetEquipmentTypes; } }
        public DbSet<AssetLot> AssetLots { get { return model.AssetLots; } }
        public DbSet<AssetLotType> AssetLotTypes { get { return model.AssetLotTypes; } }
        public DbSet<AssetManHole> AssetManHoles { get { return model.AssetManHoles; } }
        public DbSet<AssetManHole_History> AssetManHole_History { get { return model.AssetManHole_History; } }
        public DbSet<AssetManHolePipeConection> AssetManHolePipeConections { get { return model.AssetManHolePipeConections; } }
        public DbSet<AssetManHoleType> AssetManHoleTypes { get { return model.AssetManHoleTypes; } }
        public DbSet<AssetMaterial> AssetMaterials { get { return model.AssetMaterials; } }
        public DbSet<AssetOwnershipStatu> AssetOwnershipStatus { get { return model.AssetOwnershipStatus; } }
        public DbSet<AssetPond> AssetPonds { get { return model.AssetPonds; } }
        public DbSet<AssetPond_History> AssetPond_History { get { return model.AssetPond_History; } }
        public DbSet<AssetPondFunction> AssetPondFunctions { get { return model.AssetPondFunctions; } }
        public DbSet<AssetPondHydrology> AssetPondHydrologies { get { return model.AssetPondHydrologies; } }
        public DbSet<AssetPondSedimentAccumulation> AssetPondSedimentAccumulations { get { return model.AssetPondSedimentAccumulations; } }
        public DbSet<AssetPondStageDischarge> AssetPondStageDischarges { get { return model.AssetPondStageDischarges; } }
        public DbSet<AssetPondType> AssetPondTypes { get { return model.AssetPondTypes; } }
        public DbSet<AssetSanitaryPumpStation> AssetSanitaryPumpStations { get { return model.AssetSanitaryPumpStations; } }
        public DbSet<AssetSanitaryPumpStation_History> AssetSanitaryPumpStation_History { get { return model.AssetSanitaryPumpStation_History; } }
        public DbSet<AssetSewerSegment> AssetSewerSegments { get { return model.AssetSewerSegments; } }
        public DbSet<AssetSewerSegment_History> AssetSewerSegment_History { get { return model.AssetSewerSegment_History; } }
        public DbSet<AssetSewerSegmentType> AssetSewerSegmentTypes { get { return model.AssetSewerSegmentTypes; } }
        public DbSet<AssetShape> AssetShapes { get { return model.AssetShapes; } }
        public DbSet<AssetsNote> AssetsNotes { get { return model.AssetsNotes; } }
        public DbSet<AssetStatu> AssetStatus { get { return model.AssetStatus; } }
        public DbSet<AssetSurfaceType> AssetSurfaceTypes { get { return model.AssetSurfaceTypes; } }
        public DbSet<AssetType> AssetTypes { get { return model.AssetTypes; } }
        public DbSet<AssetWebCam> AssetWebCams { get { return model.AssetWebCams; } }
        public DbSet<AssetWebCamLease> AssetWebCamLeases { get { return model.AssetWebCamLeases; } }
        public DbSet<CoordinatSystem> CoordinatSystems { get { return model.CoordinatSystems; } }
        public DbSet<Country> Countries { get { return model.Countries; } }
        public DbSet<DataExportHost> DataExportHosts { get { return model.DataExportHosts; } }
        public DbSet<DataExportSensor> DataExportSensors { get { return model.DataExportSensors; } }
        public DbSet<DefaultLayerStyle> DefaultLayerStyles { get { return model.DefaultLayerStyles; } }
        public DbSet<DefaultLayerType> DefaultLayerTypes { get { return model.DefaultLayerTypes; } }
        public DbSet<DefaultMunicipalityBoundaryLayerData> DefaultMunicipalityBoundaryLayerDatas { get { return model.DefaultMunicipalityBoundaryLayerDatas; } }
        public DbSet<DefaultMunicipalityBoundaryLayer> DefaultMunicipalityBoundaryLayers { get { return model.DefaultMunicipalityBoundaryLayers; } }
        public DbSet<DefectDischarge> DefectDischarges { get { return model.DefectDischarges; } }
        public DbSet<DefectDrainageDirection> DefectDrainageDirections { get { return model.DefectDrainageDirections; } }
        public DbSet<DefectProgress> DefectProgresses { get { return model.DefectProgresses; } }
        public DbSet<DefectSourceOutlet> DefectSourceOutlets { get { return model.DefectSourceOutlets; } }
        public DbSet<DefectSourceOutletDyeIntensity> DefectSourceOutletDyeIntensities { get { return model.DefectSourceOutletDyeIntensities; } }
        public DbSet<DefectSourceOutletType> DefectSourceOutletTypes { get { return model.DefectSourceOutletTypes; } }
        public DbSet<DefectState> DefectStates { get { return model.DefectStates; } }
        public DbSet<DefectType> DefectTypes { get { return model.DefectTypes; } }
        public DbSet<DesignStorm> DesignStorms { get { return model.DesignStorms; } }
        public DbSet<DesignStormDuration> DesignStormDurations { get { return model.DesignStormDurations; } }
        public DbSet<DesignStormEquation> DesignStormEquations { get { return model.DesignStormEquations; } }
        public DbSet<DesignStormEquationType> DesignStormEquationTypes { get { return model.DesignStormEquationTypes; } }
        public DbSet<DesignStormReturnInterval> DesignStormReturnIntervals { get { return model.DesignStormReturnIntervals; } }
        public DbSet<DocumentStorageType> DocumentStorageTypes { get { return model.DocumentStorageTypes; } }
        public DbSet<DocumentType> DocumentTypes { get { return model.DocumentTypes; } }
        public DbSet<FlowVolumeRelation> FlowVolumeRelations { get { return model.FlowVolumeRelations; } }
        public DbSet<GeometryLayerCategory> GeometryLayerCategories { get { return model.GeometryLayerCategories; } }
        public DbSet<GeometryLayerData> GeometryLayerDatas { get { return model.GeometryLayerDatas; } }
        public DbSet<GeometryLayer> GeometryLayers { get { return model.GeometryLayers; } }
        public DbSet<GeometryLayerType> GeometryLayerTypes { get { return model.GeometryLayerTypes; } }
        public DbSet<HomePageType> HomePageTypes { get { return model.HomePageTypes; } }
        public DbSet<IIAnalysisSummaryEventStat> IIAnalysisSummaryEventStats { get { return model.IIAnalysisSummaryEventStats; } }
        public DbSet<IIAnalysisSummaryEventStats_Hisotry> IIAnalysisSummaryEventStats_Hisotry { get { return model.IIAnalysisSummaryEventStats_Hisotry; } }
        public DbSet<IIAnalysisSummaryGWIEOption> IIAnalysisSummaryGWIEOptions { get { return model.IIAnalysisSummaryGWIEOptions; } }
        public DbSet<IIAnalysisSummaryProjectSetting> IIAnalysisSummaryProjectSettings { get { return model.IIAnalysisSummaryProjectSettings; } }
        public DbSet<IIAnalysisSummaryProjectSettings_History> IIAnalysisSummaryProjectSettings_History { get { return model.IIAnalysisSummaryProjectSettings_History; } }
        public DbSet<IIAnalysisSummaryReturnPeriod> IIAnalysisSummaryReturnPeriods { get { return model.IIAnalysisSummaryReturnPeriods; } }
        public DbSet<IIAnalysisSummaryStationResult> IIAnalysisSummaryStationResults { get { return model.IIAnalysisSummaryStationResults; } }
        public DbSet<IIAnalysisSummaryStationResults_History> IIAnalysisSummaryStationResults_History { get { return model.IIAnalysisSummaryStationResults_History; } }
        public DbSet<IIAnalysisSummaryStationSetting> IIAnalysisSummaryStationSettings { get { return model.IIAnalysisSummaryStationSettings; } }
        public DbSet<IIAnalysisSummaryStationSettings_History> IIAnalysisSummaryStationSettings_History { get { return model.IIAnalysisSummaryStationSettings_History; } }
        public DbSet<IIAnalysisSummaryStormEvent> IIAnalysisSummaryStormEvents { get { return model.IIAnalysisSummaryStormEvents; } }
        public DbSet<IIAnalysisSummaryStormEvent_History> IIAnalysisSummaryStormEvent_History { get { return model.IIAnalysisSummaryStormEvent_History; } }
        public DbSet<IMPClientReference> IMPClientReferences { get { return model.IMPClientReferences; } }
        public DbSet<IMPProjectReference> IMPProjectReferences { get { return model.IMPProjectReferences; } }
        public DbSet<IMPSensorReference> IMPSensorReferences { get { return model.IMPSensorReferences; } }
        public DbSet<IMPSensorStormReference> IMPSensorStormReferences { get { return model.IMPSensorStormReferences; } }
        public DbSet<IMPSiteStationReference> IMPSiteStationReferences { get { return model.IMPSiteStationReferences; } }
        public DbSet<IMPSiteVisitReference> IMPSiteVisitReferences { get { return model.IMPSiteVisitReferences; } }
        public DbSet<IMPUserReference> IMPUserReferences { get { return model.IMPUserReferences; } }
        public DbSet<LayerSymbologyType> LayerSymbologyTypes { get { return model.LayerSymbologyTypes; } }
        public DbSet<MeasuringStationAlarmLog> MeasuringStationAlarmLogs { get { return model.MeasuringStationAlarmLogs; } }
        public DbSet<MenuItem> MenuItems { get { return model.MenuItems; } }
        public DbSet<MenuItemType> MenuItemTypes { get { return model.MenuItemTypes; } }

        public DbSet<MonitoringData> MonitoringDatas
        {
            get
            {
                return model.MonitoringDatas;
            }
        }

        public DbSet<MonitoringDataImportLog> MonitoringDataImportLogs { get { return model.MonitoringDataImportLogs; } }
        public DbSet<MonitoringDataRaw> MonitoringDataRaws { get { return model.MonitoringDataRaws; } }
        public DbSet<MonitoringDataReactorBlueTreeLog> MonitoringDataReactorBlueTreeLogs { get { return model.MonitoringDataReactorBlueTreeLogs; } }
        public DbSet<MonitoringDataReactorBlueTreeMessageType> MonitoringDataReactorBlueTreeMessageTypes { get { return model.MonitoringDataReactorBlueTreeMessageTypes; } }
        public DbSet<MonitoringLogger> MonitoringLoggers { get { return model.MonitoringLoggers; } }
        public DbSet<MonitoringLoggerChannel> MonitoringLoggerChannels { get { return model.MonitoringLoggerChannels; } }
        public DbSet<MonitoringLoggerServer> MonitoringLoggerServers { get { return model.MonitoringLoggerServers; } }
        public DbSet<MonitoringLoggerServerType> MonitoringLoggerServerTypes { get { return model.MonitoringLoggerServerTypes; } }
        public DbSet<MonitoringLoggerServiceData> MonitoringLoggerServiceDatas { get { return model.MonitoringLoggerServiceDatas; } }
        public DbSet<MonitoringLoggerType> MonitoringLoggerTypes { get { return model.MonitoringLoggerTypes; } }
        public DbSet<MonitoringSensor> MonitoringSensors { get { return model.MonitoringSensors; } }
        public DbSet<MonitoringSensor_History> MonitoringSensor_History { get { return model.MonitoringSensor_History; } }

        public DbSet<MonitoringSensorAlarm> MonitoringSensorAlarms
        {
            get
            {
                return model.MonitoringSensorAlarms;
            }
        }

        public DbSet<MonitoringSensorAnalysisSetting> MonitoringSensorAnalysisSettings { get { return model.MonitoringSensorAnalysisSettings; } }
        public DbSet<MonitoringSensorAnotation> MonitoringSensorAnotations { get { return model.MonitoringSensorAnotations; } }
        public DbSet<MonitoringSensorCalculatedDependency> MonitoringSensorCalculatedDependencies { get { return model.MonitoringSensorCalculatedDependencies; } }
        public DbSet<MonitoringSensorCleaningSetting> MonitoringSensorCleaningSettings { get { return model.MonitoringSensorCleaningSettings; } }
        public DbSet<MonitoringSensorDataType> MonitoringSensorDataTypes { get { return model.MonitoringSensorDataTypes; } }
        public DbSet<MonitoringSensorDesignStorm> MonitoringSensorDesignStorms { get { return model.MonitoringSensorDesignStorms; } }
        public DbSet<MonitoringSensorEquation> MonitoringSensorEquations { get { return model.MonitoringSensorEquations; } }
        public DbSet<MonitoringSensorImportExportMap> MonitoringSensorImportExportMaps { get { return model.MonitoringSensorImportExportMaps; } }
        public DbSet<MonitoringSensorOffsitePoint> MonitoringSensorOffsitePoints { get { return model.MonitoringSensorOffsitePoints; } }
        public DbSet<MonitoringSensorStaticData> MonitoringSensorStaticDatas { get { return model.MonitoringSensorStaticDatas; } }
        public DbSet<MonitoringSensorStatu> MonitoringSensorStatus { get { return model.MonitoringSensorStatus; } }
        public DbSet<MonitoringSensorType> MonitoringSensorTypes { get { return model.MonitoringSensorTypes; } }
        public DbSet<MonitoringSensorUser> MonitoringSensorUsers { get { return model.MonitoringSensorUsers; } }
        public DbSet<MonitoringSensorUsers_History> MonitoringSensorUsers_History { get { return model.MonitoringSensorUsers_History; } }
        public DbSet<MonitoringService> MonitoringServices { get { return model.MonitoringServices; } }
        public DbSet<MonitoringService_History> MonitoringService_History { get { return model.MonitoringService_History; } }
        public DbSet<MonitoringServiceBoundary> MonitoringServiceBoundaries { get { return model.MonitoringServiceBoundaries; } }
        public DbSet<MonitoringServiceGeometryLayerData> MonitoringServiceGeometryLayerDatas { get { return model.MonitoringServiceGeometryLayerDatas; } }
        public DbSet<MonitoringServiceGeometryLayer> MonitoringServiceGeometryLayers { get { return model.MonitoringServiceGeometryLayers; } }
        public DbSet<MonitoringServiceLayerGroup> MonitoringServiceLayerGroups { get { return model.MonitoringServiceLayerGroups; } }
        public DbSet<MonitoringServiceStation> MonitoringServiceStations { get { return model.MonitoringServiceStations; } }

        public DbSet<MonitoringStationAlarm> MonitoringStationAlarms { get { return model.MonitoringStationAlarms; } }

        public DbSet<MonitoringStationAlarmConditionLog> MonitoringStationAlarmConditionLogs
        {
            get
            {
                return model.MonitoringStationAlarmConditionLogs;
            }
        }

        public DbSet<MonitoringStationActivity> MonitoringStationActivities { get { return model.MonitoringStationActivities; } }

        public DbSet<MonitoringStationAlarmCondition> MonitoringStationAlarmConditions
        {
            get
            {
                return model.MonitoringStationAlarmConditions;
            }
        }

        public DbSet<MonitoringStationAlarmConditionType> MonitoringStationAlarmConditionTypes { get { return model.MonitoringStationAlarmConditionTypes; } }
        public DbSet<MonitoringStationAlarmLog> MonitoringStationAlarmLogs { get { return model.MonitoringStationAlarmLogs; } }
        public DbSet<MonitoringStationAlarmSeverity> MonitoringStationAlarmSeverities { get { return model.MonitoringStationAlarmSeverities; } }
        public DbSet<MonitoringStationAlarmType> MonitoringStationAlarmTypes { get { return model.MonitoringStationAlarmTypes; } }
        public DbSet<MonitoringStationCatchmentArea> MonitoringStationCatchmentAreas { get { return model.MonitoringStationCatchmentAreas; } }
        public DbSet<MonitoringStationDocument> MonitoringStationDocuments { get { return model.MonitoringStationDocuments; } }
        public DbSet<MonitoringStationManualMeasurment> MonitoringStationManualMeasurments { get { return model.MonitoringStationManualMeasurments; } }
        public DbSet<MonitoringStationNote> MonitoringStationNotes { get { return model.MonitoringStationNotes; } }
        public DbSet<MonitoringStationPopulation> MonitoringStationPopulations { get { return model.MonitoringStationPopulations; } }
        public DbSet<MonitoringStationsMap> MonitoringStationsMaps { get { return model.MonitoringStationsMaps; } }
        public DbSet<MonitoringStationsOffSiteSensor> MonitoringStationsOffSiteSensors { get { return model.MonitoringStationsOffSiteSensors; } }
        public DbSet<MonitoringStationState> MonitoringStationStates { get { return model.MonitoringStationStates; } }
        public DbSet<MonitoringStationTC> MonitoringStationTCs { get { return model.MonitoringStationTCs; } }
        public DbSet<MonitoringStationTrafficLevel> MonitoringStationTrafficLevels { get { return model.MonitoringStationTrafficLevels; } }
        public DbSet<MonitoringStationType> MonitoringStationTypes { get { return model.MonitoringStationTypes; } }
        public DbSet<MonitoringTaskFormat> MonitoringTaskFormats { get { return model.MonitoringTaskFormats; } }
        public DbSet<MonitoringTaskPriority> MonitoringTaskPriorities { get { return model.MonitoringTaskPriorities; } }
        public DbSet<MonitoringTaskStatu> MonitoringTaskStatus { get { return model.MonitoringTaskStatus; } }
        public DbSet<MonitoringTaskType> MonitoringTaskTypes { get { return model.MonitoringTaskTypes; } }
        public DbSet<MonitoringUnitConversion> MonitoringUnitConversions { get { return model.MonitoringUnitConversions; } }
        public DbSet<ObservationType> ObservationTypes { get { return model.ObservationTypes; } }
        public DbSet<Organization> Organizations { get { return model.Organizations; } }
        public DbSet<Organization_History> Organization_History { get { return model.Organization_History; } }
        public DbSet<OrganizationDefaultUnitSet> OrganizationDefaultUnitSets { get { return model.OrganizationDefaultUnitSets; } }
        public DbSet<OrganizationDefectType> OrganizationDefectTypes { get { return model.OrganizationDefectTypes; } }
        public DbSet<OrganizationDocument> OrganizationDocuments { get { return model.OrganizationDocuments; } }
        public DbSet<OrganizationGeometryAssetLayerData> OrganizationGeometryAssetLayerDatas { get { return model.OrganizationGeometryAssetLayerDatas; } }
        public DbSet<OrganizationGeometryLayer> OrganizationGeometryLayers { get { return model.OrganizationGeometryLayers; } }
        public DbSet<OrganizationLayerGroup> OrganizationLayerGroups { get { return model.OrganizationLayerGroups; } }
        public DbSet<OrganizationLocation> OrganizationLocations { get { return model.OrganizationLocations; } }
        public DbSet<OrganizationPermission> OrganizationPermissions { get { return model.OrganizationPermissions; } }
        public DbSet<OrganizationPermission_History> OrganizationPermission_History { get { return model.OrganizationPermission_History; } }
        public DbSet<OrganizationPersonelType> OrganizationPersonelTypes { get { return model.OrganizationPersonelTypes; } }
        public DbSet<OrganizationRemediationType> OrganizationRemediationTypes { get { return model.OrganizationRemediationTypes; } }
        public DbSet<OrganizationRolePermission> OrganizationRolePermissions { get { return model.OrganizationRolePermissions; } }
        public DbSet<OrganizationRole> OrganizationRoles { get { return model.OrganizationRoles; } }
        public DbSet<OrganizationService> OrganizationServices { get { return model.OrganizationServices; } }
        public DbSet<OrganizationSiteVisitDefaultTemplate> OrganizationSiteVisitDefaultTemplates { get { return model.OrganizationSiteVisitDefaultTemplates; } }
        public DbSet<OrganizationSiteVisitTemplate> OrganizationSiteVisitTemplates { get { return model.OrganizationSiteVisitTemplates; } }
        public DbSet<OrganizationSiteVisitTemplateField> OrganizationSiteVisitTemplateFields { get { return model.OrganizationSiteVisitTemplateFields; } }
        public DbSet<OrganizationSiteVisitTemplateFieldOption> OrganizationSiteVisitTemplateFieldOptions { get { return model.OrganizationSiteVisitTemplateFieldOptions; } }
        public DbSet<OrganizationSiteVisitTemplateFieldType> OrganizationSiteVisitTemplateFieldTypes { get { return model.OrganizationSiteVisitTemplateFieldTypes; } }
        public DbSet<OrganizationTask> OrganizationTasks { get { return model.OrganizationTasks; } }
        public DbSet<OrganizationTestType> OrganizationTestTypes { get { return model.OrganizationTestTypes; } }
        public DbSet<OrganizationTool> OrganizationTools { get { return model.OrganizationTools; } }
        public DbSet<OrganizationType> OrganizationTypes { get { return model.OrganizationTypes; } }
        public DbSet<OrganizationUserRole> OrganizationUserRoles { get { return model.OrganizationUserRoles; } }
        public DbSet<OrganizationUserRoles_History> OrganizationUserRoles_History { get { return model.OrganizationUserRoles_History; } }
        public DbSet<PartnerOrganization> PartnerOrganizations { get { return model.PartnerOrganizations; } }
        public DbSet<Permission> Permissions { get { return model.Permissions; } }
        public DbSet<Project> Projects { get { return model.Projects; } }
        public DbSet<Project_History> Project_History { get { return model.Project_History; } }
        public DbSet<ProjectAsset> ProjectAssets { get { return model.ProjectAssets; } }
        public DbSet<ProjectBoundary> ProjectBoundaries { get { return model.ProjectBoundaries; } }
        public DbSet<ProjectDefect> ProjectDefects { get { return model.ProjectDefects; } }
        public DbSet<ProjectDefect_History> ProjectDefect_History { get { return model.ProjectDefect_History; } }
        public DbSet<ProjectDefectDocument> ProjectDefectDocuments { get { return model.ProjectDefectDocuments; } }
        public DbSet<ProjectDefectNote> ProjectDefectNotes { get { return model.ProjectDefectNotes; } }
        public DbSet<ProjectDefectOutlet> ProjectDefectOutlets { get { return model.ProjectDefectOutlets; } }
        public DbSet<ProjectDocument> ProjectDocuments { get { return model.ProjectDocuments; } }
        public DbSet<ProjectGeometryAssetLayerData> ProjectGeometryAssetLayerDatas { get { return model.ProjectGeometryAssetLayerDatas; } }
        public DbSet<ProjectGeometryLayer> ProjectGeometryLayers { get { return model.ProjectGeometryLayers; } }
        public DbSet<ProjectLayerGroup> ProjectLayerGroups { get { return model.ProjectLayerGroups; } }
        public DbSet<ProjectLotInspectionResult> ProjectLotInspectionResults { get { return model.ProjectLotInspectionResults; } }
        public DbSet<ProjectLotInspectionResult_History> ProjectLotInspectionResult_History { get { return model.ProjectLotInspectionResult_History; } }
        public DbSet<ProjectLotInspectionResultDocument> ProjectLotInspectionResultDocuments { get { return model.ProjectLotInspectionResultDocuments; } }
        public DbSet<ProjectLotInspectionResultNote> ProjectLotInspectionResultNotes { get { return model.ProjectLotInspectionResultNotes; } }
        public DbSet<ProjectNote> ProjectNotes { get { return model.ProjectNotes; } }
        public DbSet<ProjectObservation> ProjectObservations { get { return model.ProjectObservations; } }
        public DbSet<ProjectOrganization> ProjectOrganizations { get { return model.ProjectOrganizations; } }
        public DbSet<ProjectResource> ProjectResources { get { return model.ProjectResources; } }
        public DbSet<ProjectServiceDocument> ProjectServiceDocuments { get { return model.ProjectServiceDocuments; } }
        public DbSet<ProjectServiceNote> ProjectServiceNotes { get { return model.ProjectServiceNotes; } }
        public DbSet<ProjectService> ProjectServices { get { return model.ProjectServices; } }
        public DbSet<ProjectServices_History> ProjectServices_History { get { return model.ProjectServices_History; } }
        public DbSet<ProjectServiceUser> ProjectServiceUsers { get { return model.ProjectServiceUsers; } }
        public DbSet<ProjectType> ProjectTypes { get { return model.ProjectTypes; } }
        public DbSet<ProjectUser> ProjectUsers { get { return model.ProjectUsers; } }
        public DbSet<Province> Provinces { get { return model.Provinces; } }
        public DbSet<RemediationType> RemediationTypes { get { return model.RemediationTypes; } }
        public DbSet<ResourceType> ResourceTypes { get { return model.ResourceTypes; } }
        public DbSet<ResourceTypeUnit> ResourceTypeUnits { get { return model.ResourceTypeUnits; } }
        public DbSet<ServiceLayerType> ServiceLayerTypes { get { return model.ServiceLayerTypes; } }
        public DbSet<ServiceLayerTypeConditionRanx> ServiceLayerTypeConditionRanges { get { return model.ServiceLayerTypeConditionRanges; } }
        public DbSet<ServiceLayerTypeCondition> ServiceLayerTypeConditions { get { return model.ServiceLayerTypeConditions; } }
        public DbSet<ServiceProgress> ServiceProgresses { get { return model.ServiceProgresses; } }
        public DbSet<Service> Services { get { return model.Services; } }
        public DbSet<ServiceType> ServiceTypes { get { return model.ServiceTypes; } }
        public DbSet<ShapeFileAttributeMapp> ShapeFileAttributeMapps { get { return model.ShapeFileAttributeMapps; } }
        public DbSet<ShapeFileTask> ShapeFileTasks { get { return model.ShapeFileTasks; } }
        public DbSet<SiteVisit> SiteVisits { get { return model.SiteVisits; } }
        public DbSet<SiteVisitDocument> SiteVisitDocuments { get { return model.SiteVisitDocuments; } }
        public DbSet<SiteVisitField> SiteVisitFields { get { return model.SiteVisitFields; } }
        public DbSet<SiteVisitNote> SiteVisitNotes { get { return model.SiteVisitNotes; } }
        public DbSet<SiteVisitUser> SiteVisitUsers { get { return model.SiteVisitUsers; } }
        public DbSet<StreetClass> StreetClasses { get { return model.StreetClasses; } }
        public DbSet<TaskType> TaskTypes { get { return model.TaskTypes; } }
        public DbSet<Test> Tests { get { return model.Tests; } }
        public DbSet<Test_History> Test_History { get { return model.Test_History; } }
        public DbSet<TestAccessibilityType> TestAccessibilityTypes { get { return model.TestAccessibilityTypes; } }
        public DbSet<TestAsset> TestAssets { get { return model.TestAssets; } }
        public DbSet<TestAsset_History> TestAsset_History { get { return model.TestAsset_History; } }
        public DbSet<TestAssetDefectOutLetPercentage> TestAssetDefectOutLetPercentages { get { return model.TestAssetDefectOutLetPercentages; } }
        public DbSet<TestAssetDefectOutlet> TestAssetDefectOutlets { get { return model.TestAssetDefectOutlets; } }
        public DbSet<TestAssetDefectOutlets_History> TestAssetDefectOutlets_History { get { return model.TestAssetDefectOutlets_History; } }
        public DbSet<TestAssetDocument> TestAssetDocuments { get { return model.TestAssetDocuments; } }
        public DbSet<TestAssetGroup> TestAssetGroups { get { return model.TestAssetGroups; } }
        public DbSet<TestAssetLot> TestAssetLots { get { return model.TestAssetLots; } }
        public DbSet<TestAssetNote> TestAssetNotes { get { return model.TestAssetNotes; } }
        public DbSet<TestAssetProgress> TestAssetProgresses { get { return model.TestAssetProgresses; } }
        public DbSet<TestAssetState> TestAssetStates { get { return model.TestAssetStates; } }
        public DbSet<TestAssetWeather> TestAssetWeathers { get { return model.TestAssetWeathers; } }
        public DbSet<TestAssetWeather_History> TestAssetWeather_History { get { return model.TestAssetWeather_History; } }
        public DbSet<TestBoundary> TestBoundaries { get { return model.TestBoundaries; } }
        public DbSet<TestContact> TestContacts { get { return model.TestContacts; } }
        public DbSet<TestEnityWeather> TestEnityWeathers { get { return model.TestEnityWeathers; } }
        public DbSet<TestEnityWeather_History> TestEnityWeather_History { get { return model.TestEnityWeather_History; } }
        public DbSet<TestEntity> TestEntities { get { return model.TestEntities; } }
        public DbSet<TestEntities_History> TestEntities_History { get { return model.TestEntities_History; } }
        public DbSet<TestEntityDefect> TestEntityDefects { get { return model.TestEntityDefects; } }
        public DbSet<TestEntityDefects_History> TestEntityDefects_History { get { return model.TestEntityDefects_History; } }
        public DbSet<TestEntityDocument> TestEntityDocuments { get { return model.TestEntityDocuments; } }
        public DbSet<TestEntityLotInspectionResult> TestEntityLotInspectionResults { get { return model.TestEntityLotInspectionResults; } }
        public DbSet<TestEntityNote> TestEntityNotes { get { return model.TestEntityNotes; } }
        public DbSet<TestGeometryAssetLayerData> TestGeometryAssetLayerDatas { get { return model.TestGeometryAssetLayerDatas; } }
        public DbSet<TestGeometryBaseLayerData> TestGeometryBaseLayerDatas { get { return model.TestGeometryBaseLayerDatas; } }
        public DbSet<TestGeometryLayer> TestGeometryLayers { get { return model.TestGeometryLayers; } }
        public DbSet<TestLayerGroup> TestLayerGroups { get { return model.TestLayerGroups; } }
        public DbSet<TestType> TestTypes { get { return model.TestTypes; } }
        public DbSet<TimeInterval> TimeIntervals { get { return model.TimeIntervals; } }
        //public DbSet<TimeZone> TimeZones { get{return model.MonitoringDatas;} }
        public DbSet<Tool> Tools { get { return model.Tools; } }
        public DbSet<ToolType> ToolTypes { get { return model.ToolTypes; } }
        public DbSet<Toronto_Area39_Projected_LatLong> Toronto_Area39_Projected_LatLong { get { return model.Toronto_Area39_Projected_LatLong; } }
        public DbSet<TorontoRainfallTask> TorontoRainfallTasks { get { return model.TorontoRainfallTasks; } }
        public DbSet<TrafficLevel> TrafficLevels { get { return model.TrafficLevels; } }
        public DbSet<Unit> Units { get { return model.Units; } }
        public DbSet<UnitType> UnitTypes { get { return model.UnitTypes; } }
        public DbSet<WeatherType> WeatherTypes { get { return model.WeatherTypes; } }
        public DbSet<WindDirection> WindDirections { get { return model.WindDirections; } }
        public DbSet<WindType> WindTypes { get { return model.WindTypes; } }
        public DbSet<WorkOrderPriority> WorkOrderPriorities { get { return model.WorkOrderPriorities; } }
        public DbSet<WorkOrderStatu> WorkOrderStatus { get { return model.WorkOrderStatus; } }
        public DbSet<WorkOrderType> WorkOrderTypes { get { return model.WorkOrderTypes; } }
        public DbSet<Toronto_SAN_Sewer> Toronto_SAN_Sewer { get { return model.Toronto_SAN_Sewer; } }
        public DbSet<Toronto_SAN_MH> Toronto_SAN_MH { get { return model.Toronto_SAN_MH; } }
        public DbSet<tempView_KawarthaMonitoringStations_Boundary> tempView_KawarthaMonitoringStations_Boundary { get { return model.tempView_KawarthaMonitoringStations_Boundary; } }
        public DbSet<tempView_KawarthaMonitoringStations_Points> tempView_KawarthaMonitoringStations_Points { get { return model.tempView_KawarthaMonitoringStations_Points; } }
        public DbSet<ViewLotInspectionLotPolygon> ViewLotInspectionLotPolygons { get { return model.ViewLotInspectionLotPolygons; } }
        public DbSet<ViewLotsInspectionPointsOfInterest> ViewLotsInspectionPointsOfInterests { get { return model.ViewLotsInspectionPointsOfInterests; } }
        public DbSet<ViewManholeInspectionProgress> ViewManholeInspectionProgresses { get { return model.ViewManholeInspectionProgresses; } }
        public DbSet<ViewMenuItemAccess> ViewMenuItemAccesses { get { return model.ViewMenuItemAccesses; } }
        public DbSet<ViewMonitoringStationBoundary> ViewMonitoringStationBoundaries { get { return model.ViewMonitoringStationBoundaries; } }
        public DbSet<ViewMonitoringStationPoint> ViewMonitoringStationPoints { get { return model.ViewMonitoringStationPoints; } }
        public DbSet<ViewMonitoringTaskList> ViewMonitoringTaskLists { get { return model.ViewMonitoringTaskLists; } }
        public DbSet<ViewPointsOfInterestCentroid> ViewPointsOfInterestCentroids { get { return model.ViewPointsOfInterestCentroids; } }
        public DbSet<ViewSmokeTestDefectBoundary> ViewSmokeTestDefectBoundaries { get { return model.ViewSmokeTestDefectBoundaries; } }
        public DbSet<ViewSmokeTestDefectCentroid> ViewSmokeTestDefectCentroids { get { return model.ViewSmokeTestDefectCentroids; } }
        public DbSet<ViewSmokeTestGroupBoundary> ViewSmokeTestGroupBoundaries { get { return model.ViewSmokeTestGroupBoundaries; } }
        public DbSet<ViewSmokeTestGroupManhole> ViewSmokeTestGroupManholes { get { return model.ViewSmokeTestGroupManholes; } }
        public DbSet<ViewSmokeTestGroupSewerLine> ViewSmokeTestGroupSewerLines { get { return model.ViewSmokeTestGroupSewerLines; } }
        public DbSet<ViewTestDefec> ViewTestDefecs { get { return model.ViewTestDefecs; } }
        public DbSet<ViewTestDefecsOutlet> ViewTestDefecsOutlets { get { return model.ViewTestDefecsOutlets; } }
        public DbSet<MonitoringTask> MonitoringTasks { get { return model.MonitoringTasks; } }
        public DbSet<MonitoringTaskExportDataType> MonitoringTaskExportDataTypes { get { return model.MonitoringTaskExportDataTypes; } }
        public DbSet<MonitoringTaskSensor> MonitoringTaskSensors { get { return model.MonitoringTaskSensors; } }
        public DbSet<AppUser> AppUsers { get { return model.AppUsers; } }
        public DbSet<AlarmAckinfo> AlarmAckinfoes { get { return model.AlarmAckinfoes; } }

        public DbSet<MonitoringStation> MonitoringStations
        {
            get
            {
                return model.MonitoringStations;
            }
        }

        public DbSet<MonitoringStation_History> MonitoringStation_History { get { return model.MonitoringStation_History; } }
    }
}
